*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections      
Library      String    

*** Variables ***


${SQL_CONSULTA_DADOS_ENTRADA_PESSOA_EXPOSTA}      SELECT C3400_RCMTIPPES AS TIPOPESSOA, CONCAT(LPAD(C3400_CLTCGC,12,'0'),
...                                               (LPAD(C3400_CLTCGCDIG ,	2 ,'0'))) FROM DNCB00.DNCB34 ORDER BY C3400_HDRDATA DESC FETCH FIRST ROW ONLY;

${SQL_VALIDAR_CONSULTAR_PESS_EXP_POLITICAMENTE}    SELECT C3400_PEPSIT, TRIM(C3400_PEPTIP), LPAD(C3400_PEPIDSERASA , 9 , '0') AS IDSERASA, LPAD(C3400_PEPDATINC , 8 , '0') AS DATAINCSERASA, LPAD(C3400_PEPDATSIT , 8 , '0') AS DATASITSERASA 
...                                                FROM DNCB00.DNCB34 WHERE C3400_CLTCGC = '{0}' AND C3400_CLTCGCDIG = '{1}' FETCH FIRST ROW ONLY

${SQL_CONSULTA_DADOS_ENTRADA_RELACIONAMETOS_PEP}    SELECT C3400_RCMTIPPES AS TIPOPESSOA, CONCAT(LPAD(C3400_CLTCGC,12,'0'),(LPAD(C3400_CLTCGCDIG ,2 ,'0'))) FROM DNCB00.DNCB34 WHERE C3400_CLTCGC IN (SELECT C3600_PRECGCREL FROM DNCB00.DNCB36)ORDER BY C3400_HDRDATA DESC FETCH FIRST ROW ONLY;

${SQL_VALIDAR_CONSULTA_RELACIONAMENTOS}             SELECT CONCAT(CONCAT(LPAD(C3600_CLTCGC ,12 ,'0'), ' - '), LPAD(C3600_CLTCGCDIG,2 , '0')) AS CPF,
...                                                 C3600_PRESIT,TRIM(C3600_PRENOMTIT),LPAD(C3600_PREDATSIT,8 ,'0')AS DATA_SIT, LPAD(C3600_PREDATINC ,8 ,'0')AS DATA_INCLUSAO FROM DNCB00.DNCB36
...                                                 WHERE C3600_PRECGCREL = {0} AND C3600_PRECGCDGREL = {1} ORDER BY C3600_CLTCGC ASC FETCH FIRST 3 ROWS ONLY

*** Keywords ***

Recuperar Dados de Entrada Pessoa Exposta Politicamente
    ${result}             Query            ${SQL_CONSULTA_DADOS_ENTRADA_PESSOA_EXPOSTA}
    ${tipo_pessoa}        Get From List    ${result}[0]      0
    ${cpf/cnpj}           Get From List    ${result}[0]      1
    ${cgc}                Get Substring    ${result}[0][1]   0    12
    ${dig}                Get Substring    ${result}[0][1]   12   14
    [Return]    ${tipo_pessoa}    ${cpf/cnpj}    ${cgc}    ${dig}
    
Validar Consultar Pessoa Exposta Politicamentes
    [Arguments]    ${codCgc}    ${digito}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTAR_PESS_EXP_POLITICAMENTE}    ${codCgc}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]
    
Recuperar Dados Acesso Consultar Relacionamentos
    ${result}             Query            ${SQL_CONSULTA_DADOS_ENTRADA_RELACIONAMETOS_PEP}
    ${tipo_pessoa}        Get From List    ${result}[0]      0
    ${cpf/cnpj}           Get From List    ${result}[0]      1
    ${cgc}                Get Substring    ${result}[0][1]   0    12
    ${dig}                Get Substring    ${result}[0][1]   12   14
    [Return]    ${tipo_pessoa}    ${cpf/cnpj}    ${cgc}    ${dig}
    
Validar Consulta Relacionamentos
    [Arguments]    ${codCgc}    ${digito}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_RELACIONAMENTOS}    ${codCgc}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}
    