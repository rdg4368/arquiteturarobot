*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String

*** Variables ***
${SQL_DADOS_INCLUIR_CONTROLE_DE_INTEGRACOES}        SELECT 'CTU'AS SISTEMA, '11111'AS SEQ_MOV ,LPAD(DAY(CURRENT_DATE) , 2 , '0')|| LPAD(MONTH(CURRENT_DATE) , 2 , '0')||YEAR(CURRENT_DATE) AS DATA_INTEGRACAO,
...                                                 LPAD(HOUR(CURRENT_TIME),2,'0')||LPAD(MINUTE(CURRENT_TIME),2,'0')||LPAD(SECOND(CURRENT_TIME),2,'0') AS HORA,
...                                                 LPAD(DAY(CURRENT_DATE) , 2 , '0')|| LPAD(MONTH(CURRENT_DATE) , 2 , '0')||YEAR(CURRENT_DATE + 1 YEAR) AS DATA_REFERENCIA ,'1' AS STATUS_INTEGRACAO , '009999' AS REG_ACEITOS, '000099'  AS REG_REJEITADOS,
...                                                 YEAR(CURRENT_DATE) || LPAD(MONTH(CURRENT_DATE) , 2 , '0')||LPAD(DAY(CURRENT_DATE) , 2 , '0') AS DATA_VALIDAR FROM DNCB00.DNCBA9 FETCH FIRST ROW ONLY

${SQL_VALIDAR_INCLUIR_CONTROLE_DE_INTEGRACOES}      SELECT * FROM DNCB00.DNCBA9 WHERE CA900_SISCODSIS = '{0}' AND CA900_MDBQUAREGAC = '{1}' AND CA900_MDBQUAREGRE = '{2}'
...                                                 AND CA900_MDBSEQMOV = '{3}' AND CA900_HDRDATA = '{4}'

${SQL_DESFAZER_INCLUIR_CONTROLE_DE_INTEGRACOES}     DELETE FROM DNCB00.DNCBA9 WHERE CA900_SISCODSIS = '{0}' AND CA900_MDBQUAREGAC = '{1}' AND CA900_MDBQUAREGRE = '{2}'
...                                                 AND CA900_MDBSEQMOV = '{3}' AND CA900_HDRDATA = '{4}'

${SQL_COD_SISTEMA_CONSULTA_CONTROLE_INTEGRACOES}    SELECT CHAR(CA900_SISCODSIS) FROM DNCB00.DNCBA9 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_CONTROLE_INTEGRACOES}        SELECT CASE WHEN CA900_MDBSTAINT = 1 THEN 'ACEITO'ELSE 'ACEITO COM ERRO'END AS STATUS_INTEGRACAO,
...                                                 LPAD(CA900_MDBSEQMOV,5,'0')AS NR_SEQ,(SUBSTR(CHAR(CA900_MDBDATINT),7,2))||'/'||SUBSTR(CHAR ( CA900_MDBDATINT ),5,2)||'/'||SUBSTR(CHAR (CA900_MDBDATINT ),1,4) AS DATA_INTEGRACAO,
...                                                 LPAD(CA900_USUNUM,5,'0')AS COD_USUARIO , LPAD(CA900_MDBQUAREGAC,6,'0') AS QNT_ACEITA
...                                                 FROM DNCB00.DNCBA9 WHERE CA900_SISCODSIS = '{0}' FETCH FIRST 12 ROW ONLY

${SQL_DADOS_ACESSO_ALTERAR_CONTROLE_INTEGRACOES}    SELECT CHAR(CA900_SISCODSIS) AS COD_SISTEMA, (SUBSTR(CHAR(CA900_MDBDATINT),7,2))||SUBSTR(CHAR ( CA900_MDBDATINT ),5,2)||SUBSTR(CHAR (CA900_MDBDATINT ),1,4) AS DATA_REFERENCIA ,
...                                                 TRIM(CA900_MDBSEQMOV) AS SEQUENCIAL ,LPAD(CA900_MDBDATINT,8,'0') AS DATA_VALIDAR, TRIM(CA900_MDBQUAREGAC) AS QNT_ACEITA_ROLLBACK, TRIM(CA900_MDBQUAREGRE) AS QNT_RECUSADA_ROLLBACK
...                                                 FROM DNCB00.DNCBA9 WHERE CA900_MDBSEQMOV = 2 AND CA900_MDBQUAREGRE <> 999999 AND CA900_MDBQUAREGRE <> 99 FETCH FIRST ROW ONLY

${SQL_VALIDAR_ALTERAR_CONTROLE_INTEGRACOES}         SELECT LPAD(CA900_MDBQUAREGAC,6,'0') AS QNT_ACEITA, LPAD(CA900_MDBQUAREGRE,6,'0') AS QNT_RECUSADA FROM DNCB00.DNCBA9 WHERE CA900_SISCODSIS = '{0}' AND CA900_MDBSEQMOV = '{1}' AND  CA900_MDBDATINT = '{2}' 
    
${SQL_DESFAZER_ALTERAR_CONTROLE_INTEGRACOES}        UPDATE DNCB00.DNCBA9 SET  CA900_MDBQUAREGAC = '{0}' , CA900_MDBQUAREGRE = '{1}'
...                                                 WHERE CA900_SISCODSIS = '{2}' AND CA900_MDBSEQMOV = '{3}' AND  CA900_MDBDATINT = '{4}'  

${SQL_DADOS_ACESSO_EXCLUIR_CONTROLE_INTEGRACOES}    SELECT (SUBSTR(CHAR(CA900_MDBDATINT),7,2))||SUBSTR(CHAR ( CA900_MDBDATINT ),5,2)||SUBSTR(CHAR (CA900_MDBDATINT ),1,4) AS DATA_REFERENCIA,
...                                                 LPAD(CA900_HDRDATA,8,'0'),TRIM(CA900_HDRHORA),TRIM (CA900_HDRESTACAO),TRIM(CA900_HDRPROGRAMA),TRIM (STATGER),TRIM (CA900_SISCODSIS),TRIM (CA900_MDBDATINT),
...                                                 TRIM (CA900_MDBHORINT),TRIM (CA900_MDBDATREF),TRIM (CA900_MDBSTAINT), TRIM(CA900_MDBQUAREGAC),TRIM (CA900_MDBQUAREGRE),TRIM (CA900_MDBSEQMOV),TRIM (CA900_USUNUM) FROM DNCB00.DNCBA9 FETCH FIRST ROW ONLY


${SQL_VALIDAR_EXCLUIR_CONTROLE_INTEGRACOES}         SELECT * FROM DNCB00.DNCBA9 WHERE CA900_MDBDATINT = '{0}' AND CA900_SISCODSIS = '{1}' AND CA900_MDBSEQMOV = '{2}'

${SQL_DESFAZER_EXCLUIR_CONTROLE_INTEGRACOES}        INSERT INTO  DNCB00.DNCBA9 (CA900_HDRDATA,CA900_HDRHORA,CA900_HDRESTACAO,CA900_HDRPROGRAMA,STATGER,CA900_SISCODSIS,CA900_MDBDATINT,CA900_MDBHORINT,CA900_MDBDATREF,CA900_MDBSTAINT,CA900_MDBQUAREGAC,CA900_MDBQUAREGRE,CA900_MDBSEQMOV,CA900_USUNUM)
...                                                 VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}' ,'{10}','{11}' ,'{12}' ,'{13}')

${SQL_DADOS_INCLUIR_CADASTRO_EMAIL}                 SELECT CHAR (CA900_SISCODSIS) AS COD_SISTEMA, 'TESTE@ROBOT.COM.BR'AS EMAIL_TESTE FROM DNCB00.DNCBA9 FETCH FIRST ROW ONLY




*** Keywords ***
Recuperar Dados Inclusao Controle De Integracoes 
    ${result}    Query    ${SQL_DADOS_INCLUIR_CONTROLE_DE_INTEGRACOES}
    [Return]     ${result}[0]
    
Validar Inclusao Controle De Integracoes
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_VALIDAR_INCLUIR_CONTROLE_DE_INTEGRACOES}    ${dados}[0]    ${dados}[6]    ${dados}[7]    ${dados}[1]    ${dados}[8]
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${status}

Desfazer Inclusao Controle De Integracoes
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_DESFAZER_INCLUIR_CONTROLE_DE_INTEGRACOES}    ${dados}[0]    ${dados}[6]    ${dados}[7]    ${dados}[1]    ${dados}[8]
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}    
    [Return]       ${status}
    
Recuperar Cod Sistema Consultar Controle De Integracoes
    ${result}    Query     ${SQL_COD_SISTEMA_CONSULTA_CONTROLE_INTEGRACOES}
    [Return]     ${result}[0][0]
    
Validar Consulta Controle De Integracoes
    [Arguments]    ${codSistema}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_CONTROLE_INTEGRACOES}    ${codSistema}
    ${result}      Query            ${query}    
    [Return]       ${result}
    
Recuperar Dados Acesso Alterar Controle De Integracoes
    ${result}    Query    ${SQL_DADOS_ACESSO_ALTERAR_CONTROLE_INTEGRACOES} 
    [Return]     ${result}[0]
    
Validar Alterar Controle Integracoes
    [Arguments]    ${dadosValidar}
    ${query}       Format String    ${SQL_VALIDAR_ALTERAR_CONTROLE_INTEGRACOES}    ${dadosValidar}[0]    ${dadosValidar}[2]    ${dadosValidar}[3]
    ${result}      Query            ${query}
    [Return]       ${result}[0]
    
Desfazer Alterar Controle Integracoes
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_DESFAZER_ALTERAR_CONTROLE_INTEGRACOES}    ${dados}[4]    ${dados}[5]    ${dados}[0]    ${dados}[2]    ${dados}[3]
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]       ${status}
    
Recuperar Dados Acesso Excluir Controle De Integracoes
    ${result}    Query    ${SQL_DADOS_ACESSO_EXCLUIR_CONTROLE_INTEGRACOES}
    [Return]     ${result}[0]
    
Validar Excluir Controle Integracoes
    [Arguments]    ${dados}
    ${query}       Format String        ${SQL_VALIDAR_EXCLUIR_CONTROLE_INTEGRACOES}    ${dados}[7]    ${dados}[6]    ${dados}[13]
    ${status}      Run Keyword And Return Status    Check If Not Exists In Database    ${query}        
    [Return]       ${status}
    
Desfazer Excluir Controle Integracoes
    [Arguments]    ${dadosRollback}
    ${query}       Format String    ${SQL_DESFAZER_EXCLUIR_CONTROLE_INTEGRACOES}    ${dadosRollback}[1]    ${dadosRollback}[2]    ${dadosRollback}[3]     ${dadosRollback}[4]     ${dadosRollback}[5]     ${dadosRollback}[6]     ${dadosRollback}[7]
    ...                                                                             ${dadosRollback}[8]    ${dadosRollback}[9]    ${dadosRollback}[10]    ${dadosRollback}[11]    ${dadosRollback}[12]    ${dadosRollback}[13]    ${dadosRollback}[14]
    ${status}      Run Keyword And Return Status    Execute Sql String              ${query}        
    [Return]       ${status}
    
Recuperar Dados Incluir Cadastro De Emails
    ${result}    Query    ${SQL_DADOS_INCLUIR_CADASTRO_EMAIL}    
    [Return]     ${result}[0]