*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String  

*** Variables ***
${SQL_RECUPERAR_DADOS_CONSULTAR_CONTROLE_REMESSAS}
...    SELECT (SUBSTR(INT(C2800_CRMDATENR),7,2))||SUBSTR(INT ( C2800_CRMDATENR ),5,2)||SUBSTR(INT (C2800_CRMDATENR - 10000),1,4)  AS "Período Inicial",
...    (SUBSTR(INT(C2800_CRMDATENR),7,2))||SUBSTR(INT ( C2800_CRMDATENR ),5,2)||SUBSTR(INT (C2800_CRMDATENR ),1,4)   AS "Período Final",
...    SUBSTR(C2800_CRMNOMARQ ,    6,    2) AS "Empresa" FROM DNCB00.DNCB28 WHERE
...    SUBSTR(C2800_CRMNOMARQ ,    1,    5) = 'SERS0' ORDER BY C2800_CRMDATENR DESC FETCH FIRST ROW ONLY;

${SQL_RESCUPERAR_DADOS_ACESSO_CONSUL_SUSTADOS}
...    SELECT LPAD(C3700_CLTCGC ,12 ,'0')||LPAD(C3700_CLTCGCDIG ,2 ,'0') FROM DNCB00.DNCB37 FETCH FIRST ROW ONLY;

${SQL_DADOS_ACESSO_CONSULTAR_QNT_NEGATIVOS}
...    SELECT (SUBSTR(INT(C2800_CRMDATENR),7,2))||SUBSTR(INT ( C2800_CRMDATENR ),5,2)||SUBSTR(INT (C2800_CRMDATENR - 10000),1,4)  AS "Período Inicial",
...    (SUBSTR(INT(C2800_CRMDATENR),7,2))||SUBSTR(INT ( C2800_CRMDATENR ),5,2)||SUBSTR(INT (C2800_CRMDATENR ),1,4)   AS "Período Final",
...    SUBSTR(C2800_CRMNOMARQ ,    6,    2) AS "Empresa", (C2800_CRMDATENR - 10000), INT (C2800_CRMDATENR) FROM DNCB00.DNCB28 WHERE
...    SUBSTR(C2800_CRMNOMARQ ,    1,    5) = 'SERS0' ORDER BY C2800_CRMDATENR DESC FETCH FIRST ROW ONLY;

${SQL_VALIDAR_CONSULTA_QTD_NEGATIVOS}    
...    SELECT  TRIM(SUM(C2800_CRMQTDENV)) , TRIM(SUM(C2800_CRMQTDACE)) , TRIM(SUM(C2800_CRMQTDERR)) FROM DNCB00.DNCB28 WHERE SUBSTR(C2800_CRMNOMARQ ,    6,    2) = '{0}'
...    AND SUBSTR(C2800_CRMNOMARQ ,    1,    5) = 'SERS0' AND C2800_CRMDATENR BETWEEN {1} AND '{2}' 

*** Keywords ***
Recuperar Dados Consultar Controle Remessas
    ${result}    Query    ${SQL_RECUPERAR_DADOS_CONSULTAR_CONTROLE_REMESSAS}    
    [Return]     ${result}[0]
    
Recuperar Dados Consultar Sustados
    ${result}    Query    ${SQL_RESCUPERAR_DADOS_ACESSO_CONSUL_SUSTADOS}    
    [Return]     ${result}[0][0]
    
Recuperar Dados Acesso Consultar Quantitativo Negativos
    ${result}    Query    ${SQL_DADOS_ACESSO_CONSULTAR_QNT_NEGATIVOS}
    [Return]     ${result}[0]
    
Consulta Quantitativo Negativos
    [Arguments]    ${dados}
    ${query}      Format String    ${SQL_VALIDAR_CONSULTA_QTD_NEGATIVOS}    ${dados}[2]    ${dados}[3]    ${dados}[4]
    ${result}     Query            ${query}    
    [Return]      ${result}[0]