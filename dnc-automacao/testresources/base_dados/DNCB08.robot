*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      Collections  
Library      String    

*** Variables ***
${SQL_DADOS_CLIENTE_BACEN_CENTRAL}    SELECT TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG), TRIM(C0800_ADMDATOCOR), TRIM(C0800_ADDCOD), TRIM(C0800_NDACOD), TRIM(C0802_BCRVALDVPR), TRIM(C0802_BCRVALDAV1), TRIM(C0802_BCRVALDVCO), TRIM(C0802_BCRVALDAV2), TRIM(C0802_BCRVALDVE2) FROM DNCB00.DNCB08 A WHERE C0800_ADMCGC > 0 AND C0800_ADDCOD = 12 AND C0800_NDACOD = 'BCR' ORDER BY C0800_HDRDATA DESC FETCH FIRST 1 ROWS ONLY
#${SQL_DADOS_CLIENTE_BACEN_CENTRAL}   SELECT TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG), TRIM(C0800_ADDCOD), 
#...                                  TRIM(C0800_NDACOD), TRIM(C0800_ADMDATOCOR), TRIM(C0800_AVDSEQMOV), 
#...                                  TRIM(LPAD(C0802_BCRVALDVPR,6,'0')), TRIM(LPAD(C0802_BCRVALDVCO,6,'0')), TRIM(LPAD(C0802_BCRVALDAV1,6,'0')) 
#...                                  FROM DNCB00.DNCB08 ORDER BY C0800_HDRDATA DESC FETCH FIRST 1 ROW ONLY  


${SQL_CLIENTE_COM_ADVERTENCIA_DP}     SELECT TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG), TRIM(C0800_ADMDATOCOR), C0800_ADDCOD, C0800_NDACOD, C0801_AVDDATBAIXA
...                                   FROM DNCB00.DNCB08 A INNER JOIN DNCB00.DNCB01 ON (C0100_ADDCOD = C0800_ADDCOD AND C0100_OADCOD = 'DEP')
...                                   INNER JOIN  AOXB00.AOXB01 ON (X0100_CLTCOD = C0800_CLTCOD AND C0800_CLTCOD > 0 )
...                                   INNER JOIN DNCB00.DNCB34 ON (C3400_CLTCGC = C0800_ADMCGC) WHERE C0800_ADMDATOCOR > 0 AND C0801_AVDDATBAIXA > 0
...                                   AND C0800_ADMCGC NOT IN  (SELECT C0800_ADMCGC FROM DNCB00.DNCB08 WHERE C0801_AVDDATBAIXA = 0 AND C0800_ADMDATOCOR = 0  
...                                   AND C0801_AVDDES = 0) ORDER BY C0800_ADMDATOCOR ASC FETCH FIRST ROWS ONLY
# ${SQL_DADOS_CLIENTE_BACEN_CENTRAL}  SELECT TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG), TRIM(C0800_ADDCOD), TRIM(C0800_NDACOD), TRIM(C0800_ADMDATOCOR), TRIM(C0800_AVDSEQMOV), TRIM(C0802_BCRVALDVPR), TRIM(C0802_BCRVALDVCO), TRIM(C0802_BCRVALDAV1) FROM DNCB00.DNCB08 ORDER BY C0800_HDRDATA DESC FETCH FIRST 1 ROW ONLY     

${SQL_VALIDAR_CONSULTA_ADVERTENCIA}   SELECT TRIM(X0100_CLTNOMSRF), LPAD(C0800_ADMCGC,12,'0')||'-'||LPAD(C0800_ADMCGCDIG,2,'0'),
...                                   TRIM (C0800_CLTCOD), SUBSTR(LPAD(C0800_CSSDATATU,8,'0'),7,2)||'/'||SUBSTR(LPAD(C0800_CSSDATATU,8,'0'),5,2)||'/'||SUBSTR(LPAD(C0800_CSSDATATU,8,'0'),1,4)
...                                   FROM DNCB00.DNCB08 INNER JOIN  AOXB00.AOXB01 ON (X0100_CLTCOD = C0800_CLTCOD )  WHERE 
...                                   C0800_ADMCGC = {0} AND C0800_ADMCGCDIG = {1} FETCH FIRST ROW ONLY


*** Keywords ***
Recuperar Dados Do Cliente CPF CNPJ
    [Arguments]     ${cnpj}     ${digito}
    ${result}       Query  SELECT LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0') FROM AOXB00.AOXB01 WHERE X0100_CLTCGC NOT IN (SELECT C0800_ADMCGC FROM DNCB00.DNCB08) ORDER BY X0100_HDRDATA DESC FETCH FIRST 1 ROW ONLY
    [Return]        ${result}
    
Recuperar Dados Do Cliente
    ${result}       Query  SELECT LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0') FROM AOXB00.AOXB01 WHERE X0100_CLTCGC NOT IN (SELECT C0800_ADMCGC FROM DNCB00.DNCB08) ORDER BY X0100_HDRDATA DESC FETCH FIRST 1 ROW ONLY
    [Return]        ${result}

Pesquisar Dados Advertencia Bacen
    [Arguments]     ${cnpj}     ${data} 
    ${result}       Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT * FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${cnpj}[0][0] AND C0800_ADMCGCDIG = ${cnpj}[0][1] AND C0800_ADMDATOCOR = ${data} AND C0802_BCRDATOCOR = ${data}
    [Return]        ${result}

Recuperar Dados Advertencia Departamento RVN
    @{result}       Query 
    ...  SELECT LPAD(C0800_ADMCGC,12,'0'), LPAD(C0800_ADMCGCDIG,2,'0'), LPAD(C0800_ADDCOD,2,'0'), C0800_NDACOD FROM DNCB00.DNCB08 WHERE C0801_AVDDATBAIXA = 0 AND C0800_ADMCGC > 0 ORDER BY C0800_HDRDATA DESC FETCH FIRST 1 ROW ONLY    
    [Return]        ${result}

Pesquisar Dados Advertencia Departamento RVN
    [Arguments]     ${advertencia} 
    ${result}       Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT * FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${advertencia}[0][0] AND C0800_ADMCGCDIG = ${advertencia}[0][1] AND C0800_ADDCOD = ${advertencia}[0][2] AND C0800_NDACOD = '${advertencia}[0][3]'
    [Return]        ${result}

Excluir Dados Advertencia Referencia
    [Arguments]     ${cpf_cpnj}  ${cpf_cpnj_digito}  ${data} 
    ${result}       Run Keyword And Return Status     Check If Exists In Database 
    ...  DELETE FROM DNCB00.DNCB48 WHERE C4800_CNSCGC = ${cpf_cpnj} AND C4800_CNSCGCDIG = ${cpf_cpnj_digito} AND C4800_CNSDATCONS = ${data}
    [Return]        ${result}   
    
Recuperar Dados Do Cliente CPF
    ${result}    Query  SELECT LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0') FROM AOXB00.AOXB01 WHERE X0100_CLTCGC NOT IN (SELECT C0800_ADMCGC FROM DNCB00.DNCB08) ORDER BY X0100_HDRDATA DESC FETCH FIRST 1 ROW ONLY
    ${cpf}       Get From List     ${result}[0]  0
    ${digito}    Get From List     ${result}[0]  1
    [Return]        ${cpf}  ${digito}

Consultar Dados Da Advertencia Bacen
    [Arguments]  ${cpf}    ${digito}
    ${result}    Run Keyword And Return Status   Check If Exists In Database  SELECT * FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${cpf} AND C0800_ADMCGCDIG = ${digito}
    [Return]   ${result} 

Desfazer Dados Advertencia
    [Arguments]    ${cpf}    ${digito}
    ${result}  Run Keyword And Return Status  Execute Sql String  
    ...   DELETE FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${cpf} AND C0800_ADMCGCDIG = ${digito}
    [Return]       ${result}

Recuperar Dados Cliente Bacen Central
   ${result}  Query    ${SQL_DADOS_CLIENTE_BACEN_CENTRAL}
   [Return]  ${result}
   
Validar Inclusao Advertencia
    [Arguments]  ${cpf}    ${digito}  ${dataAtual}
    ${result}    Run Keyword And Return Status   Check If Exists In Database  SELECT * FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${cpf} AND C0800_ADMCGCDIG = ${digito} AND C0800_ADDCOD = 11 AND C0800_NDACOD = '22' AND C0800_ADMDATOCOR = ${dataAtual}
    [Return]     ${result}

Validar Dados Consulta Advertencia
    [Arguments]  ${cpf}    ${digito}
    ${query}     Format String     ${SQL_VALIDAR_CONSULTA_ADVERTENCIA}     ${cpf}    ${digito}
    ${result}    Query             ${query}
    [Return]     ${result}

Desfazer Inclusao Advertencia
    [Arguments]    ${cpf}    ${digito}  ${dataAtual}
    ${result}    Run Keyword And Return Status  Execute Sql String
    ...  DELETE FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${cpf} AND C0800_ADMCGCDIG = ${digito} AND C0800_ADDCOD = 11 AND C0800_NDACOD = '22' AND C0800_ADMDATOCOR = ${dataAtual}
    [Return]    ${result}
  
Consultar Dados Do Cliente
    [Arguments]    ${dataAtual}
    ${result}      Query   SELECT TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG) FROM DNCB00.DNCB08 A INNER JOIN DNCB00.DNCB03 ON (C0300_ADDCOD = C0800_ADDCOD AND C0300_NDACOD = C0800_NDACOD) WHERE C0800_ADMCGC > 0 AND C0801_AVDDATBAIXA > 0 AND C0800_ADMDATOCOR NOT IN (${dataAtual}) AND C0800_ADDCOD NOT IN (11) FETCH FIRST 1 ROWS ONLY
    [Return]       ${result}

Consultar Cliente Advertencia Departamental
    ${result}     Query    ${SQL_CLIENTE_COM_ADVERTENCIA_DP}
    [Return]      ${result}
