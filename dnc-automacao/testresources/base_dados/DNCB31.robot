*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections      
Library      String    

*** Variables ***                                    

${SQL_CONSULTAR_SALDO_MEDIO_APA_FFA_FCB_CC}
...    SELECT LPAD(C3100_CTCCHVCONTA,	10,	'0') AS CONTA FROM DNCB00.DNCB31, DNCB00.DNCB18 WHERE 
...    C3100_SMATIPSLD != 1 AND C1800_CTCCHVCONTA = C3100_CTCCHVCONTA FETCH FIRST ROW ONLY

${SQL_CONSULTAR_SALDO_MEDIO_APA_FFA_FCB_CLTCOD}
...    SELECT LPAD(C3100_CLTCOD, 8, '0') AS CLIENTE FROM DNCB00.DNCB31, DNCB00.DNCB18 WHERE 
...    C3100_SMATIPSLD != 1 AND C1800_CTCCHVCONTA = C3100_CTCCHVCONTA AND C3100_HDRDATA > 20190901 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_SALDO_FCB_CC}
...    SELECT TRIM(ROUND(DECIMAL(SUM(C3100_SMASLD) , 24, 2) , 1) / 12) AS MEDIA_FCB FROM DNCB00.DNCB31 WHERE 
...    C3100_CTCCHVCONTA = {0} AND C3100_SMADATREF >= CONCAT(YEAR(CURRENT_DATE - 1 YEAR) , VARCHAR_FORMAT(CURRENT TIMESTAMP, 'MM'))
...    AND C3100_SMATIPSLD IN (3, 4) FETCH FIRST 12 ROWS ONLY

${SQL_VALIDAR_CONSULTA_SALDO_FCB_CLIENTE}
...    SELECT ROUND(DECIMAL(SUM(C3100_SMASLD) , 24, 2) , 1) / 12 AS MEDIA_FCB FROM DNCB00.DNCB31 WHERE C3100_CLTCOD = {0}
...    AND C3100_SMADATREF >= CONCAT(YEAR(CURRENT_DATE - 1 YEAR) , VARCHAR_FORMAT(CURRENT TIMESTAMP, 'MM')) 
...    AND C3100_SMATIPSLD IN (3,4)                                                 

${SQL_VALIDAR_CONSULTA_SALDO_CONTA}
...    SELECT LPAD(C3100_CTCCHVCONTA,10,'0') AS CONTA, TRIM(X0100_CLTNOM) AS NOME, LPAD(X0100_CLTCGC,12,'0'), 
...    LPAD(X0100_CLTCGCDIG,2,'0'), LPAD(C3100_CLTCOD,8,'0') AS COD_CLIENTE, C3100_SMASLD FROM DNCB00.DNCB31, AOXB00.AOXB01
...    WHERE C3100_CTCCHVCONTA = {0} AND C3100_CLTCOD = X0100_CLTCOD AND C3100_SMATIPSLD IN (1,2) FETCH FIRST 5 ROWS ONLY

${SQL_VALIDAR_CONSULTA_SALDO_POUPANCA_CLIENTE}     
...    SELECT TRIM(C1800_CLTCOD) AS CLIENTE, TRIM(C1800_CTCCHVCONTA) AS CONTA
...    FROM DNCB00.DNCB18 WHERE C1800_HDRDATA > 190101 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_SALDO_FUNDOS_POR_CLIENTE}
...    SELECT LPAD(C3100_CLTCOD,8,'0') AS COD_CLIENTE, TRIM(X0100_CLTNOM) AS NOME, LPAD(X0100_CLTCGC,12,'0'), 
...    LPAD(X0100_CLTCGCDIG,2,'0'), LPAD(C3100_CTCCHVCONTA,10,'0') AS CONTA FROM DNCB00.DNCB31, AOXB00.AOXB01
...    WHERE C3100_CLTCOD = {0} AND C3100_CLTCOD = X0100_CLTCOD FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTA_SALDO_CDB_POR_CLIENTE}
...    SELECT LPAD(C3100_CLTCOD,8,'0') AS COD_CLIENTE, TRIM(X0100_CLTNOM) AS NOME, LPAD(X0100_CLTCGC,12,'0'), 
...    LPAD(X0100_CLTCGCDIG,2,'0'), LPAD(C3100_CTCCHVCONTA,10,'0') AS CONTA FROM DNCB00.DNCB31, AOXB00.AOXB01
...    WHERE C3100_CLTCOD = {0} AND C3100_CLTCOD = X0100_CLTCOD FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTA_SALDO_TODAS_CONTAS}
...    SELECT DISTINCT C3100_CLTCOD, TRIM(X0100_CLTNOM), LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0'), 
...    LPAD(C1400_CTCCHVCONTA,10,'0') FROM DNCB00.DNCB31, AOXB00.AOXB01, DNCB00.DNCB14
...    WHERE C3100_CLTCOD = {0} AND C3100_CLTCOD = X0100_CLTCOD AND C3100_CLTCOD = C1400_CLTCOD

*** Keywords ***
Recuperar Conta Corrente Consultar Saldo Medio APA FFA FCB
    ${result}    Query    ${SQL_CONSULTAR_SALDO_MEDIO_APA_FFA_FCB_CC}    
    [Return]     ${result}[0][0]
    
Recuperar Codigo Cliente Consultar Saldo Medio APA FFA FCB
    ${result}    Query    ${SQL_CONSULTAR_SALDO_MEDIO_APA_FFA_FCB_CLTCOD}
    [Return]     ${result}[0][0]
        
Consulta Saldo Medio FCB Por CC
    [Arguments]   ${contaCorrente}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_FCB_CC}    ${contaCorrente}    
    ${result}     Query                ${query}    
    [Return]      ${result}[0][0]

Consulta Saldo Medio FCB Por Cliente
    [Arguments]   ${cltCod}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_FCB_CLIENTE}    ${cltCod}    
    ${result}     Query                ${query}    
    [Return]      ${result}[0][0]
    
Consulta Saldo Mensal por Conta Corrente
    [Arguments]   ${contaCorrente}
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_CONTA}    ${contaCorrente}    
    ${result}     Query                ${query}    
    [Return]      ${result}
    
Consulta Saldo Mensal Poupanca por Cliente
    ${result}     Query               ${SQL_VALIDAR_CONSULTA_SALDO_POUPANCA_CLIENTE}  
    [Return]      ${result}
    
Recuperar Dados Validar Dados Mensal Fundos por Cliente
    [Arguments]   ${cltCod}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_FUNDOS_POR_CLIENTE}    ${cltCod}    
    ${result}     Query                ${query}    
    [Return]      ${result}

Recuperar Dados Validar Dados Mensal CDB por Cliente
    [Arguments]   ${cltCod}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_CDB_POR_CLIENTE}       ${cltCod}    
    ${result}     Query                ${query}    
    [Return]      ${result}
    
Recuperar Dados Validar Dados Mensal Todas Contas
    [Arguments]   ${cltCod}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_TODAS_CONTAS}          ${cltCod}    
    ${result}     Query                ${query}    
    [Return]      ${result}
