*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
   

*** Variables ***
${SQL_DADOS_ENTRADA_CONSULTA_SERASA}      SELECT CONCAT(LPAD(X0100_CLTCGC,12,'0'),LPAD(X0100_CLTCGCDIG,2,'0'))AS CGC_CPF FROM AOXB00.AOXB01 FETCH FIRST ROW ONLY


*** Keywords ***
Recuperar Dados Advertencia Referencia
    [Arguments]     ${data}
    @{result}       Query 
    ...  SELECT LPAD(X0100_CLTCOD,8,'0'), X0100_HDRDATA FROM AOXB00.AOXB01 INNER JOIN DNCB00.DNCB11 ON(C1100_CNSCGC = X0100_CLTCGC) INNER JOIN DNCB00.DNCB12 ON(C1200_CNSCGC = X0100_CLTCGC) WHERE X0100_CLTCGC > 0 AND C1100_CNSDATCONS > 0 AND C1200_ADSTIPREG = 121 AND C1100_CNSCGC NOT IN (SELECT C4800_CNSCGC FROM DNCB00.DNCB48 WHERE C4800_CNSDATCONS = 20190903)AND C1100_CNSCGC NOT IN (SELECT C4800_CNSCGC FROM DNCB00.DNCB48 WHERE C4800_CNSDATCONS = ${data}) FETCH FIRST 1 ROW ONLY    
    [Return]        ${result}[0][0]
    
Pesquisar Dados Advertencia Referencia
    [Arguments]     ${codigo_cliente}     ${data} 
    ${result}       Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT C.* FROM AOXB00.AOXB01 INNER JOIN DNCB00.DNCB48 C ON (C4800_CNSCGC = X0100_CLTCGC AND C4800_CNSCGCDIG = X0100_CLTCGCDIG) WHERE X0100_CLTCOD = ${codigo_cliente} AND C4800_CNSDATCONS = ${data}
    [Return]        ${result}
    
Recuperar Dados CPF/CNPJ
    [Arguments]     ${codigo_cliente} 
    ${result}       Query 
    ...  SELECT LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0') FROM AOXB00.AOXB01 WHERE X0100_CLTCOD = ${codigo_cliente}
    [Return]        ${result}
    
Recuperar Dados Cliente Serasa
     ${result}       Query 
    ...  SELECT LPAD(X0100_CLTCOD,8,'0') FROM AOXB00.AOXB01 INNER JOIN DNCB00.DNCB11 ON(C1100_CNSCGC = X0100_CLTCGC) INNER JOIN DNCB00.DNCB12 B ON(C1200_CNSCGC = X0100_CLTCGC) WHERE C1100_CNSDATCONS > 0 AND C1200_ADSTIPREG = 121 AND C1100_CNSCODMSG NOT IN (501) AND C1200_TPREGISTRO IN (10,11,13,15,16,18) FETCH FIRST 1 ROW ONLY    
    [Return]        ${result}[0][0]

Detalhar Dados Serasa
    [Arguments]     ${codigo_cliente}
    ${result}       Query  
    ...  SELECT LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0'), X0100_CLTNOM, LPAD(C1100_CNSDATCONS,8,'0') FROM AOXB00.AOXB01 INNER JOIN DNCB00.DNCB11 ON (C1100_CNSCGC = X0100_CLTCGC AND C1100_CNSCGCDIG = X0100_CLTCGCDIG AND C1100_CNSDATCONS = (SELECT MAX(C1100_CNSDATCONS) FROM DNCB00.DNCB11 WHERE C1100_CNSCGC = X0100_CLTCGC AND C1100_CNSCGCDIG = X0100_CLTCGCDIG)) WHERE X0100_CLTCOD = ${codigo_cliente}
    [Return]        ${result}
    
Recuperar Dados Scr Bacen
     ${result}       Query 
    ...  SELECT LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0') FROM AOXB00.AOXB01 INNER JOIN DNCB00.DNCB11 ON(C1100_CNSCGC = X0100_CLTCGC) INNER JOIN DNCB00.DNCB12 B ON(C1200_CNSCGC = X0100_CLTCGC) WHERE C1100_CNSDATCONS > 0 AND C1200_ADSTIPREG = 121 AND C1100_CNSCODMSG NOT IN (501) AND C1200_TPREGISTRO IN (10,11,13,15,16,18) FETCH FIRST 1 ROW ONLY    
    [Return]        ${result}
    
Recuperar Dados Novo Scr Bacen
     ${result}       Query
     ...  SELECT LPAD(C1900_CLTCGC,12,'0'), LPAD(C1900_CLTCGCDIG,2,'0') FROM DNCB00.DNCB19 ORDER BY C1900_HDRDATA FETCH FIRST 1 ROW ONLY
    [Return]        ${result}
    
Recuperar cgc/cpf Para Consulta Serasa
    ${result}    Query    ${SQL_DADOS_ENTRADA_CONSULTA_SERASA}    
    [Return]     ${result}[0][0]
    
