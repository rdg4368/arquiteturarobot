*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String    

*** Variables ***

${RECUPERAR_ACESSO_CONTEST_CLIENTE}        SELECT LPAD(CB200_CLTCGC,	12,	'0') ||  LPAD(CB200_CLTCGCDIG,	2,	'0') AS CPF
...                                        FROM DNCB00.DNCBB2 INNER JOIN DNCB00.DNCBB7 ON CB200_CLTCGC = CB700_CLTCGC
...                                        WHERE CB700_CAPSITCON = 2 ORDER BY CB700_CAPHORCON DESC FETCH FIRST ROW ONLY

${RECUPERAR_INFORMACOES_SINTETICAS_CLIENTE}         SELECT CB200_CLTCGC||LPAD(CB200_CLTCGCDIG,2,'0') AS CPF
...                                                 FROM DNCB00.DNCBB2
...                                                 INNER JOIN DNCB00.DNCBB7 ON CB200_CLTCGC = CB700_CLTCGC
...                                                 WHERE CB700_CAPSITCON = 1
...                                                 ORDER BY CB700_CAPHORCON DESC FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_ACESSO_EXCLUIR_CONTEST}       SELECT LPAD(CB700_CLTCGC,12,'0')|| LPAD(CB700_CLTCGCDIG,2,'0') AS CPF
...                                                 FROM DNCB00.DNCBB7 WHERE CB700_CAPTIPCON = 'O'

${SQL_VALIDAR_JUSTIFICATIVA_CONTESTACAO}            SELECT TRIM(CB700_CAPJUSTCON) AS JUSTIFICATIVA, LPAD(CB200_CLTCGC,	12,	'0') || LPAD(CB200_CLTCGCDIG,2,'0') AS CPF
...                                                 FROM DNCB00.DNCBB2 INNER JOIN DNCB00.DNCBB7 ON CB200_CLTCGC = CB700_CLTCGC
...                                                 WHERE CB700_CAPSITCON = 1 AND CB700_CLTCGC = {0}
...                                                 ORDER BY CB700_CAPHORCON DESC FETCH FIRST ROW ONLY	

${SQL_DESFAZER_JUSTIFICATIVA}                       DELETE FROM DNCB00.DNCBB7
...                                                 WHERE CB700_CAPSITCON = 1 AND CB700_CLTCGC = {0}

${SQL_VALIDAR_ALTERACAO}                            SELECT LPAD(CB200_CLTCGC , 12 , '0')|| LPAD(CB200_CLTCGCDIG, 2,'0') AS CPF,CB700_CAPJUSTCON
...                                                 FROM DNCB00.DNCBB2
...                                                 INNER JOIN DNCB00.DNCBB7 ON CB200_CLTCGC = CB700_CLTCGC WHERE CB700_CLTCGC = {0}
...                                                 ORDER BY CB700_HDRDATA DESC ,CB700_HDRHORA DESC FETCH FIRST ROW ONLY  

${SLQ_DESFAZER_ALTERACAO}                           DELETE FROM DNCB00.DNCBB7
...                                                 WHERE LPAD(CB700_CLTCGC,12 ,'0') || LPAD(CB700_CLTCGCDIG,2,'0') = {0} AND CB700_CAPJUSTCON = '{1}' 

${SQL_VALIDAR_CONSULTA_SINTETICA_CLIENTE}           SELECT
...                                                 LPAD(CB200_CLTCGC, 12,'0')||LPAD(CB200_CLTCGCDIG, 2,'0') AS CPF,
...                                                 CB200_CLTNOM AS NOME,
...                                                 TRIM(CB700_CAPTIPCLI) AS TIPO_CLIENTE,
...                                                 LPAD(CB700_CAPDTACOCON,8,'0') AS DATA_ACOLHIDA,
...                                                 LPAD(CB700_CAPSEQCON,8,'0') AS NUMERO_SEQUENCIAL,
...                                                 TRIM(CB700_CAPSITCON) AS SITUACAO,
...                                                 LPAD(CB700_CAPDATCON,8,'0') AS DATA_CONTESTACAO,
...                                                 LPAD(CB700_CAPCGCIF,12,'0') || LPAD(CB700_CAPCGCIFDIG,2,'0') AS CNPJ_INST_FINANCEIRA,
...                                                 LPAD(CB700_CAPCGCGBDOR,12,'0') || LPAD(CB700_CAPCGCGBDOD,2,'0') AS CNPJ_GBD_ORIGEM,
...                                                 TRIM(CB700_CAPJUSTCON) AS JUSTIFICATIVA    
...                                                 FROM DNCB00.DNCBB2 INNER JOIN DNCB00.DNCBB7 ON CB200_CLTCGC = CB700_CLTCGC
...                                                 WHERE CB700_CLTCGC = {0} ORDER BY CB700_CAPSEQCON DESC FETCH FIRST ROW ONLY 

${SQL_VALIDAR_INFORMACOES_HISTORICO_CLIENTE}        SELECT
...                                                 LPAD(CB200_CLTCGC, 12,'0')||LPAD(CB200_CLTCGCDIG, 2,'0') AS CPF,
...                                                 TRIM(CB200_CLTNOM) AS NOME,
...                                                 TRIM(CB700_CAPTIPCLI) AS TIPO_CLIENTE,
...                                                 LPAD(CB700_CAPDTACOCON,8,'0') AS DATA_ACOLHIDA,
...                                                 TRIM(CB700_CAPSITCON) AS SITUACAO,
...                                                 LPAD(CB700_CAPDATCON,8,'0') AS DATA_CONTESTACAO,
...                                                 LPAD(CB700_CAPCGCIF,12,'0') || LPAD(CB700_CAPCGCIFDIG,2,'0') AS CNPJ_INST_FINANCEIRA,
...                                                 LPAD(CB700_CAPCGCGBDOR,12,'0') || LPAD(CB700_CAPCGCGBDOD,2,'0') AS CNPJ_GBD_ORIGEM,
...                                                 LPAD(CB700_DEPSEQ,4,'0') AS DEP,
...                                                 TRIM(CB700_CAPJUSTCON) AS JUSTIFICATIVA,
...                                                 TRIM(CB700_CCSCOD) AS CONTRATO
...                                                 FROM DNCB00.DNCBB2
...                                                 INNER JOIN DNCB00.DNCBB7 ON CB200_CLTCGC = CB700_CLTCGC
...                                                 WHERE CB700_CLTCGC = {0}
...                                                 ORDER BY CB700_CAPSEQCON  FETCH FIRST ROW ONLY    

*** Keywords ***
# CT011
Recuperar Acesso Contestacao Cliente
    ${query}    Query      ${RECUPERAR_ACESSO_CONTEST_CLIENTE}    
    [Return]    ${query}[0][0] 

Recuperar Acesso Informacoes Sintetica
    ${query}    Query      ${RECUPERAR_ACESSO_CONTEST_CLIENTE}    
    [Return]    ${query}[0][0]

Recupera Dados Excluir Contestacao Operacao
    ${query}    Query    ${SQL_RECUPERAR_DADOS_ACESSO_EXCLUIR_CONTEST}    
    [Return]    ${query}[0][0]

Validar Justificativa
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_VALIDAR_JUSTIFICATIVA_CONTESTACAO}    ${cnpj}
    ${result}      Run Keyword And Return Status     Check If Exists In Database   ${query}  
    [Return]       ${result}        

Desfaz Dados Incluir Justificativa
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_DESFAZER_JUSTIFICATIVA}    ${cnpj}
    ${result}      Execute Sql String    ${query}
    [Return]       ${result}
    
# CT012
Validar Alteracao Justificativa
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_VALIDAR_ALTERACAO}    ${cnpj}
    ${result}      Run Keyword And Return Status     Check If Exists In Database   ${query}  
    [Return]       ${result} 
    
Desfazer Alteracao De Justificativa
    [Arguments]    ${cnpj}    ${justificativa}
    ${query}       Format String    ${SLQ_DESFAZER_ALTERACAO}    ${cnpj}    ${justificativa}
    ${result}      Execute Sql String    ${query}
    [Return]       ${result}
       
# CT014
Consultar Sintetica Cliente
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_SINTETICA_CLIENTE}    ${cnpj}
    ${result}      Query    ${query}      
    [Return]       ${result}      

# CT015
Consultar Historico Cliente
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_VALIDAR_INFORMACOES_HISTORICO_CLIENTE}    ${cnpj}
    ${result}      Query    ${query}      
    [Return]       ${result}      

    
