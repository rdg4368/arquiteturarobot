*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      Collections    
Library      String    

*** Variables ***
${SQL_DADOS_ACESS_CONSULTAR_MANDATOS}    SELECT C3500_RCMTIPPES, CONCAT(LPAD(C3500_CLTCGC,12 ,'0'),(LPAD(C3500_CLTCGCDIG ,2 ,'0'))) FROM DNCB00.DNCB35 ORDER BY C3500_HDRDATA DESC FETCH FIRST ROW ONLY;

${SQL_VALIDAR_CONSULTAR_MANDATOS}        SELECT TRIM(C3200_PORNOMORG),TRIM(C3300_PCANOMCARGO),LPAD(C3200_PORCODORG , 5 , '0'),LPAD(C3500_PMAIDSERASA, 9 , '0'),TRIM(C3300_PCACODCARGO),
...                                      LPAD(C3500_PMADATNOMEA , 8 , '0'), LPAD(C3500_PMADATEXONE , 8 , '0') FROM DNCB00.DNCB35 INNER JOIN DNCB00.DNCB32 ON C3500_PMACODORG = C3200_PORCODORG INNER JOIN DNCB00.DNCB33 ON C3500_PMACODCARGO = C3300_PCACODCARGO 
...                                      WHERE C3500_CLTCGC = '{0}' AND C3500_CLTCGCDIG = '{1}' AND C3500_RCMTIPPES = 'F' ORDER BY C3500_HDRDATA DESC FETCH FIRST 3 ROWS ONLY;

*** Keywords ***
Recuperar Dados Acesso Consultar Mandatos
    ${result}             Query    ${SQL_DADOS_ACESS_CONSULTAR_MANDATOS}    
    ${tipo_pessoa}        Get From List    ${result}[0]      0
    ${cpf/cnpj}           Get From List    ${result}[0]      1
    ${cgc}                Get Substring    ${result}[0][1]   0    12
    ${dig}                Get Substring    ${result}[0][1]   12   14
    [Return]    ${tipo_pessoa}    ${cpf/cnpj}    ${cgc}    ${dig}
    
Validar Consulta Mandatos
    [Arguments]    ${codCgc}    ${digito}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTAR_MANDATOS}    ${codCgc}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}