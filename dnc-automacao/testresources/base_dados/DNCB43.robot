*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections    
Library      String    
*** Variables ***
${SQL_DADOS_ACEESO_CONSULTAR_REMESSA}    SELECT (SUBSTR(CHAR(C4300_ESRDTREMESS),7,2))||SUBSTR(CHAR ( C4300_ESRDTREMESS ),5,2)||SUBSTR(CHAR (C4300_ESRDTREMESS ),1,4)AS DATAFORMATADA, TRIM(C4300_ESRDTREMESS) FROM DNCB00.DNCB43 ORDER BY C4300_ESRDTREMESS DESC

${SQL_VALIDAR_CONSULTA_REMESSA}    SELECT LPAD(C4300_ESRQTREGREC,7,'0') AS QTD_RECUSADA ,LPAD(C4300_ESRQTREGACE,7,'0') AS QTD_ACEITA ,TRIM(C4300_SISCODSIS) AS SISTEMA FROM  DNCB00.DNCB43 WHERE C4300_ESRDTREMESS = {0} FETCH FIRST 10 ROW ONLY


*** Keywords ***
Recuperar Dados Acesso Consultar Remessa
    ${result}     Query    ${SQL_DADOS_ACEESO_CONSULTAR_REMESSA}    
    ${data}       Get From List    ${result}[0]    0
    ${dataInv}    Get From List    ${result}[0]    1
    [Return]      ${data}    ${dataInv}

Validar Consulta Remessa
    [Arguments]    ${dta}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_REMESSA}    ${dta}    
    ${result}      Query            ${query}    
    [Return]       ${result}