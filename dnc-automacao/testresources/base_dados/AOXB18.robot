*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
   
*** Variables ***
${SQL_DADOS_ACESSO_CONSULTAR_CLASS_CLIENTE}
...    SELECT TRIM(X1800_CLTCOD) AS COD_CLIENTE 
...    FROM AOXB00.AOXB18 A INNER JOIN AOXB00.AOXB01 ON (X1800_CLTCOD = X0100_CLTCOD) 
...    ORDER BY A.SEDIDR ASC FETCH FIRST ROW ONLY

*** Keywords ***
Recuperar Dados Acesso Consultar Classificacao Cliente
    ${result}    Query    ${SQL_DADOS_ACESSO_CONSULTAR_CLASS_CLIENTE}    
    [Return]     ${result}