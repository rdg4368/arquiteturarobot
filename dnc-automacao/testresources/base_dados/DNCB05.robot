*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String

*** Variables ***
${SQL_CONTA_EXCLUIR_SAQUE_DESCOBERTO}     SELECT CONCAT (LPAD(C0500_CTCCODDEPG,3,'0'),CONCAT (LPAD(C0500_CTCNUMSEQ,6,'0'),TRIM(C0500_CTCDGVCONTA)))AS CTA_CORRENTE FROM DNCB00.DNCB05 FETCH FIRST  ROW ONLY

${SQL_DADOS_ROLLBACK_EXLCUI_SQ_DESC}        SELECT TRIM(SEDIDR)AS ID, TRIM(C0500_HDRDATA),TRIM(C0500_HDRHORA),TRIM(C0500_HDRESTACAO),TRIM(C0500_HDRPROGRAMA),TRIM(C0500_HDRSTATGER),
...                                        TRIM(C0500_CTCCODDEPG),TRIM (C0500_CTCNUMSEQ),TRIM(C0500_CTCDGVCONTA),TRIM (C0500_SQDDATREF),TRIM(C0500_ADDCOD),TRIM(C0500_NDACOD),
...                                        TRIM(C0500_CLTCGC),TRIM (C0500_CLTCGCDIG),TRIM(C0500_CLTCOD),TRIM(C0500_SQDQTDDIA),TRIM(C0500_SQDVALMED),TRIM(C0500_SQDDATULT),TRIM(C0500_CSSDATATU) FROM DNCB00.DNCB05 WHERE C0500_CLTCOD = '{0}' FETCH FIRST ROW ONLY  

${SQL_DESFAZER_EXCLUSAO_SQ_DESC}            INSERT INTO DNCB00.DNCB05(C0500_HDRDATA,C0500_HDRHORA,C0500_HDRESTACAO,C0500_HDRPROGRAMA,C0500_HDRSTATGER,
...                                         C0500_CTCCODDEPG, C0500_CTCNUMSEQ,C0500_CTCDGVCONTA, C0500_SQDDATREF,C0500_ADDCOD,C0500_NDACOD,
...                                         C0500_CLTCGC, C0500_CLTCGCDIG,C0500_CLTCOD,C0500_SQDQTDDIA,C0500_SQDVALMED,C0500_SQDDATULT,C0500_CSSDATATU)
...                                         VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}', '{12}','{13}','{14}','{15}','{16}','{17}')

${SQL_VALIDAR_EXCLUSAO_SQ_DESC}            SELECT * FROM DNCB00.DNCB05 WHERE SEDIDR = '{0}'

${SQL_HIST_SAQUE_DESCOBERTO}               SELECT LPAD(C0500_CLTCGC,12,'0'), LPAD(C0500_CLTCGCDIG,2,'0') FROM DNCB00.DNCB05 FETCH FIRST 1 ROW ONLY
${SQL_VALIDAR_SAQUE_DESCOBERTO}            SELECT CONCAT(CONCAT (LPAD(C0500_CTCCODDEPG,3,'0'),LPAD(C0500_CTCNUMSEQ,6,'0')), TRIM (C0500_CTCDGVCONTA) ) AS CONTA_CORRENTE, LPAD(C0500_SQDDATREF,6,'0'), LPAD(C0500_SQDQTDDIA,2,'0') AS DIAS,
...                                        C0500_SQDVALMED AS VALOR_MEDIO, LPAD(C0500_SQDDATULT,8,'0') AS ULTIMO_DIA, X0100_CLTCGC AS CPF_CGC, X0100_CLTNOM AS NOME
...                                        FROM DNCB00.DNCB05, AOXB00.AOXB01 WHERE C0500_CLTCGC = '{0}' AND C0500_CLTCOD = X0100_CLTCOD ORDER BY C0500_SQDDATREF DESC

*** Keywords ***
Recuperar Conta Corrente Excluir Saque Descoberto
    ${result}    Query    ${SQL_CONTA_EXCLUIR_SAQUE_DESCOBERTO}    
    [Return]     ${result}[0][0]
    
Armazenar Dados Exclusao Saque Descoberto
    [Arguments]    ${cltCod}    
    ${query}       Format String    ${SQL_DADOS_ROLLBACK_EXLCUI_SQ_DESC}        ${cltCod} 
    ${result}      Query            ${query}
    [Return]       ${result}[0]
    
Validar Excluir Saque Descoberto
    [Arguments]    ${id}
    ${query}       Format String    ${SQL_VALIDAR_EXCLUSAO_SQ_DESC}    ${id}
    ${status}      Run Keyword And Return Status    Check If Not Exists In Database    ${query}        
    [Return]       ${status}
    
Desfazer Excluir Saque Descoberto
    [Arguments]    ${rollback}
    ${query}       Format String    ${SQL_DESFAZER_EXCLUSAO_SQ_DESC}    ${rollback}[1]    ${rollback}[2]     ${rollback}[3]       ${rollback}[4]    ${rollback}[5]    ${rollback}[6]    ${rollback}[7]     ${rollback}[8]
    ...                                                                 ${rollback}[9]    ${rollback}[10]    ${rollback}[11]      ${rollback}[12]   ${rollback}[13]   ${rollback}[14]   ${rollback}[15]    ${rollback}[16]    
    ...                                                                 ${rollback}[17]   ${rollback}[18]    
    ${status}      Run Keyword And Return Status   Execute Sql String   ${query}        
    [Return]       ${status}
    
# PACOTE 06 - 001 - CONSULTAS DIVERSAS
# CT010
Recuperar Dados Entrada Historico Saque Descoberto
    ${result}    Query    ${SQL_HIST_SAQUE_DESCOBERTO} 
    [Return]     ${result}
    
Recuperar Dados Validacao Historico Saque Descoberto
    [Arguments]  ${cpf/cgc}
    ${query}     Format String   ${SQL_VALIDAR_SAQUE_DESCOBERTO}       ${cpf/cgc}     
    ${result}    Query           ${query}       
    [Return]     ${result}


