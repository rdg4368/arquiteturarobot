*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections    
Library      String    

*** Variables ***
${SQL_DESFAZER_SOLICITACAO_AQR_CCS005}        DELETE FROM DNCB00.DNCB29 WHERE C2900_HDRDATA = YEAR(CURRENT_DATE)|| LPAD(MONTH(CURRENT_DATE) , 2 , '0')|| LPAD(DAY(CURRENT_DATE) , 2 , '0')
...                                           AND C2900_HDRPROGRAMA = 'DNCNG1' AND C2900_OPMCODOPE = '1600'

${SQL_VALIDAR_CONSULTA_SOLIC_ENVIADAS}        SELECT (SUBSTR(CHAR(C2900_HDRDATA),7,2))||'/'||SUBSTR(CHAR ( C2900_HDRDATA ),5,2)||'/'||SUBSTR(CHAR (C2900_HDRDATA ),1,4) ,
...                                           (SUBSTR(CHAR(C2900_HDRHORA),1,2))||':'||(SUBSTR(CHAR(C2900_HDRHORA),3,2))||':'||(SUBSTR(CHAR(C2900_HDRHORA),5,4)), LPAD(C2900_OPMCODOPE,5,'0')
...                                           FROM DNCB00.DNCB29 WHERE C2900_HDRPROGRAMA = 'DNCNG1' AND C2900_OPMCODOPE = '1600' ORDER BY C2900_HDRDATA ASC FETCH FIRST 7 ROWS ONLY

*** Keywords ***
Desfazer Solicitacao Arq CCS005 Posicao
    Execute Sql String    ${SQL_DESFAZER_SOLICITACAO_AQR_CCS005}    
    

Validar Consulta Solicitacoes Enviadas
    ${result}    Query    ${SQL_VALIDAR_CONSULTA_SOLIC_ENVIADAS}    
    [Return]     ${result}