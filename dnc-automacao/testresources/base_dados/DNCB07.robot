*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String    

*** Variables ***
${VALIDAR_INCLUSAO_TIPO_RESPONSABILIDADE}        SELECT TRIM(C0700_TRNCOD), TRIM(C0700_TRNDES) FROM DNCB00.DNCB07 WHERE C0700_HDRDATA = {0}

${SQL_DESFAZER_INCLUIR_TP_RESPONSABILIDADE}      DELETE FROM DNCB00.DNCB07 WHERE C0700_HDRDATA = {0} AND C0700_TRNCOD = '{1}' AND C0700_TRNDES = '{2}'

${SQL_VALIDAR_CONSULTA_TP_RESPONSABILIDADE}      SELECT TRIM(C0700_TRNCOD), TRIM(C0700_TRNDES) FROM DNCB00.DNCB07 ORDER BY C0700_TRNCOD FETCH FIRST 12 ROW ONLY
  
${SQL_VALIDAR_EXCLUIR_TIPO_RESPONSABILIDADE}     SELECT * FROM DNCB00.DNCB07 WHERE C0700_TRNCOD = '{0}' AND C0700_TRNDES = '{1}'

*** Keywords ***
Validar Incluir Tipo Responsabilidade
    [Arguments]    ${data}
    ${query}     Format String    ${VALIDAR_INCLUSAO_TIPO_RESPONSABILIDADE}    ${data}
    ${result}    Query            ${query}    
    [Return]     ${result}
    
Desfazer Incluir Tipo Responsabilidade
    [Arguments]       ${data}    ${trnCod}   ${trnDes}
    ${query}    Format String    ${SQL_DESFAZER_INCLUIR_TP_RESPONSABILIDADE}      ${data}    ${trnCod}   ${trnDes}
    ${status}   Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]    ${status}
    
Validar Consulta Tipo Responsabilidade
    ${result}    Query    ${SQL_VALIDAR_CONSULTA_TP_RESPONSABILIDADE}
    [Return]     ${result}
    
Validar Excluir Tipo Responsabilidade
    [Arguments]    ${trnCod}   ${trnDes}
    ${query}    Format String    ${SQL_VALIDAR_EXCLUIR_TIPO_RESPONSABILIDADE}   ${trnCod}   ${trnDes}
    ${status}   Run Keyword And Return Status    Check If Not Exists In Database            ${query}
    [Return]    ${status}