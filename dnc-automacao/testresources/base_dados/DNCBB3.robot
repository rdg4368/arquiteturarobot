*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      utils    
Library      utilsMask    
Library      String    

*** Variables ***
${SQL_RECUPERAR_DADOS_INCLUIR_GBDS}
...     SELECT CB300_CAPCGCGBD || CB300_CAPCGCGBDIG AS CNPJ, CB300_CAPNOMEGBD AS NOME,
...            CB300_CAPENDGBD AS LOGRADOURO, CB300_CAPCIDGBD AS CIDADE, CB300_CAPUFGBD AS UF
...     FROM DNCB00.DNCBB3 ORDER BY CB300_HDRDATA DESC FETCH FIRST ROW ONLY;

${SQL_EXCLUIR_REGISTRO}
...     DELETE FROM DNCB00.DNCBB3
...     WHERE CB300_CAPCGCGBD = '{0}'

${SQL_DESFAZER_DADOS_GBDS}
...    DELETE FROM 
...    DNCB00.DNCBB3 
...    WHERE CB300_CAPNOMEGBD = 'TESTE AUTOMACAO'

${SQL_VALIDAR_DADO_INLCUSO} 
...    SELECT * FROM DNCB00.DNCBB3 WHERE CB300_CAPCGCGBD = {0} AND CB300_CAPNOMEGBD = '{1}' AND 
...                  CB300_CAPENDGBD = '{2}' AND CB300_CAPCIDGBD = '{3}' AND CB300_CAPUFGBD = '{4}'

${SQL_VALIDAR_DADOS_ALTERACAO}         
...    SELECT * FROM 
...    DNCB00.DNCBB3	
...    WHERE CB300_CAPCGCGBD = {0} AND CB300_CAPNOMEGBD = '{1}' AND
...          CB300_CAPENDGBD = '{2}' FETCH FIRST ROW ONLY;

${SQL_VALIDAR_DADOS_CONSULTA_HISTORICO}
...    SELECT LPAD(SUBSTR(CB300_CAPCGCGBD,1,2),2,'0') || LPAD(SUBSTR(CB300_CAPCGCGBD,3,3),3,'0') ||
...           LPAD(SUBSTR(CB300_CAPCGCGBD,6,3),3,'0') || LPAD(SUBSTR(CB300_CAPCGCGBD,9,4),4,'0') ||
...           LPAD(CB300_CAPCGCGBDIG,2,'0') , TRIM(CB300_CAPNOMEGBD) AS NOME, TRIM(CB300_CAPSITGBD) AS SITUACAO, 
...           TRIM(CB300_USUNUM) AS USUARIO
...    FROM DNCB00.DNCBB3 FETCH FIRST 13 ROWS ONLY;

${SQL_VOLTAR_EXCLUSAO}                     
...    INSERT INTO DNCB00.DNCBB3 (CB300_CAPCGCGBD, CB300_CAPCGCGBDIG, CB300_CAPNOMEGBD, CB300_CAPENDGBD, CB300_CAPCIDGBD, CB300_CAPUFGBD,
...                 CB300_CAPDTCADGBD, CB300_CAPHRCADGBD) 
...    VALUES ({0},{1},'{2}','{3}','{4}','{5}','{6}','{7}');

${SQL_VALIDAR_EXCLUSAO_GBDS}
...    SELECT
...        *
...    FROM
...        DNCB00.DNCBB3
...    WHERE
...        CB300_CAPSITGBD = 1
...        AND CB300_CAPCGCGBD = {0} FETCH FIRST ROW ONLY

${SQL_CONSULTAS_GBDS} 
...    SELECT LPAD(SUBSTR(CB300_CAPCGCGBD,1,2),2,'0') || LPAD(SUBSTR(CB300_CAPCGCGBD,3,3),3,'0') ||
...           LPAD(SUBSTR(CB300_CAPCGCGBD,6,3),3,'0') || LPAD(SUBSTR(CB300_CAPCGCGBD,9,4),4,'0') ||
...           LPAD(CB300_CAPCGCGBDIG,2,'0') , TRIM(CB300_CAPNOMEGBD) AS NOME,
...           TRIM(CB300_CAPENDGBD) AS LOGRADOURO, TRIM(CB300_CAPCIDGBD) AS CIDADE, TRIM(CB300_CAPUFGBD) AS UF
...    FROM DNCB00.DNCBB3 FETCH FIRST ROW ONLY;

${SQL_VALIDAR_INCLUIR_AUTORIZACAO}
...    SELECT * FROM DNCB00.DNCBB2 WHERE CB200_CLTCGC = {0} AND CB200_CAPSITCLI = 1 
...    ORDER BY CB200_HDRDATA DESC FETCH FIRST ROW ONLY
 
${SQL_VALIDAR_CONSULTA_AUTORIZACAO}
...    SELECT
...    	CB200_CLTNOM AS NOME,
...    	TRIM(CB200_CAPDTCADAU) AS DATA_CADASTRO,
...    	TRIM(CB200_CAPDTSITCLI) AS DT_SITUACAO
...    FROM
...    	DNCB00.DNCBB2
...    WHERE
...    	CB200_CLTCGC = {0} FETCH FIRST ROW ONLY

${SQL_CONSULTA_GBDS}
...    SELECT
...    	CB300_CAPCGCGBD || CB300_CAPCGCGBDIG AS CNPJ,
...    	CB300_CAPNOMEGBD AS NOME,
...    	CB300_CAPENDGBD AS LOGRADOURO,
...    	CB300_CAPCIDGBD AS CIDADE,
...    	CB300_CAPUFGBD AS UF
...    FROM
...    	DNCB00.DNCBB3
...    ORDER BY
...    	SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_INSERT_GBDS}
...    INSERT INTO DNCB00.DNCBB3 (CB300_HDRDATA,CB300_HDRHORA,CB300_HDRESTACAO,CB300_HDRPROGRAMA,CB300_HDRSTATGER,CB300_CAPCGCGBD,
...    CB300_CAPCGCGBDIG,CB300_CAPDTCADGBD,CB300_CAPHRCADGBD,CB300_CAPNOMEGBD,CB300_CAPENDGBD,CB300_CAPCIDGBD,CB300_CAPUFGBD,CB300_CAPSITGBD,CB300_CAPDTSITGBD,CB300_USUNUM) 
...    VALUES 
...    (20200324,152016,800,'DNCNJA','0',51244791,83,20200324,152016,'TESTE AUTOMACAO               ','TESTE                                   ','BRASILIA              ','DF',1,20200324,0)
...    ;



*** Keywords ***
Desfazer Dados GBDS
    Execute Sql String     ${SQL_DESFAZER_DADOS_GBDS}    
    
Recuperar Dados Incluir GBSD
    ${result}    Query    ${SQL_RECUPERAR_DADOS_INCLUIR_GBDS}
    [Return]    ${result}

Valida Dados Incluir GBD'S
    [Arguments]    ${cnpj}    ${nome}    ${logradouro}    ${cidade}    ${uf}
    ${query}    Format String    ${SQL_VALIDAR_DADO_INLCUSO}    ${cnpj}    ${nome}    ${logradouro}    ${cidade}    ${uf}
    ${status}   Run Keyword And Return Status     Check If Exists In Database    ${query} 
    [Return]    ${status}      
    
Excluir registro de inclusao
    [Arguments]    ${cnpj}
    ${query}    Format String    ${SQL_EXCLUIR_REGISTRO}     ${cnpj}
    ${status}   Execute Sql String    ${query} 
    [Return]    ${query}
    
Recuperar Dados GBDS
    ${result}    query    ${SQL_CONSULTA_GBDS}
    [Return]    ${result} 
    
# CT003
Desfazer Exclusao GBDS
    [Arguments]    ${cnpj}    ${digitCnpj}    ${nome}    ${logradouro}    ${cidade}    ${uf}    ${data}    ${hora}
    ${query}    Format String     ${SQL_VOLTAR_EXCLUSAO}    ${cnpj}    ${digitCnpj}    ${nome}    ${logradouro}    ${cidade}    ${uf}    ${data}    ${hora}
    ${status}    Query    ${query}    
    [Return]    ${status}    
     
Consultar Dados Exclusao GBDS
    [Arguments]    ${cnpj}
    ${query}    Format String    ${SQL_VALIDAR_EXCLUSAO_GBDS}     ${cnpj}
    ${status}   Run Keyword And Return Status    Check If Exists In Database    ${query} 
    [Return]    ${status}

Consultar Validacao Historico
    ${query}    Query    ${SQL_VALIDAR_DADOS_CONSULTA_HISTORICO}
    [Return]    ${query}
    
# CT004
Validar Consulta Detalhada GBDS
    ${status}    Query    ${SQL_CONSULTAS_GBDS}
    [Return]     ${status}       

# CT006
Valida Dados Incluir Autorizacao
    [Arguments]    ${cnpj}
    ${query}    Format String    ${SQL_VALIDAR_INCLUIR_AUTORIZACAO}    ${cnpj}
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}                   
    [Return]       ${status}   
    
# CT009
Validar Consulta Autorizacao
    [Arguments]    ${cnpj}
    ${query}     Format String    ${SQL_VALIDAR_CONSULTA_AUTORIZACAO}    ${cnpj}
    ${result}    Query    ${query}    
    [Return]     ${result}         

Preparar Dados GBDS
    Execute Sql String    ${SQL_INSERT_GBDS}
    
