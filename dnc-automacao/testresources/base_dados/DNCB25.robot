*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String

*** Variables ***
${SQL_DADOS_ENTRADA_CONSULTAR_BDV}              SELECT LPAD(C2500_CLTCGC,12,'0') || LPAD(C2500_CLTCGCDIG,2,'0') AS CNPJ_CPF, 
...                                             CHAR(C2500_BDVTIPPES) AS TIPO_PESSOA, LPAD(C2500_CLTCOD,8,'0') AS COD_CLIENTE FROM DNCB00.DNCB25 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_BDV}                     SELECT LPAD(C2500_CTCCHVCONTA,10,'0') , LPAD(C2500_BDVNUMOPE,12,'0') ,CHAR (C2500_SISCODSIS),
...                                             LPAD (C2500_DEPSEQ,4,'0') ,(SUBSTR(CHAR(C2500_BDVDATINI),7,2))||'/'||SUBSTR(CHAR ( C2500_BDVDATINI ),5,2)||'/'||SUBSTR(CHAR (C2500_BDVDATINI ),1,4) 
...                                             FROM DNCB00.DNCB25 WHERE C2500_CLTCGC = '{0}' AND C2500_CLTCGCDIG = '{1}' AND C2500_CLTCOD = '{2}' FETCH FIRST 6 ROWS ONLY

${SQL_ROLLBACK_ALTERAR_BDV}                    SELECT LPAD(C2500_BDVDATINI,8,'0') FROM DNCB00.DNCB25 WHERE C2500_CLTCGC = '{0}' AND C2500_CLTCGCDIG = '{1}' AND C2500_CLTCOD = '{2}' FETCH FIRST ROW ONLY


${SQL_VALIDAR_ALTERAR_BDV}                     SELECT * FROM DNCB00.DNCB25 WHERE C2500_BDVDATINI = '{0}' AND C2500_CLTCGC = '{1}' AND C2500_CLTCGCDIG = '{2}' AND C2500_CLTCOD = '{3}' FETCH FIRST ROW ONLY
    
${SQL_DESFAZER_ALTERACAO_BDV}                  UPDATE DNCB00.DNCB25 SET C2500_BDVDATINI = '{0}' WHERE C2500_BDVDATINI = '{1}' AND C2500_CLTCGC = '{2}' AND C2500_CLTCGCDIG = '{3}' AND C2500_CLTCOD = '{4}'	

${SQL_DADOS_ENTRADA_ALTERAR_BDV}               SELECT LPAD(C2500_CLTCGC,12,'0') || LPAD(C2500_CLTCGCDIG,2,'0') AS CNPJ_CPF, 
...                                            CHAR(C2500_BDVTIPPES) AS TIPO_PESSOA, LPAD(C2500_CLTCOD,8,'0') AS COD_CLIENTE 
...                                            FROM DNCB00.DNCB25 WHERE C2500_BDVDATINI <> YEAR(CURRENT_DATE)|| LPAD(MONTH(CURRENT_DATE) , 2 , '0')|| LPAD(DAY(CURRENT_DATE) , 2 , '0') FETCH FIRST ROW ONLY 

*** Keywords ***
Recuperar Dados Entrada Consultar BDV
    ${result}    Query    ${SQL_DADOS_ENTRADA_CONSULTAR_BDV}    
    [Return]     ${result}[0]
    
Validar Consulta BDV
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]    0    12
    ${digito}      Get Substring    ${dados}[0]    12    14
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_BDV}    ${cpf_cnpj}    ${digito}    ${dados}[2]
    ${result}      Query            ${query} 
    [Return]       ${result}   
    
Recuperar Dado Rollback Alterar BDV
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]    0    12
    ${digito}      Get Substring    ${dados}[0]    12    14    
    ${query}       Format String    ${SQL_ROLLBACK_ALTERAR_BDV}    ${cpf_cnpj}    ${digito}    ${dados}[2]
    ${result}      Query            ${query} 
    [Return]       ${result}[0][0]
    
Validar Alteracao BDV
    [Arguments]    ${dataAlterada}    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]    0    12
    ${digito}      Get Substring    ${dados}[0]    12    14
    ${query}       Format String    ${SQL_VALIDAR_ALTERAR_BDV}    ${dataAlterada}    ${cpf_cnpj}    ${digito}    ${dados}[2]
    ${status}      Run Keyword And Return Status    Check If Exists In Database      ${query}        
    [Return]       ${status}
    

Desfazer Alteracao BDV
    [Arguments]    ${data}    ${dadoRollback}    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]    0    12
    ${digito}      Get Substring    ${dados}[0]    12    14
    ${query}       Format String    ${SQL_DESFAZER_ALTERACAO_BDV}    ${dadoRollback}    ${data}    ${cpf_cnpj}    ${digito}    ${dados}[2]
    ${status}      Run Keyword And Return Status    Execute Sql String      ${query}        
    [Return]       ${status}

Recuperar Dados Entrada Alterar BDV
   ${result}       Query    ${SQL_DADOS_ENTRADA_ALTERAR_BDV}
    [Return]       ${result}[0]
    