*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  


*** Variables ***
${SQL_ANOTACOES_DEPARTAMENTAIS}
...    select LPAD(C1000_SRECGC,12,'0'),
...    TRIM(C1000_SRECGCDIG), 
...    TRIM(X0100_CLTCOD),
...    TRIM(C1000_SREDATCONS)
...     from DNCB00.DNCB10, AOXB00.AOXB01 
...    where C1000_OADCOD = 'DEP' and C1000_CLTCOD = X0100_CLTCOD  FETCH FIRST ROW ONLY

${SQL_ANOTACOES_CONTRATOS}
...    SELECT LPAD(C1000_SRECGC,12,'0'),
...    TRIM(LPAD(C1000_SRECGCDIG,2,'0')),
...    TRIM(C1000_SREDATCONS) 
...    FROM DNCB00.DNCB10 
...    where C1000_OADCOD = 'CTR' FETCH FIRST ROW ONLY

${SQL_CHEQUES_DEVOLVIDOS}
...    SELECT DISTINCT LPAD(C1000_SRECGC,12,'0'),
...     LPAD(C1000_SRECGCDIG,2,'0'),
...     TRIM(C1000_SREDATCONS)
...     FROM DNCB00.DNCB10 
...    WHERE C1004_RCDQTDCHQ > 0 AND C1000_TPREGISTRO = 4 
...    FETCH FIRST ROW ONLY

${SQL_SAQUE_DESCOBERTO}
...    SELECT LPAD(C1000_SRECGC,12,'0'),
...     LPAD(C1000_SRECGCDIG,2,'0'), 
...    TRIM(C1000_SREDATCONS) 
...    FROM DNCB00.DNCB10 
...    WHERE C1003_RSDDATREF > 0 AND C1000_TPREGISTRO = 3 
...    FETCH FIRST 1 ROW ONLY    

${SQL_PROPOSTAS_INDEFERIDAS}
...    SELECT LPAD(C1000_SRECGC,12,'0'), 
...    LPAD(C1000_SRECGCDIG,2,'0'), 
...    TRIM(C1000_SREDATCONS) 
...    FROM DNCB00.DNCB10 
...    WHERE C1000_OADCOD = 'GCR' 
...    FETCH FIRST ROW ONLY

${SQL_INFORMACOES_GERAIS_SPC}       
...    SELECT LPAD(C1000_SRECGC,12,'0'), LPAD(C1000_SRECGCDIG,2,'0'), 
...    LPAD(C1000_SREDATCONS,8,'0') FROM DNCB00.DNCB10 
...    WHERE C1000_ADDCOD = 41 FETCH FIRST 1 ROW ONLY

${SQL_INFORMACOES_SERASA}
...    SELECT LPAD(C1000_SRECGC,12,'0'), 
...    LPAD(C1000_SRECGCDIG,2,'0'),
...     TRIM(C1000_SREDATCONS) 
...    FROM DNCB00.DNCB10 
...    WHERE C1000_ADDCOD = 22 FETCH FIRST ROW ONLY

# 003 - Consultas - SPC
${SQL_RESTRICAO_CREDITO}
...    SELECT LPAD(C1000_SRECGC,12,'0'),
...    LPAD(C1000_SRECGCDIG,2,'0'),
...    TRIM(C1000_SREDATCONS) FROM DNCB00.DNCB10 
...    WHERE C1000_ADDCOD = 42 
...    FETCH FIRST ROW ONLY

${SQL_CHEQUE_LOJISTA}
...    SELECT LPAD(C1000_SRECGC,12,'0'), 
...    LPAD(C1000_SRECGCDIG,2,'0'), 
...    TRIM(C1000_SREDATCONS) 
...    FROM DNCB00.DNCB10 
...    WHERE C1000_ADDCOD = 43 
...    FETCH FIRST ROW ONLY

${SQL_CONSULTA_ACAO_JUDICIAL_SERASA}
...     SELECT
...        	TRIM(C1000_SRECGC) AS CPF_CGC,
...         TRIM(C1000_SRECGCDIG) AS DIGITO,
...         TRIM(C1000_SREDATCONS)  
...    FROM
...    	    DNCB00.DNCB10
...    WHERE
...    	    C1000_ADDCOD = 24
...    	    AND C1000_SRECGC >= 129346210001
...    ORDER BY
...        	C1000_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_CONSULTA_CHEQUE_SEM_FUNDO}
...     SELECT
...        	TRIM(C1000_SRECGC) AS CPF_CGC,
...         TRIM(C1000_SRECGCDIG) AS DIGITO,
...         TRIM(C1000_SREDATCONS)  
...    FROM
...    	    DNCB00.DNCB10
...    WHERE
...    	    C1000_ADDCOD = 29
...    	    AND C1000_SRECGC >= 129346210001
...    ORDER BY
...        	C1000_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_CONSULTA_RECEITA_FEDERAL}
...     SELECT
...        	TRIM(C1000_SRECGC) AS CPF_CGC,
...         TRIM(C1000_SRECGCDIG) AS DIGITO,
...         TRIM(C1000_SREDATCONS)  
...    FROM
...    	    DNCB00.DNCB10
...    WHERE
...    	    C1000_ADDCOD = 30
...    	    AND C1000_SRECGC >= 129346210001
...    ORDER BY
...        	C1000_HDRDATA DESC FETCH FIRST ROW ONLY

*** Keywords ***
# PACOTE 06 - 001-Consultas Diversas
# CT002
Recuperar Dados Entrada Anotacoes Departamentais
    ${result}  Query  ${SQL_ANOTACOES_DEPARTAMENTAIS}
    [Return]   ${result}
    
Recuperar Dados Validacao Anotacoes Departamentais
    [Arguments]  ${codigo_cliente}
    ${result}    Query    select LPAD(C1000_SRECGC,12,'0'), TRIM(C1000_SRECGCDIG), TRIM(X0100_CLTNOM) as NOME, TRIM(X0100_CLTCOD) as Cod_Cliente, TRIM(C1000_SREDATCONS) as DATA_CONSULTA, TRIM(C1000_ADDCOD) as COD_ADVERTENCIA, C1000_NDACOD as DESCRICAO_ADVERTENCIA from dncb00.dncb10, aoxb00.aoxb01 where C1000_OADCOD = 'DEP' and C1000_CLTCOD = X0100_CLTCOD and C1000_CLTCOD = ${codigo_cliente} FETCH FIRST ROW ONLY
    [Return]     ${result}
    
# CT003
Recuperar Dados Entrada Anotacoes Contratos
    ${result}    Query    ${SQL_ANOTACOES_CONTRATOS}
    [Return]     ${result}
    
Recuperar Dados Validacao Anotacoes Contratos
    [Arguments]  ${cpf/cgc}  
    ${result}    Query  SELECT LPAD(C1000_SRECGC,12,'0'),LPAD(C1000_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM) AS NOME, TRIM(C1000_CLTCOD) AS COD_CLIENTE, TRIM(C1000_SREDATCONS) AS DATA_CONSULTA, LPAD(C1000_ADDCOD,2,'0') AS COD_ADVERTENCIA, TRIM(C0100_ADDDES) AS DESC_ADVERTENCIAM, C1000_NDACOD, TRIM(C0300_NDADES) FROM DNCB00.DNCB10, AOXB00.AOXB01, DNCB00.DNCB03, DNCB00.DNCB01 WHERE C1000_OADCOD = 'CTR' AND C1000_SRECGC = ${cpf/cgc} AND X0100_CLTCOD = C1000_CLTCOD AND C0300_ADDCOD = C1000_ADDCOD AND C0100_ADDCOD = C1000_ADDCOD ORDER BY C1000_HDRDATA DESC FETCH FIRST ROW ONLY    
    [Return]     ${result}

# CT004    
Recuperar Dados Entrada Cheques Devolvidos
    ${result}    Query    ${SQL_CHEQUES_DEVOLVIDOS}
    [Return]     ${result}
    
Recuperar Dados Validacao Cheques Devolvidos
    [Arguments]  ${cpf/cgc}
    ${result}    Query    SELECT LPAD(C0900_SRECGC,12,'0'), LPAD(C0900_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM), TRIM(C0900_SREDATCONS), TRIM(C0900_CLTCOD), C1004_RCDVALCHQ FROM DNCB00.DNCB09 A INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD = C0900_CLTCOD) INNER JOIN DNCB00.DNCB10 ON (C1000_SRECGC=C0900_SRECGC AND C1000_SRECGCDIG=C0900_SRECGCDIG AND C1004_RCDQTDCHQ > 0 AND C1000_TPREGISTRO = 4) WHERE C0900_SRECGC = ${cpf/cgc} AND C0900_SREDATCONS = (SELECT MAX(C0900_SREDATCONS) FROM DNCB00.DNCB09 WHERE C0900_SRECGC = ${cpf/cgc}) FETCH FIRST 1 ROW ONLY
    [Return]     ${result}    

# CT005
Recuperar Dados Entrada Saque Descoberto
    ${result}    Query    ${SQL_SAQUE_DESCOBERTO}
    [Return]     ${result}
    
Recuperar Dados Validacao Saque Descoberto
    [Arguments]  ${cpf/cgc}  ${data_solicitacao}  
    ${result}    Query    SELECT LPAD(C1000_SRECGC,12,'0'), LPAD(C1000_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM), LPAD(C1000_SREDATCONS,8,'0'), TRIM(C1000_CLTCOD), TRIM(C1003_RSDDATREF), LPAD(C1003_RSDQTDDIA,2,'0'), REPLACE(TRIM(C1003_RSDVALMED),'.',',') , TRIM(LPAD(C1003_RSDDATULT,8,'0')) FROM DNCB00.DNCB10 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD=C1000_CLTCOD) WHERE C1000_SRECGC = ${cpf/cgc} AND C1000_SREDATCONS = ${data_solicitacao} AND C1000_TPREGISTRO = 3
    [Return]     ${result}

# CT006
Recuperar Dados Entrada Propostas Indeferidas
    ${result}    Query    ${SQL_PROPOSTAS_INDEFERIDAS}    
    [Return]     ${result}
    
Recuperar Dados Validacao Propostas Indeferidas
    [Arguments]  ${cpf/cgc}
    ${result}    Query    SELECT LPAD(C1000_SRECGC,12,'0'), LPAD(C1000_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM) AS NOME, C1000_ADRDATOBT AS DATA_CONSULTA, TRIM(C0100_ADDDES) AS DESC_ADVERTENCIA, C0300_NDACOD AS DESCRICAO FROM DNCB00.DNCB10, AOXB00.AOXB01, DNCB00.DNCB03, DNCB00.DNCB01 WHERE C0300_ADDCOD = 2 AND C0300_NDACOD = 13 AND C1000_SRECGC = X0100_CLTCGC AND C1000_SRECGC = ${cpf/cgc} AND C0100_ADDDES = 'PROPOSTA INDEFERIDA' FETCH FIRST ROW ONLY    
    [Return]     ${result}
    
# CT007
Recuperar Dados Entrada Informacoes SPC
    ${result}    Query    ${SQL_INFORMACOES_GERAIS_SPC}
    [Return]     ${result}
    
Recuperar Dados Validacao Informacoes SPC
    [Arguments]  ${cpf/cgc}  ${digito}  ${data_solicitacao}  
    ${result}    Query    SELECT LPAD(C0900_SRECGC,12,'0'), LPAD(C0900_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM), TRIM(LPAD(C0900_SREDATCONS,8,'0')), TRIM(C0900_CLTCOD) FROM DNCB00.DNCB09 INNER JOIN DNCB00.DNCB10 ON (C1000_SRECGC = C0900_SRECGC AND C1000_SRECGCDIG = C0900_SRECGCDIG AND C1000_SREDATCONS = C0900_SREDATCONS) INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD=C0900_CLTCOD) WHERE C1000_SRECGC = ${cpf/cgc} AND C1000_SRECGCDIG = ${digito} AND C1000_SREDATCONS = ${data_solicitacao} AND C1000_ADDCOD = 41
    [Return]     ${result}

# CT008
Recuperar Dados Entrada Informacoes SERASA
    ${result}    Query    ${SQL_INFORMACOES_SERASA}
    [Return]     ${result}
    
Recuperar Dados Validacao Informacoes SERASA
    [Arguments]  ${cpf/cgc}  ${digito}  ${data_solicitacao}
    ${result}    Query    SELECT LPAD(C0900_SRECGC,12,'0'), LPAD(C0900_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM), TRIM(LPAD(C0900_SREDATCONS,8,'0')), TRIM(C0900_CLTCOD) FROM DNCB00.DNCB09 INNER JOIN DNCB00.DNCB10 ON (C1000_SRECGC = C0900_SRECGC AND C1000_SRECGCDIG = C0900_SRECGCDIG AND C1000_SREDATCONS = C0900_SREDATCONS) INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD=C0900_CLTCOD) WHERE C1000_SRECGC = ${cpf/cgc} AND C1000_SRECGCDIG = ${digito} AND C1000_SREDATCONS = ${data_solicitacao} AND C1000_ADDCOD = 22    
    [Return]     ${result}
    
# PACOTE 06 - 003 - Consultas SPC
# CT001
Recuperar Dados Entrada Restricao Credito
    ${result}  Query    ${SQL_RESTRICAO_CREDITO}    
    [Return]   ${result}

Recuperar Dados Validacao Restricao Credito
    [Arguments]  ${cpf/cgc}  ${digito}  ${data_solicitacao}  
    ${result}    Query    SELECT LPAD(C0900_SRECGC,12,'0'), LPAD(C0900_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM), TRIM(LPAD(C0900_SREDATCONS,8,'0')), TRIM(C0900_CLTCOD) FROM DNCB00.DNCB09 INNER JOIN DNCB00.DNCB10 ON (C1000_SRECGC = C0900_SRECGC AND C1000_SRECGCDIG = C0900_SRECGCDIG AND C1000_SREDATCONS = C0900_SREDATCONS) INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD=C0900_CLTCOD) WHERE C1000_SRECGC = ${cpf/cgc} AND C1000_SRECGCDIG = ${digito} AND C1000_SREDATCONS = ${data_solicitacao} AND C1000_ADDCOD = 42
    [Return]     ${result}
    
# CT002
Recuperar Dados Entrada Cheque Lojista
    ${result}    Query    ${SQL_CHEQUE_LOJISTA}
    [Return]     ${result}
    
Recuperar Dados Validacao Cheque Lojista
    [Arguments]  ${cpf/cgc}  ${digito}  ${data_solicitacao}
    ${result}    Query    SELECT LPAD(C0900_SRECGC,12,'0'), LPAD(C0900_SRECGCDIG,2,'0'), TRIM(X0100_CLTNOM), TRIM(LPAD(C0900_SREDATCONS,8,'0')), TRIM(C0900_CLTCOD) FROM DNCB00.DNCB09 INNER JOIN DNCB00.DNCB10 ON (C1000_SRECGC = C0900_SRECGC AND C1000_SRECGCDIG = C0900_SRECGCDIG AND C1000_SREDATCONS = C0900_SREDATCONS) INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD=C0900_CLTCOD) WHERE C1000_SRECGC = ${cpf/cgc} AND C1000_SRECGCDIG = ${digito} AND C1000_SREDATCONS = ${data_solicitacao} AND C1000_ADDCOD = 43
    [Return]     ${result}      
    
Recuperar Acao Judicial
    ${query}    Query    ${SQL_CONSULTA_ACAO_JUDICIAL_SERASA}
    [Return]    ${query}    
    
Recuperar Dados Entrada Cheques Sem Fundo
    ${result}   Query    ${SQL_CONSULTA_CHEQUE_SEM_FUNDO}   
    [Return]    ${result}
    
Recuperar Dados Entrada Receita Federal
    ${result}   Query    ${SQL_CONSULTA_RECEITA_FEDERAL}
    [Return]    ${result}