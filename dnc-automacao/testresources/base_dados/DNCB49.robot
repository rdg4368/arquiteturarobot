*** Settings ***
Resource     ../../resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      ../utils/utilsMask.py  
Library      String  


*** Variables ***
# CT002
${SQL_SUSTAR_ESPECIFICO_SPC}
...    SELECT LPAD(C4900_CLTCGC,12,'0'), LPAD(C4900_CLTCGCDIG,2,'0'), C4900_CCSCOD, C4900_SISCODSIS, C4900_PRUCOD, C4900_OPVMOD 
...    FROM DNCB00.DNCB49 WHERE C4900_NSPSTAAUT = 'S' 
...    FETCH FIRST ROW ONLY 
    
${SQL_VALIDAR_SUSTAR_ESPECIFICO_SPC}
...    SELECT * FROM DNCB00.DNCB49 WHERE C4900_CLTCGC = {0} AND C4900_NSPSTAAUT = 'N' AND C4900_HDRDATA = {1}    

${SQL_EXCLUI_SUSTAR_ESPECIFICO_SPC}
...    UPDATE DNCB00.DNCB49 SET C4900_NSPSTAAUT = 'S' WHERE C4900_CLTCGC = {0} AND C4900_NSPSTAAUT = 'N' AND C4900_HDRDATA = {1} 

${SQL_EXCLUIR_SPC}
...    SELECT LPAD(C4900_CLTCGC,12,'0'), LPAD(C4900_CLTCGCDIG,2,'0'), C4900_CCSCOD, C4900_SISCODSIS, C4900_PRUCOD, C4900_OPVMOD 
...    FROM DNCB00.DNCB49 WHERE C4900_NSPSTACTR = '5' 
...    FETCH FIRST ROW ONLY 

${SQL_VALIDA_EXCLUIR_SPC}
...    SELECT * FROM DNCB00.DNCB49 WHERE C4900_CLTCGC = {0} AND C4900_HDRDATA = {1} AND C4900_NSPSTACTR = 0

${SQL_EXCLUI_INCLUSAO_EXCLUIR_SPC}
...    UPDATE DNCB00.DNCB49 SET C4900_NSPSTACTR = 5, C4900_NSPMOTBAIXA = 0 WHERE C4900_CLTCGC = {0} AND C4900_HDRDATA = {1} AND C4900_NSPSTACTR = 0

${SQL_REEENVIAR_SPC}
...    SELECT LPAD(C4900_CLTCGC,12,'0'), LPAD(C4900_CLTCGCDIG,2,'0'), C4900_CCSCOD, C4900_SISCODSIS, C4900_PRUCOD, C4900_OPVMOD 
...    FROM DNCB00.DNCB49 
...    FETCH FIRST ROW ONLY 
    
${SQL_VALIDA_REENVIAR_SPC}    
...    SELECT * FROM DNCB00.DNCB49 
...    WHERE C4900_CLTCGC = {0} 
...    AND C4900_HDRDATA = {1}
...    AND C4900_NSPSTACTR = 7
    
${SQL_EXCLUI_REENVIAR_SPC}
...    UPDATE DNCB00.DNCB49 SET C4900_NSPSTACTR = 1, 
...    C4900_ADDDATINC = 0, 
...    C4900_NSESTAAUT = 'N' 
...    WHERE C4900_CLTCGC = {0}
...    AND C4900_HDRDATA = {1} 
...    AND C4900_NSPSTACTR = 7

${SQL_ENVIO_SELETIVO}
...    SELECT LPAD(C4900_CLTCGC,12,'0'), LPAD(C4900_CLTCGCDIG,2,'0') FROM DNCB00.DNCB49 
...    WHERE C4900_NSPSTAAUT = 'N' AND C4900_NSPSTACTR IN (0,1,6,7,9) FETCH FIRST 1 ROW ONLY

${SQL_CANCELA_ENVIO_SPC}
...    SELECT C4900_CLTCGC, C4900_CLTCGCDIG, C4900_CCSCOD FROM DNCB00.DNCB49  
...    WHERE C4900_NSPSTACTR IN (9) FETCH FIRST 1 ROW ONLY

${SQL_REGULARIZAR_BAIXA}
...    SELECT LPAD(C4900_CLTCGC,12,'0'), LPAD(C4900_CLTCGCDIG,2,'0'), C4900_CCSCOD FROM DNCB00.DNCB49 
...    WHERE C4900_NSPSTACTR IN (6) AND C4900_NSPMOTBAIXA > 0 FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_REGULARIZAR_BAIXA}
...    SELECT * FROM DNCB00.DNCB49 WHERE C4900_CLTCGC = {0} AND C4900_CCSCOD = {1} AND C4900_HDRDATA = {2} AND C4900_NSPSTACTR = 2

${SQL_DESFAZER_REGULARIZAR_BAIXA}
...    UPDATE DNCB00.DNCB49 SET C4900_NSPSTACTR = 6 WHERE C4900_CLTCGC = {0} AND C4900_CCSCOD = {1} AND C4900_HDRDATA = {2} AND C4900_NSPSTACTR = 2

${SQL_INSERT_CANCELA_ENVIO_SPC}
...    INSERT INTO DNCB00.DNCB49 (C4900_HDRDATA,C4900_HDRHORA,C4900_HDRESTACAO,C4900_HDRPROGRAMA,STATGER,C4900_CLTCGC,
...    C4900_CLTCGCDIG,C4900_SPTDATOCOR,C4900_NSPSTACTR,C4900_CNSCODSOLIC,C4900_SPFVALPEND,C4900_SPFNOMINST,C4900_BDVTIPPES,
...    C4900_NSPMOTBAIXA,C4900_OPVMOD,C4900_SISCODSIS,C4900_PRUCOD,C4900_CCSCOD,C4900_CLTCOD,C4900_NSPTITULAR,C4900_CCSVAL,
...    C4900_CCSSLDDEV,C4900_DEPSEQ,C4900_DEPEMP,C4900_CCSDATINI,C4900_CCSDATATVAL,C4900_CCSDATFIM,C4900_CCSDATPRIPA,
...    C4900_NSPSTAAUT,C4900_NSPIDINCSPC,C4900_NSPCTNEGSPC  ,C4900_NSPCNSCGC,C4900_NSPCODPREMB,C4900_ADDDATINC,C4900_CCSVALPRISD,
...    C4900_NSPCODNAT) VALUES ('20200319','140106','300','DNCNHB','','183153801' ,'68','20151116','9','644971',3586.27,'BRB','F',
...    1,2,'ECC',26,06300041530026001,725902,'0',2800.00,3586.27,63,1,20000529,20151112,20151030,20151030,'S','N',06300041530026001,
...    100,'',20200108,3405.20,'EC')

${SQL_VALIDAR_CANCELA_ENVIO_SPC}
...    SELECT C4900_CLTCGC, C4900_CLTCGCDIG, C4900_CCSCOD FROM DNCB00.DNCB49  
...    WHERE C4900_NSPSTACTR IN (9) AND C4900_CLTCGC = {0} AND C4900_CCSCOD = {1} FETCH FIRST 1 ROW ONLY

${SQL_ENTRADA_NEGATIVAR_MANUAL}
...    SELECT R0900_OPVSLDCED, R0900_CCSDATPRIPA, C4900_CLTCGC, C4900_CLTCGCDIG, C4900_CCSCOD, C4900_SISCODSIS, C4900_PRUCOD, 
...    C4900_OPVMOD FROM DNCB00.DNCB49 INNER JOIN CTRB00.CTRB09 ON (R0900_CCSCOD = C4900_CCSCOD AND R0900_SISCODSIS = C4900_SISCODSIS AND R0900_PRUCOD = C4900_PRUCOD AND R0900_OPVMOD = C4900_OPVMOD)
...    INNER JOIN DNCB00.DNCB40 ON (C4000_PRUCOD = R0900_PRUCOD) INNER JOIN DNCB00.DNCB41 ON (C4100_CLTCGC = C4900_CLTCGC) 
...    INNER JOIN DNCB00.DNCB42 ON (C4200_CCSCOD = C4900_CCSCOD AND C4200_SISCODSIS = C4200_SISCODSIS AND C4200_PRUCOD = C4900_PRUCOD AND C4200_OPVMOD = C4900_OPVMOD) 
...    WHERE R0900_CCSDATPRIPA <= 20200323

${SQL_HISTORICO_NEGATIVACAO}
...    SELECT LPAD(C4900_CLTCGC, 12, '0'), LPAD(C4900_CLTCGCDIG, 2, '0'), LPAD(C4900_CLTCOD,8,'0') FROM DNCB00.DNCB49 FETCH FIRST ROW ONLY

${SQL_CONSULTA_HISTORICO_NEGATIVACAO}
...    SELECT TRIM(C4900_CCSCOD), TRIM(C4900_SISCODSIS), TRIM(C4900_PRUCOD), TRIM(C4900_OPVMOD) 
...    FROM DNCB00.DNCB49 WHERE C4900_CLTCGC = {0} FETCH FIRST 13 ROW ONLY 

${SQL_CONSULTA_HISTOCIO_INTERVENCOES}
...    SELECT TRIM(CA500_CLTCGC), TRIM(CA500_CLTCGCDIG), TRIM(CA500_CCSCOD) FROM DNCB00.DNCB49 
...    INNER JOIN DNCB00.DNCBA5 ON(CA500_CLTCGC = C4900_CLTCGC AND CA500_CCSCOD = C4900_CCSCOD AND CA500_SISCODSIS = C4900_SISCODSIS AND CA500_PRUCOD = C4900_PRUCOD AND CA500_OPVMOD = C4900_OPVMOD) 
...    WHERE CA500_NSPSTAENVIO > 10 FETCH FIRST ROW ONLY 

${SQL_VALIDA_HISTORICO_INTERVENCOES}
...    SELECT TRIM(CA500_CLTCGC), TRIM(CA500_CLTCGCDIG), TRIM(CA500_CCSCOD), TRIM(CA500_NSPDATENVIO), TRIM(CA500_NSPHORENVIO), TRIM(CA500_CNSCODSOLIC), TRIM(CA500_NSPSTAENVIO) 
...    FROM DNCB00.DNCB49 INNER JOIN DNCB00.DNCBA5 ON(CA500_CLTCGC = C4900_CLTCGC AND CA500_CCSCOD = C4900_CCSCOD AND CA500_SISCODSIS = C4900_SISCODSIS AND CA500_PRUCOD = C4900_PRUCOD AND CA500_OPVMOD = C4900_OPVMOD) 
...    WHERE C4900_CLTCGC = {0} AND CA500_CCSCOD = {1} AND CA500_NSPSTAENVIO > 10 FETCH FIRST 12 ROW ONLY    

${SQL_CONSULTA_OPERACOES_PERIODO}
...    SELECT (C2800_CRMDATENR - 10000) AS "Período Inicial", C2800_CRMDATENR AS "Período Final", SUBSTR(C2800_CRMNOMARQ , 6, 2) AS "Empresa" FROM 
...    DNCB00.DNCB28 WHERE SUBSTR(C2800_CRMNOMARQ , 1, 5) = 'SERS0' ORDER BY C2800_CRMDATENR DESC FETCH FIRST ROW ONLY

${SQL_DESFAZER_NEGATIVACAO_MANUAL}
...    DELETE FROM DNCB00.DNCB49 A WHERE C4900_CCSCOD = {0} AND C4900_HDRDATA = {1}

${SQL_VALIDA_CONSULTA_OPERACOES_PERIODO}
...    SELECT SUM(C2800_CRMQTDENV) , SUM(C2800_CRMQTDACE) , SUM(C2800_CRMQTDERR) FROM DNCB00.DNCB28 WHERE SUBSTR(C2800_CRMNOMARQ , 6, 2) = {0} AND SUBSTR(C2800_CRMNOMARQ , 1, 5) = 'SERS0' 
...    AND C2800_CRMDATENR BETWEEN {1} AND {2}
    
${SQL_SITUACAO_CONTRATO}    
...    SELECT CONCAT( LPAD(C4900_CLTCGC, 12, '0') , LPAD(C4900_CLTCGCDIG, 2, '0') ), C4900_CLTCOD FROM DNCB00.DNCB49 FETCH FIRST ROW ONLY

${SQL_VALIDAR_HISTORICO_NEGATIVACAO}
...    SELECT LPAD(C4900_CLTCGC, 12 , '0'), LPAD(C4900_CLTCGCDIG, 2 , '0'), C4900_CCSCOD, LPAD(C4900_SISCODSIS, 3 , '0'), LPAD(C4900_CLTCOD, 8 , '0'), TRIM(X0100_CLTNOM), LPAD(C4900_PRUCOD, 4 , '0'), LPAD(D0400_PRUNOM, 2 , '0'), LPAD(C4900_OPVMOD, 4 , '0'), C4900_NSESTACTR, C4900_NSESTAAUT, C4900_NSECTNEGSER, C4900_NCEIDINCSER, C4900_SPFVALPEND, LPAD(C4900_SPTDATOCOR, 8 , '0'), LPAD(C4900_CCSDATINI, 8 , '0'), LPAD(C4900_CCSDATFIM, 8, '0'), LPAD(C4900_CCSDATPRIPA, 8, '0'), LPAD(C4900_CCSDATATVAL, 8, '0') FROM DNCB00.DNCB49 INNER JOIN AOXB00.AOXB01 ON C4900_CLTCOD = X0100_CLTCOD INNER JOIN PODB00.PODB04 ON C4900_PRUCOD = D0400_PRUCOD 
...    WHERE C4900_CLTCOD = {0} FETCH FIRST ROW ONLY

${SQL_DADO_CONSULTA_REMESSA}
...    SELECT (C0200_CSSDATREM - 10000) AS "Período Inicial", C0200_CSSDATREM AS "Período Final" FROM DNCB00.DNCB02 ORDER BY C0200_CSSDATREM DESC FETCH FIRST ROW ONLY

${SQL_VALIDAAR_HISTORICO_SERASA_REMESSAS}
...    SELECT C0200_CSSDATREM, C0200_CSSSEQMOV, C0200_CSSDATPROC FROM DNCB00.DNCB02 WHERE C0200_CSSDATREM > {0} AND C0200_CSSDATREM < {1} ORDER BY C0200_CSSDATREM ASC FETCH FIRST 13 ROWS ONLY

${SQL_VALIDA_NEGATIVACAO_MANUAL}

*** Keywords ***   
# CT002
Recuperar Dados Entrada Sustar Especifico SPC
    ${result}      Query      ${SQL_SUSTAR_ESPECIFICO_SPC}
    [Return]       ${result}
    
Recuperar Dados Validacao Sustar Especifico SPC
    [Arguments]    ${cgc_cpf}  ${data_atual}    
    ${query}       Format String    ${SQL_VALIDAR_SUSTAR_ESPECIFICO_SPC}  ${cgc_cpf}  ${data_atual}  
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}         
    [Return]       ${result} 
    
Desfazer Dados Sustar Especifico SPC
    [Arguments]    ${cgc_cpf}  ${data_atual}
    ${query}       Format String    ${SQL_EXCLUI_SUSTAR_ESPECIFICO_SPC}  ${cgc_cpf}  ${data_atual}   
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result} 

#ct003
Recuperar Dados Entrada Excluir SPC
    ${result}      Query        ${SQL_EXCLUIR_SPC}
    [Return]       ${result}
    
Recuperar Dados Validacao Excluir SPC
    [Arguments]    ${cgc_cpf}  ${data_atual}
    ${query}       Format String    ${SQL_VALIDA_EXCLUIR_SPC}  ${cgc_cpf}  ${data_atual}    
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result}

Desfazer Dados Excluir SPC
    [Arguments]    ${cgc_cpf}  ${data_atual}
    ${query}       Format String    ${SQL_EXCLUI_INCLUSAO_EXCLUIR_SPC}   ${cgc_cpf}    ${data_atual}
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${result}
    
#CT004
Recuperar Dados Entrada Reenviar SPC
    ${result}      Query      ${SQL_REEENVIAR_SPC}
    [Return]       ${result}    
    
Recuperar Dados Validacao Reenviar SPC
    [Arguments]    ${cgc_cpf}  ${data_atual}
    ${query}       Format String     ${SQL_VALIDA_REENVIAR_SPC}  ${cgc_cpf}  ${data_atual}   
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${result}
    
Desfazer Dados Reenviar SPC
    [Arguments]    ${cgc_cpf}  ${data_atual}
    ${query}       Format String      ${SQL_EXCLUI_REENVIAR_SPC}    ${cgc_cpf}  ${data_atual}
    ${result}      Run Keyword And Return Status    Execute Sql String    ${query}
    [Return]       ${result}
    
#CT005
Recuperar Dados Entrada Envio Seletivo
    ${result}      Query      ${SQL_ENVIO_SELETIVO}
    [Return]       ${result}
    
#CT006
Incluir Dados Cancela Envio SPC
    ${result}      Execute Sql String   ${SQL_INSERT_CANCELA_ENVIO_SPC}
    [Return]       ${result}      
 
Recuperar Dados Entrada Cancela Envio SPC
    ${result}      Query      ${SQL_CANCELA_ENVIO_SPC}
    [Return]       ${result}
    
#CT007
Pesquisar Dados Cliente Regularizar Baixa
    ${result}      Query      ${SQL_REGULARIZAR_BAIXA}
    [Return]       ${result}

Recuperar Dados Cliente Validar Regularizar Baixa
    [Arguments]    ${cgc_cpf}  ${contrato}  ${data_atual}    
    ${query}       Format String    ${SQL_VALIDAR_REGULARIZAR_BAIXA}  ${cgc_cpf}  ${contrato}  ${data_atual}  
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}         
    [Return]       ${result} 

Desfazer Dados Regularizar Baixa
    [Arguments]    ${cgc_cpf}  ${contrato}  ${data_atual}    
    ${query}       Format String    ${SQL_DESFAZER_REGULARIZAR_BAIXA}  ${cgc_cpf}  ${contrato}  ${data_atual}  
    ${result}      Run Keyword And Return Status    Execute Sql String    ${query}         
    [Return]       ${result}

Recuperar Dados Entrada Negativacao Manual
    ${result}      Query        ${SQL_ENTRADA_NEGATIVAR_MANUAL}
    [Return]       ${result}
    
Recuperar Dados Valida Negativacao Manual
    [Arguments]    ${contrato}    ${data_atual}
    ${query}       Format String    ${SQL_VALIDA_NEGATIVACAO_MANUAL}    ${contrato}    ${data_atual}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query}
    [Return]       ${status}
    
Desfazer Dados Negativacao Manual
    [Arguments]    ${contrato}    ${data_atual}
    ${query}       Format String  ${SQL_DESFAZER_NEGATIVACAO_MANUAL}    ${contrato}    ${data_atual}
    ${result}      Query          ${query}
    [Return]       ${result}

Recuperar Dados Consulta Operacoes Periodo
    ${result}      Query           ${SQL_CONSULTA_OPERACOES_PERIODO}
    [Return]       ${result}

Validar Dados Consulta Operacoes Periodo
    [Arguments]    ${empresa}      ${periodoInicial}    ${periodoFinal}
    ${query}       Format String   ${SQL_VALIDA_CONSULTA_OPERACOES_PERIODO}       ${empresa}      ${periodoInicial}    ${periodoFinal}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query}
    [Return]       ${status}
    
Recuperar Dados Situacao do Contrato
    ${result}      Query    ${SQL_SITUACAO_CONTRATO}
    [Return]       ${result}
    
Recuperar Dados Historico Negativacao
    ${result}      Query    ${SQL_HISTORICO_NEGATIVACAO}
    [Return]       ${result}
    
Validar Dados Historico Negativacao
    [Arguments]    ${codCliente}
    ${query}       Format String   ${SQL_VALIDAR_HISTORICO_NEGATIVACAO}  ${codCliente}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query}
    [Return]       ${status}   

Validar Dados Operacao Historico Negativacao
    [Arguments]    ${cpf_cnpj}
    ${query}       Format String  ${SQL_CONSULTA_HISTORICO_NEGATIVACAO}  ${cpf_cnpj}
    ${result}      Query   ${query}
    [Return]       ${result}    
    
Recuperar Dados Controle das Remessas
    ${result}      Query    ${SQL_DADO_CONSULTA_REMESSA}
    [Return]       ${result}
    
Validar Dados Historico Serasa Remessas 
    [Arguments]    ${periodoInicial}    ${periodoFinal} 
    ${query}       Format String   ${SQL_VALIDAAR_HISTORICO_SERASA_REMESSAS}  ${periodoInicial}    ${periodoFinal}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query}
    [Return]       ${status}   
    
Recuperar Dados Historico Intervencoes
    ${result}      Query            ${SQL_CONSULTA_HISTOCIO_INTERVENCOES}
    [Return]       ${result}
    
Validar Dados Historico Intervencoes
    [Arguments]    ${cpf_cnpj}    ${codContrato}
    ${query}       Format String  ${SQL_VALIDA_HISTORICO_INTERVENCOES}  ${cpf_cnpj}    ${codContrato}
    ${result}      Query  ${query}
    [Return]       ${result}
    
Recuperar Dados Validacao do Cancelamento do Envio SPC  
    [Arguments]    ${dados_cliente}
    ${result}      Format String       ${SQL_VALIDAR_CANCELA_ENVIO_SPC}   ${dados_cliente}[0][0]  ${dados_cliente}[0][2]
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${result}
    [Return]       ${status}