*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      Collections       


*** Variables ***
${SQL_CLIENTE_SERASA}     SELECT TRIM(C1100_CNSCGC), TRIM(C1100_CNSCGCDIG), TRIM(C1100_CNSDATCONS) 
...                       FROM DNCB00.DNCB11 ORDER BY C1100_HDRDATA DESC FETCH FIRST 1 ROWS ONLY

*** Keywords ***
Consultar Cliente Serasa
    ${result}    Query    ${SQL_CLIENTE_SERASA}
    [Return]    ${result}

Validar Exlusao Serasa
    [Arguments]    ${cpf}    ${digito}    ${data}
    ${result}    Query   
     ...   SELECT * FROM DNCB00.DNCB11 INNER JOIN DNCB00.DNCB09 ON (C0900_SRECGC = C1100_CNSCGC ) WHERE C1100_CNSCGC = ${cpf} AND C1100_CNSCGCDIG = ${digito} AND C1100_CNSDATCONS = ${data} FETCH FIRST 1 ROWS ONLY
    ${status}    Run Keyword And Return Status   Check If Exists In Database    ${result}
    [Return]    ${status}
    