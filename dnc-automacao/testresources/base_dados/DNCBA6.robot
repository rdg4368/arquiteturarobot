*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String       

*** Variables ***
${SQL_COD_PRODUTO_SPC}                                  SELECT LPAD(CA600_PRUCOD,4,'0') FROM DNCB00.DNCBA6 WHERE CA600_GRRREGSIT = '1' AND CA600_PRUCOD <> 0 ORDER BY CA600_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_COD_PRODUTO_SPC_ALTERAR}                          SELECT LPAD(CA600_PRUCOD,4,'0') FROM DNCB00.DNCBA6 WHERE  CA600_PRUCOD <> 0 ORDER BY CA600_HDRDATA DESC FETCH FIRST ROW ONLY 

${SQL_VALIDAR_ALTERAR_PARAMETRO_PRODUTO}                SELECT * FROM DNCB00.DNCBA6 WHERE CA600_PRUCOD = '{0}' AND CA600_HDRPROGRAMA = 'DNCNIG' AND CA600_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYMMDD')

${SQL_DESFAZER_ALTERAR_PARAMETRO_PRODUTO}               DELETE FROM DNCB00.DNCBA6 WHERE CA600_PRUCOD = '{0}' AND CA600_HDRPROGRAMA = 'DNCNIG' AND CA600_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYMMDD') AND CA600_GRRREGSIT = '2'

${SQL_VALIDAR_CONSULTA_PARAMETRO_PRODUTO}               SELECT LPAD(CA600_PRUCOD,4,'0'), LPAD(CA600_RPNQTDDIACA,4,'0'), LPAD(CA600_RPNQTDDIAEN,4,'0'),SUBSTR(LPAD(CA600_DNCDATAOCOR,8,'0'),7,2)||'/'||
...                                                     SUBSTR(LPAD(CA600_DNCDATAOCOR,8,'0'),5,2)||'/'||SUBSTR(LPAD(CA600_DNCDATAOCOR,8,'0'),1,4) ,
...                                                     SUBSTR(LPAD(CA600_DNCHORAOCOR,6,'0'),1,2)||':'||SUBSTR(LPAD(CA600_DNCHORAOCOR,6,'0'),3,2)||':'||SUBSTR(LPAD(CA600_DNCHORAOCOR,6,'0'),5,2)
...                                                     FROM DNCB00.DNCBA6 WHERE CA600_PRUCOD = {0} ORDER BY CA600_DNCDATAOCOR DESC FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_HISTORICO_PARAMETRO_PROD}        SELECT LPAD(CA600_PRUCOD,4,'0'), LPAD(CA600_RPNQTDDIACA,4,'0'), LPAD(CA600_RPNQTDDIAEN,4,'0'),SUBSTR(LPAD(CA600_DNCDATAOCOR,8,'0'),7,2)||'/'||
...                                                     SUBSTR(LPAD(CA600_DNCDATAOCOR,8,'0'),5,2)||'/'||SUBSTR(LPAD(CA600_DNCDATAOCOR,8,'0'),1,4) ,
...                                                     SUBSTR(LPAD(CA600_DNCHORAOCOR,6,'0'),1,2)||':'||SUBSTR(LPAD(CA600_DNCHORAOCOR,6,'0'),3,2)||':'||SUBSTR(LPAD(CA600_DNCHORAOCOR,6,'0'),5,2)
...                                                     FROM DNCB00.DNCBA6  ORDER BY CA600_PRUCOD ASC,CA600_DNCDATAOCOR ASC,CA600_DNCHORAOCOR ASC  FETCH FIRST 13 ROWS ONLY

*** Keywords ***
Recuperar Codigo Produto SPC
    ${result}    Query    ${SQL_COD_PRODUTO_SPC}
    [Return]     ${result}[0][0]
    
Recuperar Codigo Produto SPC Alterar Parametro
    ${result}    Query    ${SQL_COD_PRODUTO_SPC}
    [Return]     ${result}[0][0]


Validar Alteracao Parametro Por Produto
    [Arguments]    ${codProd}
    ${query}       Format String    ${SQL_VALIDAR_ALTERAR_PARAMETRO_PRODUTO}       ${codProd}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${status}
    
Desfazer Alteracao Parametro Por Produto
    [Arguments]    ${codProd}
    ${query}       Format String    ${SQL_DESFAZER_ALTERAR_PARAMETRO_PRODUTO}       ${codProd}
    ${status}      Run Keyword And Return Status    Execute Sql String              ${query}        
    [Return]       ${status}
    
Validar Consulta Parametro Spc Por Produto
    [Arguments]    ${codProd}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_PARAMETRO_PRODUTO}       ${codProd}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]
    
Validar Consulta Historico Parametro Produto
    ${result}     Query    ${SQL_VALIDAR_CONSULTA_HISTORICO_PARAMETRO_PROD}   
    [Return]      ${result} 