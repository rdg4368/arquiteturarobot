*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem   
Library      String      
Library      Collections    

*** Variables ***
${SQL_VALIDAR_ALTERAR_PARAMETRO}                
...    SELECT TRIM(SUBSTR(G021_ITEM,20,23)) FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '01' AND G021_ARQUIVO = '01' FETCH FIRST ROW ONLY


${SQL_RECUPERAR_DADOS_PARAMETRO}               
...     SELECT TRIM(G021_ITEM) FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '01' AND G021_ARQUIVO = '01'

${SQL_DESFAZER_ALTERACAO_PARAMETROS}           
...     UPDATE DNCB00.SBGB02 SET G021_ITEM = '{0}' WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '01' AND G021_ARQUIVO = '01';

${SQL_VALIDAR_INCLUSAO_PARAMETROS}              
...    SELECT * FROM DNCB00.SBGB02 WHERE G021_COMPL = '{0}' AND G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05'

${SQL_DADOS_ROLLBACK_ALTERAR_PARAMETRO}         
...    SELECT TRIM (G021_ITEM) FROM DNCB00.SBGB02 WHERE G021_COMPL = '{0}' AND G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05'

${SQL_VALIDAR_ALTERAR_PARAMETRO_VL}             
...    SELECT TRIM(G021_ITEM) FROM DNCB00.SBGB02 WHERE G021_COMPL = '{0}' AND G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05'

${SQL_DESFAZER_ALTERAR_PARAM_VL}                
...    UPDATE DNCB00.SBGB02 SET G021_ITEM = '{0}' WHERE G021_COMPL = '{1}' AND G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05'

${SQL_VALIDAR_CONSULTA_PARAM_VL}                
...    SELECT SUBSTR((G021_ITEM),3,3) FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05' FETCH FIRST 15 ROW ONLY

${SQL_DADOS_ROLLBACK_EXCLUIR}                   
...    SELECT TRIM(G021_HDRDATA),TRIM(G021_HDRHORA), TRIM(G021_HDRESTACAO), TRIM(G021_HDRSTATGER), TRIM(G021_SISTEMA), TRIM(G021_SUBSIST), TRIM(G021_ARQUIVO), TRIM(G021_REGISTRO),
...    TRIM(G021_COMPL), TRIM(G021_COMPL2) ,TRIM(G021_SENHA), TRIM(G021_TAMREG), TRIM(G021_ITEM) FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05' AND G021_COMPL = '15' FETCH FIRST 15 ROW ONLY

${SQL_VALIDAR_EXCLUSAO_PARAM_VAL}              
...    SELECT * FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '05' AND G021_COMPL = '{0}' FETCH FIRST 15 ROW ONLY

${SQL_DESFAZER_EXCLUSAO_PARAM_VAL}              
...    INSERT INTO DNCB00.SBGB02 (G021_HDRDATA,G021_HDRHORA, G021_HDRESTACAO, G021_HDRSTATGER, G021_SISTEMA, G021_SUBSIST, G021_ARQUIVO, G021_REGISTRO, G021_COMPL, G021_COMPL2 ,G021_SENHA, G021_TAMREG, G021_ITEM)
...    VALUES ('{0}','{1}', '{2}','{3}','{4}','{5}','{6}','{7}', '{8}','{9}','{10}','{11}','{12}')

${SQL_VALIDAR_ALTERAR_PARAMETRO_SPC}            
...    SELECT * FROM DNCB00.SBGB02 WHERE SUBSTR(G021_ITEM,47,15) = '{0}' AND  SUBSTR(G021_ITEM,62,15) = '{1}' AND G021_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD' ) 
...    AND G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = 09 AND G021_REGISTRO = 01

${SQL_DESFAZER_ALTERAR_PARAMETRO_SPC}           
...    DELETE FROM DNCB00.SBGB02 WHERE SUBSTR(G021_ITEM,47,15) = '{0}' AND  SUBSTR(G021_ITEM,62,15) = '{1}' AND G021_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD' ) 
...    AND G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = 09 AND G021_REGISTRO = 01

${SQL_VALIDAR_CONSULTA_PARAMETRO_SPC}           
...    SELECT  SUBSTR(LPAD(G021_HDRDATA,8,'0'),7,2)||'/'||SUBSTR(LPAD(G021_HDRDATA,8,'0'),5,2)||'/'||SUBSTR(LPAD(G021_HDRDATA,8,'0'),1,4) AS DATA_CADAST
...    ,SUBSTR(LPAD(G021_HDRHORA,6,'0'),1,2)||':'||SUBSTR(LPAD(G021_HDRHORA,6,'0'),3,2)||':'||SUBSTR(LPAD(G021_HDRHORA,6,'0'),5,2) AS HR_CADAST, SUBSTR(G021_ITEM,7,4), SUBSTR(G021_ITEM,11,4),
...    SUBSTR(G021_ITEM,15,4), SUBSTR(G021_ITEM,19,4), SUBSTR(G021_ITEM,23,4), SUBSTR(G021_ITEM,77,3), SUBSTR(G021_ITEM,80,3), SUBSTR(G021_ITEM,83,3), SUBSTR(G021_ITEM,86,3) 
...    FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = 09 AND G021_REGISTRO = 01 ORDER BY G021_HDRDATA DESC , G021_HDRHORA DESC FETCH FIRST ROW ONLY

${SQL_VALIDAR_ALTERAR_PARAMETRO_NEGATIVASAO_SERASA}
...    SELECT * FROM DNCB00.SBGB02 WHERE G021_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AND G021_TAMREG = 91 AND SUBSTR(G021_ITEM, '7',    '4') = {0} AND SUBSTR(G021_ITEM,'47','15') = {1}

${SQL_DESFAZER_ALTERAR_PARAMETRO_NEGATIVASAO_SERASA}
...   DELETE FROM DNCB00.SBGB02 WHERE G021_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AND G021_TAMREG = 91 AND SUBSTR(G021_ITEM, '7',    '4') = {0} AND SUBSTR(G021_ITEM,'47','15') = {1}
 
${SQL_VALIDAR_CONSULTA_PARAMETROS_NEGATIVACAO_SERASA}
...    SELECT     SUBSTR(G021_ITEM,     '7',     '4')AS PRAZO_MAXIMO,     SUBSTR(G021_ITEM,     '11',     '4' ) AS PRAZO_MINIMO_PJ,     SUBSTR(G021_ITEM,     '15',     '4' ) AS PRAZO_MINIMO_PF,    
...    SUBSTR(G021_ITEM,'19','4') AS PRAZO_MINIMO_PF_GBD,SUBSTR(G021_ITEM,'23','4') AS PRAZO_MINIMO_PF_BRB,SUBSTR(G021_ITEM,'27','4') AS PRAZO_SERASA_SELECAO,    
...    SUBSTR(G021_ITEM,'31','4') AS PRAZO_SERASA_PJ,SUBSTR(G021_ITEM,'35','4') AS PRAZO_SERASA_PF,SUBSTR(G021_ITEM,'39','4') AS PRAZO_SERASA_PF_GDF,    
...    SUBSTR(G021_ITEM,'43','4') AS PRAZO_SERASA_PF_BRB, SUBSTR(G021_ITEM,'62','3') AS SISTEMA_NAO_SEL_AUTOMATICO,   
...    SUBSTR(G021_ITEM,'65','3') AS SISTEMA_NAO_SEL_AUTOMATICO1,SUBSTR(G021_ITEM,'68','3') AS SISTEMA_NAO_SEL_AUTOMATICO2,SUBSTR(G021_ITEM,'71','3') AS SISTEMA_NAO_SEL_AUTOMATICO3,  
...    SUBSTR(G021_ITEM,'74','3') AS SISTEMA_NAO_SEL_AUTOMATICO4,SUBSTR(G021_ITEM,'77','3') AS SISTEMA_NAO_CTR_INTEGR_MANUAL,SUBSTR(G021_ITEM,'80','3') AS SISTEMA_NAO_CTR_INTEGR_MANUAL1,   
...    SUBSTR(G021_ITEM,'83','3') AS SISTEMA_NAO_CTR_INTEGR_MANUAL2,SUBSTR(G021_ITEM,'86','3') AS SISTEMA_NAO_CTR_INTEGR_MANUAL3,SUBSTR(G021_ITEM,'89','3') AS SISTEMA_NAO_CTR_INTEGR_MANUAL4    
...    FROM     DNCB00.SBGB02 WHERE      G021_TAMREG = 91  ORDER BY     G021_HDRDATA DESC ,G021_HDRHORA DESC  FETCH FIRST 1 ROW ONLY  


*** Keywords ***
Validar Alteracao De Parametros
    ${result}    Query    ${SQL_VALIDAR_ALTERAR_PARAMETRO}    
    [Return]    ${result}[0][0]
    
Recuperar Dado Parametro Para Rollback
    ${result}    Query    ${SQL_RECUPERAR_DADOS_PARAMETRO}    
    [Return]     ${result}[0][0]
    
Desfazer Alteracao De Parametros
    [Arguments]    ${dadoRollback}
    ${query}       Format String    ${SQL_DESFAZER_ALTERACAO_PARAMETROS}    ${dadoRollback}
    ${status}      Run Keyword And Return Status    Execute Sql String      ${query}
    [Return]       ${status}
    
Validar Consulta Parametros
    ${result}    Query    ${SQL_RECUPERAR_DADOS_PARAMETRO}    
    ${validadePJ}            Get Substring    ${result}[0][0]    01    04
    ${validadePFG}           Get Substring    ${result}[0][0]    05    08
    ${validadePFGDF}         Get Substring    ${result}[0][0]    09    12
    ${validadePFBRB}         Get Substring    ${result}[0][0]    13    16
    ${pesquisaImpont}        Get Substring    ${result}[0][0]    16    19
    ${validadePesqSPCCDL}    Get Substring    ${result}[0][0]    19    23
    [Return]     ${validadePJ}  ${validadePFG}    ${validadePFGDF}    ${validadePFBRB}     ${pesquisaImpont}      ${validadePesqSPCCDL}    
    
Validar Inclusao Parametros Validade
    [Arguments]    ${cod}
    ${query}    Format String    ${SQL_VALIDAR_INCLUSAO_PARAMETROS}    ${cod}
    ${status}   Run Keyword And Return Status    Check If Exists In Database    ${query} 
    [Return]    ${status}
    
Recuperar Dados Alterar Parametros De Validade
    [Arguments]    ${cod}
    ${query}      Format String   ${SQL_DADOS_ROLLBACK_ALTERAR_PARAMETRO}         ${cod}
    ${result}     Query           ${query}    
    [Return]      ${result}[0][0]
    
Validar Alterar Parametro Validade
    [Arguments]    ${cod}
    ${query}       Format String    ${SQL_VALIDAR_ALTERAR_PARAMETRO_VL}    ${cod}
    ${result}      Query            ${query}    
    [Return]       ${result}[0][0]
    
Desfazer Alterar Parametros De Validade
    [Arguments]    ${rollback}   ${cod}    
    ${query}      Format String    ${SQL_DESFAZER_ALTERAR_PARAM_VL}       ${rollback}    ${cod}   
    ${status}     Run Keyword And Return Status    Execute Sql String     ${query}      
    [Return]      ${status}  
    
Validar Consulta Parametros Validade
    ${result}    Query    ${SQL_VALIDAR_CONSULTA_PARAM_VL}   
    [Return]     ${result} 
    
Recuperar Dados Exluir Parametros Validade
    ${result}    Query    ${SQL_DADOS_ROLLBACK_EXCLUIR}
    [Return]    ${result}[0]
    
Validar Excluir Parametros Validade
    [Arguments]   ${cod}
    ${query}      Format String    ${SQL_VALIDAR_EXCLUSAO_PARAM_VAL}    ${cod}
    ${status}     Run Keyword And Return Status    Check If Not Exists In Database    ${query}    
    [Return]      ${status}
    
Desfazer Excluir Parametros Validade
    [Arguments]    ${rollback}
    ${query}       Format String    ${SQL_DESFAZER_EXCLUSAO_PARAM_VAL}    ${rollback}[0]    ${rollback}[1]    ${rollback}[2]    ${rollback}[3]     ${rollback}[4]      ${rollback}[5]    ${rollback}[6]
    ...                                                                   ${rollback}[7]    ${rollback}[8]    ${rollback}[9]    ${rollback}[10]    ${rollback}[11]     ${rollback}[12]
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}            
    [Return]       ${status}
    
Validar Alteracao Parametro Spc
    [Arguments]    ${cargaOpVencMin}    ${cargaOpVencMax} 
    ${query}       Format String    ${SQL_VALIDAR_ALTERAR_PARAMETRO_SPC}           ${cargaOpVencMin}    ${cargaOpVencMax}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${status}

Desfazer Alteracao Parametro Spc
    [Arguments]    ${cargaOpVencMin}    ${cargaOpVencMax} 
    ${query}       Format String        ${SQL_DESFAZER_ALTERAR_PARAMETRO_SPC}           ${cargaOpVencMin}    ${cargaOpVencMax}
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]       ${status}
    
Validar Consulta Parametro Spc
    ${result}    Query    ${SQL_VALIDAR_CONSULTA_PARAMETRO_SPC}    
    [Return]     ${result}[0]
    
Validar Alteracao De Parametros Negativacao Serasa
    [Arguments]     ${prazoMaxAtraso}    ${vlrCargaOpVenc}
     ${query}       Format String       ${SQL_VALIDAR_ALTERAR_PARAMETRO_NEGATIVASAO_SERASA}    ${prazoMaxAtraso}    ${vlrCargaOpVenc}
     ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}         
     [Return]       ${status}
     
Desfazer Alteracao De Parametros Negativacao Serasa
     [Arguments]     ${prazoMaxAtraso}    ${vlrCargaOpVenc}
     ${query}        Format String       ${SQL_DESFAZER_ALTERAR_PARAMETRO_NEGATIVASAO_SERASA}    ${prazoMaxAtraso}    ${vlrCargaOpVenc}
     ${status}       Run Keyword And Return Status    Execute Sql String    ${query}         
     [Return]        ${status}
     
Validar Consulta Parametro Negativacao Serasa
    ${result}      Query    ${SQL_VALIDAR_CONSULTA_PARAMETROS_NEGATIVACAO_SERASA}
    [Return]       ${result}[0]