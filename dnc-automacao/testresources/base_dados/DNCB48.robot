*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
   

*** Keywords ***
Excluir Dados Advertencia Referencia DNCB48
    [Arguments]     ${cpf_cpnj}  ${cpf_cpnj_digito}  ${data} 
    ${result}      Run Keyword And Return Status  Execute Sql String     
    ...  DELETE FROM DNCB00.DNCB48 WHERE C4800_CNSCGC = ${cpf_cpnj} AND C4800_CNSCGCDIG = ${cpf_cpnj_digito} AND C4800_CNSDATCONS = ${data}
    [Return]        ${result}   