*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections      
Library      String    

*** Variables ***
${SQL_ACESSO_CONSULTAR_ORGAOS}    SELECT TRIM(C3200_PORCODORG) FROM DNCB00.DNCB32 WHERE C3200_CLTCGC <> 0 AND C3200_POREMAIL LIKE '%@%' AND C3200_PORNUMERO <> 0 AND C3200_PORCEP <> 0 AND C3200_PORCODORSUP <> 0 AND C3200_PORCOMPLEM != '' ORDER BY C3200_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_ORGAOS}    SELECT LPAD(C3200_CLTCGC , 12 , '0'), LPAD(C3200_CLTCGCDIG , 2 , '0'), LPAD(C3200_PORCODORG , 5 , '0'), TRIM(C3200_PORNOMORG), LPAD(C3200_PORCODORSUP , 5 , '0'),
...                               TRIM(C3200_PORSIGORSUP) FROM DNCB00.DNCB32 WHERE C3200_PORCODORG = '{0}'
*** Keywords ***
Recuperar Dados Acesso Consultar Orgao
    ${result}    Query    ${SQL_ACESSO_CONSULTAR_ORGAOS}    
    [Return]     ${result}[0][0]
    
Validar Consulta Orgaos
    [Arguments]    ${orgao}
    ${query}      Format String    ${SQL_VALIDAR_CONSULTA_ORGAOS}    ${orgao}
    ${result}     Query            ${query}    
    [Return]      ${result}[0]