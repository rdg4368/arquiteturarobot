*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String  


*** Variables ***
# PACOTE 11
# 001_negativacao_serasa
${SQL_NEGATIVAR_MANUAL_CGC_CPF}
...    SELECT LPAD(X0100_CLTCGC,12,'0')|| 
...    LPAD(X0100_CLTCGCDIG,2,'0') 
...    FROM CTRB00.CTRB09 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD = R0900_CLTCOD) 
...    WHERE R0900_CCSSITDIASL IN ('A','P') AND R0900_CCSTIPCONT <> 1 FETCH FIRST 1 ROW ONLY

${SQL_VALIDA_INCLUSAO_NEGATIVAR_MANUAL_CGC_CPF}
...    SELECT * FROM DNCB00.DNCB37 A WHERE 
...    C3700_CCSCOD = {0}  AND 
...    C3700_HDRDATA = {1}

${SQL_EXCLUI_INCLUSAO_NEGATIVAR_MANUAL_CGC_CPF}
...    DELETE FROM DNCB00.DNCB37 A WHERE C3700_CCSCOD = {0}  
...    AND C3700_HDRDATA = {1}

# CT002
${SQL_SUSTAR_ESPECIFICO}
...    SELECT LPAD(C3700_CLTCGC,12,'0')|| 
...    LPAD(C3700_CLTCGCDIG,2,'0') 
...    FROM DNCB00.DNCB37 
...    FETCH FIRST 1 ROW ONLY

${SQL_RECUPERA_SUSTAR_ESPECIFICO}
...    SELECT * FROM DNCB00.DNCB37 WHERE C3700_CLTCGC = {0} AND C3700_CLTCGCDIG = {1}
...    AND C3700_NSESTAAUT = 'N' AND C3700_HDRDATA = {2}

${SQL_EXCLUI_SUSTAR_ESPECIFICO}
...    UPDATE DNCB00.DNCB37 SET C3700_NSESTAAUT = 'S' 
...    WHERE C3700_CLTCGC = {0} 
...    AND C3700_HDRDATA = {1}

# CT003
${SQL_EXCLUIR_SERASA}
...    SELECT LPAD(C3700_CLTCGC,12,'0')|| 
...    LPAD(C3700_CLTCGCDIG,2,'0') 
...    FROM DNCB00.DNCB37 WHERE C3700_NSESTACTR = '5' 
...    FETCH FIRST 1 ROW ONLY

${SQL_VALIDA_EXCLUIR_SERASA}
...    SELECT * FROM DNCB00.DNCB37 
...    WHERE C3700_CLTCGC = {0} AND C3700_CLTCGCDIG = {1}
...    AND C3700_HDRDATA = {2} 
...    AND C3700_NSESTACTR = 0

${SQL_EXCLUI_INCLUSAO_EXCLUIR_SERASA}
...    UPDATE DNCB00.DNCB37 SET C3700_NSESTACTR = 5, 
...    C3700_NSEMOTBAIXA = 0 
...    WHERE C3700_CLTCGC = {0} AND C3700_CLTCGCDIG = {1} 
...    AND C3700_HDRDATA = {2} 
...    AND C3700_NSESTACTR = 0

# CT004
${SQL_REENVIAR_SERASA}
...    SELECT DISTINCT LPAD(C3700_CLTCGC,12,'0')||
...    LPAD(C3700_CLTCGCDIG,2,'0') 
...    FROM DNCB00.DNCB37 WHERE C3700_NSESTACTR <> 7 AND C3700_NSESTACTR IN (1,6,9) 
...    FETCH FIRST 1 ROW ONLY

${SQL_VALIDA_REENVIAR_SERASA}
...    SELECT * FROM DNCB00.DNCB37 
...    WHERE C3700_CLTCGC = {0} 
...    AND C3700_HDRDATA = {1}
...    AND C3700_NSESTACTR = 7

${SQL_EXCLUI_REENVIAR_SERASA}
...    UPDATE DNCB00.DNCB37 SET C3700_NSESTACTR = 1, 
...    C3700_ADDDATINC = 0, 
...    C3700_NSESTAAUT = 'N' 
...    WHERE C3700_CLTCGC = {0}
...    AND C3700_HDRDATA = {1} 
...    AND C3700_NSESTACTR = 7

${SQL_EXCLUI_SUSTAR_ENVIO_SELETIVO}
...    UPDATE DNCB00.DNCB37 SET C3700_NSESTAAUT = 'S' 
...    WHERE C3700_CLTCGC = {0} 
...    AND C3700_HDRDATA = {1} 
...    AND C3700_NSESTAAUT = 'N'

# CT006
${SQL_CANCELAR_ENVIO}
...    SELECT LPAD(C3700_CLTCGC ,12,'0'), 
...    LPAD(C3700_CLTCGCDIG ,2,'0') 
...    FROM DNCB00.DNCB37 FETCH FIRST ROW ONLY

${SQL_VALIDA_CANCELAR_ENVIO}
...    SELECT LPAD(C3700_CLTCGC,12 , '0'), 
...    LPAD(C3700_CLTCGCDIG,    2 , '0'), 
...    TRIM(C3700_CCSCOD), 
...    LPAD(C3700_DEPSEQ,       4 , '0'), 
...    LPAD(C3700_CLTCOD,       8 , '0'), 
...    TRIM(X0100_CLTNOM), 
...    LPAD(C3700_SISCODSIS,    3 , '0'), 
...    LPAD(C3700_PRUCOD,       4 , '0'), 
...    LPAD(D0400_PRUNOM,       2 , '0'), 
...    LPAD(C3700_OPVMOD,       4 , '0'), 
...    LPAD(C3700_SPFCODNAT,    3 , '0'), 
...    C3700_SPFVALPEND, 
...    LPAD(C3700_ADDDATINC,    8 , '0'), 
...    LPAD(C3700_SPTDATOCOR,   8 , '0'), 
...    LPAD(C3700_CCSDATINI,    8 , '0'), 
...    LPAD(C3700_CCSDATFIM,    8 , '0'), 
...    LPAD(C3700_CCSDATPRIPA,  8 , '0'), 
...    LPAD(C3700_CCSDATATVAL,   8 , '0') 
...    FROM DNCB00.DNCB37 INNER JOIN AOXB00.AOXB01 ON C3700_CLTCOD = X0100_CLTCOD 
...    INNER JOIN PODB00.PODB04 ON C3700_PRUCOD = D0400_PRUCOD WHERE C3700_CCSCOD = '{0}'

# CT008
${SQL_NEGATIVAR_MANUAL}
...    SELECT TRIM(C3700_CCSCOD), 
...    TRIM(C3700_SISCODSIS) 
...    FROM DNCB00.DNCB37 
...    FETCH FIRST 1 ROW ONLY

${SQL_VALIDA_NEGATIVAR_MANUAL}
...    SELECT * FROM DNCB00.DNCB37 A WHERE C3700_CCSCOD = {0}  
...    AND C3700_HDRDATA = {1}

${SQL_EXCLUI_NEGATIVAR_MANUAL}
...    DELETE FROM DNCB00.DNCB37 A WHERE C3700_CCSCOD = {0}  
...    AND C3700_HDRDATA = {1}

${SQL_QUANTITATIVO_NEGATIVOS}
...    SELECT TRIM(C2800_CRMDATENR - 10000) AS "Período Inicial", 
...    TRIM(C2800_CRMDATENR) AS "Período Final", 
...    SUBSTR(C2800_CRMNOMARQ ,    6,    2) AS "Empresa" 
...    FROM DNCB00.DNCB28 WHERE SUBSTR(C2800_CRMNOMARQ ,    1,    5) = 'SERS0' 
...    ORDER BY C2800_CRMDATENR DESC FETCH FIRST ROW ONLY

# PACOTE 11
# 002_bloqueio_judicial
${SQL_INC_ATU_CLIENTE}
...    SELECT LPAD(X0100_CLTCGC ,12 ,'0') AS CPF, 
...    LPAD(X0100_CLTCGCDIG ,2 ,'0') AS DIGITO, 
...    LPAD(R0900_CLTCOD ,8 ,'0') AS CLTCOD 
...    FROM CTRB00.CTRB09 INNER JOIN AOXB00.AOXB01 
...    ON R0900_CLTCOD = X0100_CLTCOD FETCH FIRST ROW ONLY

${SQL_VALIDA_INC_ATU_CLIENTE}
...    SELECT LPAD(R0900_CLTCOD ,8,'0'), 
...    LPAD(X0100_CLTNOM ,33,'0'),
...    LPAD(X0100_CLTCGC ,12 ,'0'), 
...    LPAD(X0100_CLTCGCDIG ,2 ,'0'), 
...    LPAD(R0900_CCSCOD ,17,'0') , 
...    LPAD(R0900_SISCODSIS ,3,'0'), 
...    LPAD(R0900_PRUCOD ,4 ,'0'), 
...    LPAD(R0900_OPVMOD ,4 ,'0') 
...    FROM CTRB00.CTRB09 INNER JOIN AOXB00.AOXB01 
...    ON R0900_CLTCOD = X0100_CLTCOD 
...    WHERE R0900_CLTCOD = {0} FETCH FIRST 12 ROWS ONLY

${SQL_CANCELA_CLIENTE}
...    SELECT LPAD(C4100_CLTCGC,12 ,'0'), 
...    LPAD(C4100_CLTCGCDIG,2 ,'0') 
...    FROM DNCB00.DNCB41 
...    INNER JOIN DNCB00.DNCB37 
...    ON CONCAT(C3700_CLTCGC,C3700_CLTCGCDIG) = CONCAT(C4100_CLTCGC,C4100_CLTCGCDIG) 
...    WHERE C4100_CLTCGC NOT IN (SELECT C4100_CLTCGC FROM DNCB00.DNCB41 WHERE C4100_CBJSTATUS = 2) AND C4100_CBJSTATUS <> 2 AND C3700_NSESTACTR = 8 FETCH FIRST ROW ONLY

${SQL_VALIDA_CANCELA_CLIENTE}
...    SELECT LPAD(C4100_CLTCGC,12,'0'), 
...    LPAD(C4100_CLTCGCDIG,2,'0'), 
...    TRIM(LPAD(C4100_CBJDATINC,8,'0')), 
...    TRIM(LPAD(C4100_CBJHORINC,6,'0')), 
...    TRIM(C4100_CBJSTATUS), 
...    TRIM(LPAD(C4100_CBJMOTIVO,2,'0')), 
...    LPAD(C4100_CBJDATMOT,8,'0'), 
...    LPAD(C4100_CBJNUMPRO,20,'0'), 
...    TRIM(C4100_CBJCAMOBS) 
...    FROM DNCB00.DNCB41 
...    WHERE C4100_CLTCGC = {0} AND C4100_CLTCGCDIG = {1}

${SQL_VALIDA STATUS_CANCELA_CLIENTE}
...    SELECT C4100_CBJSTATUS, 
...    CASE WHEN C4100_CBJSTATUS = 2 
...    THEN 'True' ELSE 'False' END AS Resultado FROM DNCB00.DNCB41 
...    WHERE C4100_CLTCGC = {0} 
...    AND C4100_CLTCGCDIG = {1} ORDER BY SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_CONSULTA_BLOQUEIO_JUDICIAL}
...    SELECT LPAD(C4100_CLTCGC,12 ,'0'), 
...    LPAD(C4100_CLTCGCDIG,2 ,'0') 
...    FROM DNCB00.DNCB41 FETCH FIRST ROW ONLY

${SQL_VALIDA_CONSULTA_BLOQUEIO_JUDICIAL}
...    SELECT LPAD(C4100_CLTCGC,12,'0'), 
...    LPAD(C4100_CLTCGCDIG ,2,'0'), 
...    TRIM(LPAD(C4100_USUNUM,6,'0')) AS Usuario, 
...    TRIM(LPAD(C4100_CBJDATINC,8,'0')) AS data_inclusao, 
...    TRIM(LPAD(C4100_CBJHORINC,6,'0')) AS hora_inclusao, 
...    CASE WHEN C4100_CBJSTATUS = 1 THEN '1 - ATIVO' ELSE '2 - INATIVO' END AS STATUS, 
...    LPAD(C4100_CBJMOTIVO,2,'0') AS motivo, 
...    TRIM(LPAD(C4100_CBJDATMOT,8,'0')) AS dt_motivo, 
...    TRIM(LPAD(C4100_CBJNUMPRO,20,'0')), 
...    TRIM(C4100_CBJCAMOBS) 
...    FROM DNCB00.DNCB41 
...    WHERE C4100_CLTCGC = {0} AND C4100_CLTCGCDIG = {1} ORDER BY SEDIDR DESC 
...    FETCH FIRST ROW ONLY

${SQL_CONSULTA_HISTORICO_CLIENTE}
...    SELECT LPAD(C4100_CLTCGC,12,'0'), 
...    LPAD(C4100_CLTCGCDIG,2,'0') 
...    FROM DNCB00.DNCB41 FETCH FIRST ROW ONLY

${SQL_VALIDA_CONSULTA_HISTORICO_CLIENTE}
...    SELECT LPAD(C4100_CLTCGC,12,'0'), 
...    LPAD(C4100_CLTCGCDIG,2,'0'), 
...    TRIM(LPAD(C4100_CBJDATINC,8,'0')) AS dt_inclusao, 
...    TRIM(LPAD(C4100_CBJHORINC,6,'0')) AS hora_inclusao, 
...    CASE WHEN C4100_CBJSTATUS = 1 THEN '1 ATIVO' ELSE '2 INATIVO' END AS STATUS, 
...    LPAD(C4100_CBJMOTIVO,2,'0') AS motivo, 
...    LPAD(C4100_CBJDATMOT,8,'0') AS dt_motivo, 
...    TRIM(LPAD(C4100_USUNUM,6,'0')) AS usuario 
...    FROM DNCB00.DNCB41 WHERE C4100_CLTCGC = {0} AND C4100_CLTCGCDIG = {1} 
...    ORDER BY C4100_CBJDATINC ASC , C4100_CBJHORINC ASC 
...    FETCH FIRST 10 ROWS ONLY

${SQL_ATUALIZACAO_CONTRATO_BLOQUEIO_JUDICIAL}
...    SELECT LPAD(C4200_CLTCGC,12,'0'), 
...    LPAD(C4200_CLTCGCDIG,2,'0'), 
...    TRIM(LPAD(R0900_CCSCOD,17,'0')), 
...    LPAD(R0900_SISCODSIS,3,'0'), 
...    LPAD(R0900_PRUCOD,4,'0'), 
...    LPAD(R0900_OPVMOD,4,'0') 
...    FROM CTRB00.CTRB09 INNER JOIN DNCB00.DNCB42 ON C4200_CCSCOD = R0900_CCSCOD 
...    FETCH FIRST ROW ONLY

${SQL_CANCELAMENTO_CONTRATO_BLOQUEIO_JUDICIAL}
...    SELECT LPAD(C4200_CLTCGC , 12 , '0'), 
...    LPAD(C4200_CLTCGCDIG , 2 , '0'), 
...    LPAD(R0900_CCSCOD , 17 , '0'), 
...    LPAD(R0900_SISCODSIS , 3 , '0'), 
...    LPAD(R0900_PRUCOD , 4 , '0'), 
...    LPAD(R0900_OPVMOD , 4 , '0') 
...    FROM CTRB00.CTRB09 INNER JOIN DNCB00.DNCB42 ON C4200_CCSCOD = R0900_CCSCOD
...    FETCH FIRST ROW ONLY

${SQL_CONSULTA_CONTRATO}
...    SELECT TRIM(C4200_CCSCOD), 
...    LPAD(C4200_SISCODSIS , 3,'0'), 
...    LPAD(C4200_PRUCOD , 4,'0'), 
...    LPAD( C4200_OPVMOD , 4,'0') 
...    FROM DNCB00.DNCB42 FETCH FIRST ROW ONLY


${SQL_VALIDA_CONSULTA_CONTRATO}
...    SELECT TRIM(C4200_CCSCOD), 
...    TRIM(LPAD(C4200_SISCODSIS , 3,'0')), 
...    LPAD(C4200_PRUCOD , 4,'0'), 
...    LPAD( C4200_OPVMOD , 4,'0'), 
...    TRIM(LPAD(C4200_CLTCGC , 12,'0')), 
...    TRIM(LPAD(C4200_CLTCGCDIG , 2,'0')), 
...    TRIM(LPAD(C4200_USUNUM , 6,'0')), 
...    LPAD(C4200_COBDATINC , 8,'0'), 
...    LPAD(C4200_COBHORINC , 6,'0'), 
...    CASE WHEN C4200_COBSTAT = 1 THEN '1 - ATIVO' ELSE '2 - INATIVO' END AS STATUS, 
...    LPAD(C4200_COBMOT , 2,'0'), 
...    LPAD(C4200_COBDATMOT , 8,'0'), 
...    TRIM(C4200_CBJNUMPRO), 
...    LPAD(C4200_CBJCAMOBS , 17,'0') 
...    FROM DNCB00.DNCB42 WHERE C4200_CCSCOD = {0}
...    ORDER BY SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_HISTORICO_CONTRATO_BLOQUEIO_JUDICIAL}
...    SELECT TRIM(C4200_CCSCOD), 
...    LPAD(C4200_SISCODSIS,3,'0'), 
...    LPAD(C4200_PRUCOD,4,'0'), 
...    LPAD( C4200_OPVMOD,4,'0') 
...    FROM DNCB00.DNCB42 FETCH FIRST ROW ONLY

${SQL_VALIDA_HISTORICO_CONTRATO_BLOQUEIO_JUDICIAL_CABECALHO}
...    SELECT TRIM(C4200_CCSCOD), 
...    LPAD(C4200_SISCODSIS,3,'0'), 
...    LPAD(C4200_PRUCOD,4,'0'), 
...    LPAD(C4200_OPVMOD,4,'0') 
...    FROM DNCB00.DNCB42 WHERE C4200_CCSCOD = {0}
...    FETCH FIRST ROW ONLY

${SQL_VALIDA_HISTORICO_CONTRATO_BLOQUEIO_JUDICIAL}
...    SELECT LPAD(C4200_COBDATINC,8,'0'), 
...    LPAD(C4200_COBHORINC,6,'0'), 
...    CASE WHEN C4200_COBSTAT = 1 THEN '1 ATIVO' ELSE '2 INATIVO' END AS STATUS, 
...    LPAD(C4200_COBMOT,2,'0'), 
...    LPAD(C4200_COBDATMOT,8,'0'), 
...    LPAD(C4200_USUNUM,6,'0') 
...    FROM DNCB00.DNCB42 WHERE C4200_CCSCOD = {0}
...    ORDER BY SEDIDR FETCH FIRST 10 ROWS ONLY

${SQL_CONSULTAR_SITUACAO_SERASA}    
...    SELECT CONCAT(LPAD(C3700_CLTCGC ,12 ,'0'),LPAD(C3700_CLTCGCDIG ,2 ,'0')) FROM DNCB00.DNCB37 FETCH FIRST ROW ONLY;

${SQL_VALIDAR_CONSULTA_SITUACAO_SERASA}
...    SELECT LPAD(C3700_CLTCGC, 12 , '0')||' - '||LPAD(C3700_CLTCGCDIG, 2 , '0'), LPAD(C3700_CLTCOD, 8 , '0'),
...    TRIM(X0100_CLTNOM),LPAD(C3700_SISCODSIS,  3 , '0'),LPAD(C3700_PRUCOD, 4 , '0'),LPAD(D0400_PRUNOM, 2 , '0'),
...    SUBSTR(LPAD(C3700_SPTDATOCOR,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3700_SPTDATOCOR,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3700_SPTDATOCOR,8,'0'),1,4),
...    SUBSTR(LPAD(C3700_CCSDATINI,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3700_CCSDATINI,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3700_CCSDATINI,8,'0'),1,4) ,
...    SUBSTR(LPAD(C3700_CCSDATFIM,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3700_CCSDATFIM,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3700_CCSDATFIM,8,'0'),1,4) ,
...    SUBSTR(LPAD(C3700_CCSDATPRIPA,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3700_CCSDATPRIPA,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3700_CCSDATPRIPA,8,'0'),1,4),
...    SUBSTR(LPAD(C3700_CCSDATATVAL,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3700_CCSDATATVAL,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3700_CCSDATATVAL,8,'0'),1,4)
...    FROM DNCB00.DNCB37 INNER JOIN AOXB00.AOXB01 ON C3700_CLTCOD = X0100_CLTCOD INNER JOIN PODB00.PODB04 ON C3700_PRUCOD = D0400_PRUCOD WHERE C3700_CCSCOD = '{0}'

${SQL_VALIDAR_CONSULTA_HIST_SERASA}
...    SELECT LPAD(C3700_CLTCGC,    12 ,    '0')||' - '|| LPAD(C3700_CLTCGCDIG,    2 ,    '0'),TRIM(X0100_CLTNOM),
...    LPAD(C3700_CLTCOD ,    8,    '0'), TRIM(C3700_CCSCOD),
...    SUBSTR(LPAD(C3800_NSEDATENVIO,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3800_NSEDATENVIO,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3800_NSEDATENVIO,8,'0'),1,4),
...    SUBSTR(LPAD(C3800_NSEHORENVIO,6,'0'),1,2)||':'||SUBSTR(LPAD(C3800_NSEHORENVIO,6,'0'),3,2)||':'||SUBSTR(LPAD(C3800_NSEHORENVIO,6,'0'),5,2),
...    SUBSTR(LPAD(C3800_NSEDATRET,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3800_NSEDATRET,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3800_NSEDATRET,8,'0'),1,4),
...    SUBSTR(LPAD(C3800_NSEHORRET,6,'0'),1,2)||':'||SUBSTR(LPAD(C3800_NSEHORRET,6,'0'),3,2)||':'||SUBSTR(LPAD(C3800_NSEHORRET,6,'0'),5,2),
...    SUBSTR(LPAD(C3800_NSEDATMOT,8,'0'),7,2)||'/'||SUBSTR(LPAD(C3800_NSEDATMOT,8,'0'),5,2)||'/'||SUBSTR(LPAD(C3800_NSEDATMOT,8,'0'),1,4)
...    FROM DNCB00.DNCB38 INNER JOIN AOXB00.AOXB01 ON  C3700_CLTCOD = X0100_CLTCOD INNER JOIN DNCB00.DNCB37 ON  C3700_CLTCOD = X0100_CLTCOD
...    WHERE C3800_CCSCOD = '{0}' AND C3700_CLTCOD = '{1}' ORDER BY C3800_NSEDATENVIO ASC , C3800_NSEHORENVIO ASC FETCH FIRST 11 ROWS ONLY 

${SQL_VALIDAR_CONSULTA_SUSTADOS}       
...    SELECT LPAD(C3700_CLTCGC,12 ,'0')||' - '||LPAD(C3700_CLTCGCDIG,2 ,'0'),TRIM(C3700_CCSCOD),CHAR(C3700_SISCODSIS),
...    LPAD(C3700_CLTCOD,8 ,'0'),TRIM(X0100_CLTNOM), LPAD(C3700_PRUCOD,4 ,'0'),
...    LPAD(D0400_PRUNOM,2 ,'0'),LPAD(C3700_OPVMOD,4 ,'0'),CHAR(C3700_NSESTAAUT),
...    (SUBSTR(INT(C3700_SPTDATOCOR),7,2))||'/'||SUBSTR(INT ( C3700_SPTDATOCOR ),5,2)||'/'||SUBSTR(INT (C3700_SPTDATOCOR ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATINI),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATINI ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATINI ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATFIM),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATFIM ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATFIM ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATPRIPA),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATPRIPA ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATPRIPA ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATATVAL),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATATVAL ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATATVAL ),1,4) 
...    FROM    DNCB00.DNCB37 INNER JOIN AOXB00.AOXB01 ON C3700_CLTCOD = X0100_CLTCOD INNER JOIN PODB00.PODB04 ON C3700_PRUCOD = D0400_PRUCOD
...    WHERE C3700_CLTCGC = '{0}' AND C3700_CLTCGCDIG = '{1}' ORDER BY C3700_CCSCOD ASC FETCH FIRST ROW ONLY;

${SQL_DADOS_ACESSO_CONSULTAR_HIST_INTERVENCOES}
...    SELECT CONCAT(LPAD(C3700_CLTCGC ,    12 ,    '0') ,
...    LPAD(C3700_CLTCGCDIG ,    2 ,    '0') ), TRIM(C3700_CCSCOD) FROM DNCB00.DNCB37
...    INNER JOIN DNCB00.DNCB38 ON C3800_CLTCOD = C3700_CLTCOD
...    WHERE C3700_HDRDATA > 20190901 ORDER BY C3700_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_DADOS_ACESSO_CANCELAR_ENVIO}
...    SELECT CONCAT(LPAD(C3700_CLTCGC ,    12 ,    '0'),
...    LPAD(C3700_CLTCGCDIG ,    2 ,    '0'))
...    FROM DNCB00.DNCB37 FETCH FIRST ROW ONLY;

${SQL_DADOS_ACESSO_REGULARIZAR_BAIXA}
...    SELECT CONCAT(LPAD(C3700_CLTCGC ,    12 ,    '0'),
...    LPAD(C3700_CLTCGCDIG ,    2 ,    '0')) ,
...    TRIM (C3700_CCSCOD),TRIM(C3700_NSESTACTR) FROM DNCB00.DNCB37
...    INNER JOIN DNCB00.DNCB38 ON C3700_CLTCOD = C3800_CLTCOD
...    WHERE C3700_NSESTACTR = 6 AND C3700_NSEMOTBAIXA > 0 ORDER BY C3700_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_VALIDAR_REGULARIZACAO_DE_BAIXA}
...    SELECT CASE WHEN C3700_NSESTACTR = 2 THEN 'REGULARIZADO' ELSE 'NAO REGULARIZADO' END
...    FROM  DNCB00.DNCB37 WHERE C3700_CCSCOD = '{0}' 
...    AND C3700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') FETCH FIRST ROW ONLY

${SQL_DESFAZER_REGULARIZACAO_DE_BAIXAS}
...    UPDATE DNCB00.DNCB37 SET C3700_NSESTACTR = '{0}' WHERE C3700_CCSCOD = '{1}' 
...    AND C3700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') 

${SQL_DADOS_ACESSO_NEGATIVAR_MANUAL}
...    SELECT TRIM(C3700_CCSCOD), TRIM(C3700_SISCODSIS) FROM DNCB00.DNCB37 FETCH FIRST 1 ROW ONLY

${SQL_ACESSO_CONSULTAR_CLIENTE_POR_CONTRATO}    
...    SELECT TRIM(C3700_CCSCOD)  FROM DNCB00.DNCB37 FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTA_CLIENTE_CONTRATO}
...    SELECT LPAD(C3700_CLTCGC,12 ,'0')||' - '||LPAD(C3700_CLTCGCDIG,2 ,'0'),TRIM(C3700_CCSCOD),CHAR(C3700_SISCODSIS),
...    LPAD(C3700_CLTCOD,8 ,'0'),TRIM(X0100_CLTNOM), LPAD(C3700_PRUCOD,4 ,'0'),
...    TRIM(D0400_PRUNOM),LPAD(C3700_OPVMOD,4 ,'0'),CHAR(C3700_NSESTAAUT),
...    (SUBSTR(INT(C3700_SPTDATOCOR),7,2))||'/'||SUBSTR(INT ( C3700_SPTDATOCOR ),5,2)||'/'||SUBSTR(INT (C3700_SPTDATOCOR ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATINI),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATINI ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATINI ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATFIM),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATFIM ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATFIM ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATPRIPA),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATPRIPA ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATPRIPA ),1,4) , 
...    (SUBSTR(INT(C3700_CCSDATATVAL),7,2))||'/'||SUBSTR(INT ( C3700_CCSDATATVAL ),5,2)||'/'||SUBSTR(INT (C3700_CCSDATATVAL ),1,4) 
...    FROM    DNCB00.DNCB37 INNER JOIN AOXB00.AOXB01 ON C3700_CLTCOD = X0100_CLTCOD INNER JOIN PODB00.PODB04 ON C3700_PRUCOD = D0400_PRUCOD WHERE  C3700_CCSCOD = '{0}'

${SQL_VALIDA_SITUACAO_CONTRATO}
...    SELECT LPAD(C3700_CLTCGC, 12 , '0'), LPAD(C3700_CLTCGCDIG, 2 , '0'), C3700_CCSCOD, LPAD(C3700_SISCODSIS, 3 , '0'), LPAD(C3700_CLTCOD, 8 , '0'), TRIM(X0100_CLTNOM), LPAD(C3700_PRUCOD, 4, '0'), LPAD(D0400_PRUNOM, 2, '0'), LPAD(C3700_OPVMOD, 4, '0'), C3700_NSESTACTR, C3700_NSESTAAUT, C3700_NSECTNEGSER, C3700_NCEIDINCSER, C3700_SPFVALPEND, LPAD(C3700_SPTDATOCOR, 8, '0'), LPAD(C3700_CCSDATINI, 8, '0'), LPAD(C3700_CCSDATFIM, 8, '0'), LPAD(C3700_CCSDATPRIPA, 8, '0'), LPAD(C3700_CCSDATATVAL, 8, '0') FROM DNCB00.DNCB37 
...    INNER JOIN AOXB00.AOXB01 ON C3700_CLTCOD = X0100_CLTCOD INNER JOIN PODB00.PODB04 ON C3700_PRUCOD = D0400_PRUCOD WHERE C3700_CLTCOD = {0} ORDER BY C3700_CCSCOD ASC FETCH FIRST ROW ONLY

*** Keywords ***
# PACOTE 11 - 001_negativacao_serasa
# CT001
Recuperar Dados Entrada Negativar Manual CGC/CPF
    ${result}      Query    ${SQL_NEGATIVAR_MANUAL_CGC_CPF}
    [Return]       ${result}[0][0]   
    
Recuperar Dados Validacao Negativar Manual CGC/CPF
    [Arguments]    ${contrato}  ${data_atual}
    ${query}       Format String    ${SQL_VALIDA_INCLUSAO_NEGATIVAR_MANUAL_CGC_CPF}  ${contrato}  ${data_atual}
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result}
    
Desfazer Dados Negativar Manual CGC/CPF
    [Arguments]    ${contrato}  ${data_atual}
    ${query}       Format String    ${SQL_EXCLUI_INCLUSAO_NEGATIVAR_MANUAL_CGC_CPF}  ${contrato}  ${data_atual}
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result}
    
# CT002
Recuperar Dados Entrada Sustar Especifico
    ${result}      Query      ${SQL_SUSTAR_ESPECIFICO}
    [Return]       ${result}[0][0]
    
Recuperar Dados Validacao Sustar Especifico
    [Arguments]    ${cgc/cpf}  ${data_atual} 
    ${cpf_cnpj}    Get Substring    ${cgc/cpf}     0    12
    ${digito}      Get Substring    ${cgc/cpf}    12    14    
    ${query}       Format String    ${SQL_RECUPERA_SUSTAR_ESPECIFICO}  ${cpf_cnpj}    ${digito}    ${data_atual}  
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}         
    [Return]       ${result} 
        
Desfazer Dados Sustar Especifico
    [Arguments]    ${cgc/cpf}  ${data_atual}
    ${query}       Format String    ${SQL_EXCLUI_SUSTAR_ESPECIFICO}  ${cgc/cpf}  ${data_atual}   
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result} 

# CT003
Recuperar Dados Entrada Excluir Serasa
    ${result}      Query        ${SQL_EXCLUIR_SERASA}
    [Return]       ${result}[0][0]

Recuperar Dados Validacao Excluir Serasa
    [Arguments]    ${cgc/cpf}  ${data_atual}
    ${cpf_cnpj}    Get Substring    ${cgc/cpf}    0    12
    ${digito}      Get Substring    ${cgc/cpf}    12    14    
    ${query}       Format String    ${SQL_VALIDA_EXCLUIR_SERASA}  ${cpf_cnpj}    ${digito}    ${data_atual}    
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result}

Desfazer Dados Excluir Serasa
    [Arguments]    ${cgc/cpf}  ${data_atual}
    ${cpf_cnpj}    Get Substring    ${cgc/cpf}    0    12
    ${digito}      Get Substring    ${cgc/cpf}    12    14    
    ${query}       Format String    ${SQL_EXCLUI_INCLUSAO_EXCLUIR_SERASA}   ${cpf_cnpj}    ${digito}   ${data_atual}
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${result}
    
# CT004
Recuperar Dados Entrada Reenviar Serasa
    ${result}      Query        ${SQL_REENVIAR_SERASA}
    [Return]       ${result}[0][0]
    
Recuperar Dados Validacao Reenviar Serasa
    [Arguments]    ${cgc/cpf}  ${data_atual}
    ${cpf_cnpj}    Get Substring    ${cgc/cpf}    0    12
    ${query}       Format String    ${SQL_VALIDA_REENVIAR_SERASA}  ${cpf_cnpj}  ${data_atual}   
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${result}

Desfazer Dados Reenviar Serasa
    [Arguments]    ${cgc/cpf}  ${data_atual}
    ${cpf_cnpj}    Get Substring    ${cgc/cpf}    0    12
    ${query}       Format String      ${SQL_EXCLUI_REENVIAR_SERASA}    ${cpf_cnpj}  ${data_atual}
    ${result}      Run Keyword And Return Status    Execute Sql String    ${query}
    [Return]       ${result}       
    
Desfazer Dados Sustar Envio Seletivo
    [Arguments]    ${cgc/cpf}  ${data_atual}
    ${cpf_cnpj}    Get Substring    ${cgc/cpf}    0    12
    ${query}       Format String    ${SQL_EXCLUI_SUSTAR_ENVIO_SELETIVO}  ${cpf_cnpj}  ${data_atual} 
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${result}         

# CT006
Recuperar Dados Cancelamento Envio SPC
    ${result}      Query    ${SQL_CANCELAR_ENVIO}
    [Return]       ${result}[0]
    
Validar Dados Consulta Cancelamento Envio SPC
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDA_CANCELAR_ENVIO}  ${contrato}    
    ${result}      Query    ${query} 
    [Return]       ${result}
        
Recuperar Dados Valida Negativacao Manual
    [Arguments]    ${contrato}  ${data_atual}
    ${query}       Format String    ${SQL_VALIDA_NEGATIVAR_MANUAL}  ${contrato}  ${data_atual}  
    ${result}      Run Keyword And Return Status    Check If Exists In Database  ${query}        
    [Return]       ${result}  

Desfazer Dados Negativacao Manual
    [Arguments]    ${contrato}  ${data_atual}
    ${query}       Format String    ${SQL_EXCLUI_NEGATIVAR_MANUAL}  ${contrato}  ${data_atual}    
    ${result}      Run Keyword And Return Status    Check If Exists In Database  ${query} 
    [Return]       ${result}          
    
Recuperar Dados Inclusao Atualizacao Bloqueio Judicial
    ${result}      Query      ${SQL_INC_ATU_CLIENTE}
    [Return]       ${result}
    
Recuperar Dados Validacao Altualizacao Bloqueio Judicial
    [Arguments]    ${clt_cod}
    ${query}       Format String    ${SQL_VALIDA_INC_ATU_CLIENTE}  ${clt_cod}
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${result}
    
# CT002
Recuperar Dados Cancelamento Bloqueio Judicial
    ${result}      Query    ${SQL_CANCELA_CLIENTE}
    [Return]       ${result}
    
Validar Dados Cancelamento Bloqueio Judicial
    [Arguments]    ${cpf}  ${digito} 
    ${query}       Format String    ${SQL_VALIDA_CANCELA_CLIENTE}  ${cpf}  ${digito} 
    ${result}      Query   ${query}
    [Return]       ${result}[0]
    
Validar Status Cancelamento Bloqueio Judicial
    [Arguments]    ${cpf}  ${digito} 
    ${query}       Format String    ${SQL_VALIDA STATUS_CANCELA_CLIENTE}  ${cpf}  ${digito} 
    ${result}      Query   ${query}
    [Return]       ${result}[0]   
    
# CT003
Recuperar Dados Consulta Bloqueio Judicial
    ${result}      Query      ${SQL_CONSULTA_BLOQUEIO_JUDICIAL}
    [Return]       ${result}

Validar Dados Consulta Bloqueio Judicial
    [Arguments]    ${cpf}  ${digito}
    ${query}       Format String    ${SQL_VALIDA_CONSULTA_BLOQUEIO_JUDICIAL}  ${cpf}  ${digito}  
    ${result}      Query            ${query}
    [Return]       ${result}[0]

# CT004
Recuperar Dados Consulta Historico Bloqueio Judicial
    ${result}      Query    ${SQL_CONSULTA_HISTORICO_CLIENTE}
    [Return]       ${result}[0]
    
Validar Dados Consulta Historico Bloqueio Judicial
    [Arguments]    ${cpf}  ${digito}
    ${query}       Format String    ${SQL_VALIDA_CONSULTA_HISTORICO_CLIENTE}  ${cpf}  ${digito}
    ${result}      Query            ${query}
    [Return]       ${result}        
 
# CT005
Recuperar Dados Inclusao Atualizacao Contrato Bloqueio Judicial
    ${result}      Query    ${SQL_ATUALIZACAO_CONTRATO_BLOQUEIO_JUDICIAL}
    [Return]       ${result}

# CT006
Recuperar Dados Cancelamento Contrato Bloqueio Judicial
    ${result}      Query    ${SQL_CANCELAMENTO_CONTRATO_BLOQUEIO_JUDICIAL}    
    [Return]       ${result}

# CT007      
Recuperar Dados Consulta Contrato Bloqueio Judicial
    ${result}      Query    ${SQL_CONSULTA_CONTRATO}
    [Return]       ${result}[0]
    
Validar Dados Consulta Contrato Bloqueio Judicial
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDA_CONSULTA_CONTRATO}  ${contrato}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]
    
# CT008
Recuperar Dados Consulta Historico Contrato Bloqueio Judicial
    ${result}      Query    ${SQL_HISTORICO_CONTRATO_BLOQUEIO_JUDICIAL}
    [Return]       ${result}[0]
    
Validar Cabecalho Consulta Historico Contrato Bloqueio Judicial
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDA_HISTORICO_CONTRATO_BLOQUEIO_JUDICIAL_CABECALHO}  ${contrato}
    ${result}      Query            ${query}
    [Return]       ${result}[0]
    
Validar Dados Consulta Historico Contrato Bloqueio Judicial
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDA_HISTORICO_CONTRATO_BLOQUEIO_JUDICIAL}  ${contrato}
    ${result}      Query            ${query}
    [Return]       ${result}

Recuperar Dados Consultar Situacao Serasa
    ${result}      Query    ${SQL_CONSULTAR_SITUACAO_SERASA}    
    [Return]       ${result}[0][0]

Valida Consultar Situacao Serasa
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_SITUACAO_SERASA}    ${contrato}
    ${result}      Query            ${query}
    [Return]       ${result}[0]
    
Validar Consulta Historico Serasa
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_HIST_SERASA}     ${dados}[0]     ${dados}[1]
    ${result}      Query            ${query}
    [Return]       ${result}
    
Validar Consulta Sustados
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}    0     12
    ${digito}      Get Substring    ${dados}    12    14    
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_SUSTADOS}       ${cpf_cnpj}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]
    
Recuperar Dados Consultar Historico Intervencoes
    ${result}      Query    ${SQL_DADOS_ACESSO_CONSULTAR_HIST_INTERVENCOES}    
    [Return]       ${result}[0]
    
Recuperar Dados Cliente Cancelar Envio
    ${result}      Query    ${SQL_DADOS_ACESSO_CANCELAR_ENVIO}    
    [Return]       ${result}[0][0]
    
Recuperar Dados Cliente Regularizar Baixa
    ${result}      Query    ${SQL_DADOS_ACESSO_REGULARIZAR_BAIXA}    
    [Return]       ${result}[0]
    
Regularizacao De Baixa
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDAR_REGULARIZACAO_DE_BAIXA}    ${contrato}
    ${result}      Query            ${query}    
    [Return]       ${result}[0][0]
    
Desfazer Regularizacao De Baixa
    [Arguments]    ${rollback}
    ${query}       Format String    ${SQL_DESFAZER_REGULARIZACAO_DE_BAIXAS}    ${rollback}[1]    ${rollback}[2]
    ${status}      Run Keyword And Return Status    Execute Sql String         ${query}        
    [Return]       ${status}
    
Recuperar Dados Entrada Negativar Manual
    ${result}      Query    ${SQL_DADOS_ACESSO_NEGATIVAR_MANUAL}    
    [Return]       ${result}[0]
    
Recuperar Dados Acesso Consultar Cliente Por Contrato
    ${result}      Query    ${SQL_ACESSO_CONSULTAR_CLIENTE_POR_CONTRATO}    
    [Return]       ${result}[0][0]
    
Consultar Cliente Por Contrato
    [Arguments]    ${contrato}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_CLIENTE_CONTRATO}    ${contrato}
    ${result}      Query            ${query}
    [Return]       ${result}[0]

Dados Situacao do Contrato
    [Arguments]    ${codCliente}
    ${query}       Format String  ${SQL_VALIDA_SITUACAO_CONTRATO}  ${codCliente}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query}
    [Return]       ${status} 
