*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections    
Library      String    
 
*** Variables ***

${SQL_RECUPERAR_DADOS_ACESSO_CLASS_CONC}   SELECT LPAD(C1300_ADDCOD,2,'0'), LPAD(C1300_CAVCOD,2,'0') ,LPAD(C1300_CLTCODSEG,3,'0')FROM DNCB00.DNCB13 ORDER BY SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_VALIDAR_INCLUSAO_CLASS_CONCEITO}     SELECT * FROM DNCB00.DNCB13 WHERE C1300_CFCFXAINI = '{0}' AND C1300_CFCFXAFIM = '{1}' AND C1300_CFCCODCONCE = '{2}'

${SQL_DESFAZER_INCLUSAO_CLASS_CONCEITO}    DELETE FROM DNCB00.DNCB13 WHERE C1300_CFCFXAINI = '{0}' AND C1300_CFCFXAFIM = '{1}' AND C1300_CFCCODCONCE = '{2}'

${SQL_VALIDAR_CONSULTA_CLASS_CONC}         SELECT LPAD(C1300_CFCFXAINI,7,'0'), LPAD(C1300_CFCFXAFIM,7,'0'), TRIM(C1300_CFCCODCONCE) FROM DNCB00.DNCB13 WHERE C1300_ADDCOD = '{0}' AND C1300_CAVCOD = '{1}' AND C1300_CLTCODSEG = '{2}'  

${SQL_LISTA_CLASSIFICACAO_CONCEITO}        SELECT TRIM(C1300_CFCCODCONCE), LPAD(C1300_CFCFXAINI,7,'0'), LPAD(C1300_CFCFXAFIM,7,'0') FROM DNCB00.DNCB13 
...                                        WHERE C1300_ADDCOD = '{0}' AND C1300_CAVCOD = '{1}' AND C1300_CLTCODSEG = '{2}' ORDER BY C1300_HDRDATA DESC FETCH FIRST 6 ROW ONLY

${SQL_VALIDAR_INCLUIR_CLASS}              SELECT * FROM DNCB00.DNCB13 WHERE C1300_CFCFXAINI = '{0}' AND C1300_CFCFXAFIM = '{1}' AND C1300_CFCCODCONCE = '{2}' AND C1300_CAVCOD = '{3}'

${SQL_DESFAZER_INCLUIR_CLASS}             DELETE FROM DNCB00.DNCB13 WHERE C1300_CFCFXAINI = '{0}' AND C1300_CFCFXAFIM = '{1}' AND C1300_CFCCODCONCE = '{2}' AND C1300_CAVCOD = '{3}'

${SQL_VALIDA_EXCLUSAO_CLASS}              SELECT * FROM DNCB00.DNCB13 WHERE C1300_CFCFXAINI = '{0}' AND C1300_CFCFXAFIM = '{1}' AND C1300_CFCCODCONCE = '{2}' AND C1300_CAVCOD = '{3}'
*** Keywords ***
Recuperar Dados Acesso Classificacao Conceito
    ${result}    Query    ${SQL_RECUPERAR_DADOS_ACESSO_CLASS_CONC}
    ${codAdv}    Get From List    ${result}[0]    0
    ${codCrit}   Get From List    ${result}[0]    1
    ${codSeg}    Get From List    ${result}[0]    2
    [Return]     ${codAdv}    ${codCrit}    ${codSeg}
    
Validar Inclusao/Classificacao Conceito
    [Arguments]  ${cfcxaIni}    ${cfcxaFim}    ${cfcConce}
    ${query}     Format String    ${SQL_VALIDAR_INCLUSAO_CLASS_CONCEITO}        ${cfcxaIni}    ${cfcxaFim}    ${cfcConce}
    ${status}    Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]     ${status}
    
Desfazer Inclusao/Classificacao Conceito
    [Arguments]    ${cfcxaIni}    ${cfcxaFim}    ${cfcConce}
    ${query}     Format String    ${SQL_DESFAZER_INCLUSAO_CLASS_CONCEITO}        ${cfcxaIni}    ${cfcxaFim}    ${cfcConce}
    ${status}    Run Keyword And Return Status    Execute Sql String    ${query}        

Validar Consultar/Classificacao Conceito
    [Arguments]    ${codAdv}    ${codCrit}    ${codSeg}
    ${query}    Format String   ${SQL_VALIDAR_CONSULTA_CLASS_CONC}    ${codAdv}    ${codCrit}    ${codSeg}
    ${result}   Query           ${query}
    [Return]    ${result}    
    
Validar Lista Classificacao De Conceito
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_LISTA_CLASSIFICACAO_CONCEITO}    ${dados}[0]    ${dados}[1]    ${dados}[2]
    ${result}      Query            ${query}    
    [Return]       ${result}
    
Validar Exclusao/Classificacao
    [Arguments]    ${cfcIni}       ${cfcFim}         ${cfcCod}      ${codCriterio}
    ${query}       Format String   ${SQL_VALIDA_EXCLUSAO_CLASS}     ${cfcIni}          ${cfcFim}         ${cfcCod}      ${codCriterio}
    ${status}      Run Keyword And Return Status    Check If Not Exists In Database    ${query}  
    [Return]       ${status}    
    
Validar Incluir/Classificacao
    [Arguments]    ${cfcIni}      ${cfcFim}         ${cfcCod}      ${codCriterio}
    ${query}       Format String    ${SQL_VALIDAR_INCLUIR_CLASS}   ${cfcIni}      ${cfcFim}         ${cfcCod}      ${codCriterio}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query}
    [Return]       ${status}
    
Desfazer Incluir/Classificacao
    [Arguments]    ${cfcIni}      ${cfcFim}          ${cfcCod}      ${codCriterio}
    ${query}       Format String    ${SQL_DESFAZER_INCLUIR_CLASS}   ${cfcIni}      ${cfcFim}         ${cfcCod}      ${codCriterio}
    ${status}      Run Keyword And Return Status    Execute Sql String   ${query}
    [Return]       ${status} 