*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String    

*** Variables ***

${SQL_VALIDAR_ALTERACAO_GBDS}
...      SELECT * FROM DNCB00.DNCBB2 WHERE CB200_CLTCGC = {0} AND CB200_CAPSITCLI = 1 ORDER BY CB200_HDRDATA DESC FETCH FIRST ROW ONLY

${SQL_RECURERAR_ID_ALTERADO}
...      SELECT LPAD(CB200_CLTCGC,12,'0'), TRIM(SEDIDR) FROM DNCB00.DNCBB2 WHERE CB200_CLTCGC = {0}

${SQL_VALIDAR_EXCLUSAO}
...      SELECT * FROM DNCB00.DNCBB2 WHERE CB200_CLTCGC = {0}
...      AND CB200_CAPSITCLI = 2 ORDER BY CB200_HDRDATA DESC FETCH FIRST ROW ONLY

${RECUPERAR_DADOS_AUTORIZACAO}         
...      SELECT LPAD(CB200_CLTCGC,12,'0') || LPAD(CB200_CLTCGCDIG,2,'0') FROM DNCB00.DNCBB2 
...      INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCGC = CB200_CLTCGC)
...      WHERE CB200_CLTCGC > 0
...      FETCH FIRST ROW ONLY 

${SQL_DESFAZER_INCLUSAO_GBDS}
...      DELETE FROM DNCB00.DNCBB2 WHERE CB200_CLTCGC = {0} AND CB200_HDRDATA = {1}

*** Keywords ***
# CT007
Validar Alteracao Feita GBDS
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_VALIDAR_ALTERACAO_GBDS}     ${cnpj}
    ${result}      Run Keyword And Return Status     Check If Exists In Database   ${query} 
    [Return]       ${result}
    

Recuperar ID Dados Alterardos 
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_RECURERAR_ID_ALTERADO}     ${cnpj}
    ${result}      Query   ${query} 
    [Return]       ${result}
    
Desfaz Dados Alterar Autorizacao 
    [Arguments]    ${cnpj}  ${data}
    ${query}       Format String    ${SQL_DESFAZER_INCLUSAO_GBDS}    ${cnpj}   ${data}    
    ${result}      Execute Sql String    ${query}
    [Return]       ${result}            
    
Validar Dado De Exclusao
    [Arguments]    ${cnpj}
    ${query}       Format String    ${SQL_VALIDAR_EXCLUSAO}     ${cnpj}
    ${result}      Run Keyword And Return Status     Check If Exists In Database   ${query}  
    [Return]       ${result}    
    
Recuperar Acesso Autorizacao
    ${query}    Query    ${RECUPERAR_DADOS_AUTORIZACAO}
    [Return]    ${query}   


Desfaz Dados Incluir Autorizacao 
    [Arguments]    ${cnpj}          ${data}
    ${query}       Format String    ${SQL_DESFAZER_INCLUSAO_GBDS}       ${cnpj}          ${data}
    ${status}      Run Keyword And Return Status  Execute Sql String    ${query}
    [Return]       ${status}  