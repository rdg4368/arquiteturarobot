*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      Collections      
Library      String    
Library      utilsMask    
*** Variables ***

${SQL_LISTAR_ADVERTENCIA}                     SELECT TRIM(C3000_CNRNOMTIT), TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG), TRIM(C0800_ADDCOD), TRIM(C0800_CLTCOD) FROM DNCB00.DNCB30 INNER JOIN DNCB00.DNCB08 ON (C3000_CLTCGC = C0800_ADMCGC) WHERE C0800_TPREGISTRO = 1 AND C0801_AVDDATBAIXA = 0 ORDER BY C0800_ADMCGC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_ACESSO_SITUACAO_CAD}              SELECT DISTINCT(CONCAT(LPAD(C3000_CLTCGC,12,'0'),LPAD(C3000_CLTCGCDIG,2,'0'))),(CAST(C3000_RCMTIPPES AS CHAR (1))),LPAD(C3000_CNRDATCONS,8,'0') 
...                                           FROM DNCB00.DNCB30 WHERE C3000_RCMTIPPES = 'J' AND C3000_CNRSITCPJ > 0 AND C3000_CNRDATSIT > 0 FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTAR_HIST_SIT}             SELECT LPAD(C3000_CNRDATCONS,8,'0'), LPAD(C3000_HDRHORA,6,'0'), TRIM(C3000_CNRSITCPF), TRIM(C3000_CNRSITCPJ) FROM DNCB00.DNCB30 WHERE C3000_CLTCGC = '{0}' AND C3000_CLTCGCDIG = '{1}' AND C3000_RCMTIPPES = '{2}' ORDER BY C3000_CNRDATCONS DESC FETCH FIRST 5 ROW ONLY

${SQL_VALIDAR_SITUACAO_CADASTRAL}             SELECT TRIM(C3000_CNRNOMTIT), TRIM(C3000_CNRNOMMAE),LPAD(C3000_CNRDATSIT,8,'0'),LPAD(C3000_CNRDATNASC,8,'0')
...                                           FROM DNCB00.DNCB30 WHERE C3000_CLTCGC = '{0}' AND C3000_CLTCGCDIG = '{1}' AND C3000_RCMTIPPES = '{2}' ORDER BY C3000_CNRDATCONS DESC FETCH FIRST 1 ROW ONLY

${SQL_CONSULTA_SITUACAO_CADASTRAL_SRF}        SELECT TRIM(C3000_CNRCNASEC),TRIM(C3000_CNRCNAPRI),TRIM(C3000_CNRNOMTIT),TRIM(C3000_CNRNOMEMP),LPAD(C3000_CNRDATNASC,8,'0'),LPAD(C3000_CNRDATSIT,8,'0')
...                                           FROM DNCB00.DNCB30 WHERE C3000_CLTCGC = '{0}' AND  C3000_CLTCGCDIG = '{1}' AND C3000_RCMTIPPES = '{2}' AND C3000_CNRDATCONS = {3}

# ${SQL_VALIDAR_CONSULTA_CLASS_CLIENTE}
# ...    SELECT LPAD(X0100_CLTCOD,8,'0'), 
# ...        TRIM(X0100_CLTNOM), SUBSTR(LPAD(X0100_CLTCGC,9,'0'),1,3)||'.'||
# ...        SUBSTR(LPAD(X0100_CLTCGC,9,'0'),4,3)||'.'|| SUBSTR(LPAD(X0100_CLTCGC,9,'0'),7,3)||'-'||LPAD(X0100_CLTCGCDIG,2,'0') AS CPF_CNPJ,  
# ...        (SUBSTR(LPAD(C3000_CNRDATCONS,8,'0'),7,2))||'/'||SUBSTR(LPAD ( C3000_CNRDATCONS,8,'0' ),5,2)||'/'||SUBSTR(LPAD(C3000_CNRDATCONS,8,'0' ),1,4) AS DATA_SITUACAO, CHAR (X0100_CLTCLSRISC) FROM AOXB00.AOXB01 INNER JOIN DNCB00.DNCB30 ON (C3000_CLTCGC = X0100_CLTCGC) 
# ...    WHERE X0100_CLTCOD = {0} ORDER BY C3000_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTA_CLASS_CLIENTE}
...    SELECT
...        TRIM(X0100_CLTCOD) AS COD_CLIENTE,
...        TRIM(X0100_CLTNOM) AS NOME,
...        TRIM(X0100_CLTCGC) || X0100_CLTCGCDIG AS CPF_CNPJ,
...        TRIM(X0100_CLTTIP) AS CLASSIFICACAO_CLIENTE,
...        TRIM(C3000_CNRDATCONS) AS DATA_SITUACAO
...    FROM
...        AOXB00.AOXB01
...    INNER JOIN DNCB00.DNCB30 ON
...        (C3000_CLTCGC = X0100_CLTCGC)
...    WHERE
...        X0100_CLTCOD = {0}
...    ORDER BY
...        C3000_HDRDATA DESC FETCH FIRST ROW ONLY

*** Keywords ***

Recuparar Lista Advertencia 
    ${result}    Query    SELECT CAST(C3000_CNRNOMTIT AS CHAR (20)), TRIM(C0800_ADMCGC), TRIM(C0800_ADMCGCDIG), TRIM(C0800_ADDCOD), TRIM(C0800_CLTCOD) FROM DNCB00.DNCB30 INNER JOIN DNCB00.DNCB08 ON (C3000_CLTCGC = C0800_ADMCGC) WHERE C0800_TPREGISTRO = 1 AND C0801_AVDDATBAIXA = 0 ORDER BY C0800_ADMCGC FETCH FIRST 1 ROW ONLY
    [Return]    ${result}   
    
Recuperar Dados Acesso Situacao Cadastral
    ${result}         Query    ${SQL_DADOS_ACESSO_SITUACAO_CAD}
    ${tipoCliente}    Get From List    ${result}[0]    1
    ${cgc/cpf}        Get From List    ${result}[0]    0
    ${dataInv}        Get From List    ${result}[0]    2
    [Return]    ${tipoCliente}    ${cgc/cpf}    ${dataInv}
    
Validar Consulta Historico Da Situacao Cadastral
    [Arguments]   ${dados}
    ${cgc/cpf}    Get Substring    ${dados}[1]        0    12
    ${digito}     Get Substring    ${dados}[1]        12   14
    ${query}      Format String    ${SQL_VALIDAR_CONSULTAR_HIST_SIT}    ${cgc/cpf}    ${digito}    ${dados}[0]
    ${result}     Query            ${query}    
    [Return]      ${result}
    
Validar Atualizar Situacao Cadastral Da SRF
    [Arguments]   ${dados}
    ${cgc/cpf}    Get Substring    ${dados}[1]        0    12
    ${digito}     Get Substring    ${dados}[1]        12   14
    ${query}      Format String    ${SQL_VALIDAR_SITUACAO_CADASTRAL}      ${cgc/cpf}    ${digito}    ${dados}[0]
    ${result}     Query            ${query}    
    [Return]      ${result}

Validar Consulta Situacao Cadastral Da SRF
    [Arguments]   ${dados}
    ${cgc/cpf}    Get Substring    ${dados}[1]        0    12
    ${digito}     Get Substring    ${dados}[1]        12   14
    ${query}      Format String    ${SQL_CONSULTA_SITUACAO_CADASTRAL_SRF}   ${cgc/cpf}    ${digito}    ${dados}[0]    ${dados}[2]
    ${result}     Query            ${query}    
    [Return]      ${result}
    
Validar Consulta Classificacao Cliente
    [Arguments]    ${codCliente}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_CLASS_CLIENTE}     ${codCliente}
    ${result}      Query            ${query}    
    [Return]       ${result}