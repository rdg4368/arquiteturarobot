*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
   

*** Keywords ***
Validar Dados De Origem Da Advertencia
    [Arguments]   ${OADCOD}  ${OADNOM}  ${OADCODPROC}
    ${result}     Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT C0400_OADCOD, C0400_OADNOM, C0400_OADCODPROC FROM DNCB00.DNCB04 WHERE C0400_OADCOD = '${OADCOD}' AND C0400_OADNOM = '${OADNOM}' AND C0400_OADCODPROC = '${OADCODPROC}' 
    [Return]    ${result}
    
Deletar Dados De Origem Da Advertencia
    [Arguments]   ${OADCOD}  ${OADNOM}  ${OADCODPROC}
    ${result}     Run Keyword And Return Status     Execute Sql String        
    ...  DELETE FROM DNCB00.DNCB04 WHERE C0400_OADCOD = '${OADCOD}' AND C0400_OADNOM = '${OADNOM}' AND C0400_OADCODPROC = '${OADCODPROC}' 
    [Return]    ${result}

Listar Dados Da Origem Da Advertencia
    ${result}     Query 
    ...  SELECT TRIM(C0400_OADCOD), TRIM(C0400_OADNOM), TRIM(C0400_OADCODPROC) FROM DNCB00.DNCB04 ORDER BY C0400_OADCOD ASC
    [Return]    ${result}
    
Recuperar Dados Entrada Consulta Remessas/Sistema
    ${result}    query     SELECT DISTINCT C0200_SISCODSIS FROM DNCB00.DNCB02
    [Return]     ${result}
       
Validar Dados Entrada Consulta Remessas/Sistema
    [Arguments]    ${cod_sis}
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT C0200_CSSDATREM AS REFERENCIA, C0200_CSSSEQMOV AS MOVIMENTO, C0200_CSSDATPROC AS DATA_PROCESSAMENTO, C0200_SISCODSIS AS COD_SIS FROM DNCB00.DNCB02 WHERE C0200_SISCODSIS = '${cod_sis}' ORDER BY C0200_CSSDATREM FETCH FIRST 13 ROWS ONLY
    [Return]       ${status}    
    
Recuperar Dados Entrada Remessas/Processamento
    ${result}      query     SELECT LPAD(C0200_CSSDATPROC,8,'0') AS DATA_ FROM DNCB00.DNCB02 WHERE C0200_SISCODSIS = 'DCC' FETCH FIRST 1 ROW ONLY
    [Return]       ${result}

Validar Dados Remessas/Processamento
    [Arguments]    ${data}
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT TRIM(C0200_SISCODSIS), C0200_CSSSEQMOV, LPAD(C0200_CSSDATREM,8,'0') FROM DNCB00.DNCB02 WHERE C0200_SISCODSIS = 'DCC' AND C0200_CSSDATPROC = ${data} FETCH FIRST 13 ROW ONLY
    [Return]       ${status}
    
Validar/Recuperar Dados Incluir Advertencias A Enviar Para GCR
    [Arguments]    ${data_atual}
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT CONCAT (G021_SISTEMA, G021_SUBSIST) AS SISTEMA, TRIM(SUBSTR(G021_ITEM,'197','198')) AS ADVERTENCIA, G021_HDRDATA AS DATA_INSERCAO FROM DNCB00.SBGB02 WHERE G021_HDRDATA = ${data_atual}
    [Return]       ${status}
    
Recupera Dados Programa Chamador de Transações
    ${result}      query     SELECT DISTINCT LPAD(X0100_CLTCOD,8,'0') AS COD_CLIENTE, LPAD(X0100_CLTCGC||X0100_CLTCGCDIG,14,'0') AS CPF FROM AOXB00.AOXB01 WHERE X0100_CLTCGC != 0 AND X0100_CLTCOD > 1000 FETCH FIRST 10 ROWS ONLY
    [Return]       ${result}  