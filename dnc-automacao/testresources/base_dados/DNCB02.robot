*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String    

*** Variables ***

${SQL_DADOS_CLIENTE}                    select lpad(c0800_ADMCGC,12,'0'), LPAD(C0800_ADMCGCDIG,2,'0') from dncB00.dncB08 where C0800_ADMCGC = '{0}' AND C0800_ADMCGCDIG = '{1}';

${SQL_DADOS_ADVERTENCIA_BACEN}          SELECT * FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = '{0}' AND C0800_ADMCGCDIG = '{1}' AND C0800_ADMDATOCOR = '{2}' AND C0802_BCRDATOCOR = '{3}'    

${SQL_RECUPERAR_DADOS_ALTERACAO_ADVERTENCIA_BACEN}   select lpad(c0800_ADMCGC,12,'0'), LPAD(C0800_ADMCGCDIG,2,'0') from dncB00.dncB08 where c0800_ADMCGC <> 0 FETCH FIRST 1 ROW ONLY

${SQL_ALTERACAO_ADVERTENCIA_BACEN}      update dncB00.dncB08 set C0802_BCRVALDVPR = '{0}' where C0800_ADMCGC = '{1}'

${SQL_DADOS_ENTRADA_CONSULTA_POR_DATA}  SELECT LPAD(C2900_OPMDATPRE,8,'0') FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY 

${SQL_DADOS_VALIDA_CONSULTA_POR_DATA}   SELECT LPAD(C2900_OPMDATPRE,8,'0'), LPAD(C2900_OPMCODOPE,5,'0'), C2900_OPETIPORI, C2900_OPENUMORI, C2900_OPENUMCCS, C2900_OPESIT, LPAD(C2900_OPEDATSIT,8,'0'), LPAD(C2900_OPEHORSIT,6,'0') FROM DNCB00.DNCB29 WHERE C2900_OPMDATPRE = '{0}' FETCH FIRST 4 ROW ONLY

${SQL_DADOS_ENTRADA_CONSULTA_CPF_CNPJ}  SELECT LPAD(C2900_OPECHVPES,20,'0') FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_VALIDA_CONSULTA_CPF_CNPJ}   SELECT LPAD(C2900_OPMDATPRE,8,'0'), LPAD(C2900_OPMCODOPE,5,'0'), C2900_OPETIPORI, C2900_OPENUMORI, C2900_OPENUMCCS, C2900_OPESIT, LPAD(C2900_OPEDATSIT,8,'0'), LPAD(C2900_OPEHORSIT,6,'0') FROM DNCB00.DNCB29 WHERE C2900_OPECHVPES = '{0}' FETCH FIRST 4 ROW ONLY;

${SQ_DADOS_ENTRADA_IDENTIFICACAO_ORIGEM}       SELECT C2900_OPENUMORI FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_ENTRADA_CONSULTA_CODIGO_OPERACAO}  SELECT LPAD(C2900_OPMCODOPE,5,'0'), LPAD(C2900_OPMDATPRE,8,'0') FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQ_DADOS_VALIDA_IDENTIFICACAO_ORIGEM}        SELECT LPAD(C2900_OPMDATPRE,8,'0'), LPAD(C2900_OPMCODOPE,5,'0'), C2900_OPETIPORI, C2900_OPENUMORI, C2900_OPENUMCCS, C2900_OPESIT, LPAD(C2900_OPEDATSIT,8,'0'), LPAD(C2900_OPEHORSIT,6,'0') FROM DNCB00.DNCB29 WHERE C2900_OPENUMORI= '{0}' FETCH FIRST 4 ROW ONLY

${SQL_DADOS_VALIDA_CONSULTA_CODIGO_OPERACAO}   SELECT LPAD(C2900_OPMDATPRE,8,'0'), LPAD(C2900_OPMCODOPE,5,'0'), C2900_OPETIPORI, C2900_OPENUMORI, C2900_OPENUMCCS, C2900_OPESIT, LPAD(C2900_OPEDATSIT,8,'0'), LPAD(C2900_OPEHORSIT,6,'0') FROM DNCB00.DNCB29 WHERE C2900_OPMCODOPE = '{0}' AND C2900_OPMDATPRE = '{1}' FETCH FIRST 4 ROW ONLY

${SQL_DADOS_ENTRADA_CONSULTA_POR_SITUACAO}     SELECT LPAD(C2900_OPMDATPRE,8,'0'), C2900_OPMCODOPE FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_VALIDA_CONSULTA_POR_SITUACAO}      SELECT LPAD(C2900_OPMDATPRE,8,'0') AS DATA_OPERACAO, LPAD(C2900_OPMCODOPE,5,'0' ) AS OPERACAO, C2900_OPETIPORI AS TIPO, C2900_OPENUMORI AS ORIGEM_OPERACAO, C2900_OPENUMCCS AS CSS, LPAD(C2900_OPEDATSIT,8,'0')AS DATA_IDENTIFICACAO, LPAD(C2900_OPEHORSIT,6,'0') AS HORA_SISTEMA FROM DNCB00.DNCB29 WHERE C2900_OPMDATPRE = '{0}' AND C2900_OPMCODOPE = '{1}'

${SQL_DADOS_ENTRADA_CONSULTA_MENSAGGEM}        SELECT LPAD(C2900_OPMCODOPE,5,'0'), LPAD(C2900_OPMDATPRE,8,'0'), C2900_OPENUMORI FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY   

${SQL_DADOS_VALIDA_CONSULTA_MENSAGGEM}         SELECT LPAD(C2900_OPMDATPRE,8,'0'), LPAD(C2900_OPMCODOPE,5,'0'), C2900_OPETIPORI, C2900_OPENUMORI, C2900_OPENUMCCS, C2900_OPESIT, LPAD(C2900_OPEDATSIT,8,'0'), LPAD(C2900_OPEHORSIT,6,'0') FROM DNCB00.DNCB29 WHERE C2900_OPMCODOPE = '{0}' AND C2900_OPMDATPRE = '{1}' AND C2900_OPENUMORI = '{2}'

${SQL_DADOS_ENTRADA_REENVIO_RESPOSTA}    SELECT LPAD(C2900_OPMCODOPE,5,'0'), LPAD(C2900_OPMDATPRE,8,'0'), C2900_OPENUMORI FROM DNCB00.DNCB29 A ORDER BY C2900_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_VALIDA_REENVIO_RESPOSTA}     SELECT LPAD(C2900_OPMDATPRE,8,'0'), LPAD(C2900_OPMCODOPE,5,'0'), C2900_OPETIPORI, C2900_OPENUMORI, C2900_OPENUMCCS, C2900_OPESIT, LPAD(C2900_OPEDATSIT,8,'0'), LPAD(C2900_OPEHORSIT,6,'0') FROM DNCB00.DNCB29 WHERE C2900_OPMCODOPE = '{0}' AND C2900_OPEDATSIT = '{1}' AND C2900_OPETIPORI = 'R'

${SQL_ALTERACAO_REENVIO_RESPOSTA}        DELETE FROM DNCB00.DNCB29 WHERE C2900_OPMCODOPE = '{0}' AND C2900_OPEDATSIT = '{1}' AND C2900_OPETIPORI = 'R'

${SQL_DADOS_ENTRADA_SOLICITACOES_PENDENTES}    SELECT DISTINCT(LPAD(C2900_OPMDATPRE,8,'0')) FROM DNCB00.DNCB29 WHERE C2900_OPMCODSIS = 'DNC' AND C2900_OPMCODOPE = '1970' AND C2900_OPMNOMOPE LIKE 'CCS%' FETCH FIRST 1 ROW ONLY

${SQL_DADOS_SOLICITACOES_PENDENTES}      SELECT LPAD(C2900_OPMDATPRE,8,'0'), C2900_OPENUMCCS FROM DNCB00.DNCB29 WHERE C2900_OPMDATPRE = '{0}' AND C2900_OPMCODSIS = 'DNC' AND C2900_OPMCODOPE = '1970' AND C2900_OPMNOMOPE LIKE 'CCS%' FETCH FIRST 6 ROW ONLY

${SQL_DADOS_RELACIONAMENTO_CLIENTE}      SELECT LPAD(C2700_CLTCGC,9,'0') AS CPF, C2700_CLTCGCDIG AS CPF_DIGITO, C2700_RCMTIPPES AS TIPO_PESSOA FROM DNCB00.DNCB27, AOXB00.AOXB01 WHERE X0100_CLTCGC = C2700_CLTCGC AND C2700_CLTCGC > 500 FETCH FIRST ROW ONLY

${SQL_DADOS_VALIDA_CONSULTA_RELACIONAMENTO_CLIENTE}    SELECT LPAD(C2700_CLTCGC,9,'0') AS CPF, C2700_CLTCGCDIG AS CPF_DIGITO, X0100_CLTNOMSRF AS NOME_CLIENTE, C2700_RCMDATINI AS DATA_INICIO, C2700_RCMDATFIM AS DATA_FINAL, C2700_RCMTIPPES AS TIPO_PESSOA FROM DNCB00.DNCB27, AOXB00.AOXB01 WHERE X0100_CLTCGC = C2700_CLTCGC AND C2700_CLTCGC = '{0}' FETCH FIRST ROW ONLY

${SQL_DADOS_ENTRADA_CONSULTA_DETALHAMENTO_RELACIONAMENTO}    SELECT LPAD(C2700_CLTCGC,9,'0') AS CPF, C2700_RCMTIPPES AS TIPO_PESSOA FROM DNCB00.DNCB27, AOXB00.AOXB01 WHERE X0100_CLTCGC = C2700_CLTCGC AND C2700_CLTCGC > 500 FETCH FIRST ROW ONLY

${SQL_DADOS_DETALHAMENTO_RELACIONAMENTO}       SELECT LPAD(C2700_CLTCGC,9,'0') AS CPF, C2700_CLTCGCDIG AS CPF_DIGITO, X0100_CLTNOMSRF AS NOME_CLIENTE, C2700_RCMDATINI AS DATA_INICIO, C2700_RCMDATFIM AS DATA_FINAL, C2700_RCMTIPPES AS TIPO_PESSOA FROM DNCB00.DNCB27, AOXB00.AOXB01 WHERE X0100_CLTCGC = C2700_CLTCGC AND C2700_CLTCGC = '{0}' FETCH FIRST ROW ONLY

${SQL_DADOS_ENTRADA_CONTA_DEPOSITO}      SELECT LPAD(C2500_DEPSEQ,4,'0'), LPAD(C2500_CTCCHVCONTA,10,'0'), LPAD(C2500_BDVDATINI,8,'0'), LPAD(C2500_BDVDATFIM,8,'0'), LPAD(C2500_BDVTIP,1,'0') FROM DNCB00.DNCB25 ORDER BY C2500_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_VALIDA_CONTA_DEPOSITO}       SELECT LPAD(C2500_CLTCGC,12,'0'), LPAD(C2500_CLTCGCDIG,2,'0'), C2500_CLTCOD, LPAD(C2500_BDVDATINI,8,'0'), LPAD(C2500_BDVDATFIM,8,'0'), TRIM(C2500_SISCODSIS), LPAD(C2500_BDVNUMOPE,6,'0'), TRIM(X0100_CLTNOM) FROM DNCB00.DNCB25 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD = C2500_CLTCOD) WHERE C2500_CTCCHVCONTA = '{0}' AND C2500_BDVTIP = '{1}' ORDER BY C2500_HDRDATA, C2500_HDRHORA, C2500_BDVDATINI FETCH FIRST 4 ROW ONLY

${SQL_DADOS_ENTRADA_MOVIMENTO_RELACIONAMENTO}  SELECT 'F', LPAD(C2600_CLTCGC,12,'0'), LPAD(C2600_CLTCGCDIG,2,'0') FROM DNCB00.DNCB26 WHERE C2600_RCMTIPPES = 'F' ORDER BY C2600_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_DADOS_VALIDA_MOVIMENTO_RELACIONAMENTO}   SELECT LPAD(C2600_CLTCGC,12,'0'), LPAD(C2600_CLTCGCDIG,2,'0'), TRIM(X0100_CLTNOM), LPAD(X0100_CLTCOD,8,'0'), LPAD(C2600_MRLDATMOV,8,'0'), C2600_MRLSEQREG, LPAD(C2600_RCMDATINI,8,'0'), TRIM(C2600_MRLSITMOV) FROM DNCB00.DNCB26 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCGC = C2600_CLTCGC) WHERE C2600_RCMTIPPES = 'F' AND C2600_CLTCGC = '{0}'

${SQL_DADOS_ENTRADA_BENS_DIREITOS_VALORES}     SELECT 'F', LPAD(C2400_CLTCGC,12,'0'), LPAD(C2400_CLTCGCDIG,2,'0'), LPAD(C2400_BDVTIP,1,'0') FROM DNCB00.DNCB24 FETCH FIRST 1 ROW ONLY

${SQL_DADOS_ROLLBACK_EXLCUI_SQ_DESC}        SELECT LPAD(C0800_ADMCGC,12,'0'), LPAD(C0800_ADMCGCDIG,2,'0') FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = '{0}' AND C0800_ADMCGCDIG = '{1}'

${SQL_ACESSO_CONSULTAR_HIST_SERASA_REMASSA}    
...    SELECT (SUBSTR(INT(C0200_CSSDATREM),7,2))||SUBSTR(INT ( C0200_CSSDATREM ),5,2)||SUBSTR(INT (C0200_CSSDATREM -10000 ),1,4) 
...    AS "Período Inicial", (SUBSTR(INT(C0200_CSSDATREM),7,2))||SUBSTR(INT ( C0200_CSSDATREM ),5,2)||SUBSTR(INT (C0200_CSSDATREM ),1,4)  
...    AS "Período Final",INT(C0200_CSSDATREM - 10000) AS INI_VALIDAR, INT(C0200_CSSDATREM) AS FINAL_VALIDAR  FROM DNCB00.DNCB02 ORDER BY C0200_CSSDATREM DESC FETCH FIRST ROW ONLY

${SQL_VALIDAR_COSNULTA_HIST_SERASA_REMESSAS} 
...    SELECT (SUBSTR(INT(C0200_CSSDATREM),7,2))||'/'||SUBSTR(INT ( C0200_CSSDATREM ),5,2)||'/'||SUBSTR(INT (C0200_CSSDATREM ),1,4) ,
...    TRIM(C0200_CSSSEQMOV),(SUBSTR(INT(C0200_CSSDATPROC),7,2))||'/'||SUBSTR(INT ( C0200_CSSDATPROC ),5,2)||'/'||SUBSTR(INT (C0200_CSSDATPROC ),1,4) 
...    FROM DNCB00.DNCB02 WHERE C0200_CSSDATREM BETWEEN '{0}' AND '{1}' AND C0200_SISCODSIS = 'SER'
...    ORDER BY C0200_CSSDATREM ASC,  C0200_CSSSEQMOV ASC FETCH FIRST 13 ROWS ONLY;


*** Keywords ***

Recuperar Dados Do Cliente
    [Arguments]    ${ADMCGC}        ${ADMCGCDIG}
    ${query}       Format String    ${SQL_DADOS_ROLLBACK_EXLCUI_SQ_DESC}  ${ADMCGC}  ${ADMCGCDIG}
    ${result}      Query            ${query}
    [Return]       ${result}

Pesquisar Dados Advertencia Bacen
    [Arguments]     ${cnpj}     ${data}
    ${query}        Format String   ${SQL_DADOS_ADVERTENCIA_BACEN}   ${cnpj}[0][0]   ${cnpj}[0][1]   ${data}   ${data}  
    ${result}       Run Keyword And Return Status     Check If Exists In Database  ${query}  
    [Return]        ${result}
#...  SELECT * FROM DNCB00.DNCB08 WHERE C0800_ADMCGC = ${cnpj}[0][0] AND C0800_ADMCGCDIG = ${cnpj}[0][1] AND C0800_ADMDATOCOR = ${data} AND C0802_BCRDATOCOR = ${data}    
 
Recuperar Dados Da Alteracao Da Advertencia Bacen
    ${result}      Query      ${SQL_RECUPERAR_DADOS_ALTERACAO_ADVERTENCIA_BACEN}
    [Return]       ${result}  removerZeroEsquerda

Atualizar Alteracao Da Advertencia Bacen
    [Arguments]    ${BCRVALDVPR}   ${ADMCGC}
    ${query}       Format String   ${SQL_ALTERACAO_ADVERTENCIA_BACEN}   ${BCRVALDVPR}   ${ADMCGC}
    ${result}      Execute Sql String  ${query}
    [Return]       ${result}
#update dncB00.dncB08 set C0802_BCRVALDVPR = ${BCRVALDVPR} where C0800_ADMCGC = ${ADMCGC}

# Deletar Massa Da Inclusao
    # [Arguments]    ${ADDCOD}
    # ${result}   Run Keyword And Return Status  Execute Sql String    DELETE FROM DNCB00.DNCB01 WHERE C0100_ADDCOD = ${ADDCOD}    
    # [Return]   ${result} 

# Consultar Dados da Advertencia
    # ${result}     Run Keyword And Return Status     Check If Exists In Database 
    # ...  SELECT C0100_ADDCOD, C0100_ADDDES, C0100_OADCOD FROM DNCB00.DNCB01 ORDER BY C0100_ADDCOD
    # [Return]    ${result}
    
# Recuperar Dados Da Natureza da Advertencia
    # [Arguments]    ${NDACOD}
    # ${result}       Query  SELECT C0300_NDACOD FROM DNCB00.DNCB03 WHERE C0300_NDACOD = ${NDACOD}
    # [Return]        ${result}
    
# Recuperar Dados Da Natureza da Advertencia Pelo Codigo
    # [Arguments]    ${ADDCOD}
    # ${result}       Query  SELECT C0300_ADDCOD FROM DNCB00.DNCB03 WHERE C0300_ADDCOD = ${ADDCOD}
    # [Return]        ${result}

# Deletar Massa Da Natureza da Advertencia
    # [Arguments]    ${ADDCOD}
    # ${result}   Run Keyword And Return Status  Execute Sql String    
    # ...  DELETE FROM DNCB00.DNCB03 WHERE C0300_ADDCOD = ${ADDCOD}
    # [Return]   ${result}
    
# Recuperar Dados De Origem Da Advertencia
    # [Arguments]    ${OADCOD}
    # ${result}    Query  SELECT C0400_OADCOD FROM DNCB00.DNCB04 WHERE C0400_OADCOD = '${OADCOD}'
    # [Return]        ${result}

# Consultar Dados Da Natureza Da Advertencia
    # ${result}     Run Keyword And Return Status     Check If Exists In Database 
    # ...  SELECT C0300_ADDCOD, C0300_NDACOD, C0300_NDADES FROM DNCB00.DNCB03 ORDER BY C0300_ADDCOD
    # [Return]    ${result}

# Deletar Massa De Origem Da Advertencia
    # [Arguments]    ${OADCOD}
    # ${result}   Run Keyword And Return Status  Execute Sql String    
    # ...  DELETE FROM DNCB00.DNCB04 WHERE C0400_OADCOD = '${OADCOD}';
    # [Return]   ${result}

# Consultar Dados Da Origem Da Advertencia
    # ${result}     Run Keyword And Return Status     Check If Exists In Database 
    # ...  SELECT C0400_OADCOD, C0400_OADNOM, C0400_OADCODPROC FROM DNCB00.DNCB04 ORDER BY C0400_OADCOD 
    # [Return]    ${result}
    
# Consultar Alteracoes Da Natureza Advetencia
    # [Arguments]    ${ADDCOD}
    # ${result}    Query  SELECT * FROM DNCB00.DNCB03 WHERE C0300_ADDCOD = ${ADDCOD}
    # [Return]   ${result}
    
# Defazer Alteracoes Da Natureza Advetencia
    # [Arguments]       ${NDADES}  ${ADDCOD}  
    # ${resultado} =    Run Keyword And Return Status      Execute Sql String  
    # ...  UPDATE DNCB00.DNCB03 SET C0300_NDADES = '${NDADES}' WHERE C0300_ADDCOD = ${ADDCOD} ;
    # [Return]          ${resultado}
    
Recuperar Dados de Entrada Consulta Por Data
    ${result}      Query     ${SQL_DADOS_ENTRADA_CONSULTA_POR_DATA}
    [Return]       ${result}
    
Validar Dados Consulta Por Data
    [Arguments]    ${data}
    ${query}       Format String   ${SQL_DADOS_VALIDA_CONSULTA_POR_DATA}   ${data}
    ${status}      Run Keyword And Return Status    Check If Exists In Database  ${query}      
    [Return]       ${status}

Recuperar Dados de Entrada Consulta CPF CNPJ
    ${result}      Query     ${SQL_DADOS_ENTRADA_CONSULTA_CPF_CNPJ}
    [Return]       ${result}

Validar Dados Consulta CPF CNPJ
    [Arguments]    ${chave}
    ${query}       Format String    ${SQL_DADOS_VALIDA_CONSULTA_CPF_CNPJ}   ${chave}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${status}
    
Recuperar Dados de Entrada Consulta Identificacao Origem
    ${result}      Query     ${SQ_DADOS_ENTRADA_IDENTIFICACAO_ORIGEM}
    [Return]       ${result}

Validar Dados Consulta Identificacao Origem
    [Arguments]    ${id_origem}
    ${query}       Format String    ${SQ_DADOS_VALIDA_IDENTIFICACAO_ORIGEM}        ${id_origem}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${status}
    
Recuperar Dados de Entrada Consulta Codigo Operacao
    ${result}      Query     ${SQL_DADOS_ENTRADA_CONSULTA_CODIGO_OPERACAO}
    [Return]       ${result}
    
Validar Dados Consulta Codigo Operacao
    [Arguments]    ${operacao}    ${data}
    ${query}       Format String  ${SQL_DADOS_VALIDA_CONSULTA_CODIGO_OPERACAO}     ${operacao}    ${data} 
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${status}
    
Recuperar Dados de Entrada Consulta Por Situacao
    ${result}      Query     ${SQL_DADOS_ENTRADA_CONSULTA_POR_SITUACAO}
    [Return]       ${result}

Validar Dados Consulta Por Situacao
    [Arguments]    ${data}        ${operacao}
    ${query}       Format String  ${SQL_DADOS_VALIDA_CONSULTA_POR_SITUACAO}    ${data}       ${operacao}    
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}  
    [Return]       ${status}

Recuperar Dados de Entrada Consulta a Mensagem
    ${result}      Query     ${SQL_DADOS_ENTRADA_CONSULTA_MENSAGGEM}
    [Return]       ${result}
    
Validar Dados Consulta a Mensagem
    [Arguments]    ${operacao}    ${data}    ${identificacao}
    ${query}       Format String  ${SQL_DADOS_VALIDA_CONSULTA_MENSAGGEM}   ${operacao}    ${data}    ${identificacao}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}
    [Return]       ${status}
    
Recuperar Dados de Entrada Reenvio da Resposta
    ${result}      Query     ${SQL_DADOS_ENTRADA_REENVIO_RESPOSTA}
    [Return]       ${result}
    
Validar Dados Reenvio da Resposta
    [Arguments]    ${operacao}    ${data_atual}
    ${query}       Format String  ${SQL_DADOS_VALIDA_REENVIO_RESPOSTA}     ${operacao}    ${data_atual}
    ${status}      Run Keyword And Return Status    Check If Exists In Database   ${query} 
    [Return]       ${status}
    
Desfazer Alteracao De Reenvio da Resposta
    [Arguments]    ${operacao}    ${data}
    ${query}       Format String  ${SQL_ALTERACAO_REENVIO_RESPOSTA}    ${operacao}    ${data}
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}
    [Return]       ${status}

Recuperar Dados Solicitacoes Pendentes
    ${result}      Query     ${SQL_DADOS_ENTRADA_SOLICITACOES_PENDENTES}
    [Return]       ${result}
    
Validar Dados Solicitacoes Pendentes
    [Arguments]    ${data}
    ${query}       Format String  ${SQL_DADOS_SOLICITACOES_PENDENTES}    ${data}
    ${status}      Run Keyword And Return Status    Execute Sql String   ${query}
    [Return]       ${status}

Recupera dados Consuta ao Relacionamento de Clientes
    ${result}      Query     ${SQL_DADOS_RELACIONAMENTO_CLIENTE}
    [Return]       ${result}
    
Valida dados Consulta ao Relacionamento de Clientes
    [Arguments]    ${cpf}
    ${query}       Format String     ${SQL_DADOS_VALIDA_CONSULTA_RELACIONAMENTO_CLIENTE}       ${cpf} 
    ${status}      Run Keyword And Return Status    Execute Sql String   ${query}
    [Return]       ${status}
    
Consultar Dados Entrada Detalhamento Do Relacionamento
    ${result}      Query     ${SQL_DADOS_ENTRADA_CONSULTA_DETALHAMENTO_RELACIONAMENTO}
    [Return]       ${result}

Validar Dados Consultar Detalhamento Do Relacionamento
    [Arguments]    ${cpf}
    ${query}       Format String  ${SQL_DADOS_DETALHAMENTO_RELACIONAMENTO}   ${cpf}
    ${status}      Run Keyword And Return Status    Execute Sql String       ${query}
    [Return]       ${status}
    
Recuperar Dados Entrada Pessoas Vinculadas A Conta De Deposito
    ${result}    Query     ${SQL_DADOS_ENTRADA_CONTA_DEPOSITO}
    [Return]     ${result}
    
Recuperar Dados Validacao Pessoas Vinculadas A Conta De Deposito
    [Arguments]    ${conta}       ${bdv_tip}
    ${query}       Format String  ${SQL_DADOS_VALIDA_CONTA_DEPOSITO}    ${conta}    ${bdv_tip}
    ${status}      Run Keyword And Return Status    Execute Sql String        ${query}
    [Return]       ${status}

Recuperar Dados Entrada Movimento De Relacionamento 
    ${result}    Query     ${SQL_DADOS_ENTRADA_MOVIMENTO_RELACIONAMENTO}
    [Return]     ${result}

Recuperar Dados Validacao Movimento De Relacionamento
    [Arguments]    ${cpf_cnpj}
    ${query}       Format String    ${SQL_DADOS_VALIDA_MOVIMENTO_RELACIONAMENTO}     ${cpf_cnpj} 
    ${status}      Run Keyword And Return Status    Execute Sql String        ${query}
    [Return]       ${status}    

Recupera Dados Entrada Movimento de Bens Direitos Valores    
    ${result}    Query     ${SQL_DADOS_ENTRADA_BENS_DIREITOS_VALORES}
    [Return]     ${result}
   
Recuperar Dados Consultar Historico Serasa Remessas
    ${result}    Query    ${SQL_ACESSO_CONSULTAR_HIST_SERASA_REMASSA}    
    [Return]    ${result}[0]
    
Validar Consulta Historico Serasa Remessas
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_VALIDAR_COSNULTA_HIST_SERASA_REMESSAS}    ${dados}[2]    ${dados}[3]
    ${result}      Query            ${query}    
    [Return]       ${result}
    
