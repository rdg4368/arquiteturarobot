*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  

*** Variables ***
${SQL_RECUPERAR_ADDCOD}    
...    SELECT TRIM(C0100_ADDCOD) FROM DNCB00.DNCB01 FETCH FIRST ROW ONLY

${SQL_CONSULTA_INCLUSAO_ADVERTENCIA}
...    SELECT LPAD(MAX(C0100_ADDCOD) +1,2,'0') FROM DNCB00.DNCB01

${SQL_CONSULTA_COD_ADVERTENCIA}
...    SELECT TRIM(C0100_ADDCOD) AS COD_ADVERTENCIA FROM DNCB00.DNCB01 ORDER BY C0100_ADDCOD DESC FETCH FIRST ROW ONLY

*** Keywords ***
Recuperar Dados Da Inclusao
    ${result}      Query  ${SQL_CONSULTA_INCLUSAO_ADVERTENCIA}
    [Return]       ${result}[0][0]
    
Recuperar Dados 
    ${result}      Query  ${SQL_CONSULTA_COD_ADVERTENCIA}
    [Return]       ${result}[0][0]

Validar Dados Da Inclusao
    [Arguments]    ${addcod}
    
    ${result}      Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT C0100_ADDCOD FROM DNCB00.DNCB01 WHERE C0100_ADDCOD = ${addcod}
   
    [Return]       ${result}

Deletar Massa Da Inclusao
    [Arguments]    ${addcod}
    ${result}      Run Keyword And Return Status  Execute Sql String    DELETE FROM DNCB00.DNCB01 WHERE C0100_ADDCOD = ${addcod}    
    [Return]       ${result} 

Listar Dados da Advertencia Banco
    ${result}      Query 
    ...  SELECT LPAD(C0100_ADDCOD,2,'0'), TRIM(C0100_ADDDES), TRIM(C0100_OADCOD) FROM DNCB00.DNCB01 ORDER BY C0100_ADDCOD ASC
    [Return]       ${result}

Consultar Dados da Advertencia
    ${result}      Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT C0100_ADDCOD, C0100_ADDDES, C0100_OADCOD FROM DNCB00.DNCB01 ORDER BY C0100_ADDCOD
    [Return]       ${result}
 
Recuperar AddCod Para Incluir Registro De Advertencia
    ${result}    Query    ${SQL_RECUPERAR_ADDCOD}
    [Return]     ${result}[0][0]
    
   