*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem  
Library      String    
Library      Collections      

*** Variables ***
${SQL_CPF_EXCECAO_ADV_DEPARTAMENTAL}    SELECT CONCAT (LPAD(C2700_CLTCGC,12,'0'),LPAD(C2700_CLTCGCDIG,2,'0')) FROM DNCB00.DNCB27 WHERE C2700_CLTCGC > 9999 FETCH FIRST ROW ONLY

${SQL_VALIDAR_INCLUSAO_EXCECAO_ADV_DEP}    SELECT TRIM(SUBSTR(G021_ITEM,15,75))AS DESCRICAO ,TRIM(SEDIDR) AS ID FROM DNCB00.SBGB02 WHERE G021_ITEM LIKE '{0}%' AND G021_ARQUIVO = '13' AND G021_REGISTRO = '01' AND G021_SISTEMA = 'DN' ORDER BY G021_HDRDATA DESC

${SQL_DESFAZER_INCLUSAO_EXCECAO_ADV_DEP}   DELETE FROM DNCB00.SBGB02 WHERE G021_COMPL ='SRF' AND G021_ARQUIVO = '13' 
...                                        AND G021_REGISTRO = '01' AND G021_SISTEMA = 'DN' AND SEDIDR = '{0}' AND G021_ITEM LIKE '{1}%'

${SQL_VALIDAR_DESCRICAO_EXCECAO_ADV_DEP}    SELECT TRIM(SUBSTR(G021_ITEM,15,75))AS DESCRICAO FROM DNCB00.SBGB02 WHERE SEDIDR = '{0}' AND G021_ARQUIVO = '13' AND G021_REGISTRO = '01' AND G021_SISTEMA = 'DN' ORDER BY G021_HDRDATA DESC

${SQL_EXCLUIR_EXCECAO_ADV_DEP}              SELECT * FROM DNCB00.SBGB02 WHERE SEDIDR = '{0}' AND G021_ARQUIVO = '13' AND G021_REGISTRO = '01' AND G021_SISTEMA = 'DN' ORDER BY G021_HDRDATA DESC

*** Keywords ***
Recuperar Cpf/Cnpj Excecao Advertencia Departamental
    ${result}    Query    ${SQL_CPF_EXCECAO_ADV_DEPARTAMENTAL}    
    [Return]    ${result}[0][0]
    
Validar Inclusao Excecao Advertencia Departamental
    [Arguments]     ${cnpjCpf}
    ${query}        Format String    ${SQL_VALIDAR_INCLUSAO_EXCECAO_ADV_DEP}        ${cnpjCpf}
    ${result}       Query            ${query}        
    ${descricao}    Get From List    ${result}[0]    0
    ${idInclusao}   Get From List    ${result}[0]    1
    [Return]        ${descricao}     ${idInclusao}    
    
Desfazer Inclusao Excecao Advertencia Departamental
    [Arguments]    ${id}    ${cpf}
    ${query}       Format String    ${SQL_DESFAZER_INCLUSAO_EXCECAO_ADV_DEP}    ${id}    ${cpf}
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query} 
    [Return]       ${status}
    
    
Validar Descricao Advertencia Departamental
    [Arguments]     ${id}
    ${query}        Format String    ${SQL_VALIDAR_DESCRICAO_EXCECAO_ADV_DEP}        ${id}[1]
    ${descricao}    Query            ${query}        
    [Return]        ${descricao}[0][0]
    
Validar Exclusao Excecao Advertencia Departamental
    [Arguments]    ${id}
    ${query}       Format String     ${SQL_EXCLUIR_EXCECAO_ADV_DEP}        ${id}[1]
    ${status}      Run Keyword And Return Status    Check If Not Exists In Database            ${query}        
    [Return]       ${status}
