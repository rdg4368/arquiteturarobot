*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem   
Library      String      
Library      Collections    

*** Variables ***
${SQL_RECUPERAR_COS_PRODUTO_ALTERAR_PARAMETROS}
...    SELECT DISTINCT LPAD(C4000_PRUCOD, 4,'0'),TRIM(SEDIDR) FROM DNCB00.DNCB40 WHERE C4000_GRRREGSIT = 1 AND C4000_PRUCOD > 0 FETCH FIRST ROW ONLY

${SQL_VALIDAR_ALTERAR_PARAMETRO_POR_PRODUTO} 
...    SELECT LPAD(C4000_RPNQTDDIACA,4,'0'), LPAD(C4000_RPNQTDDIAEN,4,'0') FROM DNCB00.DNCB40 WHERE C4000_GRRREGSIT = 1 AND  C4000_PRUCOD = {0}  AND C4000_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYMMDD')

${SQL_DESFAZER_ALTERAR_PARAMETRO_POR_PRODUTO}
...    DELETE FROM DNCB00.DNCB40 WHERE C4000_GRRREGSIT = 1 AND C4000_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYMMDD') AND  C4000_PRUCOD = '{0}' AND C4000_RPNQTDDIACA = '{1}' AND C4000_RPNQTDDIAEN = '{2}'

${SQL_DESFAZER_ALTERAR_PARAMETRO_POR_PRODUTO2}
...    	UPDATE DNCB00.DNCB40 SET C4000_GRRREGSIT = 1 WHERE SEDIDR = {0}
 
${SQL_VALIDAR_CONSULTA_PARAMETRO_POR_PRODUTO}
...    SELECT CHAR (C4000_SPFCODNAT), LPAD (C4000_PRUCOD,4,'0'),LPAD(C4000_RPNQTDDIACA,4,'0'),LPAD(C4000_RPNQTDDIAEN,4,'0'),(SUBSTR(INT(C4000_DNCHORAOCOR),1,2))||':'||(SUBSTR(INT(C4000_DNCHORAOCOR),3,2))||':'||TRIM((SUBSTR(INT(C4000_DNCHORAOCOR),5,4))),
...    (SUBSTR(INT(C4000_DNCDATAOCOR),7,2))||'/'||SUBSTR(INT ( C4000_DNCDATAOCOR ),5,2)||'/'||TRIM(SUBSTR(INT (C4000_DNCDATAOCOR ),1,4)) FROM DNCB00.DNCB40 WHERE SEDIDR = {0}

${SQL_VALIDAR_CONSULTA_HISTORICO_PARAMETRO_POR_PRODUTO}
...    SELECT CHAR (C4000_SPFCODNAT), LPAD (C4000_PRUCOD,4,'0'), LPAD(C4000_RPNQTDDIACA,4,'0'),LPAD(C4000_RPNQTDDIAEN,4,'0'),
...    (SUBSTR(CHAR(C4000_DNCHORAOCOR),1,2))||':'||(SUBSTR(CHAR(C4000_DNCHORAOCOR),3,2))||':'||TRIM((SUBSTR(CHAR(C4000_DNCHORAOCOR),5,4))),
...    (SUBSTR(INT(C4000_DNCDATAOCOR),7,2))||'/'||SUBSTR(INT ( C4000_DNCDATAOCOR ),5,2)||'/'|| TRIM(SUBSTR(INT (C4000_DNCDATAOCOR ),1,4)) 
...    FROM DNCB00.DNCB40 WHERE C4000_PRUCOD = '{0}' ORDER BY C4000_HDRDATA ASC, C4000_HDRHORA ASC  FETCH FIRST 10 ROWS ONLY
   

*** Keywords ***
Recuperar CodProduto Parametro Negativacao Serasa
    ${result}    Query    ${SQL_RECUPERAR_COS_PRODUTO_ALTERAR_PARAMETROS}    
    [Return]     ${result}[0]
    
Validar Alterar Parametro Por Produto
    [Arguments]     ${codProduto}
    ${query}        Format String       ${SQL_VALIDAR_ALTERAR_PARAMETRO_POR_PRODUTO}          ${codProduto} 
    ${result}       Query               ${query}    
    [Return]        ${result}[0]
    
Desfazer Alterar Parametro Por Produto
    [Arguments]    ${dadoRollback}     ${qtdDiasCarga}     ${qtdDiasEnvio}
    ${query}       Format String       ${SQL_DESFAZER_ALTERAR_PARAMETRO_POR_PRODUTO}      ${dadoRollback}[0]     ${qtdDiasCarga}     ${qtdDiasEnvio}
    ${status}      Run Keyword And Return Status    Execute Sql String                    ${query}    
    ${query2}      Format String       ${SQL_DESFAZER_ALTERAR_PARAMETRO_POR_PRODUTO2}     ${dadoRollback}[1]   
    ${status2}     Run Keyword And Return Status    Execute Sql String                    ${query2}    
    [Return]       ${status}    ${status2}
    
Validar Consulta Parametro Por Produto
    [Arguments]    ${codValidar}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_PARAMETRO_POR_PRODUTO}    ${codValidar}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]
    
Validar Consulta Historico Parametro Por Produto
    [Arguments]    ${codValidar}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_HISTORICO_PARAMETRO_POR_PRODUTO}    ${codValidar}
    ${result}      Query            ${query}    
    [Return]       ${result}