*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String    

*** Variables ***
${SQL_VALIDAR_INCLUIR_REGISTRO_ADVERTENCIA}     SELECT * FROM DNCB00.DNCB06 WHERE C0600_ADDCOD = '{0}' AND C0600_TRGDES = '{1}' AND C0600_TRGCOD = '{2}'

${SQL_DESFAZER_INCLUIR_REGISTRO_ADVERTENCIA}    DELETE FROM DNCB00.DNCB06 WHERE C0600_ADDCOD = '{0}' AND C0600_TRGDES = '{1}' AND C0600_TRGCOD = '{2}'

${SQL_VALIDAR_CONSULTAR_REGISTRO_ADVERTENCIA}    SELECT LPAD(C0600_TRGCOD,3,'0'), LPAD(C0600_ADDCOD,2,'0'),LPAD(C0600_TRGCODSERA,3,'0'),TRIM(C0600_TRGDES) FROM DNCB00.DNCB06 FETCH FIRST 13 ROW ONLY

${SQL_RECUPERAR_DADOS_EXCLUIR_REG_ADV}           SELECT TRIM(C0600_HDRDATA), TRIM(C0600_HDRHORA),TRIM(C0600_HDRESTACAO),TRIM(C0600_HDRPROGRAMA),TRIM(C0600_HDRSTATGER),TRIM(C0600_TRGCOD),TRIM(C0600_TRGDES),TRIM(C0600_ADDCOD),TRIM(C0600_TRGCODSERA),TRIM(C0600_TRGUSUNUM),
...                                              TRIM(C0600_TRGDATINC) FROM DNCB00.DNCB06 ORDER BY C0600_TRGDATINC FETCH FIRST ROW ONLY

${SQL_VALIDAR_EXCLUIR_REG_ADV}                  SELECT * FROM DNCB00.DNCB06 WHERE C0600_ADDCOD = '{0}' AND C0600_TRGCOD = '{1}' AND C0600_TRGDES = '{2}'

${SQL_DESFAZER_EXCLUIR_REG_ADV}                INSERT INTO DNCB00.DNCB06 (C0600_HDRDATA, C0600_HDRHORA, C0600_HDRESTACAO, C0600_HDRPROGRAMA, C0600_HDRSTATGER, C0600_TRGCOD, C0600_TRGDES, C0600_ADDCOD, C0600_TRGCODSERA, C0600_TRGUSUNUM, C0600_TRGDATINC) 
...                                             VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}'); 

${SQL_RECUPERAR_DADOS_CLIENTES_GDB_PERIODO}
...    SELECT
...    	LPAD(CONCAT(CB300_CAPCGCGBD ,
...    	CB300_CAPCGCGBDIG) ,
...    	14 ,
...    	'0') AS "CPF/CNPJ",
...    	LPAD(CB400_CAPDTSITCLI - 1 ,
...    	8 ,
...    	'0') AS "Período inicial",
...    	LPAD(CB400_CAPDTSITCLI + 1 ,
...    	8 ,
...    	'0') AS "Período final"
...    FROM
...    	DNCB00.DNCBB3
...    INNER JOIN DNCB00.DNCBB2 ON
...    	CONCAT(CB200_CLTCGC,
...    	CB200_CLTCGCDIG) = CONCAT(CB400_CLTCGC,
...    	CB400_CLTCGCDIG)
...    INNER JOIN DNCB00.DNCBB4 ON
...    	CONCAT(CB300_CAPCGCGBD,
...    	CB300_CAPCGCGBDIG) = CONCAT(CB400_CAPCGCGBDAU,
...    	CB400_CAPCGCGBDAD)
...   WHERE
...    	CONCAT(CB300_CAPCGCGBD,
...    	CB300_CAPCGCGBDIG) IN (
...    	SELECT
...    		CONCAT(CB400_CAPCGCGBDAU,
...    		CB400_CAPCGCGBDAD)
...    	FROM
...    		DNCB00.DNCBB4) FETCH FIRST ROW ONLY;

${SQL_CONSULTA_CLIENTES_GBD_PERIODO}
...    SELECT
...        CB200_CLTCGC || CB200_CLTCGCDIG AS CPF,
...        CB200_CLTNOM AS NOME,
...        TRIM(CB200_CAPDTSITCLI) AS DATA
...    FROM
...    DNCB00.DNCBB2
...    WHERE
...        CB200_CLTNOM = '{0}'
...    ORDER BY
...        CB200_CAPDTSITCLI DESC FETCH FIRST ROW ONLY

*** Keywords ***
Validar Incluir Registro De Advertencia
    [Arguments]    ${addCod}    ${trgDes}    ${trgCod}
    ${query}    Format String      ${SQL_VALIDAR_INCLUIR_REGISTRO_ADVERTENCIA}     ${addCod}    ${trgDes}    ${trgCod}
    ${status}   Run Keyword And Return Status    Check If Exists In Database       ${query}    
    [Return]    ${status}

Desfazer Incluir Registro De Advertencia
    [Arguments]    ${addCod}    ${trgDes}    ${trgCod}
    ${query}    Format String   ${SQL_DESFAZER_INCLUIR_REGISTRO_ADVERTENCIA}   ${addCod}    ${trgDes}    ${trgCod}
    ${status}   Run Keyword And Return Status    Execute Sql String    ${query}
    [Return]    ${status}

Validar Consulta Registro De Advertencia    
    ${result}    Query    ${SQL_VALIDAR_CONSULTAR_REGISTRO_ADVERTENCIA}    
    [Return]     ${result}

Recuperar Dados Acesso Excluir Registro De Advertencia
    ${result}    Query    ${SQL_RECUPERAR_DADOS_EXCLUIR_REG_ADV}    
    [Return]     ${result}[0]
    
Validar Excluir Registro De Advertencia
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_VALIDAR_EXCLUIR_REG_ADV}    ${dados}[7]    ${dados}[5]    ${dados}[6]
    ${status}      Run Keyword And Return Status    Check If Not Exists In Database    ${query}        
    [Return]       ${status}
    
Desfazer Excluir Registro De Advertencia
    [Arguments]    ${dadosRollback}
    ${query}       Format String   ${SQL_DESFAZER_EXCLUIR_REG_ADV}       ${dadosRollback}[0]    ${dadosRollback}[1]    ${dadosRollback}[2]    ${dadosRollback}[3]    ${dadosRollback}[4]
    ...                                                                   ${dadosRollback}[5]    ${dadosRollback}[6]    ${dadosRollback}[7]    ${dadosRollback}[8]    ${dadosRollback}[9]    ${dadosRollback}[10]
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]       ${status}
    
    
Recuperar Dados Autorizacao Cliente
    ${result}      query     SELECT CONCAT(LPAD(CB200_CLTCGC, 12, '0'), LPAD(CB200_CLTCGCDIG, 2, '0')) FROM DNCB00.DNCBB2 WHERE CB200_CAPORINFO = 2 ORDER BY CB200_HDRDATA DESC, CB200_CAPDTSITCLI DESC;
    [Return]       ${result}  

Validar Dados Autorizacao Cliente
    [Arguments]    ${cpf_cnpj}
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT LPAD(CB400_CLTCGC, 12 , '0'), LPAD(CB400_CLTCGCDIG, 2 , '0'), LPAD(CB400_CAPCGCGBDAU, 12 , '0'), LPAD(CB200_CAPHRCADAU, 6 , '0'), LPAD(CB200_CAPDTSITCLI, 8 , '0') FROM DNCB00.DNCBB4 INNER JOIN DNCB00.DNCBB2 ON CONCAT(CB200_CLTCGC, CB200_CLTCGCDIG) = CONCAT(CB400_CLTCGC, CB400_CLTCGCDIG) WHERE CONCAT(CB400_CLTCGC , CB400_CLTCGCDIG) = ${cpf_cnpj} ORDER BY CB200_CAPDTSITCLI DESC FETCH FIRST ROW ONLY;
    [Return]       ${status}

Recuperar Dados Clientes Enviados GBD e Periodo
    ${result}      query     ${SQL_RECUPERAR_DADOS_CLIENTES_GDB_PERIODO}    
    [Return]       ${result}

Consultar Dados Clientes Enviados GBD e Periodo
    [Arguments]    ${nome}
    ${query}     Format String     ${SQL_CONSULTA_CLIENTES_GBD_PERIODO}    ${nome}
    ${result}    Query    ${query}
    [Return]     ${result}

Recuperar Dados Inclusao Classificacao Risco
    ${result}      query     SELECT LPAD((G021_COMPL + 1), 5, '0') FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' ORDER BY G021_COMPL DESC FETCH FIRST ROW ONLY;
    [Return]       ${result}
    
Validar Dados Inclusao Classificacao Risco
    [Arguments]    ${cod_class}
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT * FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' AND G021_COMPL = ${cod_class}
    [Return]       ${status}
    
Recuperar Dados Inclusao Classificacao Risco Alteracao
    ${result}      query     SELECT LPAD(G021_COMPL, 5, '0') , G021_ITEM FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' ORDER BY G021_COMPL DESC FETCH FIRST ROW ONLY
    [Return]       ${result}
    
Validar Dados Alteracao Classificacao Risco
    [Arguments]    ${descricao}
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT LPAD(G021_COMPL, 5, '0') , G021_ITEM FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' AND G021_COMPL = '${descricao}'
    [Return]       ${status}
    
Validar Dados Classificacao Risco
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT LPAD(G021_COMPL, 5, '0'), G021_ITEM FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' ORDER BY G021_COMPL FETCH FIRST 10 ROWS ONLY;
    [Return]       ${status}
    
Recuperar Dados Exclusao Classificacao Risco
    ${result}      query     SELECT LPAD(G021_COMPL, 5, '0') AS CODIGO, SEDIDR FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' ORDER BY G021_COMPL DESC FETCH FIRST ROW ONLY;
    [Return]       ${result}
    
Validar Dados Exclusao Classificacao Risco
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT SEDIDR FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' ORDER BY G021_COMPL DESC FETCH FIRST ROW ONLY;
    [Return]       ${status}
    
Recuperar Dados Inclusao Prazo Abertura Encerramento
    Execute Sql String          DELETE FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '11' AND G021_REGISTRO = '01'
    

Validar Dados Incluir Prazo de Abertura
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT * FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '11' AND G021_REGISTRO = '01'
    [Return]       ${status}
   
Validar Dados Consulta Prazo Abertura Encerramento 
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT SEDIDR FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '10' AND G021_REGISTRO = '01' ORDER BY G021_COMPL DESC FETCH FIRST ROW ONLY;
    [Return]       ${status}
    
Recuperar Dados Modelo De Credito Score
    Execute Sql String          DELETE FROM DNCB00.SBGB02 WHERE G021_COMPL = '9999'

Validar Dados Modelo De Credito Score
    ${status}      Run Keyword And Return Status    Execute Sql String        G021_COMPL AS "Modelo" FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '12' AND G021_COMPL = '9999' AND G021_REGISTRO = '01' ORDER BY SEDIDR ASC;
    [Return]       ${status}  

Recuperar Dados Exclusao De Credito Score
    ${result}      query     SELECT SUBSTR(G021_COMPL2, 1, 1) AS "Tipo Cliente", SUBSTR(G021_COMPL2, 4, 2) AS "Tipo Servidor / Cod Porte", SUBSTR(G021_COMPL2, 6, 3) AS "Application / Behaviour" FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '12' AND G021_COMPL = '9999' AND G021_REGISTRO = '01' ORDER BY SEDIDR ASC;
    [Return]       ${result}
    
Alterar Dados Exclusao De Credito Score
    ${result}      query     DELETE FROM DNCB00.SBGB02 WHERE G021_COMPL = '9999'
    [Return]       ${result}
    
Validar Dados Consulta De Credito Score
    ${status}      Run Keyword And Return Status    Execute Sql String        SELECT * FROM DNCB00.SBGB02 WHERE G021_SISTEMA = 'DN' AND G021_SUBSIST = 'C' AND G021_ARQUIVO = '12' AND G021_REGISTRO = '01' ORDER BY SUBSTR(G021_COMPL2, 1, 1) ASC , SUBSTR(G021_COMPL2, 2, 2) ASC FETCH FIRST 13 ROWS ONLY;
    [Return]       ${status}