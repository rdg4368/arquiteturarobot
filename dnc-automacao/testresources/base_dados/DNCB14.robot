*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections    
Library      String    

*** Variables ***
${SQL_DADO_ACESSO_CONSULTA_MOVIM_POR_CC}
...    SELECT DISTINCT LPAD(C1400_CTCCHVCONTA,10,'0')AS CONTA_CORRENTE FROM DNCB00.DNCB14 
...    WHERE C1400_MFCVALMOV > 0 AND C1400_MFCVALCTR > 0 AND C1400_MFCSLDMMDCC > 0 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_MOVIM_POR_CC}
...    SELECT C1400_MFCSLDMMDCC AS SALDO_MEDIO , C1400_MFCVALMOV AS MOV_FINANCEIRO, C1400_MFCVALAPA AS ANTECIPACAO_RECEBIVEIS
...    FROM DNCB00.DNCB14 WHERE C1400_CTCCHVCONTA = '{0}' FETCH FIRST 6 ROW ONLY

${SQL_RECUPERAR_CLTDCOD}
...    SELECT DISTINCT LPAD(C1400_CLTCOD,8,'0') AS CODIGO_CLIENTE FROM DNCB00.DNCB14 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_MOVIM_POR_CLTCOD}
...    SELECT C1400_MFCVALMOV FROM DNCB00.DNCB14 WHERE C1400_CLTCOD = '{0}' FETCH FIRST 6 ROW ONLY

${SQL_RECUPERAR_GRUPO_ECONOMICO} 
...    SELECT LPAD (C1400_TGECOD,4,'0') FROM DNCB00.DNCB14 WHERE C1400_TGECOD != 0 FETCH FIRST ROWS ONLY

${SQL_VALIDAR_CONSULTA_MOVIMENTACAO_GRUPO_ECONOM}
...    SELECT TRIM(C1400_TGECOD) AS GRUPO FROM DNCB00.DNCB14 WHERE C1400_TGECOD = '{0}' 
...    ORDER BY C1400_MFCDATREF FETCH FIRST ROW ONLY

*** Keywords ***
Recuperar Dado Acesso Consultar Movimentacao Por CC
    ${result}    Query    ${SQL_DADO_ACESSO_CONSULTA_MOVIM_POR_CC}    
    [Return]     ${result}[0][0]
    
Consulta Movimentacao Financeira Por Conta Corrente
    [Arguments]    ${conta}
    ${query}     Format String    ${SQL_VALIDAR_CONSULTA_MOVIM_POR_CC}      ${conta}
    ${result}    Query            ${query}    
    [Return]     ${result}
    
Recuperar Codigo de Cliente
    ${result}   Query    ${SQL_RECUPERAR_CLTDCOD}    
    [Return]    ${result}[0][0]
    
Consulta Movimentacao Financeira Por Codigo de Cliente    
    [Arguments]     ${codCliente}
    ${query}        Format String    ${SQL_VALIDAR_CONSULTA_MOVIM_POR_CLTCOD}    ${codCliente}
    ${result}       Query            ${query}  
    ${result1}      Get From List    ${result}[0]    0
    ${result2}      Get From List    ${result}[1]    0
    ${result3}      Get From List    ${result}[2]    0
    ${result4}      Get From List    ${result}[3]    0
    ${result5}      Get From List    ${result}[4]    0
    ${result6}      Get From List    ${result}[5]    0
    [Return]        ${result1}    ${result2}    ${result3}    ${result4}    ${result5}    ${result6}
    
Recuperar Grupo Economico
    ${result}    Query    ${SQL_RECUPERAR_GRUPO_ECONOMICO}
    [Return]     ${result}[0][0]
    
Consulta Movimentacao Financeira Por Grupo Economico
    [Arguments]    ${grupoEconocimo}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_MOVIMENTACAO_GRUPO_ECONOM}    ${grupoEconocimo}
    ${result}      Query            ${query}
    [Return]       ${result}[0][0]