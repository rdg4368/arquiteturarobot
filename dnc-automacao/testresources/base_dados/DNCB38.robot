*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String  

*** Variables ***
${SQL_VALIDAR_CONSULTA_HIST_INTERVENCOES}
...    SELECT (SUBSTR(INT(C3800_NSEDATENVIO),7,2))||'/'||SUBSTR(INT ( C3800_NSEDATENVIO ),5,2)||'/'||SUBSTR(INT (C3800_NSEDATENVIO ),1,4) ,
...    SUBSTR(LPAD(C3800_NSEHORENVIO,6,'0'),1,2)||':'||SUBSTR(LPAD(C3800_NSEHORENVIO,6,'0'),3,2)||':'||SUBSTR(LPAD(C3800_NSEHORENVIO,6,'0'),5,2) ,
...    LPAD(C3800_CNSCODSOLIC,6,'0') ,LPAD(C3800_NSESTAENVIO,2,'0') FROM DNCB00.DNCB38
...    WHERE C3800_CCSCOD = '{0}' AND  C3800_CLTCGC = '{1}' AND C3800_CLTCGCDIG = '{2}' AND  C3800_NSESTAENVIO > 10 
...    ORDER BY C3800_NSEDATENVIO ASC FETCH FIRST 11 ROWS ONLY;

*** Keywords ***
Validar Consulta Historico Intervencoes
    [Arguments]    ${dados}
    ${cpfCnpj}     Get Substring    ${dados}[0]    0    12
    ${digito}      Get Substring    ${dados}[0]    12    14   
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_HIST_INTERVENCOES}    ${dados}[1]    ${cpfCnpj}    ${digito}
    ${result}      Query            ${query}
    [Return]       ${result}