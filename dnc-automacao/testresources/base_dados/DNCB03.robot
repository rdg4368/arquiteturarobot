*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String    

*** Variables ***
${SQL_CONSULTAR_INCLUSAO_ADVERTENCIA}
...    SELECT * FROM DNCB00.DNCB03 WHERE C0300_ADDCOD = {0} AND C0300_NDACOD = 'BCR' AND C0300_NDATIP = 'A'

*** Keywords ***
Validar Dados Da Inclusao Advertencia
    [Arguments]    ${addcod}
    ${query}       Format String     ${SQL_CONSULTAR_INCLUSAO_ADVERTENCIA}    ${addcod}    
    ${result}      Run Keyword And Return Status     Check If Exists In Database    ${query}    
    [Return]       ${result}

Deletar Dados Da Inclusao Advertencia
    [Arguments]    ${addcod}
    ${result}      Run Keyword And Return Status  Execute Sql String    DELETE FROM DNCB00.DNCB03 WHERE C0300_ADDCOD = ${addcod} AND C0300_NDACOD = 'BCR' AND C0300_NDATIP = 'A'    
    [Return]       ${result} 

Consultar Dados Da Natureza Da Advertencia
    ${result}     Query 
    ...  SELECT LPAD(C0300_ADDCOD,2,'0'), TRIM(C0300_NDACOD), TRIM(SUBSTR(C0300_NDADES,1,34)) FROM DNCB00.DNCB03 ORDER BY C0300_ADDCOD ASC
    [Return]    ${result}

Validar Dados Da Origem Da Advertencia
    [Arguments]    ${addcod}
    ${result}     Run Keyword And Return Status     Check If Exists In Database 
    ...  SELECT * FROM DNCB00.DNCB03 WHERE C0300_ADDCOD = ${addcod} AND C0300_NDACOD = 'BCR' AND C0300_NDATIP = 'A' 
    [Return]    ${result}
    
Recuperar Dados Da Natureza da Advertencia
    [Arguments]   ${descricao}  ${pto}  ${tip} 
    ${result}     Run Keyword And Return Status     Check If Exists In Database
    ...  SELECT * FROM DNCB00.DNCB03 WHERE C0300_NDADES = '${descricao}' AND C0300_NDAPTOIND = '${pto}' AND C0300_NDATIP = '${tip}'
    [Return]    ${result} 
    
