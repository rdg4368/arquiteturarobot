*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String

*** Variables ***
${SQL_VALIDAR_INCLUIR_CADASTRO_EMAIL}             SELECT * FROM DNCB00.DNCBB1 WHERE CB100_SISCODSIS = '{0}' AND CB100_MDBEMA = '{1}'  

${SQL_DESFAZER_INCLUIR_CADASTRO_EMAIL}            DELETE FROM DNCB00.DNCBB1 WHERE CB100_SISCODSIS = '{0}' AND CB100_MDBEMA = '{1}'  

${SQL_INSERT_CONSULTA_CADASTRO_EMAIL}             INSERT INTO DNCB00.DNCBB1 (CB100_HDRDATA,CB100_HDRHORA,CB100_HDRESTACAO,CB100_HDRPROGRAMA,STATGER,CB100_MDBDATCADEM,CB100_MDBHORCADEM,CB100_USUNUM,CB100_SISCODSIS,CB100_MDBEMA )
...                                               VALUES  ('20191126','183038','400','DNCNG8','0','20191126','143038','644971','CTU','AUTOMACAOBRB@RSI.COM.BR') 

${SQL_VALIDAR_CONSULTA_CADASTRO_EMAIL}            SELECT TRIM(CB100_MDBEMA), (SUBSTR(CHAR(CB100_MDBDATCADEM),7,2))||'/'||SUBSTR(CHAR ( CB100_MDBDATCADEM ),5,2)||'/'||SUBSTR(CHAR (CB100_MDBDATCADEM ),1,4), TRIM(CB100_USUNUM) FROM DNCB00.DNCBB1 WHERE CB100_SISCODSIS = '{0}' FETCH FIRST 10 ROW ONLY 

${SQL_VALIDAR_ALTERAR_CADASTRO_EMAIL}             SELECT  * FROM DNCB00.DNCBB1 WHERE CB100_MDBEMA = '{0}' AND CB100_SISCODSIS = 'CTU'

${SQL_DADO_ROLLBACK_ALTERAR_CADASTRO_EMAIL}       SELECT  TRIM (CB100_MDBEMA) FROM DNCB00.DNCBB1 WHERE CB100_SISCODSIS = 'CTU' FETCH FIRST ROW ONLY

${SQL_DESFAZER_ALTERAR_CADASTRO_EMAIL}            UPDATE DNCB00.DNCBB1 SET CB100_MDBEMA = '{0}' WHERE CB100_MDBEMA = '{1}' AND CB100_SISCODSIS = 'CTU'

${SQL_DADOS_ROLLBACK_EXCLUIR_CADASTRO_EMAIL}      SELECT  TRIM(CB100_HDRDATA),TRIM (CB100_HDRHORA),TRIM(CB100_HDRESTACAO),TRIM(CB100_HDRPROGRAMA),TRIM (STATGER),
...                                               TRIM (CB100_MDBDATCADEM), TRIM (CB100_MDBHORCADEM), TRIM (CB100_USUNUM), TRIM (CB100_SISCODSIS), TRIM (CB100_MDBEMA) FROM DNCB00.DNCBB1 WHERE CB100_SISCODSIS = 'CTU' FETCH FIRST ROW ONLY

${SQL_VALIDAR_EXCLUIR_CADASTRO_EMAIL}             SELECT  * FROM DNCB00.DNCBB1 WHERE CB100_MDBEMA = '{0}' AND CB100_SISCODSIS = '{1}'

${SQL_DESFAZER_EXCLUIR_CADASTRO_EMAIL}            INSERT INTO DNCB00.DNCBB1 (CB100_HDRDATA,CB100_HDRHORA,CB100_HDRESTACAO,CB100_HDRPROGRAMA,STATGER,CB100_MDBDATCADEM,CB100_MDBHORCADEM,CB100_USUNUM,CB100_SISCODSIS,CB100_MDBEMA )
...                                               VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}') 


*** Keywords ***
Validar Incluir Cadastro Emails
    [Arguments]    ${dadosValidar}
    ${query}      Format String    ${SQL_VALIDAR_INCLUIR_CADASTRO_EMAIL}    ${dadosValidar}[0]    ${dadosValidar}[1]
    ${status}     Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]      ${status}
    
Desfazer Incluir Cadastro Emails
    [Arguments]    ${dados}
    ${query}      Format String    ${SQL_DESFAZER_INCLUIR_CADASTRO_EMAIL}    ${dados}[0]    ${dados}[1]
    ${status}     Run Keyword And Return Status    Execute Sql String    ${query}  
    [Return]      ${status}
    
Insert Consulta Cadastro Emails
    ${status}    Run Keyword And Return Status    Execute Sql String    ${SQL_INSERT_CONSULTA_CADASTRO_EMAIL}        
    [Return]     ${status}
    
Validar Consulta Cadastro Emails
    [Arguments]   ${sistema}
    ${query}      Format String    ${SQL_VALIDAR_CONSULTA_CADASTRO_EMAIL}    ${sistema}
    ${result}     Query            ${query}    
    [Return]      ${result}
    
Validar Alterar Cadastro Email
    [Arguments]    ${validar}
    ${query}       Format String    ${SQL_VALIDAR_ALTERAR_CADASTRO_EMAIL}    ${validar}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}        
    [Return]       ${status}
    
Recuperar Dado Rollback Alterar Cadastro De Emails
    ${result}    Query    ${SQL_DADO_ROLLBACK_ALTERAR_CADASTRO_EMAIL}    
    [Return]     ${result}[0][0]
    
    
Desfazer Alterar Cadastro Email    
    [Arguments]    ${dadoRollback}    ${emailAlterar} 
    ${query}       Format String    ${SQL_DESFAZER_ALTERAR_CADASTRO_EMAIL}    ${dadoRollback}    ${emailAlterar} 
    ${status}      Run Keyword And Return Status    Execute Sql String        ${query}            
    [Return]       ${status}
    
Recuperar Dados Rollback Excluir Cadastro De Emails
    ${result}      Query    ${SQL_DADOS_ROLLBACK_EXCLUIR_CADASTRO_EMAIL}
    [Return]       ${result}[0]
    
Validar Excluir Cadastro Emails
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_VALIDAR_EXCLUIR_CADASTRO_EMAIL}    ${dados}[8]    ${dados}[9]
    ${status}      Run Keyword And Return Status    Check If Not Exists In Database    ${query}        
    [Return]       ${status}
    
Desfazer Excluir Cadastro Emails
    [Arguments]    ${dados}
    ${query}       Format String    ${SQL_DESFAZER_EXCLUIR_CADASTRO_EMAIL}    ${dados}[0]    ${dados}[1]    ${dados}[2]    ${dados}[3]    ${dados}[4]    ${dados}[5]
    ...                                                                       ${dados}[6]    ${dados}[7]    ${dados}[8]    ${dados}[9]
    ${status}      Run Keyword And Return Status    Execute Sql String        ${query}
    [Return]       ${status}