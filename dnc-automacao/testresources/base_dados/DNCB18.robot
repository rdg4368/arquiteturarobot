*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      Collections      
Library      String   

*** Variables ***
${SQL_VALIDAR_CONSULTA_SALDO_APA_CC}
...     SELECT ROUND(DECIMAL(SUM(C1800_SMPSLDMED) , 24, 2) , 1) / 12 AS MEDIA_APA FROM DNCB00.DNCB18 
...     WHERE C1800_CTCCHVCONTA = {0} AND C1800_SMPDATREF >= CONCAT(YEAR(CURRENT_DATE - 1 YEAR) , 
...     VARCHAR_FORMAT(CURRENT TIMESTAMP, 'MM'))

${SQL_VALIDAR_CONSULTA_SALDO_APA_CLTCOD}
...     SELECT ROUND(DECIMAL(SUM(C1800_SMPSLDMED) , 24, 2) , 1) / 12 AS MEDIA_FFA FROM DNCB00.DNCB18 WHERE C1800_CLTCOD = {0}
...     AND C1800_SMPDATREF >= CONCAT(YEAR(CURRENT_DATE - 1 YEAR) , VARCHAR_FORMAT(CURRENT TIMESTAMP, 'MM'))

${SQL_RECUPERAR_CC_CONSULTAR_SALDO_MENSAL_PP_CC}
...     SELECT LPAD(C1800_CTCCHVCONTA,	10,	'0') AS CONTA FROM DNCB00.DNCB18 WHERE C1800_HDRDATA > 190101 FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_SALDO_MENSAL_PP_POR_CC}
...     SELECT  TRIM (X0100_CLTNOM) AS NOME,  LPAD(X0100_CLTCOD,8,'0') AS CODIGO_CLIENTE,  C1800_SMPSLD AS Saldo_Medio
...     FROM DNCB00.DNCB18, AOXB00.AOXB01 WHERE C1800_CTCCHVCONTA = {0} AND C1800_CLTCOD = X0100_CLTCOD FETCH FIRST 12 ROWS ONLY

${SQL_RECUPERAR_CLTCOD_CONSULTAR_SALDO_MENSAL_PP_CC}
...     SELECT LPAD(C1800_CLTCOD, 8, '0') FROM DNCB00.DNCB18 WHERE C1800_HDRDATA > 190101 FETCH FIRST ROWS ONLY

${SQL_RECUPERAR_CONTA_SALDO_MENSAL_APLICACAO}
...     SELECT LPAD(C3100_CTCCHVCONTA,	10,	'0') FROM DNCB00.DNCB31 WHERE C3100_HDRDATA > 190101 FETCH FIRST ROW ONLY 

${SQL_RECUPERAR_CLTCOD_CONSULTAR_SALDO_MENSAL}
...     SELECT LPAD(C3100_CLTCOD,8,'0') FROM DNCB00.DNCB31 WHERE C3100_HDRDATA > 190101 FETCH FIRST ROWS ONLY

${SQL_CONSULTA_MENSAL_POUPANCA_CLIENTE}
...    SELECT
...    	TRIM(C1800_CLTCOD),
...    	TRIM(X0100_CLTCGC),
...    	TRIM(X0100_CLTCGCDIG),
...    	X0100_CLTNOM
...    FROM
...    	DNCB00.DNCB18 INNER JOIN 
...    	AOXB00.AOXB01 ON (C1800_CLTCOD = X0100_CLTCOD)
...    WHERE
...    	C1800_HDRDATA > 190101
...    	AND C1800_CLTCOD = {0}
...    	FETCH FIRST ROW ONLY

*** Keywords ***
Consulta Saldo Medio APA Por CC
    [Arguments]   ${contaCorrente}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_APA_CC}    ${contaCorrente}    
    ${result}     Query                ${query}    
    [Return]      ${result}[0][0]
    
Consulta Saldo Medio APA Por Cleinte
    [Arguments]   ${cltCod}    
    ${query}      Format String        ${SQL_VALIDAR_CONSULTA_SALDO_APA_CLTCOD}    ${cltCod}    
    ${result}     Query                ${query}    
    [Return]      ${result}[0][0]
    
Recuperar Conta Corrente Consultar Saldo Mensal Poupanca Por CC
    ${result}    Query    ${SQL_RECUPERAR_CC_CONSULTAR_SALDO_MENSAL_PP_CC}    
    [Return]     ${result}[0][0]
    
Consulta Saldo Mensal Poupanca por Conta Corrente
    [Arguments]    ${contaCorrente}
    ${query}       Format String    ${SQL_VALIDAR_CONSULTA_SALDO_MENSAL_PP_POR_CC}    ${contaCorrente}
    ${result}      Query            ${query}
    [Return]       ${result}
    
Recuperar Cltcod Consultar Saldo Mensal Poupanca
    ${result}    Query        ${SQL_RECUPERAR_CLTCOD_CONSULTAR_SALDO_MENSAL_PP_CC}
    [Return]     ${result}[0][0]

Recuperar Dados Consultar Saldo Mensal Aplicações Por Conta
    ${result}    Query        ${SQL_RECUPERAR_CONTA_SALDO_MENSAL_APLICACAO}
    [Return]     ${result}[0][0]
    
Recuperar Cltcod Consultar Saldo Mensal de Fundos Por Cliente
    ${result}    Query        ${SQL_RECUPERAR_CLTCOD_CONSULTAR_SALDO_MENSAL}
    [Return]     ${result}[0][0]
    
Recuperar Cltcod Consultar Saldo Mensal CDB Por Cliente
    ${result}    Query        ${SQL_RECUPERAR_CLTCOD_CONSULTAR_SALDO_MENSAL}
    [Return]     ${result}[0][0]
    
Recuperar Cltcod Consultar Saldo Mensal Todas Contas
    ${result}    Query        ${SQL_RECUPERAR_CLTCOD_CONSULTAR_SALDO_MENSAL}
    [Return]     ${result}[0][0]
    
Validar consulta Saldo Mensal Poupanca por Cliente
    [Arguments]    ${codCliente}
    ${query}    Format String    ${SQL_CONSULTA_MENSAL_POUPANCA_CLIENTE}    ${codCliente}
    ${result}    Query    ${query}
    [Return]      ${result} 