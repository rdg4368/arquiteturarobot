*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
   

*** Keywords ***
Detalhar Informações Novo SCR Bacen
    [Arguments]     ${cpf_cpnj}   
    ${result}       Query 
    ...  SELECT LPAD(C1900_CLTCOD,8,'0'), X0100_CLTNOM, LPAD(C1900_INFDATBAS,6,'0'), LPAD(C1900_INFDATCON,8,'0'), C1900_INFPCTDOCP, C1900_INFPCTVOLP FROM DNCB00.DNCB19 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD=C1900_CLTCOD) WHERE C1900_CLTCGC = ${cpf_cpnj}[0][0] AND C1900_CLTCGCDIG = ${cpf_cpnj}[0][1] AND C1900_INFDATCON = (SELECT MAX(C1900_INFDATCON) FROM DNCB00.DNCB19 WHERE C1900_CLTCGC = ${cpf_cpnj}[0][0] AND C1900_CLTCGCDIG = ${cpf_cpnj}[0][1])
    [Return]        ${result}
    
