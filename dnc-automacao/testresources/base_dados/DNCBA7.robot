*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String       

*** Variables ***
${SQL_DADOS_ACESSO_INCLUIR_ALTERAR_CONTRATO_BLOQ_JUDICIAL}         SELECT LPAD(CA700_CLTCGC,12,'0')||LPAD(CA700_CLTCGCDIG,2,'0'), CA700_CCSCOD, CA700_SISCODSIS, LPAD(CA700_PRUCOD,4,'0'), LPAD(CA700_OPVMOD,4,'0') 
...                                                                FROM DNCB00.DNCBA7 INNER JOIN CTRB00.CTRB09 ON (CA700_CCSCOD = R0900_CCSCOD ) WHERE CA700_CLTCGC NOT IN (SELECT CA700_CLTCGC FROM DNCB00.DNCBA7 WHERE CA700_COBSTAT = 1 ) FETCH FIRST ROW ONLY 

${SQL_VALIDAR_INCLUIR_CONTRATO_BLOQ_JUDICIAL}                      SELECT * FROM DNCB00.DNCBA7 WHERE CA700_CLTCGC = '{0}' AND CA700_CLTCGCDIG = '{1}' AND CA700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AND CA700_COBSTAT = 1

${SQL_DESFAZER_INCLUIR_CONTRATO_BLOQ_JUDICIAL}                     DELETE FROM DNCB00.DNCBA7 WHERE CA700_CLTCGC = '{0}' AND CA700_CLTCGCDIG = '{1}' AND CA700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AND CA700_COBSTAT = 1

${SQL_VALIDAR_ALTERAR_CONTRATO_BLOQUEIO_JUDICIAL}                  SELECT * FROM DNCB00.DNCBA7 WHERE CA700_CBSNUMPRO = '{0}' AND CA700_CBSCAMOBS = '{1}' AND CA700_CLTCGC = '{2}' AND CA700_CLTCGCDIG = '{3}' AND  CA700_COBSTAT = 1 AND CA700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD')

${SQL_DESFAZER_ALTERAR_CONTRATO_BLOQUEIO_JUDICIAL}                 DELETE FROM DNCB00.DNCBA7 WHERE CA700_CBSNUMPRO = '{0}' AND CA700_CBSCAMOBS = '{1}' AND CA700_CLTCGC = '{2}' AND CA700_CLTCGCDIG = '{3}' AND CA700_COBSTAT = 1 AND CA700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD')

${SQL_DADOS_ACESSO_EXCLUIR_CONTRATO_BLOQ_JUDICIAL}                 SELECT LPAD(CA700_CLTCGC,12,'0')||LPAD(CA700_CLTCGCDIG,2,'0'), CA700_CCSCOD, CA700_SISCODSIS, LPAD(CA700_PRUCOD,4,'0'), LPAD(CA700_OPVMOD,4,'0') FROM DNCB00.DNCBA7 WHERE 
...                                                                CA700_CLTCGC NOT IN (SELECT CA700_CLTCGC FROM DNCB00.DNCBA7 WHERE CA700_COBSTAT = 2 ) FETCH FIRST ROW ONLY

${SQL_VALIDAR_EXCLUSAO_CONTRATO_BLOQ_JUDICIAL}                     SELECT * FROM DNCB00.DNCBA7 WHERE CA700_CLTCGC = '{0}' AND CA700_CLTCGCDIG = '{1}' AND CA700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AND CA700_COBSTAT = '2'

${SQL_DESFAZER_EXCLUSAO_CONTRATO_BLOQ_JUDICIAL}                    DELETE FROM DNCB00.DNCBA7 WHERE CA700_CLTCGC = '{0}' AND CA700_CLTCGCDIG = '{1}' AND CA700_HDRDATA = VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AND CA700_COBSTAT = '2'

${SQL_DADOS_ACESSO_CONSULTAR_CONTRATO_BLOQ_JUD}                    SELECT LPAD(CA700_CLTCGC,12,'0')||LPAD(CA700_CLTCGCDIG,2,'0'), CA700_CCSCOD, CA700_SISCODSIS, LPAD(CA700_PRUCOD,4,'0'), LPAD(CA700_OPVMOD,4,'0') FROM DNCB00.DNCBA7 ORDER BY SEDIDR DESC FETCH FIRST ROW ONLY




*** Keywords ***
Recuperar Dados Acesso Incluir Alterar Contrato Bloqueio Judicial
    ${result}    Query    ${SQL_DADOS_ACESSO_INCLUIR_ALTERAR_CONTRATO_BLOQ_JUDICIAL}    
    [Return]     ${result}[0]
    
Validar Inclusao Contrato Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}     0    12
    ${digito}      Get Substring    ${dados}    12    14
    ${query}       Format String    ${SQL_VALIDAR_INCLUIR_CONTRATO_BLOQ_JUDICIAL}    ${cpf_cnpj}    ${digito}
    ${status}      Run Keyword And Return Status    Check If Exists In Database      ${query}        
    [Return]       ${status}    
    
Desfazer Inclusao Contrato Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}     0    12
    ${digito}      Get Substring    ${dados}    12    14
    ${query}       Format String    ${SQL_DESFAZER_INCLUIR_CONTRATO_BLOQ_JUDICIAL}    ${cpf_cnpj}    ${digito}
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]       ${status}


Validar Alteracao Contrato Bloqueio Judicial 
    [Arguments]    ${dadosAcesso}      ${nrProcesso}        ${obsAlteracao}
    ${cpf_cnpj}    Get Substring       ${dadosAcesso}     0    12
    ${digito}      Get Substring       ${dadosAcesso}    12    14  
    ${query}       Format String       ${SQL_VALIDAR_ALTERAR_CONTRATO_BLOQUEIO_JUDICIAL}      ${nrProcesso}        ${obsAlteracao}    ${cpf_cnpj}    ${digito}
    ${status}      Run Keyword And Return Status    Check If Exists In Database      ${query}        
    [Return]       ${status}    
    
Desfazer Alteracao Contrato Bloqueio Judicial
    [Arguments]       ${dadosAcesso}   ${nrProcesso}        ${obsAlteracao}
    ${cpf_cnpj}    Get Substring       ${dadosAcesso}     0    12
    ${digito}      Get Substring       ${dadosAcesso}    12    14  
    ${query}       Format String       ${SQL_DESFAZER_ALTERAR_CONTRATO_BLOQUEIO_JUDICIAL}      ${nrProcesso}        ${obsAlteracao}    ${cpf_cnpj}    ${digito}
    ${status}      Run Keyword And Return Status    Execute Sql String      ${query}        
    [Return]       ${status}  
    
Recuperar Dados Acesso Excluir Contrato Bloqueio Judicial
    ${result}      Query    ${SQL_DADOS_ACESSO_EXCLUIR_CONTRATO_BLOQ_JUDICIAL}    
    [Return]       ${result}[0]
    
Validar Exclusao Contrato Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring       ${dados}[0]     0    12
    ${digito}      Get Substring       ${dados}[0]    12    14  
    ${query}       Format String       ${SQL_VALIDAR_EXCLUSAO_CONTRATO_BLOQ_JUDICIAL}    ${cpf_cnpj}    ${digito}    
    ${status}      Run Keyword And Return Status    Check If Exists In Database      ${query}        
    [Return]       ${status}
    
Desfazer Exclusao Contrato Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring       ${dados}[0]     0    12
    ${digito}      Get Substring       ${dados}[0]    12    14   
    ${query}       Format String       ${SQL_DESFAZER_EXCLUSAO_CONTRATO_BLOQ_JUDICIAL}    ${cpf_cnpj}    ${digito}        
    ${status}      Run Keyword And Return Status    Execute Sql String      ${query}        
    [Return]       ${status}   
    
Recuperar Dados Acesso Consultar Contrato Bloqueio Judicial
    ${result}      Query    ${SQL_DADOS_ACESSO_CONSULTAR_CONTRATO_BLOQ_JUD}    
    [Return]       ${result}[0]