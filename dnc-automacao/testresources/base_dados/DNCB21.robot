*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  

*** Variables ***
${SQL_RECUPERAR_CONSULTAR_CLASSIFICACAO_CLIENTES}
...    SELECT TRIM(C2300_CLTCLSNEG) AS APPLICATION_BEHAVIOUR, TRIM(C2300_CLTTIP) AS TIPO_CLIENTE
...    FROM DNCB00.DNCB21, DNCB00.DNCB23 WHERE C2300_CLTTIP <> 0 AND C2300_CLTCLSNEG != 0
...    FETCH FIRST 1 ROWS ONLY
    
${SQL_VALIDAR_DADO_TELA}
...    SELECT DISTINCT TRIM(C2100_CLTCODSEG) AS SEGMENTO_CLIENTE, LPAD(C2300_CCLVERSAO,4,'0') AS VERSAO,TRIM(C2300_CCLDATINI) AS DATA_INICIO, 
...                    LPAD(C2300_CCLHORINI,6,'0') AS HORA_INICIO, TRIM(C2300_CLTCLSNEG) AS COD_PORTE, TRIM(C2300_CLTTIP) AS TIPO_CLIENTE, 
...                    LPAD(C2300_CCLCLA,3,'0') AS CLASSIFICACAO
...    FROM DNCB00.DNCB23, DNCB00.DNCB21 WHERE C2300_CCLHORINI = 084728 FETCH FIRST 4 ROWS ONLY
 
*** Keywords ***
Recuperar Dados Consultar Classificacao Clientes
    ${result}    Query    ${SQL_RECUPERAR_CONSULTAR_CLASSIFICACAO_CLIENTES}    
    [Return]     ${result}
    
Recuperar Consulta Classificacao Clientes
    ${result}    Query    ${SQL_VALIDAR_DADO_TELA}
    [Return]    ${result}
