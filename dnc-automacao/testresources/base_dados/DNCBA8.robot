*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem 
Library      String       

*** Variables ***
${SQL_RECUPERAR_DADOS_INCLUIR_BLOQ_JUDICIAL}                SELECT LPAD(X0100_CLTCGC,12,'0') || LPAD (X0100_CLTCGCDIG,2,'0') AS CPF_CNPJ, '01' AS MOTIVO,  VARCHAR_FORMAT(CURRENT_DATE,'DDMMYYYY') AS DATA_MOTIVO,
...                                                         VARCHAR_FORMAT(CURRENT_DATE,'DDMMYYYY') AS DATA_INVERTIDA  FROM AOXB00.AOXB01 INNER JOIN CTRB00.CTRB09  ON (X0100_CLTCOD = R0900_CLTCOD ) 
...                                                         WHERE X0100_CLTCGC NOT IN  (SELECT CA800_CLTCGC FROM DNCB00.DNCBA8) AND X0100_CLTCGCDIG NOT IN  (SELECT CA800_CLTCGCDIG FROM DNCB00.DNCBA8) FETCH FIRST ROWS ONLY
 

${SQL_VALIDAR_INCLUIR_CLIENTE_BLOQUEIO_JUDICIAL}            SELECT * FROM DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' AND CA800_CBSNUMPRO = '{2}' AND  CA800_CBSCAMOBS = '{3}'

${SQL_DESFAZER_INCLUIR_CLIENTE_BLOQUEIO_JUDICIAL}           DELETE FROM DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' AND CA800_CBSNUMPRO = '{2}' AND  CA800_CBSCAMOBS = '{3}'
 
${SQL_RECUPERAR_DADOS_ALTERAR_BLOQ_JUDICIAL}                SELECT LPAD(CA800_CLTCGC,12,'0')|| LPAD(CA800_CLTCGCDIG,2,'0') FROM DNCB00.DNCBA8 WHERE CA800_CBSSTATUS = 2  FETCH FIRST ROW ONLY
 
${SQL_VALIDAR_ALTERACAO_CLIENTE_BLOQQ_JUDICIAL}             SELECT * FROM DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' AND CA800_CBSNUMPRO   = '{2}' AND CA800_CBSCAMOBS  = '{3}' AND CA800_CBSSTATUS = 1

${SQL_DESFAZER_ALTERACAO_CLIENTE_BLOQ_JUDICIAL}             DELETE FROM DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' AND CA800_CBSNUMPRO   = '{2}' AND CA800_CBSCAMOBS  = '{3}' AND CA800_CBSSTATUS = 1
      
${SQL_RECUPERAR_DADOS_ACESSO_EXCLUIR_CLIENTE_BLOQ_JUD}       SELECT LPAD(CA800_CLTCGC,12,'0')|| LPAD(CA800_CLTCGCDIG,2,'0') AS CPF_CNPJ_ACESSO,
...                                                          VARCHAR_FORMAT(CURRENT_DATE,'YYYYMMDD') AS DATA_INSERIDA
...                                                          FROM DNCB00.DNCBA8  WHERE CA800_CLTCGC NOT IN  (SELECT CA800_CLTCGC FROM DNCB00.DNCBA8 WHERE CA800_CBSSTATUS = '2')  FETCH FIRST ROW ONLY 

${SQL_VALIDAR_EXCLUSAO_CLIENTE_BLOQ_JUD}                     SELECT * FROM  DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' AND CA800_HDRDATA = '{2}' AND CA800_CBSSTATUS = '2'

${SQL_DESFAZER_EXCLUSAO_CLIENTE_BLOQ_JUD}                    DELETE FROM  DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' AND CA800_HDRDATA = '{2}' AND CA800_CBSSTATUS = '2'

${SQL_DADO_ACESSO_CONSULTA_CLIENTE_BLOQ_JUDCIAL}             SELECT LPAD(CA800_CLTCGC,12,'0')|| LPAD(CA800_CLTCGCDIG,2,'0')  FROM DNCB00.DNCBA8  FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTAR_CLIENTE_BLOQ_JUD}                    SELECT LPAD(CA800_CLTCGC,12,'0') ||' - '|| LPAD(CA800_CLTCGCDIG,2,'0')AS CPF_CNPJ, LPAD(CA800_USUNUM,6,'0') AS USUARIO, 
...                                                          (SUBSTR(CHAR(CA800_HDRDATA),7,2))||'/'||SUBSTR(CHAR ( CA800_HDRDATA ),5,2)||'/'||SUBSTR(CHAR (CA800_HDRDATA ),1,4)AS DATA_INCLUSAO, 
...                                                          (SUBSTR(CHAR(CA800_CBSHORINC),1,2))||':'||(SUBSTR(CHAR(CA800_CBSHORINC),3,2))||':'||(SUBSTR(CHAR(CA800_CBSHORINC),5,2)),
...                                                          CASE WHEN CA800_CBSSTATUS = 1 THEN 'ATIVO'ELSE 'INATIVO'END AS STATUS, CASE WHEN CA800_CBSMOTIVO = 1 THEN 'LIMINAR'ELSE 'ACORDO NOS AUTOS' END AS MOTIVO, 
...                                                          (SUBSTR(CHAR(CA800_CBSDATMOT),7,2))||'/'||SUBSTR(CHAR ( CA800_CBSDATMOT ),5,2)||'/'||SUBSTR(CHAR (CA800_CBSDATMOT ),1,4) AS DATA_MOTIVO, 
...                                                          CA800_CBSNUMPRO FROM DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' ORDER BY SEDIDR DESC FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTAR_HISTORICO_CLIENTE_BLOQ_JUD}          SELECT LPAD(CA800_CLTCGC,12,'0') ||' - '|| LPAD(CA800_CLTCGCDIG,2,'0')AS CPF_CNPJ,  LPAD(CA800_USUNUM,6,'0') AS USUARIO, 
...                                                          (SUBSTR(CHAR(CA800_HDRDATA),7,2))||'/'||SUBSTR(CHAR ( CA800_HDRDATA ),5,2)||'/'||SUBSTR(CHAR (CA800_HDRDATA ),1,4)AS DATA_INCLUSAO, 
...                                                          (SUBSTR(CHAR(CA800_CBSHORINC),1,2))||':'||(SUBSTR(CHAR(CA800_CBSHORINC),3,2))||':'||(SUBSTR(CHAR(CA800_CBSHORINC),5,2)),
...                                                          CASE WHEN CA800_CBSSTATUS = 1 THEN 'ATIVO'ELSE 'INATIVO'END AS STATUS, CASE WHEN CA800_CBSMOTIVO = 1 THEN 'LIMINAR'ELSE 'ACORDO NOS AUTOS' END AS MOTIVO, 
...                                                          (SUBSTR(CHAR(CA800_CBSDATMOT),7,2))||'/'||SUBSTR(CHAR ( CA800_CBSDATMOT ),5,2)||'/'||SUBSTR(CHAR (CA800_CBSDATMOT ),1,4) AS DATA_MOTIVO
...                                                          FROM DNCB00.DNCBA8 WHERE CA800_CLTCGC = '{0}' AND CA800_CLTCGCDIG = '{1}' ORDER BY SEDIDR ASC FETCH FIRST 8 ROW ONLY

${SQL_VALIDAR_CONSULTAR_CONTRATO_BLOQ_JUD}                   SELECT LPAD(CA700_CLTCGC,12,'0') ||' '|| LPAD(CA700_CLTCGCDIG,2,'0')AS CPF_CNPJ, LPAD(CA700_USUNUM,6,'0') AS USUARIO, 
...                                                          (SUBSTR(CHAR(CA700_HDRDATA),7,2))||'/'||SUBSTR(CHAR ( CA700_HDRDATA ),5,2)||'/'||SUBSTR(CHAR (CA700_HDRDATA ),1,4)AS DATA_INCLUSAO, 
...                                                          CASE WHEN CA700_COBSTAT = 1 THEN 'ATIVO'ELSE 'INATIVO'END AS STATUS, CASE WHEN  CA700_COBMOT = 1 THEN 'LIMINAR'ELSE 'ACORDO NOS AUTOS' END AS MOTIVO, 
...                                                          (SUBSTR(CHAR(CA700_COBDATMOT),7,2))||'/'||SUBSTR(CHAR ( CA700_COBDATMOT ),5,2)||'/'||SUBSTR(CHAR (CA700_COBDATMOT ),1,4) AS DATA_MOTIVO,LPAD(CA700_OPVMOD,4,'0'), CHAR (CA700_SISCODSIS),
...                                                          TRIM(CA700_CBSNUMPRO),TRIM (CA700_CCSCOD) FROM DNCB00.DNCBA7 WHERE CA700_CLTCGC = '{0}' AND CA700_CLTCGCDIG = '{1}' ORDER BY SEDIDR DESC FETCH FIRST 1 ROW ONLY

${SQL_VALIDAR_CONSULTAR_HISTORICO_CONTRATO_BLOQ_JUD}         SELECT (SUBSTR(CHAR(CA700_HDRDATA),7,2))||'/'||SUBSTR(CHAR ( CA700_HDRDATA ),5,2)||'/'||SUBSTR(CHAR (CA700_HDRDATA ),1,4)AS DATA_INCLUSAO, 
...                                                          CASE WHEN CA700_COBSTAT = 1 THEN 'ATIVO'ELSE 'INATIVO'END AS STATUS, CASE WHEN  CA700_COBMOT = 1 THEN 'LIMINAR'ELSE 'ACORDO NOS AUTOS' END AS MOTIVO, 
...                                                          (SUBSTR(CHAR(CA700_COBDATMOT),7,2))||'/'||SUBSTR(CHAR ( CA700_COBDATMOT ),5,2)||'/'||SUBSTR(CHAR (CA700_COBDATMOT ),1,4) AS DATA_MOTIVO,
...                                                          LPAD(CA700_OPVMOD,4,'0'), CHAR (CA700_SISCODSIS),TRIM (CA700_CCSCOD) 
...                                                          FROM DNCB00.DNCBA7 WHERE CA700_CLTCGC = '{0}' AND CA700_CLTCGCDIG = '{1}' AND CA700_CCSCOD = '{2}'  ORDER BY SEDIDR ASC FETCH FIRST 8 ROW ONLY




*** Keywords ***
Recuperar Dados Acesso Incluir Cliente Bloqueio Judicial
    ${result}    Query    ${SQL_RECUPERAR_DADOS_INCLUIR_BLOQ_JUDICIAL}
    [Return]     ${result}[0]
    
Validar Incluir Cliente Bloqueio Judicial
    [Arguments]    ${dadosInclusao}    ${nrProcesso}    ${observacao}
    ${cpf_cnpj}    Get Substring       ${dadosInclusao}[0]     0    12
    ${digito}      Get Substring       ${dadosInclusao}[0]    12    14
    ${query}       Format String       ${SQL_VALIDAR_INCLUIR_CLIENTE_BLOQUEIO_JUDICIAL}    ${cpf_cnpj}    ${digito}    ${nrProcesso}    ${observacao}
    ${status}      Run Keyword And Return Status    Check If Exists In Database            ${query}        
    [Return]       ${status}
    
Desfazer Incluir Cliente Bloqueio Judicial
    [Arguments]    ${dadosInclusao}    ${nrProcesso}    ${observacao}
    ${cpf_cnpj}    Get Substring       ${dadosInclusao}[0]     0    12
    ${digito}      Get Substring       ${dadosInclusao}[0]    12    14    
    ${query}       Format String       ${SQL_DESFAZER_INCLUIR_CLIENTE_BLOQUEIO_JUDICIAL}    ${cpf_cnpj}    ${digito}    ${nrProcesso}    ${observacao}
    ${status}      Run Keyword And Return Status    Execute Sql String                      ${query}        
    [Return]       ${status}    
    
Recuperar Dado Acesso Alterar Cliente Bloqueio Judicial
    ${result}    Query    ${SQL_RECUPERAR_DADOS_ALTERAR_BLOQ_JUDICIAL}   
    [Return]     ${result}[0][0]
    
Validar Alteracao Cliente Bloqueio Judicial 
    [Arguments]     ${dados}    ${nrProcesso}        ${observacao}
    ${cpf_cnpj}     Get Substring         ${dados}    0    12
    ${digito}       Get Substring         ${dados}    12    14  
    ${query}        Format String         ${SQL_VALIDAR_ALTERACAO_CLIENTE_BLOQQ_JUDICIAL}     ${cpf_cnpj}    ${digito}    ${nrProcesso}        ${observacao}
    ${status}       Run Keyword And Return Status   Check If Exists In Database      ${query}
    [Return]        ${status}
    
Desfazer Alteracao Cliente Bloqueio Judicial
    [Arguments]    ${dados}        ${nrProcesso}        ${obsAlteracao}
    ${cpf_cnpj}    Get Substring    ${dados}     0    12
    ${digito}      Get Substring    ${dados}    12    14    
    ${query}       Format String    ${SQL_DESFAZER_ALTERACAO_CLIENTE_BLOQ_JUDICIAL}     ${cpf_cnpj}    ${digito}    ${nrProcesso}    ${obsAlteracao}
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]       ${status}
    
Recuperar Dados Acesso Excluir Cliente Bloqueio Judicial
    ${result}      Query    ${SQL_RECUPERAR_DADOS_ACESSO_EXCLUIR_CLIENTE_BLOQ_JUD}    
    [Return]       ${result}[0]
    
Validar Exclusao Cliente Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]     0    12
    ${digito}      Get Substring    ${dados}[0]    12    14  
    ${query}       Format String    ${SQL_VALIDAR_EXCLUSAO_CLIENTE_BLOQ_JUD}       ${cpf_cnpj}    ${digito}    ${dados}[1]
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${query}        #exclusao inclui novo registro com status = 2
    [Return]       ${status}
    
Desfazer Exclusao Cliente Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]     0    12
    ${digito}      Get Substring    ${dados}[0]    12    14  
    ${query}       Format String    ${SQL_DESFAZER_EXCLUSAO_CLIENTE_BLOQ_JUD}       ${cpf_cnpj}    ${digito}    ${dados}[1]
    ${status}      Run Keyword And Return Status    Execute Sql String    ${query}        
    [Return]       ${status}    
    
Recuperar Dados Acesso Consultar Cliente Bloqueio Judicial
    ${result}      Query    ${SQL_DADO_ACESSO_CONSULTA_CLIENTE_BLOQ_JUDCIAL}  
    [Return]       ${result}[0][0]  
    
Validar Consulta Cliente Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}     0    12
    ${digito}      Get Substring    ${dados}    12    14 
    ${query}       Format String    ${SQL_VALIDAR_CONSULTAR_CLIENTE_BLOQ_JUD}    ${cpf_cnpj}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]
    
Validar Consulta Historico Cliente Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}     0    12
    ${digito}      Get Substring    ${dados}    12    14
    ${query}       Format String    ${SQL_VALIDAR_CONSULTAR_HISTORICO_CLIENTE_BLOQ_JUD}    ${cpf_cnpj}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}
    
Validar Consulta Contrato Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}     0    12
    ${digito}      Get Substring    ${dados}    12    14      
    ${query}       Format String    ${SQL_VALIDAR_CONSULTAR_CONTRATO_BLOQ_JUD}    ${cpf_cnpj}    ${digito}
    ${result}      Query            ${query}    
    [Return]       ${result}[0]    
    
Validar Consulta Historico Contrato Bloqueio Judicial
    [Arguments]    ${dados}
    ${cpf_cnpj}    Get Substring    ${dados}[0]     0    12
    ${digito}      Get Substring    ${dados}[0]    12    14
    ${query}       Format String    ${SQL_VALIDAR_CONSULTAR_HISTORICO_CONTRATO_BLOQ_JUD}    ${cpf_cnpj}    ${digito}    ${dados}[1]
    ${result}      Query            ${query}    
    [Return]       ${result}    