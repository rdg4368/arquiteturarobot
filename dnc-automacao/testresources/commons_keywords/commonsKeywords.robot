# Neste arquivo se encontram todas as Keywords reaproveitáveis em suítes diferentes
*** Settings ***
Resource    resources/commons/Mainframe.robot
Library     testresources/utils/utils.py 
Library     DateTime

#coment

*** Keywords ***
Gerar Data
    ${dataAtual} =            Get Current Date	UTC  result_format=%d%m%Y    #19102019    dia mes ano(4 dig)
    ${dataAtualFormato2} =    Get Current Date	UTC  result_format=%y%m%d    #191022      ano(2 dig) mes dia
    ${dataAtualInvertida} =   Get Current Date	UTC  result_format=%Y%m%d    #20191022    ano(4 dig) mes dia
    ${dataOntem} =            Get Current Date	UTC  result_format=%Y%m%d  
    ${dataOntem} =	          Add Time To Date	${dataOntem}	-1 day  result_format=%Y%m%d     
    ${dataFutura}=            Add Time To Date  ${dataAtualInvertida}  365 days  result_format=%d%m%Y     #DATA ATUAL MAIS 365 DIAS
    [Return]    ${dataAtual}  ${dataAtualInvertida}  ${dataOntem}   ${dataFutura}    ${dataAtualFormato2} 
    
Acessar Host
    [Arguments]  ${host}
    Trocar Host  ${host}

Validar Campos na Tela
    [Arguments]               @{campos}
    ${cnt} =    Get length    @{campos}
    :FOR    ${i}    IN RANGE    ${cnt}
    \    ${cmpComparacao} =   Set Variable  ${campos}[0][${i}][3]     #Captura e percorre o (CAMPO), última posição da lista dada como parametro        
    \    ${cmp} =     Retira Elemento Lista   ${campos}[0][${i}]   3  #Retira o elemento CAMPO da lista , restando apenas as posições que serão utilizadas para capturar o campo da tela
    \    ${cmpTela} =     Ler Dado Tela     ${cmp}                    #Captura o elemento na tela por (X,Y,tamanho)       
    \    Comparar Valores na Tela  ${cmpComparacao}  ${cmpTela}       #Compara o campo vindo da tela o campo setado como parâmetro na lista( @{campos} )

Acessar Menu De Apoio DNC1
    [Arguments]    ${codAdvertencia}       ${codNatureza}    ${codOrigem}      ${codResponsabilidade}    
    ...            ${codRegAdvertencia}    ${codCriterio}    ${codSegmento}    ${faixaInClassif}    ${opcao}
    Trocar Host           DNC1
    Digitar Na Posicao    ${codAdvertencia}       19    24
    Digitar Na Posicao    ${codNatureza}          19    51
    Digitar Na Posicao    ${codOrigem}            19    73
    Digitar Na Posicao    ${codResponsabilidade}  20    24
    Digitar Na Posicao    ${codRegAdvertencia}    20    51
    Digitar Na Posicao    ${codCriterio}          20    73
    Digitar Na Posicao    ${codSegmento}          21    24
    Digitar Na Posicao    ${faixaInClassif}       21    51
    Digitar Na Posicao    ${opcao}                21    73
    Transmitir
    
Acessar Submenu De Situacao Cadastral Na Receita Federal
    [Arguments]    ${tipof/j}    ${cgc/cpf}    ${cdcliente}    ${dtPesquisa}    ${codClaNeg}    ${opcao}
    Trocar Host    DNC2
    Digitar Na Posicao     50    21    69    #opcao 50 MENU SIT.CAD.RECEITA FEDERAL
    Transmitir
    Digitar Na Posicao    ${tipof/j}     19    15
    Digitar Na Posicao    ${cgc/cpf}     19    31
    Digitar Na Posicao    ${cdcliente}   19    63
    Digitar Na Posicao    ${dtPesquisa}  20    22
    Digitar Na Posicao    ${codClaNeg}   20    51
    Digitar Na Posicao    ${opcao}       20    75
    Transmitir

Acessar Submenu De Consultas DNC3
    [Arguments]    ${cpf/cgc}  ${digito}  ${data_solicitacao}  ${periodo_inicial}  ${data_atual}  ${opcao} 
    Trocar Host         DNC3
    Digitar Na Posicao  ${cpf/cgc}                         20   10
    Digitar Na Posicao  ${digito}                          20   25 
    Digitar Na Posicao  ${data_solicitacao}                20   71
    Digitar Na Posicao  ${periodo_inicial}                 21   10
    Digitar Na Posicao  ${data_atual}                      21   23
    Digitar Na Posicao  ${opcao}                           21   71
    Transmitir    
    
Acessar Host DNC4
    [Arguments]    ${codSistema}    ${cliente}    ${cgc/cpf}    ${addcod/ndacod}    ${data}    ${progChamador}    ${opcao}
    Trocar Host    DNC4
    # Digitar Na Posicao     50    21    69    #opcao 50 MENU SIT.CAD.RECEITA FEDERAL
    # Transmitir
    Digitar Na Posicao    ${codSistema}         17    17
    Digitar Na Posicao    ${cliente}            17    38
    Digitar Na Posicao    ${cgc/cpf}            17    61
    Digitar Na Posicao    ${addcod/ndacod}      18    17
    Digitar Na Posicao    ${data}               18    38
    Digitar Na Posicao    ${progChamador}       19    61
    Digitar Na Posicao    ${opcao}              19    73
    Transmitir
    
Acessar Submenu De Consulta Pessoa Exposta Politicamente
    [Arguments]    ${tipoFJ}    ${cgc/cpf}        ${orgao}    ${opcao}
    Trocar Host    DNC2
    Digitar Na Posicao         70    21    69
    Transmitir
    Digitar Na Posicao  ${tipoFJ}    20    15
    Digitar Na Posicao  ${cgc/cpf}   20    29
    Digitar Na Posicao  ${orgao}     21    59
    Digitar Na Posicao  ${opcao}     21    75
    Transmitir           
    
Acessar Menu De Cadastro Manual
    [Arguments]    ${cgc/cpf}    ${cdCliente}    ${dataRefencia}    ${addCod/ndaCod}    ${dataFim}    ${sistema}    ${opcao}
    Trocar Host    DNC2
    Digitar Na Posicao    ${cgc/cpf}          20    11
    Digitar Na Posicao    ${cdCliente}        20    41
    Digitar Na Posicao    ${dataRefencia}     20    70
    Digitar Na Posicao    ${addCod/ndaCod}    21    17
    Digitar Na Posicao    ${dataFim}          21    36
    Digitar Na Posicao    ${sistema}          21    55
    Digitar Na Posicao    ${opcao}            21    69
    Transmitir

Acessar Menu Movimentacao Financeira
    [Arguments]    ${contaCorrente}    ${codCliente}    ${grupoEconomico}    ${opcao}
    Trocar Host    DNC5
    Digitar Na Posicao    ${contaCorrente}        20    18
    Digitar Na Posicao    ${codCliente}           20    46
    Digitar Na Posicao    ${grupoEconomico}       20    76
    Digitar Na Posicao    ${opcao}                22    74
    Transmitir
    
Acessar GBDS
    [Arguments]    ${cpf/cnpj}    ${sequencial}    ${opcao}
    Digitar Na Posicao    ${cpf/cnpj}      21    13
    Digitar Na Posicao    ${sequencial}    21    47
    Digitar Na Posicao    ${opcao}         21    71
    Transmitir

Acessar Classificacao Clientes
    [Arguments]    ${tipoCliente}    ${tipoServidor/codPorte}    ${Aplication/Behaviour}    ${versao}    ${dataVigencia}
    ...    ${codCliente}    ${opcao}
    Trocar Host    DNC6
    Digitar Na Posicao    ${tipoCliente}                20    20
    Digitar Na Posicao    ${tipoServidor/codPorte}      20    53
    Digitar Na Posicao    ${Aplication/Behaviour}       21    30
    Digitar Na Posicao    ${versao}                     21    53
    Digitar Na Posicao    ${dataVigencia}               22    25
    Digitar Na Posicao    ${codCliente}                 22    53
    Digitar Na Posicao    ${opcao}                      22    72
    Transmitir    
    
Acessar Submenu De Consultas DNC7
    [Arguments]    ${cgc/cpf}    ${contrato}  ${sistema}  ${data_inicio}  ${data_fim}  ${empresa}  ${produto}  ${modalidade}  ${opcao} 
    Trocar Host         DNC7
    Digitar Na Posicao  ${cgc/cpf}      18   14
    Digitar Na Posicao  ${contrato}     18   45
    Digitar Na Posicao  ${sistema}      18   76
    Digitar Na Posicao  ${data_inicio}  19   22
    Digitar Na Posicao  ${data_fim}     19   43
    Digitar Na Posicao  ${empresa}      19   68
    Digitar Na Posicao  ${produto}      20   14
    Digitar Na Posicao  ${modalidade}   20   47 
    Digitar Na Posicao  ${opcao}        21   71
    Transmitir    
    
Acessar Submenu De Bloqueio Judicial DNC7
    [Arguments]  ${opcao_40}  ${cgc/cpf}  ${digito}  ${contrato}  ${sistema}  ${produto}  ${modalidade}  ${opcao} 
    Trocar Host         DNC7
    Digitar Na Posicao  ${opcao_40}     21   71
    Transmitir
    Digitar Na Posicao  ${cgc/cpf}      18   19
    Digitar Na Posicao  ${digito}       18   33 
    Digitar Na Posicao  ${contrato}     18   61
    Digitar Na Posicao  ${sistema}      19   19
    Digitar Na Posicao  ${produto}      19   42
    Digitar Na Posicao  ${modalidade}   19   68 
    Digitar Na Posicao  ${opcao}        20   70
    Transmitir   

Acessar Menu DNC9
    [Arguments]         ${cgc/cpf}  ${digito}  ${contrato}  ${sistema}  ${data_inicio}  ${data_fim}  ${empresa}  ${produto}  ${modalidade}  ${opcao} 
    Trocar Host         DNC9
    Digitar Na Posicao  ${cgc/cpf}      18   14
    Digitar Na Posicao  ${digito}       18   27
    Digitar Na Posicao  ${contrato}     18   46
    Digitar Na Posicao  ${sistema}      18   77
    Digitar Na Posicao  ${data_inicio}  19   21
    Digitar Na Posicao  ${data_fim}     19   42
    Digitar Na Posicao  ${empresa}      19   78
    Digitar Na Posicao  ${produto}      20   14
    Digitar Na Posicao  ${modalidade}   20   48 
    Digitar Na Posicao  ${opcao}        22   69
    Transmitir

Acessar Submenu Negativacao Serasa Tabelas
    [Arguments]    ${tabela}    ${argumento}    ${opcao}
    Trocar Host    DNC7.060
    Digitar Na Posicao    ${tabela}       15    19
    Digitar Na Posicao    ${argumento}    16    19
    Digitar Na Posicao    ${opcao}        20    69
    Transmitir




    