#Este arquivo contém alguns métodos gerais de auxílio nos testes
import string
import gc
import sys
from telnetlib import theNULL
from _ast import arg
from builtins import int

#Compara dois valores
def comparaValores(arg1,arg2):
    if  removeEspaco(arg1) == removeEspaco(arg2): 
        return True
    else:
        return  False

#Exclúi elemento de uma lista: (lista , posição do elemento)
def retiraElementoLista(arg1,arg2):
    del(arg1[int(arg2)])
    return  arg1

#Destruir Objeto
def assassinarObj(arg1):
    arg1 = None
    gc.collect()
    
def auto_str(cls):
        def __str__(self):
            return '%s(%s)' % ( type(self).__name__, ', '.join('%s=%s' % item for item in vars(self).items())        )
        cls.__str__ = __str__
        return cls

#valida se objeto é vazio
def isEmpty(arg1):
    a=[] # Empty lists evaluate to False
    if not a:
        print ("list is empty")
    else:
        print ("list is not empty")

    return False    
        
def removeEspaco(instring):
    return instring.strip()

def validaTamanhoPagina(arg1):
    if (arg1 > 15):
        arg1 = 15
    return arg1

def situcaoCadastral(arg,agr2):
    sit=""
    if(agr2=="F"):
        sit= getSituacaoCadastralPessoaFisica(arg)
    elif (agr2=="J"):
        sit= getSituacaoCadastralPessoaJuridica(arg)
    return sit
            
def getSituacaoCadastralPessoaJuridica(arg1):
    switcher={ 
        1: "NULA",
        2: "ATIVA",
        3: "SUSPENSA",
        4: "INAPTA",
        8: "BAIXADA"
    }
    return switcher.get(int(arg1),'')  

def getSituacaoCadastralPessoaFisica(arg1):  
    switcher={ 
        0: "REGULAR",
        1: "CANCELADA POR ENCERRAMENTO DE ESPOLIO",
        2: "SUSPENSA",
        3: "TITULAR FALECIDO",
        4: "PENDENTE DE REGULARIZACAO",
        5: "CANCELADA POR MUITIPLICIDADE",
        8: "NULA",
        9: "CANCELADA DE OFICIO"
    }
    return switcher.get(int(arg1),'')
        
        
def formatarTipo(arg):
    tiponovo = str(arg)
    return tiponovo

def tirar_media(arg):
    soma = 0
    soma = sum(arg)
    qtd_elem = len(arg)
    media = soma/qtd_elem
    result = round(media , 2)
    a = '{:,.1f}'.format(float(result))
    b = a.replace(',','v')
    c = b.replace('.',',')
    return c.replace('v','.')
