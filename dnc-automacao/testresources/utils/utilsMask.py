#Este arquivo contém alguns métodos para o tratamento de dados que são retornados do banco com os caracteres '.0' entre outras máscaras disponíveis.
import string

#[arg1 = String a ser modificada]  [arg2 = Quantidade de caracteres a serem retirados do fim]
def removeUltimosCaracteres(arg1,arg2):
    arg1 = str(arg1)
    arg1 = arg1[: - int(arg2)]
    return  arg1

#[arg1 = String a ser modificada]  [arg2 = Quantidade de caracteres a serem retirados do fim]
def removeUltimosCaracteresFloat(arg1,arg2):
    arg1 = arg1[: - float(arg2)]
    return  arg1

#[arg1 = String a ser modificada]  [arg2 = Quantidade de caracteres a serem retirados do início]
def removePrimeirosCaracteres(arg1,arg2):
    arg1 = arg1[int(arg2):]
    return  arg1    

#[arg1 = cpf]  [arg2 = Dígito Verificador]
def formatarMascaraCpf(arg1,arg2):
    cpf = arg1
    cod = removeUltimosCaracteres(arg2,2)
    cpfConcat = cpf + cod
    cpfFull = removePrimeirosCaracteres(cpfConcat,3)
    cpfFormatado = cpfFull[:3] + "." + cpfFull[3:6] + "." + cpfFull[6:9] + "-" + cpfFull[9:]
    return  cpfFormatado

#[arg1 = cnpj]  [arg2 = Dígito Verificador]
def formatarMascaraCnpj(arg1,arg2):
    cnpj= cgcApenasDigitos(arg1,arg2)    
    cnpjFormatado = cnpj[:2] + "." + cnpj[2:5] + "." + cnpj[5:8] + "/" + cnpj[8:12]+ "-"  + cnpj[12:15] 
    #cnpjFormatado = cnpj[:2] + "." + cnpj[3:6] + "." + cnpj[6:10] + "/" + cnpj[10:15]+ "-"
    return  cnpjFormatado

#[arg1 = capital]
def formatarMascaraCapital(arg1):
   capital = str(arg1).replace("." ,",")
   capitalFormatado = capital[:2] + "." + capital[2:8]
   return capitalFormatado + "0"     

#[arg1 = data]
def formatarMascaraData(arg1):
   data = removeUltimosCaracteres(arg1,2)
   ano = data[:4]
   mes = data[4:6]
   dia = data[6:8]
#    dia = data[7:8]
   #dataFormatada = capital[:2] + "." + capital[2:8]
   dataFormatada = dia + "/" +  mes + "/" + ano
   return dataFormatada   

#[arg1 = cgc]  [arg2 = Dígito Verificador]
def cgcApenasDigitos(arg1,arg2):
    cgc = removeUltimosCaracteres(arg1,2)
    cod = removeUltimosCaracteres(arg2,2)
    cgcConcat = cgc + cod
    return  cgcConcat

#[arg1 = cnpj] 
def apenasDigitos(arg1):
    arg = removeUltimosCaracteres(arg1,2)
    return  arg

#[arg1 = cpfConjuge]  [arg2 = Dígito Verificador]
def formatarMascaraCpfConjuge(arg1,arg2):
    cpf = removeUltimosCaracteres(arg1,2)
    cod = removeUltimosCaracteres(arg2,2)
    cpfConcat = cpf + cod
    cpfConjugeFormatado = cpfConcat[:3] + "." + cpfConcat[3:6] + "." + cpfConcat[6:9] + " - " + cpfConcat[9:]
    return  cpfConjugeFormatado

#[arg1 = cpf]  [arg2 = Dígito Verificador]
def formatarCpfApenasDigitos(arg1,arg2):
    cpf = arg1
    cod = removeUltimosCaracteres(arg2,2)
    cpfConcat = cpf + cod
    return  cpfConcat

#[arg1 = cpf formatado com 000+ digitoVerificador Ex:00088289737149]
def formatarCpfParametroBanco(arg1):
    cpfBd = removePrimeirosCaracteres(arg1,3)
    return  cpfBd


# #[arg1 = cpf]  [arg2 = Dígito Verificador]
# def setarMascaraCodigoCliente(arg1,arg2):
#     cod = removeUltimosCaracteres(arg1,2)
#     digCod = removeUltimosCaracteres(arg2,2)
#     codConcat = '0' + cod + '-' + digCod
#     return  codConcat


#[arg1 = codigo]  [arg2 = Dígito codigo]
def formatarMascaraCodigoCliente(arg1,arg2):
    cod = removeUltimosCaracteres(arg1,2)
    digCod = removeUltimosCaracteres(arg2,2)
    codConcat = cod + '-' + digCod
    return  codConcat

#[arg1 = cnpj]  [arg2 = Dígito Verificador]
def formatarMascaraCodigoClientePj(arg1,arg2):
    cod = removeUltimosCaracteres(arg1,2)
    digCod = removeUltimosCaracteres(arg2,2)
    codConcat = cod + '-' + digCod
    return  codConcat

#[arg1 = agencia]  
def formatarMascaraAgencia(arg1):
    agen = removeUltimosCaracteres(str(arg1),2)
    agenFor = removePrimeirosCaracteres(agen,1)
    return  agenFor

#[arg1 = agencia]  
def formatarMascaraCodMunicipio(arg1):
    codMun = removeUltimosCaracteres(str(arg1),2)
    return  codMun


#[arg1 = cep] 
def formatarMascaraCep(arg1):
    cep= removeUltimosCaracteres(arg1,2)
    cepSize= len(cep)
    if cepSize >= 4:
        cepFormatado =  cep[:1] + "-" + cep[1:4]
    if cepSize >= 5:
        cepFormatado = "000" + cep[:2] + "-" + cep[2:5]
    if cepSize >= 6:
        cepFormatado = "00" + cep[:3] + "-" + cep[3:6]
    if cepSize >= 7:
        cepFormatado =  "0" + cep[:4] + "-" + cep[4:7]
    if cepSize >= 8:
        cepFormatado =   cep[:5] + "-" + cep[5:8]        
    return  cepFormatado


#[arg1 = codigo]  [arg2 = Dígito codigo]
def formatarCodigoClienteCompletaZeros(arg1,arg2):
    cod = removeUltimosCaracteres(arg1,2)
    digCod = removeUltimosCaracteres(arg2,2)
    codSize= len(cod)
    if codSize >= 3:
        codConcat = "00000" + cod + '-' + digCod
    if codSize >= 4:
        codConcat = "0000" + cod + '-' + digCod
    if codSize >= 5:
        codConcat = "000" + cod + '-' + digCod
    if codSize >= 6:
        codConcat = "00" + cod + '-' + digCod
    if codSize >= 7:
        codConcat = "0" + cod + '-' + digCod        
    return  codConcat


#[arg1 = hora]
def formatarMascaraHora(arg1):
   arg1 = removeUltimosCaracteres(arg1,2)
   hora    = arg1[:2]
   minuto  = arg1[2:4]
   segundo = arg1[4:6]
   horaFormatada = hora + ":" +  minuto + ":" + segundo 
   return horaFormatada 
#[arg1 = data]
#1234567890
#201904
#012346
def formatarMascaraDataBase(arg1):
   if (len(arg1) == 6):
       mes = arg1[4:6]
       ano = arg1[:4]
   dataFormatada = mes + "/" + ano
   return dataFormatada  
#[arg1 = data]
   ano = arg1[:4]
def formatarMascaraDataInvertida(arg1):
   mes = arg1[4:6]
   dia = arg1[6:8]
   ano = arg1[:4]
   dataFormatada = dia + "/" +  mes + "/" + ano
   return dataFormatada   
def removeEspaco(instring):
    return instring.strip()

def removeZeroEsquerda(arg1):
   res = removeEspaco(arg1)
   res = res.lstrip('0')
   return res
def removerZeroEsquerda(arg1):
    res = removerEsquerda(arg1)
    return res.lstrip( "0" )

def formatarDataNumeros(arg1):
   data = (arg1)
   ano = data[:4]
   mes = data[4:6]
   dia = data[6:8]
   dataFormatada = dia  +  mes +  ano
   return dataFormatada   

def formatarMoeda(my_value):
    a = '{:,.2f}'.format(float(my_value))
    b = a.replace(',','v')
    c = b.replace('.',',')
    return c.replace('v','.')

def formatarMascaraHoraTrim(arg1):
   hora    = arg1[:2]
   minuto  = arg1[2:4]
   segundo = arg1[4:6]
   horaFormatada = hora + ":" +  minuto + ":" + segundo 
   return horaFormatada 
def formatarMascaraDataTrim(arg1):
    ano = arg1[:4]
    mes = arg1[4:6]
    dia = arg1[6:8]
    if (int(ano) == 0):
        dataFormatada = ""
    else:
        dataFormatada = dia + "/" +  mes + "/" + ano          
    return dataFormatada 

def formatarDataSaqueDescoberto(data):
   ano = data[:4]
   mes = data[4:6]
   dataFormatada = mes + "/" + ano
   return dataFormatada  
def formatarConta(arg):
   conta = arg[:9] + "-" + arg[9:10]
   return conta
def formatarMoeda(my_value):
    a = '{:,.2f}'.format(float(my_value))
    b = a.replace(',','v')
    c = b.replace('.',',')
    c = c.replace('v','.')
    return c

def formatarValor(my_value):
    aux = str(my_value)
    valor = aux.replace('.',',')
    return valor

def formatarMascaraDataInvertidaTrim(arg1):
    ano = arg1[:4]
    mes = arg1[4:6]
    dia = arg1[6:8]
    if (int(ano) == 0):
        dataFormatada = ""
    else:
        dataFormatada = ano + "/" +  mes + "/" + dia          
    return dataFormatada 

def formatarMoeda2(my_value):                   #formata moeda com uma casa decimal
    a = '{:,.1f}'.format(float(my_value))
    b = a.replace(',','v')
    c = b.replace('.',',')
    return c.replace('v','.')

def formatarContaCorrente(arg):
   conta = arg[:3] + "." + arg[3:9] + "." + arg[9:10]
   return conta
def formatarMascaraCnpjConcat(arg1):
    cnpj = ''
    cnpj= arg1    
    cnpjFormatado = cnpj[:2] + "." + cnpj[2:5] + "." + cnpj[5:8] + "/" + cnpj[8:12]+ "/"  + cnpj[12:15] 
    #cnpjFormatado = cnpj[:2] + "." + cnpj[3:6] + "." + cnpj[6:10] + "/" + cnpj[10:15]+ "-"
    return  cnpjFormatado

def formatarMascaraCnpjPonto(arg1):
    cnpj = ''
    cnpj= arg1    
    cnpjFormatado = cnpj[:3] + "." + cnpj[3:6] + "." + cnpj[6:9] + "." + cnpj[9:12]+ "-"  + cnpj[12:14] 
    #cnpjFormatado = cnpj[:2] + "." + cnpj[3:6] + "." + cnpj[6:10] + "/" + cnpj[10:15]+ "-"
    return  cnpjFormatado

def formatarMascaraCnpjPonto2(arg1):
    cnpj = ''
    cnpj= arg1    
    cnpjFormatado = cnpj[:2] + "." + cnpj[2:5] + "." + cnpj[5:8] + "." + cnpj[8:12]+ "-"  + cnpj[12:15]  
    #cnpjFormatado = cnpj[:2] + "." + cnpj[3:6] + "." + cnpj[6:10] + "/" + cnpj[10:15]+ "-"
    return  cnpjFormatado

def formatarTamanhoPagina(arg1, arg2):
    tam = len(arg1)
    if(tam > int(arg2)):
        tam = int(arg2)
    return tam

def formatarDataInvertida(data):
   ano = data[:4]
   mes = data[4:6]
   dia = data[6:8]
   dataFormatada = dia +  mes + ano
   return dataFormatada 
   
def formatarMoedaUmaCasaDecimal(my_value):
    a = '{:,.1f}'.format(float(my_value))
    b = a.replace(',','v')
    c = b.replace('.',',')
    c = c.replace('v','.')
    return c  

#Adiciona 000 a String [arg1 = String a ser modificada] 
def adicionaCaracteres(arg1):
    arg1 = 000 + arg1
    return  arg1

#Adicionar zero a esqueda do numero
def adicionarZeroEsquerda(arg1,arg2):    
    aux = '' 
    for x in arg2:
        aux += '0'
    aux += str(arg1)    
    return  aux