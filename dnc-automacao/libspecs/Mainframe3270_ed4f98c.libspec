<?xml version="1.0" encoding="UTF-8"?>
<keywordspec name="Mainframe3270" type="LIBRARY" format="ROBOT" scope="SUITE" namedargs="true" generated="2021-03-31T19:39:56Z" specversion="2" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\__init__.py" lineno="6">
<version>2.10</version>
<scope>test suite</scope>
<namedargs>yes</namedargs>
<doc>Mainframe3270 is a library for Robot Framework based on [https://pypi.org/project/py3270/|py3270 project],
a Python interface to x3270, an IBM 3270 terminal emulator. It provides an API to a x3270 or s3270 subprocess.

= Installation  =

For use this library you need to install the [http://x3270.bgp.nu/download.html|x3270 project]
and put the directory on your PATH. On Windows, you need to download wc3270 and put
the "C:\Program Files\wc3270" in PATH of the Environment Variables.

= Notes  =

By default the import set the visible argument to true, on this option the py3270 is running the wc3270.exe,
but is you set the visible to false, the py3270 will run the ws3270.exe.

= Example =

| ***** Settings *****
| Library           Mainframe3270
| Library           BuiltIn
|
| ***** Test Cases *****
| Example
|     Open Connection    Hostname    LUname
|     Change Wait Time    0.4
|     Change Wait Time After Write    0.4
|     Set Screenshot Folder    C:\\Temp\\IMG
|     ${value}    Read    3    10    17
|     Page Should Contain String    ENTER APPLICATION
|     Wait Field Detected
|     Write Bare    applicationname
|     Send Enter
|     Take Screenshot
|     Close Connection</doc>
<init source="..\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="14">
<arguments>
<arg>visible=True</arg>
<arg>timeout=30</arg>
<arg>wait_time=0.5</arg>
<arg>wait_time_after_write=0</arg>
<arg>img_folder=.</arg>
</arguments>
<doc>You can change to hide the emulator screen set the argument visible=${False}

For change the wait_time see `Change Wait Time`, to change the img_folder
see the `Set Screenshot Folder` and to change the timeout see the `Change Timeout` keyword.</doc>
</init>
<kw name="Change Timeout" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="41">
<arguments>
<arg>seconds</arg>
</arguments>
<doc>Change the timeout for connection in seconds.</doc>
</kw>
<kw name="Change Wait Time" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="76">
<arguments>
<arg>wait_time</arg>
</arguments>
<doc>To give time for the mainframe screen to be "drawn" and receive the next commands, a "wait time" has been
created, which by default is set to 0.5 seconds. This is a sleep applied AFTER the follow keywords:

`Execute Command`
`Send Enter`
`Send PF`
`Write`
`Write in position`

If you want to change this value just use this keyword passing the time in seconds.

Examples:
    | Change Wait Time | 0.1 |
    | Change Wait Time | 2 |</doc>
</kw>
<kw name="Change Wait Time After Write" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="94">
<arguments>
<arg>wait_time_after_write</arg>
</arguments>
<doc>To give the user time to see what is happening inside the mainframe, a "change wait time after write" has
been created, which by default is set to 0 seconds. This is a sleep applied AFTER the string sent in this
keywords:

`Write`
`Write Bare`
`Write in position`
`Write Bare in position`

If you want to change this value just use this keyword passing the time in seconds.

Note: This keyword is useful for debug purpose

Examples:
    | Change Wait Time After Write | 0.5 |
    | Change Wait Time After Write | 2 |</doc>
</kw>
<kw name="Close Connection" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="67">
<arguments>
</arguments>
<doc>Disconnect from the host.</doc>
</kw>
<kw name="Delete Char" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="186">
<arguments>
<arg>ypos=None</arg>
<arg>xpos=None</arg>
</arguments>
<doc>Delete character under cursor. If you want to delete a character that is in
another position, simply pass the coordinates "ypos"/"xpos".

Co-ordinates are 1 based, as listed in the status area of the
terminal.

Examples:
    | Delete Char |
    | Delete Char | ypos=9 | xpos=25 |</doc>
</kw>
<kw name="Delete Field" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="201">
<arguments>
<arg>ypos=None</arg>
<arg>xpos=None</arg>
</arguments>
<doc>Delete a entire contents in field at current cursor location and positions
cursor at beginning of field. If you want to delete a field that is in
another position, simply pass the coordinates "ypos"/"xpos" of any part of the field.

Co-ordinates are 1 based, as listed in the status area of the
terminal.

Examples:
    | Delete Field |
    | Delete Field | ypos=12 | xpos=6 |</doc>
</kw>
<kw name="Execute Command" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="129">
<arguments>
<arg>cmd</arg>
</arguments>
<doc>Execute an [http://x3270.bgp.nu/wc3270-man.html#Actions|x3270 command].

Examples:
    | Execute Command | Enter |
    | Execute Command | Home |
    | Execute Command | Tab |
    | Execute Command | PF(1) |</doc>
</kw>
<kw name="Move Next Field" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="223">
<arguments>
</arguments>
<doc>Move the cursor to the next input field. Equivalent to pressing the Tab key.</doc>
</kw>
<kw name="Move Previous Field" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="228">
<arguments>
</arguments>
<doc>Move the cursor to the previous input field. Equivalent to pressing the Shift+Tab keys.</doc>
</kw>
<kw name="Open Connection" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="46">
<arguments>
<arg>host</arg>
<arg>LU=None</arg>
<arg>port=23</arg>
</arguments>
<doc>Create a connection with IBM3270 mainframe with the default port 23. To make a connection with the mainframe
you only must inform the Host. You can pass the Logical Unit Name and the Port as optional.

Example:
    | Open Connection | Hostname |
    | Open Connection | Hostname | LU=LUname |
    | Open Connection | Hostname | port=992 |</doc>
</kw>
<kw name="Page Should Contain All Strings" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="388">
<arguments>
<arg>list_string</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Search if all of the strings in a given list exists on the mainframe screen.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True}
and you can edit the raise exception message with error_message.

Example:
    | Page Should Contain All Strings | ${list_of_string} |
    | Page Should Contain All Strings | ${list_of_string} | ignore_case=${True} |
    | Page Should Contain All Strings | ${list_of_string} | error_message=New error message |</doc>
</kw>
<kw name="Page Should Contain Any String" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="355">
<arguments>
<arg>list_string</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Search if one of the strings in a given list exists on the mainframe screen.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True}
and you can edit the raise exception message with error_message.

Example:
    | Page Should Contain Any String | ${list_of_string} |
    | Page Should Contain Any String | ${list_of_string} | ignore_case=${True} |
    | Page Should Contain Any String | ${list_of_string} | error_message=New error message |</doc>
</kw>
<kw name="Page Should Contain Match" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="473">
<arguments>
<arg>txt</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Fails unless the given string matches the given pattern.

Pattern matching is similar as matching files in a shell, and it is always case-sensitive.
In the pattern, * matches to anything and ? matches to any single character.

Note that the entire screen is only considered a string for this keyword, so if you want to search
for the string "something" and it is somewhere other than at the beginning or end of the screen it
should be reported as follows: **something**

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True} and you
can edit the raise exception message with error_message.

Example:
    | Page Should Contain Match | **something** |
    | Page Should Contain Match | **so???hing** |
    | Page Should Contain Match | **someTHING** | ignore_case=${True} |
    | Page Should Contain Match | **something** | error_message=New error message |</doc>
</kw>
<kw name="Page Should Contain String" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="320">
<arguments>
<arg>txt</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Search if a given string exists on the mainframe screen.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True}
and you can edit the raise exception message with error_message.

Example:
    | Page Should Contain String | something |
    | Page Should Contain String | someTHING | ignore_case=${True} |
    | Page Should Contain String | something | error_message=New error message |</doc>
</kw>
<kw name="Page Should Contain String X Times" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="422">
<arguments>
<arg>txt</arg>
<arg>number</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Search if the entered string appears the desired number of times on the mainframe screen.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True} and you
can edit the raise exception message with error_message.

Example:
       | Page Should Contain String X Times | something | 3 |
       | Page Should Contain String X Times | someTHING | 3 | ignore_case=${True} |
       | Page Should Contain String X Times | something | 3 | error_message=New error message |</doc>
</kw>
<kw name="Page Should Match Regex" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="447">
<arguments>
<arg>regex_pattern</arg>
</arguments>
<doc>Fails if string does not match pattern as a regular expression. Regular expression check is
implemented using the Python [https://docs.python.org/2/library/re.html|re module]. Python's
regular expression syntax is derived from Perl, and it is thus also very similar to the syntax used,
for example, in Java, Ruby and .NET.

Backslash is an escape character in the test data, and possible backslashes in the pattern must
thus be escaped with another backslash (e.g. \\d\\w+).</doc>
</kw>
<kw name="Page Should Not Contain All Strings" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="401">
<arguments>
<arg>list_string</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Fails if one of the strings in a given list exists on the mainframe screen. if one of the string
are found, the keyword will raise a exception.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True}
and you can edit the raise exception message with error_message.

Example:
    | Page Should Not Contain All Strings | ${list_of_string} |
    | Page Should Not Contain All Strings | ${list_of_string} | ignore_case=${True} |
    | Page Should Not Contain All Strings | ${list_of_string} | error_message=New error message |</doc>
</kw>
<kw name="Page Should Not Contain Any String" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="374">
<arguments>
<arg>list_string</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Fails if one or more of the strings in a given list exists on the mainframe screen. if one or more of the
string are found, the keyword will raise a exception.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True}
and you can edit the raise exception message with error_message.

Example:
    | Page Should Not Contain Any Strings | ${list_of_string} |
    | Page Should Not Contain Any Strings | ${list_of_string} | ignore_case=${True} |
    | Page Should Not Contain Any Strings | ${list_of_string} | error_message=New error message |</doc>
</kw>
<kw name="Page Should Not Contain Match" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="504">
<arguments>
<arg>txt</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Fails if the given string matches the given pattern.

Pattern matching is similar as matching files in a shell, and it is always case-sensitive.
In the pattern, * matches to anything and ? matches to any single character.

Note that the entire screen is only considered a string for this keyword, so if you want to search
for the string "something" and it is somewhere other than at the beginning or end of the screen it
should be reported as follows: **something**

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True} and you
can edit the raise exception message with error_message.

Example:
    | Page Should Not Contain Match | **something** |
    | Page Should Not Contain Match | **so???hing** |
    | Page Should Not Contain Match | **someTHING** | ignore_case=${True} |
    | Page Should Not Contain Match | **something** | error_message=New error message |</doc>
</kw>
<kw name="Page Should Not Contain String" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="338">
<arguments>
<arg>txt</arg>
<arg>ignore_case=False</arg>
<arg>error_message=None</arg>
</arguments>
<doc>Search if a given string NOT exists on the mainframe screen.

The search is case sensitive, if you want ignore this you can pass the argument: ignore_case=${True}
and you can edit the raise exception message with error_message.

Example:
    | Page Should Not Contain String | something |
    | Page Should Not Contain String | someTHING | ignore_case=${True} |
    | Page Should Not Contain String | something | error_message=New error message |</doc>
</kw>
<kw name="Page Should Not Match Regex" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="460">
<arguments>
<arg>regex_pattern</arg>
</arguments>
<doc>Fails if string does match pattern as a regular expression. Regular expression check is
implemented using the Python [https://docs.python.org/2/library/re.html|re module]. Python's
regular expression syntax is derived from Perl, and it is thus also very similar to the syntax used,
for example, in Java, Ruby and .NET.

Backslash is an escape character in the test data, and possible backslashes in the pattern must
thus be escaped with another backslash (e.g. \\d\\w+).</doc>
</kw>
<kw name="Read" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="114">
<arguments>
<arg>ypos</arg>
<arg>xpos</arg>
<arg>length</arg>
</arguments>
<doc>Get a string of "length" at screen co-ordinates "ypos"/"xpos".

Co-ordinates are 1 based, as listed in the status area of the terminal.

Example for read a string in the position y=8 / x=10 of a length 15:
    | ${value} | Read | 8 | 10 | 15 |</doc>
</kw>
<kw name="Send Enter" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="217">
<arguments>
</arguments>
<doc>Send a Enter to the screen.</doc>
</kw>
<kw name="Send PF" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="233">
<arguments>
<arg>PF</arg>
</arguments>
<doc>Send a Program Function to the screen.

Example:
       | Send PF | 3 |</doc>
</kw>
<kw name="Set Screenshot Folder" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="141">
<arguments>
<arg>path</arg>
</arguments>
<doc>Set a folder to keep the html files generated by the `Take Screenshot` keyword.

Example:
    | Set Screenshot Folder | C:\\Temp\\Images |</doc>
</kw>
<kw name="Take Screenshot" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="153">
<arguments>
<arg>height=410</arg>
<arg>width=670</arg>
</arguments>
<doc>Generate a screenshot of the IBM 3270 Mainframe in a html format. The
default folder is the log folder of RobotFramework, if you want change see the `Set Screenshot Folder`.

The Screenshot is printed in a iframe log, with the values of height=410 and width=670, you
can change this values passing them from the keyword. 

Examples:
    | Take Screenshot |
    | Take Screenshot | height=500 | width=700 |</doc>
</kw>
<kw name="Wait Field Detected" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="172">
<arguments>
</arguments>
<doc>Wait until the screen is ready, the cursor has been positioned
on a modifiable field, and the keyboard is unlocked.

Sometimes the server will "unlock" the keyboard but the screen
will not yet be ready.  In that case, an attempt to read or write to the
screen will result in a 'E' keyboard status because we tried to read from
a screen that is not yet ready.

Using this method tells the client to wait until a field is
detected and the cursor has been positioned on it.</doc>
</kw>
<kw name="Wait Until String" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="292">
<arguments>
<arg>txt</arg>
<arg>timeout=5</arg>
</arguments>
<doc>Wait until a string exists on the mainframe screen to perform the next step. If the string not appear on
5 seconds the keyword will raise a exception. You can define a different timeout.

Example:
    | Wait Until String | something |
    | Wait Until String | something | timeout=10 |</doc>
</kw>
<kw name="Write" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="242">
<arguments>
<arg>txt</arg>
</arguments>
<doc>Send a string to the screen at the current cursor location *and a Enter.*

Example:
    | Write | something |</doc>
</kw>
<kw name="Write Bare" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="250">
<arguments>
<arg>txt</arg>
</arguments>
<doc>Send only the string to the screen at the current cursor location.

Example:
    | Write Bare | something |</doc>
</kw>
<kw name="Write Bare In Position" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="269">
<arguments>
<arg>txt</arg>
<arg>ypos</arg>
<arg>xpos</arg>
</arguments>
<doc>Send only the string to the screen at screen co-ordinates "ypos"/"xpos".

Co-ordinates are 1 based, as listed in the status area of the
terminal.

Example:
    | Write Bare in Position | something | 9 | 11 |</doc>
</kw>
<kw name="Write In Position" source="C:\Users\ayrto\AppData\Local\Programs\Python\Python38\Lib\site-packages\robotframework-mainframe3270-2.10\Mainframe3270\x3270.py" lineno="258">
<arguments>
<arg>txt</arg>
<arg>ypos</arg>
<arg>xpos</arg>
</arguments>
<doc>Send a string to the screen at screen co-ordinates "ypos"/"xpos" and a Enter.

Co-ordinates are 1 based, as listed in the status area of the
terminal.

Example:
    | Write in Position | something | 9 | 11 |</doc>
</kw>
</keywordspec>