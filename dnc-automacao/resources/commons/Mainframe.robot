*** Settings ***
Variables         properties.py    
Library           Mainframe3270                
Library           testresources/utils/utils.py 
Library           testresources/utils/utilsMask.py     
                   
*** Variable ***
${MAINFRAME}          ${properties.mainframe}
${OPCAO_SADS_CICS}    ${properties.opcao_cics}
${USUARIO_SADS}       ${properties.usuario_sads}
${PASSWORD_SADS}      ${properties.password_sads}
${AMBIENTE}           ${properties.ambiente_sads}
${EMPRESA}            ${properties.empresa_sads}
${ESTABELECIMENTO}    ${properties.estabelecimento_sads}
${PF3}                PF(03)
${LIMPAR}   Clear

*** Keywords ***

Validacao Disponibilidade SADS         
    ${on} =   Run Keyword And Return Status    Wait Until String   HABILITACAO DE USUARIO   20   
    Run Keyword If    '${on}' == 'False'   Log              AMBIENTE INSTÁVEL OU INDISPONÍVEL ...   
    Run Keyword If    '${on}' == 'False'   Log To Console   AMBIENTE INSTÁVEL OU INDISPONÍVEL ...  
    
Abrir Terminal
    Open Connection    ${MAINFRAME}

Autenticar SADS    
    Digitar  ${OPCAO_SADS_CICS}
    Transmitir          
    Validacao Disponibilidade SADS
    Digitar Na Posicao  ${USUARIO_SADS}     17  20
    Digitar Na Posicao  ${PASSWORD_SADS}    17  42 
    Digitar Na Posicao  ${AMBIENTE}         19  16
    Digitar Na Posicao  ${EMPRESA}          19  30
    Digitar Na Posicao  ${ESTABELECIMENTO}  19  53
#    Digitar Na Posicao  ${UNIDADE}          19  70  
    Transmitir

Autenticar SADS Estabelecimento Especifico 
    [Arguments]     ${ESTABELECIMENTOESP}  
    Trocar Host     SBT2
    Digitar Na Posicao    23    21    70
    Transmitir
    Digitar Na Posicao  ${ESTABELECIMENTOESP}  19  46
    Transmitir

Acessar SADS
    Abrir Terminal
    Autenticar SADS
    
Fechar Terminal
    Close Connection

Trocar Host
   [Arguments]       ${HOST}
   Execute Command   PF(03)
   Digitar           ${HOST}
   Transmitir

Transmitir
   Capturar Screenshot 
   Send Enter   
   Capturar Screenshot

Capturar Screenshot
    Take Screenshot

Digitar
   [Arguments]   ${conteudo}  
   Write Bare  ${conteudo}

Digitar Na Posicao 
   [Arguments]  ${text}  ${y}  ${x} 
   Write Bare In Position  ${text}  ${y}  ${x}

Digitar Na Posicao Por Lista
    [Arguments]   ${params} 
    #String , posicao Y , posição X
    Write Bare In Position  ${params}[0][0]  ${params}[0][1]  ${params}[0][2]

Tabular
   Execute Command  Tab  

Limpar campo
    [Arguments]     ${x}  ${y}   
    Delete Field    ${x}  ${y}
    
Limpar Campo Por Lista
    [Arguments]     ${params}        
    Delete Field    ${params}[0][0]  ${params}[0][1]
    

Tabular "${qnt}" Vezes     
    Change Wait Time    0.01
    :FOR    ${i}    IN RANGE    ${qnt}
    \    Tabular
    Change Wait Time    0.2

Verificar se existe 
   [Arguments]   ${conteudo}  
   Page Should Contain String  ${conteudo}

Validar Informacoes Tela
    [Arguments]         ${campos}  
    Page Should Contain All Strings     ${campos}

Ler Dado Tela
    [Arguments]        @{params}   
    ${valorCampo} =     Read  ${params}[0][0]  ${params}[0][1]  ${params}[0][2] 
    [Return]           ${valorCampo}  
  
Ler Dado Tela 2
    [Arguments]        ${y}  ${x}  ${l}
    ${valorCampo} =     Read  ${y}  ${x}  ${l}  
    [Return]           ${valorCampo} 
    
Comparar Valores na Tela
    [Arguments]     ${dadoTela}   ${dadoBanco}   
    ${saoIguais} =  utils.comparaValores  ${dadoTela}  ${dadoBanco}    
    Run keyword if   '${saoIguais}'=='False'  FAIL      

Validar Informacoes Tela Dinamicamente
    [Arguments]    @{input}
    ${cnt}=    Get length    ${input}
    :FOR    ${i}    IN RANGE    ${cnt}
    \    Page Should Contain String     ${input}[${i}]
    
Validar Mensagem
    [Arguments]                  ${msgValidacao}
    Page Should Contain String   ${msgValidacao}

Alterar Campo
    [Arguments]   @{params} 
    Digitar Na Posicao Por Lista   ${params} 
    Transmitir
 

   


    
    

    
    