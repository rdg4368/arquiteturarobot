*** Settings ***
Resource     testresources/base_dados/DNCB01.robot 
Resource     resources/commons/Mainframe.robot
Library      Mainframe3270  
Library      String
Library      DateTime  
      


*** Keywords ***

Inserir Valor 
    [Arguments]            @{params}    # Valor    ${xPos}    ${yPos} 
    Digitar Na Posicao     ${params}[0][0]    ${params}[0][1]    ${params}[0][2]    
    Transmitir

Validar Mensagem Campo Obrigatorio
    [Arguments]              ${msgTela}     ${msgEsperada}    
    ${msg} =  Ler Dado Tela  ${msgTela} 
    Should Be Equal           ${msg}        ${msgEsperada} 
    
Transmitir Transacao
    Tabular
    Transmitir   
    
Gerar Data Atual
    ${dataAtual} =            Get Current Date	UTC  result_format=%d%m%Y 
    ${dataAtualInvertida} =   Get Current Date	UTC  result_format=%Y%m%d     
    [Return]    ${dataAtual}  ${dataAtualInvertida}