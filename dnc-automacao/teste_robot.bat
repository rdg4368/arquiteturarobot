:: Este script auxilia a integração do allure com o robot
:: Para rodar corretamente, certifique-se que o Python, o servidor do Apache 
:: e o caminho para o .bat do allure, estejam devidamente configurados na 
:: variável de ambiente Path do Windows. 
@echo off
set DIR_ATUAL=%cd%
set CLASSPATH=D:\lib\ojdbc6-11.2.0.3.jar;D:\lib\db2jcc4-10.1.jar;%CLASSPATH%

For /f "tokens=1-4 delims=/ " %%a in ('date /t') do (set mydate=%%c%%a%%b)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)

::Na maquina do jenkins, o python está no C:, mas se for rodar local, coloque o python da sua maquina
::set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_66
set JAVA_HOME=D:\jdk1.8.0_66
set PATH=%DIR_ATUAL%\src\main\resources;%PATH%
set PATH=D:\allure-2.11.0\bin\;%PATH%
set PATH=D:\python37\;D:\python37\Scripts;%PATH%
set CATALINA_HOME=D:\apache-tomcat-9.0.17
set APACHE_PATH=%CATALINA_HOME%\
set dataatual=%mydate%_%mytime%

::set PATH=C:\python37\;c:\python37\Scripts;%PATH% 

echo Variaveis ambiente configuradas
echo %DIR_ATUAL%\allure-report

if exist %DIR_ATUAL%\output (rmdir /Q /S output)

mkdir output
cd output
mkdir images
cd ..

path
echo Start robot.....
python --version
python -m robot --listener allure_robotframework --outputdir results   robot/
echo Fim robot.


echo Inicio geracao allure....
copy .\environment_py3.xml .\output\allure\environment.xml 
call allure generate output\allure\ --clean
echo Fim geracao allure


cd allure-report\data\
mkdir imagens
cd ../..


rmdir /Q /S %APACHE_PATH%webapps\allure

rename allure-report allure
move allure %APACHE_PATH%webapps\


call %APACHE_PATH%\bin\startup.bat

del *.html
rmdir /Q /S images
rmdir /Q /S output

echo "Testes DNA executados."


