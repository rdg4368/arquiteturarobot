*** Settings ***
Resource    testresources/commons_keywords/commonsKeywords.robot
Resource    testresources/base_dados/DNCB14.robot
Library     utilsMask    
Library     utils    

*** Keywords ***
#CT01
Validar Consultar Movimentacao Financeira Por Conta Corrente
    [Arguments]    ${conta}
    ${validar}    Consulta Movimentacao Financeira Por Conta Corrente     ${conta}
    ${cnt}        Get Length    ${validar}
    :FOR  ${i}    IN RANGE  ${cnt}
    \    ${saldoMedio}           Formatar Moeda    ${validar}[${i}][0] 
    \    ${movFinanceiro}        Formatar Moeda    ${validar}[${i}][1] 
    \    ${antecipRecebiveis}    Formatar Moeda    ${validar}[${i}][2] 
    \    Verificar se existe     ${saldoMedio}
    \    Verificar se existe     ${movFinanceiro}
    \    Verificar se existe     ${antecipRecebiveis}
    
#CT02
Validar Consultar Movimentacao Financeira Por Codigo de Cliente
    [Arguments]    ${codCliente}
    ${validar}      Consulta Movimentacao Financeira Por Codigo de Cliente     ${codCliente}
    ${media}       Tirar Media    ${validar}
    Verificar se existe            ${media}    
    
#CT03
Validar Consultar Movimentacao Financeira Por Grupo Economico
    [Arguments]    ${grupoEconomico}
    ${validar}     Consulta Movimentacao Financeira Por Grupo Economico    ${grupoEconomico}
    Verificar se existe    ${validar}
    Log To Console    \nValidação não está completa, \npois o sistema apresenta contas zeradas \ne as mesmas não se encontram na base de dados.