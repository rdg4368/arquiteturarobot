*** Settings ***
Resource       testresources/commons_keywords/commonsKeywords.robot
Resource       testresources/base_dados/DNCB31.robot
Resource       testresources/base_dados/DNCB18.robot
Library        utilsMask    
Library        String



*** Keywords ***
#CT01
Validar Consultar Saldo Medio APA FFA FCB por Conta Corrente
    [Arguments]     ${contaCorrente}
    ${MediaAPA}          Consulta Saldo Medio APA Por CC   ${contaCorrente}
    ${MediaFCB}          Consulta Saldo Medio FCB Por CC   ${contaCorrente}
    ${formatarMediaAPA}  Formatar Moeda                            ${MediaAPA}
    ${formatarMediaFCB}  Formatar Moeda                            ${mediaFCB}
    Verificar se existe                                            ${formatarMediaAPA}
    Verificar se existe                                            ${formatarMediaFCB}
#CT02
Validar Consultar Saldo Medio APA FFA FCB por Cliente
    [Arguments]    ${codCliente}
    ${mediaAPA}    Consulta Saldo Medio APA Por Cleinte    ${codCliente}
    ${mediaFCB}    Consulta Saldo Medio FCB Por Cliente    ${codCliente}
    ${formatarMediaAPA}  Formatar Moeda Uma Casa Decimal  ${MediaAPA}
    ${formatarMediaFCB}  Formatar Moeda Uma Casa Decimal  ${mediaFCB}
    Verificar se existe                   ${formatarMediaAPA}
    Verificar se existe                   ${formatarMediaFCB}
    
    
#CT03
Validar Consultar Saldo Mensal Poupanca por Conta Corrente
    [Arguments]    ${contaCorrente}
    ${validar}         Consulta Saldo Mensal Poupanca por Conta Corrente    ${contaCorrente}
    Verificar se existe        ${validar}[0][0] 
    Verificar se existe        ${validar}[0][1]
    ${cnt}    Get Length       ${validar}
    :FOR  ${i}    IN RANGE     ${cnt}
    \     ${saldoMensal}       Formatar Moeda    ${validar}[${i}][2]
    \    Verificar se existe                     ${saldoMensal}

#CT04
Validar Consultar Saldo Mensal Poupanca por Cliente 
    [Arguments]    ${codCliente}       
     ${validar}    Validar consulta Saldo Mensal Poupanca por Cliente    ${codCliente}   
     Verificar se existe       ${validar}[0][0]
     Verificar se existe       ${validar}[0][1]
     Verificar se existe       ${validar}[0][2]
     Verificar se existe       ${validar}[0][3]
    
#CT05
Validar Dados Consultar Saldo Mensal Aplicações Por Conta
    [Arguments]    ${contaCorrente}
    ${validar}     Consulta Saldo Mensal por Conta Corrente    ${contaCorrente}
    ${conta}       Formatar Conta Corrente  ${validar}[0][0]
    Verificar se existe        ${conta} 
    Verificar se existe        ${validar}[0][1]
    Verificar se existe        ${validar}[0][2]
    Verificar se existe        ${validar}[0][3]
    Verificar se existe        ${validar}[0][4]
    ${cnt}    Get Length       ${validar}
    :FOR  ${i}    IN RANGE     ${cnt}
    \     ${saldoMensal}       Formatar Moeda    ${validar}[${i}][5]
    \    Verificar se existe                     ${saldoMensal}
        
#CT06
Validar Dados Mensal Fundos por Cliente
    [Arguments]     ${cltCod}
    ${validar}      Recuperar Dados Validar Dados Mensal Fundos por Cliente  ${cltCod}
    Verificar se existe        ${validar}[0][0]
    Verificar se existe        ${validar}[0][1]
    Verificar se existe        ${validar}[0][2]
    Verificar se existe        ${validar}[0][3]    
    ${conta}       Formatar Conta Corrente  ${validar}[0][4]
    Verificar se existe        ${conta}

#CT07
Validar Dados Mensal CDB por Cliente
    [Arguments]     ${cltCod}
    ${validar}      Recuperar Dados Validar Dados Mensal CDB por Cliente  ${cltCod}
    Verificar se existe        ${validar}[0][0]
    Verificar se existe        ${validar}[0][1]
    Verificar se existe        ${validar}[0][2]
    Verificar se existe        ${validar}[0][3]    
    ${conta}       Formatar Conta Corrente  ${validar}[0][4]
    Verificar se existe        ${conta}

#CT08
Validar Consultar Saldo Mensal Todas Contas
    [Arguments]     ${cltCod}
    ${validar}      Recuperar Dados Validar Dados Mensal Todas Contas  ${cltCod}
    ${codigoPessoa}            Remove Ultimos Caracteres               ${validar}[0][0]  2
    Verificar se existe        ${codigoPessoa}
    Verificar se existe        ${validar}[0][1]
    Verificar se existe        ${validar}[0][2]
    Verificar se existe        ${validar}[0][3]    
    ${cnt}    Get Length       ${validar}
    :FOR  ${i}    IN RANGE     ${cnt}
    \     ${conta}       Formatar Conta Corrente  ${validar}[0][4]
    \     Verificar se existe                     ${conta} 
  