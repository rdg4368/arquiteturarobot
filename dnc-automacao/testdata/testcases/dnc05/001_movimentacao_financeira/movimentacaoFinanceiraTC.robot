*** Settings ***
Resource      testdata/keywords/dnc05/001_movimentacao_financeira/movimentacaoFinanceiraKW.robot

*** Variables ***
${opcao10}    10
${opcao20}    20
*** Keywords ***
#CT01
Consultar Movimentacao Financeira Por Conta Corrente
    ${conta}    Recuperar Dado Acesso Consultar Movimentacao Por CC
    Acessar Menu Movimentacao Financeira        ${conta}    ${space}    ${space}    ${opcao10}
    Validar Consultar Movimentacao Financeira Por Conta Corrente        ${conta}
    
#CT02
Consultar Movimentacao Financeira Por Codigo de Cliente
    ${codigoCliente}    Recuperar Codigo de Cliente
    Acessar Menu Movimentacao Financeira          ${space}    ${codigoCliente}    ${space}    ${opcao20}
    Validar Consultar Movimentacao Financeira Por Codigo de Cliente               ${codigoCliente}
    
#CT03
Consultar Movimentacao Financeira Por Grupo Economico
    ${grupoEconomico}    Recuperar Grupo Economico
    Acessar Menu Movimentacao Financeira          ${space}    ${space}    ${grupoEconomico}    ${opcao20}
    Validar Consultar Movimentacao Financeira Por Grupo Economico         ${grupoEconomico}