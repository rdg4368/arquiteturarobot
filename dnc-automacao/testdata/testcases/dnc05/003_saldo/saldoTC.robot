*** Settings ***
Resource      testdata/keywords/dnc05/003_saldo/saldoKW.robot
Library       testresources/utils/utilsMask.py    

*** Variables ***
${opcao46}    46
${opcao47}    47
${opcao48}    48
${opcao49}    49
${opcao51}    51
${opcao52}    52
${opcao53}    53
${opcao54}    54

*** Keywords ***
#CT01
Consultar Saldo Medio APA-FFA-FCB por Conta Corrente
    ${contaCorrente}    Recuperar Conta Corrente Consultar Saldo Medio APA FFA FCB
    Acessar Menu Movimentacao Financeira    ${contaCorrente}    ${space}    ${space}    ${opcao46}
    Validar Consultar Saldo Medio APA FFA FCB por Conta Corrente            ${contaCorrente}
    
#CT02
Consultar Saldo Medio APA-FFA-FCB por Cliente
    ${codCliente}    Recuperar Codigo Cliente Consultar Saldo Medio APA FFA FCB 
    Acessar Menu Movimentacao Financeira    ${space}    ${codCliente}    ${space}    ${opcao47}
    Validar Consultar Saldo Medio APA FFA FCB por Cliente                ${codCliente}
    
#CT03
Consultar Saldo Mensal Poupanca por Conta Corrente
    ${contaCorrente}    Recuperar Conta Corrente Consultar Saldo Mensal Poupanca Por CC
    Acessar Menu Movimentacao Financeira    ${contaCorrente}    ${space}    ${space}    ${opcao48}
    Validar Consultar Saldo Mensal Poupanca por Conta Corrente              ${contaCorrente}
    
#CT04
Consultar Saldo Mensal Poupanca por Cliente
    ${cltCod}       Recuperar Cltcod Consultar Saldo Mensal Poupanca
    Acessar Menu Movimentacao Financeira        ${space}        ${cltCod}    ${space}    ${opcao49}
    Validar Consultar Saldo Mensal Poupanca por Cliente    ${cltCod}         
    
#CT05
Consultar Saldo Mensal Aplicacoes por Conta
     ${contaCorrente}              Recuperar Dados Consultar Saldo Mensal Aplicações Por Conta   
     Acessar Menu Movimentacao Financeira    ${contaCorrente}    ${space}    ${space}    ${opcao51}
     Validar Dados Consultar Saldo Mensal Aplicações Por Conta   ${contaCorrente}    

#CT06
Consultar Saldo Mensal Fundos por Cliente
     ${cltCod}       Recuperar Cltcod Consultar Saldo Mensal de Fundos Por Cliente
     Acessar Menu Movimentacao Financeira        ${space}        ${cltCod}    ${space}    ${opcao52}
     Validar Dados Mensal Fundos por Cliente     ${cltCod}
     
#CT07
Consultar Saldo Mensal CDB por Cliente
     ${cltCod}       Recuperar Cltcod Consultar Saldo Mensal CDB Por Cliente
     Acessar Menu Movimentacao Financeira        ${space}        ${cltCod}    ${space}    ${opcao53}
     Validar Dados Mensal CDB por Cliente        ${cltCod}
    
#CT08
Consultar Saldo Mensal de Todas as Contas de um Cliente
     ${cltCod}       Recuperar Cltcod Consultar Saldo Mensal Todas Contas
     Acessar Menu Movimentacao Financeira        ${space}        ${cltCod}    ${space}    ${opcao54}
     Validar Consultar Saldo Mensal Todas Contas                 ${cltCod}