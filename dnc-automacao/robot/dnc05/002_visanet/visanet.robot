*** Settings ***
Resource           resources/commons/Base.robot
Resource           testdata/testcases/dnc05/002_visanet/visanetTC.robot
Suite Setup        Preparar Suite
Suite Teardown     Terminar Suite

*** Test Cases ***

CT001 -FP- Consultar Visanet por Conta Corrente
    [Documentation]  Validar a funcionalidade 41. CONSULTAR VISANET POR CONTA CORRENTE
    [Tags]           FluxoPrincipal
    Consultar Movimentacao Financeira Por Conta Corrente
    
CT002 -FP- Consultar Visanet por Cliente
    [Documentation]  Validar a funcionalidade 42. CONSULTAR VISANET POR CLIENTE
    [Tags]           FluxoPrincipal
    Consultar Visanet por Cliente
    