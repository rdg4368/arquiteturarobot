*** Settings ***
Resource           resources/commons/Base.robot
Resource           testdata/testcases/dnc05/003_saldo/saldoTC.robot
Suite Setup        Preparar Suite
Suite Teardown     Terminar Suite

*** Test Cases ***
CT001 -FP- Consultar Saldo Medio APA-FFA-FCB por Conta Corrente
    [Documentation]  Validar a funcionalidade 46. CONSULTAR SALDO MEDIO APA-AFA-FCB POR CC
    [Tags]           FluxoPrincipal
    Consultar Saldo Medio APA-FFA-FCB por Conta Corrente
    
CT002 -FP- Consultar Saldo Medio APA-FFA-FCB por Cliente
    [Documentation]  Validar a funcionalidade 47. CONSULTAR SALDO MEDIO APA-AFA-FCB POR CLIENTE
    [Tags]           FluxoPrincipal
    Consultar Saldo Medio APA-FFA-FCB por Cliente
    
CT003 -FP- Consultar Saldo Mensal Poupanca por Conta Corrente
    [Documentation]  Validar a funcionalidade 48. CONSULTAR SALDO POUPANÇA POR CC
    [Tags]           FluxoPrincipal
    Consultar Saldo Mensal Poupanca por Conta Corrente
    
CT004 -FP- Consultar Saldo Mensal Poupanca por Cliente
    [Documentation]  Validar a funcionalidade 49. CONSULTAR SALDO POUPANÇA POR CLIENTE
    [Tags]           FluxoPrincipal
    Consultar Saldo Mensal Poupanca por Cliente
    
CT005 -FP- Consultar Saldo Mensal Aplicacoes por Conta
   [Documentation]  Validar a funcionalidade 51. CONSULTAR SALDO MENSAL APLICAÇOES POR CONTA
   [Tags]           FluxoPrincipal
   Consultar Saldo Mensal Aplicacoes por Conta
    
CT006 -FP- Consultar Saldo Mensal Fundos por Cliente
    [Documentation]  Validar a funcionalidade 52. CONSULTAR SALDO MENSAL FUNDOS POR CLIENTE
    [Tags]           FluxoPrincipal
    Consultar Saldo Mensal Fundos por Cliente
    
CT007 -FP- Consultar Saldo Mensal CDB por Cliente
   [Documentation]  Validar a funcionalidade 53. CONSULTAR SALDO MENSAL CDB POR CLIENTE
   [Tags]           FluxoPrincipal
   Consultar Saldo Mensal CDB por Cliente
    
CT008 -FP- Consultar Saldo Mensal de Todas as Contas de um Cliente
   [Documentation]  Validar a funcionalidade 54. CONSULTAR SALDO MENSAL DE TODAS AS CONTAS DE UM CLIENTE
   [Tags]           FluxoPrincipal
   Consultar Saldo Mensal de Todas as Contas de um Cliente