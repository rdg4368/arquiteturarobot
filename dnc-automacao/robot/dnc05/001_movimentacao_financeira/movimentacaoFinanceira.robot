*** Settings ***
Resource           resources/commons/Base.robot
Resource           testdata/testcases/dnc05/001_movimentacao_financeira/movimentacaoFinanceiraTC.robot
Suite Setup        Preparar Suite
Suite Teardown     Terminar Suite

*** Test Cases ***

CT001 -FP- Consultar Movimentacao Financeira Por Conta Corrente
    [Documentation]  Validar a funcionalidade 10. CONSULTAR MOVIMENTAÇÃO FINANCEIRA POR CC
    [Tags]           FluxoPrincipal
    Consultar Movimentacao Financeira Por Conta Corrente
    
CT002 -FP- Consultar Movimentacao Financeira Por Codigo de Cliente
    [Documentation]  Validar a funcionalidade 20.  CONSULTAR MOVIMENTAÇÃO FINANCEIRA POR CLTCOD
    [Tags]           FluxoPrincipal
    Consultar Movimentacao Financeira Por Codigo de Cliente
    
CT003 -FP- Consultar Movimentacao Financeira Por Grupo Economico
    [Documentation]  Validar a funcionalidade 30.  CONSULTAR MOVIMENTAÇÃO FINANCEIRA POR GRUPO ECONOMICO
    [Tags]           FluxoPrincipal
    Consultar Movimentacao Financeira Por Grupo Economico