*** Settings ***
Resource         resources/commons/Base.robot
Resource         testdata/testcases/transferencia_doc/transferencia_outros_bancos_doc_TC.robot
Resource         resources/commons/Database.robot
Suite Setup      Preparar Suite
Suite Teardown   Terminar Suite
Test Setup       Acessar Pagina Inicial

*** Test Cases ***
CT001 - FP - Transfere Valores para Outros Bancos por DOC - Gestor - CC para CC - PF
    [Documentation]    Verificar o comportamento do sistema ao solicitar "Transferência de Valores para Outros Bancos - DOC" como "Gestor do Telebanco",  utilizando um "Cliente Pessoa Física" com "Conta Corrente" para conta corrente.
    [Tags]             DOC_CC  PF
    Realizar DOC - Gestor - CC para CC - PF

CT002 - FP - Transfere Valores para Outros Bancos por DOC - Gestor - CP para CC - PF
    [Documentation]    Verificar o comportamento do sistema ao solicitar "Transferência de Valores para Outros Bancos - DOC" como "Gestor do Telebanco",  utilizando um "Cliente Pessoa Física" com "Conta Poupança" para conta corrente.
    [Tags]             DOC_CP  PF
    Realizar DOC - Gestor - CP para CC - PF

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente
## ID:14080: CT004-FP-P-Transfere Valores para Outros Bancos por DOC - Atendente - CC para CP - PF

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente
## ID:14092: CT005-FP-P-Transfere Valores para Outros Bancos por DOC - Atendente - CP para CP - PF

CT007 - FP - Transfere Valores para Outros Bancos por DOC - Gestor - CC para CC - PJ
    [Documentation]    Verificar o comportamento do sistema ao solicitar "Transferência de Valores para Outros Bancos - DOC" como "Gestor do Telebanco",  utilizando um "Cliente Pessoa Juridíca" com "Conta Corrente" para conta corrente.
    [Tags]             DOC_CC  PJ
    Realizar DOC - Gestor - CC para CC - PJ

CT008 - FP - Transfere Valores para Outros Bancos por DOC - Gestor - CP para CC - PJ
    [Documentation]    Verificar o comportamento do sistema ao solicitar "Transferência de Valores para Outros Bancos - DOC" como "Gestor do Telebanco",  utilizando um "Cliente Pessoa Juridíca" com "Conta Poupança" para conta corrente.
    [Tags]             DOC_CP  PJ
    Realizar DOC - Gestor - CP para CC - PJ

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente    
## ID:14188: CT010-FP-P-Transfere Valores para Outros Bancos por DOC - Atendente - CC para CP - PJ

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente
## ID:14191: CT011-FP-P-Transfere Valores para Outros Bancos por DOC - Atendente - CP para CP - PJ

#Aguardando resposta do e-mail do que é um pré-cadastro
##CT028 - FA4 - Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Gestor - CC para CC - PF

#Aguardando resposta do e-mail do que é um pré-cadastro    
##CT029 - FA4 - Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Gestor - CP para CC- PF

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente    
## ID:14380: CT031-FA4-P-Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Atendente - CC para CP - PF

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente
## ID:14382: CT032-FA4-P-Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Atendente - CP para CP - PF

#Aguardando resposta do e-mail do que é um pré-cadastro
##CT034 - FA4 - Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Gestor - CC para CC PJ

#Aguardando resposta do e-mail do que é um pré-cadastro
##CT035 - FA4 - Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Gestor - CP para CC PJ

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente    
## ID:14410: CT037-FA4-P-Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Atendente - CC para CP - PJ

#Aguardando resposta do e-mail de como simular um atendimento com o perfil Atendente
## ID:14412: CT038-FA4-P-Realiza DOC sem Pré-Cadastro e para Mesma Titularidade - Atendente - CP para CP - PJ

CT059 - FE6 - Valor Transferência Ultrapassa Limite do Cliente Telebanco - Selecionando Favorecido
    [Documentation]    Verificar o comportamento do sistema quando o valor da tranferência ultrapassa o valor limite do cliente.
    [Tags]             DOC  FE
    Realizar DOC - Limite ultrapassado para favorecido selecionado

#Aguardando resposta do e-mail encaminhado dia 17/05/2021 às 17:10 referente aos campos duplicados.
# CT060 - FE6 - Valor Transferência Ultrapassa Limite do Cliente Telebanco - Mesma Titularidade
    # [Documentation]    Verificar o comportamento do sistema quando o valor da tranferência ultrapassa o valor limite do cliente.
    # [Tags]             DOC  FE
    # Realizar DOC - Limite ultrapassado para mesma titularidade

CT046 - FE3 - Dados Obrigatórios Não Informados - Selecionando Favorecido
    [Documentation]    Verificar o comportamento do sistema quando não for informado dados para os campos obrigatórios e tentar prosseguir.
    [Tags]             DOC  FE
    Realizar DOC - Dados obrigatórios não informados para favorecido selecionado

CT047 - FE3 - Dados Obrigatórios Não Informados - Mesma Titularidade
    [Documentation]    Verificar o comportamento do sistema quando não for informado dados para os campos obrigatórios e tentar prosseguir.
    [Tags]             DOC  FE
    Realizar DOC - Dados obrigatórios não informados para mesma titularidade

CT061 - FE7 - Assinatura Eletrônica Informada não Confere
    [Documentation]    Verificar o comportamento do sistema quando a assinatura eletrônica informada seja diferente da cadastrada no sistema.
    [Tags]             DOC  FE
    Realizar DOC - Assinatura eletrônica incorreta

CT062 - FE8 - Número de Tentativa Excedido
    [Documentation]    Verificar o comportamento do sistema quando o número de tentativas de assinatura eletrônica for excedido.
    [Tags]             DOC  FE
    Realizar DOC - Realizar transferência com número de tentativas excedido

CT114 - FE - N - Sem saldo suficiente para realizar a transferência
    [Documentation]    Validar o comportamento do sistema quando o saldo da Conta Origem for insuficiente para realizar a transferência.
    [Tags]             DOC  FE
    Realizar DOC - Realizar transferência com saldo insuficiente