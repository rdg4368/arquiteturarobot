***Settings***
Library    DatabaseLibrary
Library    String

*** Variables ***
${SQL_COLOCAR_SALDO}    UPDATE BLK2.SLD SET SLDVAL = 10000000000000 WHERE CTAID IN (SELECT A.CTAID FROM BLK2.CTA A, BLK2.RCC B WHERE A.CTAID = B.CTAID AND A.CTANUM = {0}) AND SLDTIP = 7

${SQL_ZERAR_SALDO}      UPDATE BLK2.SLD SET SLDVAL = 0 WHERE CTAID IN (SELECT A.CTAID FROM BLK2.CTA A, BLK2.RCC B WHERE A.CTAID = B.CTAID AND A.CTANUM = {0}) AND SLDTIP = 7

***Keywords***

Colocar saldo
    [Arguments]     ${conta}
    ${SQL}  Format String    ${SQL_COLOCAR_SALDO}    ${conta}
    Execute Sql String    ${SQL}
    
Zerar saldo
    [Arguments]     ${conta}
    ${SQL}  Format String    ${SQL_ZERAR_SALDO}    ${conta}
    Execute Sql String    ${SQL}