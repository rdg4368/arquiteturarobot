*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      String
Variables    resources/commons/properties.py    

*** Variable ***
${SQL_DESFAZER_TRANSACAO}
...    DELETE FROM BLK2.FTN_CMV WHERE CMVNSUTRN = {0}

${SQL_DESFAZER_TRANSACAO_INVALIDA}
...    DELETE FROM BLK2.FTN_CMV WHERE CMVCTANUM = {0} AND TO_CHAR(CMVDATHORTRN,'yyyy-mm-dd hh24:mi:ss') > (TO_CHAR(TRUNC(SYSDATE) ,'yyyy-mm-dd') )

*** Keywords ***    
Desfazer transação
    [Arguments]     ${nsu}
    ${SQL}  Format String    ${SQL_DESFAZER_TRANSACAO}    ${nsu}
    Execute Sql String       ${SQL}

Desfazer transação inválida    
    [Arguments]     ${conta}
    ${SQL}  Format String    ${SQL_DESFAZER_TRANSACAO_INVALIDA}    ${conta}
    Execute Sql String       ${SQL}