*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      String
Variables    resources/commons/properties.py    

*** Variable ***
${SQL_REMOVER_FAVORECIDOS}
...    DELETE FROM BLK2.CAM WHERE CAMNOM = 'MYB FAVORECIDO ROBOT'

*** Keywords ***    
Remover favorecidos
    Execute Sql String       ${SQL_REMOVER_FAVORECIDOS}