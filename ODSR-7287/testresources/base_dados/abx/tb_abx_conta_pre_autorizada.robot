*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      String
Variables    resources/commons/properties.py    

*** Variable ***
${SQL_REMOVER_FAVORECIDO_OUTROS_BANCOS}
...    DELETE FROM ABX.TB_ABX_CONTA_PRE_AUTORIZADA WHERE NR_CONTA_DESTINO = 1234567891011 AND NR_BANCO_DESTINO = 1 AND NR_AGENCIA_DESTINO = 1111

*** Keywords ***    
Remover favorecido de outros bancos do ABX
    Execute Sql String       ${SQL_REMOVER_FAVORECIDO_OUTROS_BANCOS}