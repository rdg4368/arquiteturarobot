*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      String
Variables    resources/commons/properties.py

*** Variable ***
${SQL_DESBLOQUAR_USUARIO}
...    UPDATE MYB.UTB SET UTBSEN = '8766a5980530c9ad7d8cc91e0a95ac9c7d02804ef083733fb426bae27f16ed4e', UTBSALT = '4fc7b416', UTBSITSEN = '0', UTBTRCSEN = '0', UTBDATSEN = TRUNC(SYSDATE), UTBQTDTNTASNELE = '0' WHERE UTBMATNUM IN ('${properties.telebanco_login}')

${SQL_BLOQUEAR_USUARIO}
...    UPDATE MYB.UTB SET UTBSITSEN = '1', UTBTRCSEN = '1' WHERE UTBMATNUM IN ('${properties.telebanco_login}')

${SQL_SENHA_EXPIRADA}
...    UPDATE MYB.UTB SET UTBDATSEN = TRUNC(SYSDATE-365) WHERE UTBMATNUM IN ('${properties.telebanco_login}')

${SQL_SENHA_PROVISORIA_EXPIRADA}
...    UPDATE MYB.UTB SET UTBSEN = '8766a5980530c9ad7d8cc91e0a95ac9c7d02804ef083733fb426bae27f16ed4e', UTBSALT = '4fc7b416', UTBSITSEN = '2', UTBTRCSEN = '1', UTBDATSEN = TRUNC(SYSDATE-365), UTBQTDTNTASNELE = '0' WHERE UTBMATNUM IN ('${properties.telebanco_login}')

${SQL_PRIMEIRO_ACESSO}
...    DELETE FROM MYB.HSU WHERE HSUNUMMATEXE = ${properties.telebanco_login}

${SQL_PRIMEIRO_ACESSO_2}
...    UPDATE MYB.UTB SET UTBSEN = '8766a5980530c9ad7d8cc91e0a95ac9c7d02804ef083733fb426bae27f16ed4e', UTBSALT = '4fc7b416', UTBSITSEN = '2', UTBTRCSEN = '1', UTBDATSEN = TRUNC(SYSDATE), UTBQTDTNTASNELE = '0' WHERE UTBMATNUM IN ('${properties.telebanco_login}')

${SQL_SENHA_PADRAO}
...    UPDATE MYB.HSU SET HSUSEN = '8766a5980530c9ad7d8cc91e0a95ac9c7d02804ef083733fb426bae27f16ed4e' WHERE HSUNUMMATEXE = ${properties.telebanco_login}

${SQL_RESETAR_QNTD_TENTATIVAS_ASSINATURA_ELETRONICA}
...    UPDATE MYB.UTB SET UTBQTDTNTASNELE = 0 WHERE UTBMATNUM = 644971

*** Keywords ***
Desbloquear Usuario Mybank
    Execute Sql String       ${SQL_DESBLOQUAR_USUARIO}

Bloquear Usuario Mybank
    Execute Sql String       ${SQL_BLOQUEAR_USUARIO}

Executar Query Senha de Acesso Expirada
    Execute Sql String       ${SQL_SENHA_EXPIRADA}

Query Senha Provisoria Expirada
    Execute Sql String       ${SQL_SENHA_PROVISORIA_EXPIRADA}

Definir primeiro acesso
    Execute Sql String       ${SQL_PRIMEIRO_ACESSO}
    Execute Sql String       ${SQL_PRIMEIRO_ACESSO_2}

Definir senha padrão
    #A senha que é utilizada nos testes é Nuteb159
    Execute Sql String    ${SQL_SENHA_PADRAO}

Resetar contador de tentativas de assinatura eletrônica incorreta
    Execute Sql String       ${SQL_RESETAR_QNTD_TENTATIVAS_ASSINATURA_ELETRONICA}