import _jpype
import os
import jaydebeapi
from properties import Properties

#OBS
#
#        ESSA IMPLEMENTAÇÃO DO CÓDIGO FIXO FOI FEITO TEMPORARIAMENTE DEVIDO AOS ERROS DE INTEGRIDADE DE DADOS DO MYB QUE ATÉ ENTÃO NÃO FORAM RESOLVIDOS
#
#
#CONTA_CORRENTE_PJ 0770098002

#[chave = chavemassa]
def recuperarMassa(chave):
    
        if chave == 'CONTA_CORRENTE_PJ':
            result = '0110349288' 
        elif chave == 'CONTA_CORRENTE_PF':
            result = '0110345932'
        elif chave == 'CONTA_POUPANCA_PF':
            result = '0110000161'
        elif chave == 'CONTA_POUPANCA_PJ':
            result = '2110016463'
        elif chave == 'CONTA_SALARIO_PF':
            result = '0110155050'
        elif chave == 'CONTA_POUPANCA_PI_PF':
            result = '0110025113'
        elif chave == 'CONTA_POUPANCA_PI_PJ':
            result = '0110037162'
        
        return result

# import _jpype
# import os
# import jaydebeapi
# from properties import Properties
# 
# 
# #[chave = chavemassa]
# def recuperarMassa(chave):
#     
#         conn =jaydebeapi.connect("com.mysql.jdbc.Driver", Properties.url_jdbc_carga , [ Properties.usuario_carga,  Properties.password_carga] )
#         cursorMySql = conn.cursor()
#         
#         
#         query = "SELECT VALOR FROM MYB_ROBOT.MASSA_DADOS WHERE CHAVE = ':param1'"
#         print(query)
#         query= query.replace(':param1', chave)
#         result = cursorMySql.execute(query)
#         result = cursorMySql.fetchall()
#         return result[0][0] 