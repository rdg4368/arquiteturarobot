*** Settings ***
Variables                       properties.py
Library                         DatabaseLibrary
Library                         SeleniumLibrary

*** Keywords ***
Conectar BD
    Connect To Database Using Custom Params  jaydebeapi  'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@${properties.db_host}:${properties.db_port}/${properties.db_name}', ['${properties.db_username}', '${properties.db_password}']

Desconectar BD
    Disconnect from Database
    
Conectar BD ABX
    Connect To Database Using Custom Params  jaydebeapi  'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@${properties.db_host}:${properties.db_port}/${properties.db_name_abx}', ['${properties.db_username_abx}', '${properties.db_password_abx}']