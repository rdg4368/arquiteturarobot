*** Settings ****
Variables               properties.py
Variables               variables/atendimento.py
Library                 SeleniumLibrary

*** Variables ***
${diretorio_screenshot}     results
${URL}                      http://mybhmo.brb.com.br/mybank/login_operador.xhtml;jsessionid=
${endereco_ass_eletronica}  http://hab340208.brb.com.br:7041/mybank/portal/assinaturaeletronica/altera_assinatura_eletronica.xhtml

*** Keywords ***

Abrir navegador
	Open Browser	            ${URL}    Chrome
	Set Screenshot Directory    ${diretorio_screenshot}
    Maximize Browser Window
    #Set Window Size    1920    1080
    Delete All Cookies

Fechar Navegador
    Close Browser

Fechar todos os navegadores
    Close All Browsers

Acessar Atendimento
    Click Element    ${atendimento}

Acessar página de assinatura eletrônica
    Go To    ${endereco_ass_eletronica}

Definir zoom para "${tamanho}"%
    Execute Javascript    document.body.style.zoom="${tamanho}%"