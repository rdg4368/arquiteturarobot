*** Settings ***
Library   SeleniumLibrary
Resource  Global.robot
Resource  Database.robot
Variables    properties.py

*** Keywords ***
Preparar Suite
    Conectar BD
    Abrir Navegador

Terminar Suite
    Desconectar BD
    Fechar Navegador

Acessar Pagina Inicial
   Delete All Cookies
   Go To          ${properties.telebanco}