*** Settings ***
Library     SeleniumLibrary
Variables   variables/atendimento.py
Variables   variables/login.py
Variables   resources/commons/properties.py
Library     String

*** Variables ***
${menu_usuarios}                        //a[@title='Usuários']
${menu_consultas}                       //a[@title='Consultas']
${menu_pagamentos}                      //a[@title='Pagamentos']
${menu_transferencias}                  //a[@title='Transferências']
${menu_servicos}                        //a[@title='Serviços']
${menu_investimentos}                   //a[@title='Investimentos']

${opcao_excluir_assinatura_eletronica}  //a[@title='Excluir Assinatura Eletrônica (Portal Multicanal)']
${opcao_saldo}                          //a[@title='Saldo']
${opcao_extrato}                        //a[@title='Extrato']
${opcao_comprovantes}                   //a[@title='Comprovantes']
${opcao_posicao_integrada_de_saldos}    //a[@title='Posição Integrada de Saldos']
${opcao_documentos_codigo_de_barras}    //a[@title='Documentos com Código de Barras']
${opcao_doc}                            //a[@title='DOC']
${opcao_ted}                            //a[@title='TED']
${opcao_transferencia_entre_contas}     //a[@title='Transferência entre contas BRB']
${opcao_gerenciar_favorecidos}          //a[@title='Gerenciar Favorecidos (TED, DOC e Contas BRB)']
${opcao_consultar_token}                //a[@title='Consultar Token']
${opcao_gerar_identificacao_positiva}   //a[@title='Gerar Identificação Positiva']
${opcao_atualizar_endereco_corresp}     //a[@title='Atualizar Endereço de Correspondência']
${opcao_aplicacao_fundos_investimento}  //a[@title='Aplicação em Fundos de Investimento']

${botao_gestao}                         //a[@title='Gestão']
${botao_atendimento}                    //a[@title='Atendimento']
${botao_administracao}                  //a[@title='Administração']
${botao_incluir_atendimento}            //span[contains(text(),'Incluir Atendimento')]
${botao_consultar}                      //button[contains(@onclick,'CONSULTAR')]
${botao_visualizar}                     //button[contains(@onclick,'VISUALIZAR')]
${botao_capturar}                       //button[contains(@onclick,'CAPTURAR')]
${botao_continuar}                      //button[contains(@onclick,'CONTINUAR')]
${botao_confirmar}                      //button[contains(@onclick,'CONFIRMAR')]
${botao_cadastrar}                      //button[contains(@onclick,'CADASTRAR')]
${botao_realizar_doc_para_mesma_titul}  //button[contains(@onclick,'REALIZAR DOC PARA MESMA TITULARIDADE')]
${botao_favorecido_conta_corrente}      (//td[contains(text(),'Conta Corrente')]//preceding::a[contains(@onclick,'Selecionar favorecido')][1])[1]
${botao_favorecido_conta_poupanca}      (//td[contains(text(),'Conta Poupança')]//preceding::a[contains(@onclick,'Selecionar favorecido')][1])[1]
${botao_verificar_conta}                //a[contains(@onclick,'Verificar Conta')]
${botao_consultar_favorecido}           //a[contains(@onclick,'Consultar Favorecido')]
${botao_cadastrar_novo_favorecido}      //button[contains(@onclick,'CADASTRAR NOVO FAVORECIDO')]

${datepicker_data_inicial}              //button[@title='Informe a data inicial']
${datepicker_data_final}                //button[@title='Informe a data final']
${datepicker_mes_anterior}              //a[@title='Anterior']
${datepicker_dia_1}                     (//*[@data-handler='selectDay'])[1]
${datepicker_dia_28}                    (//*[@data-handler='selectDay'])[28]

${input_conta}                          //input[@title='Informe a Conta']
${input_ramal}                          id=formulario:ramal
${input_telefone_discado}               //input[@title='Informe o número do telefone discado']
${input_telefone_digitado}              //input[@title='Informe o número do telefone digitado']
${input_valor_transferencia}            //input[@title='Informe o valor da transferência']
${input_identificador_transferencia}    (//span[contains(text(),'Identificador da Transferência')]//following::input)[1]
${input_assinatura_eletronica}          (//span[contains(text(),'Assinatura Eletrônica')]//following::input)[1]
${input_conta_de_destino}               (//span[contains(text(),'Conta de Destino')]//following::input)[1]
${input_descricao}                      (//span[contains(text(),'Descrição')]//following::input)[1]

${combobox_tipo_de_servico}             //label[contains(text(),'Selecione o Tipo de Serviço')]
${combobox_tipo_de_pessoa}              //label[contains(text(),'Selecione o Tipo de Pessoa')]
${combobox_tipo_de_extrato}             //div[@title='Seleciona uma opção para o extrato']
${combobox_periodo_de_consulta}         //div[@title='Informe']
${combobox_finalidade}                  //div[@title='Selecione uma finalidade']

${option_23}                            //li[@data-label=23]
${option_pessoa_fisica}                 //li[@data-label='Pessoa Física']
${option_pessoa_juridica}               //li[@data-label='Pessoa Jurídica']
${option_diario}                        //li[@data-label='Diário']
${option_mensal}                        //li[@data-label='Mensal']
${option_por_periodo}                   //li[@data-label='Por Período']
${option_primeira_finalidade}           (//li[@data-label]//following::li)[1]

${radiobutton_poupanca_integrada}       //label[contains(text(),'Poupança Integrada')]
${radiobutton_conta_corrente}           //label[contains(text(),'Conta Corrente')]

*** Keywords ***
Incluir atendimento manual
    [Arguments]                      ${conta}    ${tipo_pessoa}
    Wait Until Page Contains         Gerar Atendimento Manual
    Press Keys                       ${input_conta}    ${conta}
    Click Element                    ${combobox_tipo_de_servico}
    Wait Until Element Is Visible    ${option_23}
    Click Element                    ${option_23}
    ${random_ramal} =  Generate Random String    4    [NUMBERS]
    Press Keys                       ${input_ramal}                ${random_ramal}
    Click Element    ${combobox_tipo_de_pessoa}
    Wait Until Element Is Visible    ${option_pessoa_fisica}
    Run Keyword If    '${tipo_pessoa}' == 'PF'    Click Element    ${option_pessoa_fisica}
    Run Keyword If    '${tipo_pessoa}' == 'PJ'    Click Element    ${option_pessoa_juridica}
    ${random_telefone} =  Generate Random String    11    [NUMBERS]
    Press Keys                       ${input_telefone_discado}       ${random_telefone}
    ${random_telefone} =  Generate Random String    11    [NUMBERS]
    Press Keys                       ${input_telefone_digitado}      ${random_telefone}
    Clicar em "Incluir Atendimento"
    Switch Window    NEW
    Close Window
    Switch Window    MAIN

Acessar a opção "${opcao}" do menu "${menu}"
    #menus
    Run Keyword If  '${menu}' == 'Usuários'        Mouse Over    ${menu_usuarios}
    Run Keyword If  '${menu}' == 'Consultas'       Mouse Over    ${menu_consultas}
    Run Keyword If  '${menu}' == 'Pagamentos'      Mouse Over    ${menu_pagamentos}
    Run Keyword If  '${menu}' == 'Transferências'  Mouse Over    ${menu_transferencias}
    Run Keyword If  '${menu}' == 'Serviços'        Mouse Over    ${menu_servicos}
    Run Keyword If  '${menu}' == 'Investimentos'   Mouse Over    ${menu_investimentos}     

    #opções de menu
    Run Keyword If  '${opcao}' == 'Excluir Assinatura Eletrônica'    Clicar em "Excluir Assinatura Eletrônica"
    Run Keyword If  '${opcao}' == 'Saldo'                            Clicar em "Saldo"
    Run Keyword If  '${opcao}' == 'Extrato'                          Clicar em "Extrato"
    Run Keyword If  '${opcao}' == 'Comprovantes'                     Clicar em "Comprovantes"
    Run Keyword If  '${opcao}' == 'Posição Integrada de Saldos'      Clicar em "Posição Integrada de Saldos"
    Run Keyword If  '${opcao}' == 'Documentos com Código de Barras'  Clicar em "Documentos com Código de Barras"
    Run Keyword If  '${opcao}' == 'DOC'                              Clicar em "DOC"
    Run Keyword If  '${opcao}' == 'TED'                              Clicar em "TED"
    Run Keyword If  '${opcao}' == 'Transferência entre contas BRB'   Clicar em "Transferência entre contas BRB"
    Run Keyword If  '${opcao}' == 'Gerenciar Favorecidos'            Clicar em "Gerenciar Favorecidos"
    Run Keyword If  '${opcao}' == 'Consultar Token'                  Clicar em "Consultar Token"    
    Run Keyword If  '${opcao}' == 'Gerar Identificação Positiva'     Clicar em "Gerar Identificação Positiva"
    Run Keyword If  '${opcao}' == 'Atualizar Endereço'               Clicar em "Atualizar Endereço"
    Run Keyword If  '${opcao}' == 'Aplicação em Fundos'              Clicar em "Aplicação em Fundos"
    

Clicar em "${opcao}"
    #botões do painel superior (Administração / Gestão / Atendimento)
    Run Keyword If  '${opcao}' == 'Administração'  Click Element             ${botao_administracao}
    Run Keyword If  '${opcao}' == 'Administração'  Wait Until Page Contains  Bem vindo

    Run Keyword If  '${opcao}' == 'Gestão'         Click Element             ${botao_gestao}
    Run Keyword If  '${opcao}' == 'Gestão'         Wait Until Page Contains  Consultar - Atendimentos

    Run Keyword If  '${opcao}' == 'Atendimento'    Click Element             ${botao_atendimento}
    Run Keyword If  '${opcao}' == 'Atendimento'    Wait Until Page Contains  Realizar Atendimento

    #opções de menu
    Run Keyword If  '${opcao}' == 'Saldo'                            Click Element    ${opcao_saldo}
    Run Keyword If  '${opcao}' == 'Extrato'                          Click Element    ${opcao_extrato}
    Run Keyword If  '${opcao}' == 'Comprovantes'                     Click Element    ${opcao_comprovantes}
    Run Keyword If  '${opcao}' == 'Posição Integrada de Saldos'      Click Element    ${opcao_posicao_integrada_de_saldos}
    Run Keyword If  '${opcao}' == 'Documentos com Código de Barras'  Click Element    ${opcao_documentos_codigo_de_barras}
    Run Keyword If  '${opcao}' == 'Excluir Assinatura Eletrônica'    Click Element    ${opcao_excluir_assinatura_eletronica}
    Run Keyword If  '${opcao}' == 'DOC'                              Click Element    ${opcao_doc}
    Run Keyword If  '${opcao}' == 'TED'                              Click Element    ${opcao_ted}
    Run Keyword If  '${opcao}' == 'Transferência entre contas BRB'   Click Element    ${opcao_transferencia_entre_contas}
    Run Keyword If  '${opcao}' == 'Gerenciar Favorecidos'            Click Element    ${opcao_gerenciar_favorecidos}
    Run Keyword If  '${opcao}' == 'Consultar Token'                  Click Element    ${opcao_consultar_token}
    Run Keyword If  '${opcao}' == 'Gerar Identificação Positiva'     Click Element    ${opcao_gerar_identificacao_positiva}
    Run Keyword If  '${opcao}' == 'Atualizar Endereço'               Click Element    ${opcao_atualizar_endereco_corresp}
    Run Keyword If  '${opcao}' == 'Aplicação em Fundos'              Click Element    ${opcao_aplicacao_fundos_investimento}


    #botões
    Run Keyword If  '${opcao}' == 'Capturar'                              Click Element    ${botao_capturar}
    Run Keyword If  '${opcao}' == 'Continuar'                             Click Element    ${botao_continuar}
    Run Keyword If  '${opcao}' == 'Confirmar'                             Click Element    ${botao_confirmar}
    Run Keyword If  '${opcao}' == 'Visualizar'                            Click Element    ${botao_visualizar}
    Run Keyword If  '${opcao}' == 'Consultar'                             Click Element    ${botao_consultar}
    Run Keyword If  '${opcao}' == 'Cadastrar'                             Click Element    ${botao_cadastrar}
    Run Keyword If  '${opcao}' == 'Incluir Atendimento'                   Click Element    ${botao_incluir_atendimento}
    Run Keyword If  '${opcao}' == 'Realizar DOC para mesma titularidade'  Click Element    ${botao_realizar_doc_para_mesma_titul}
    Run Keyword If  '${opcao}' == 'Verificar Conta'                       Click Element    ${botao_verificar_conta}
    Run Keyword If  '${opcao}' == 'Consultar Favorecido'                  Click Element    ${botao_consultar_favorecido}
    Run Keyword If  '${opcao}' == 'Cadastrar Novo Favorecido'             Click Element    ${botao_cadastrar_novo_favorecido}

    #comboboxes
    Run Keyword If  '${opcao}' == 'Períodos de Consulta'  Click Element    ${combobox_periodo_de_consulta}
    Run Keyword If  '${opcao}' == 'Tipo de extrato'       Click Element    ${combobox_tipo_de_extrato}

    #options de combobox
    Run Keyword If  '${opcao}' == 'Por Período'  Click Element   ${option_por_periodo}
    Run Keyword If  '${opcao}' == 'Diário'       Click Element   ${option_diario}
    Run Keyword If  '${opcao}' == 'Mensal'       Click Element   ${option_mensal}

    #radiobuttons
    Run Keyword If  '${opcao}' == 'Poupança Integrada'    Click Element    ${radiobutton_poupanca_integrada}

Informar período válido
    Wait Until Element Is Visible    ${datepicker_data_inicial}
    Click Element    ${datepicker_data_inicial}
    Click Element    ${datepicker_mes_anterior}
    Click Element    ${datepicker_dia_1}
    Click Element    ${datepicker_data_final}
    Click Element    ${datepicker_mes_anterior}
    Click Element    ${datepicker_dia_28}

Preencher dados de conta destino "${fluxo_de_transacao}"
    Press Keys    None    END
    Press Keys    None    END
    Wait Until Element Is Visible    ${combobox_finalidade}
    Sleep    1
    Click Element  ${combobox_finalidade}
    Wait Until Element Is Visible    ${option_primeira_finalidade}
    Click Element  ${option_primeira_finalidade}
    Press Keys    None    END
    Press Keys    None    END
    Sleep    1
    Run Keyword If    '${fluxo_de_transacao}'=='válido'    Press Keys     ${input_valor_transferencia}          1
    Run Keyword If    '${fluxo_de_transacao}'=='com valor superior ao limite'    Press Keys     ${input_valor_transferencia}          10000000000000
    Input Text     ${input_identificador_transferencia}  MYB AUTOMACAO ROBOT TRANSFERENCIA

Selecionar favorecido com "${tipo_de_conta}"
    Press Keys    None    END
    Sleep    1
    Run Keyword If    '${tipo_de_conta}'=='Conta Corrente'    Click Element    ${botao_favorecido_conta_corrente}
    Run Keyword If    '${tipo_de_conta}'=='Conta Poupança'    Click Element    ${botao_favorecido_conta_poupanca}
    Wait Until Page Contains    Dados da Conta Destino

Informar assinatura eletrônica "${fluxo_de_transacao}"
    Press Keys    None    END
    Wait Until Element Is Visible    ${input_assinatura_eletronica}
    Run Keyword If    '${fluxo_de_transacao}'=='válida'    Input Text    ${input_assinatura_eletronica}    ${properties.telebanco_assEle}
    Run Keyword If    '${fluxo_de_transacao}'=='inválida'  Input Text    ${input_assinatura_eletronica}    123

Validar mensagem de erro
    [Arguments]    ${screenshot}    ${mensagem}
    Wait Until Page Contains  ${mensagem}
    Capture Page Screenshot   ${screenshot}

Realizar excesso de tentativas de assinatura eletrônica
    :FOR  ${i}  IN RANGE  2
    \  Press Keys    None    DOWN
    \  Press Keys    None    DOWN
    \  Input Text    ${input_assinatura_eletronica}    123
    \  Sleep    1
    \  Clicar em "Confirmar"
    \  Wait Until Page Contains    A assinatura eletrônica informada não confere com a cadastrada
    Press Keys    None    DOWN
    Press Keys    None    DOWN
    Input Text    ${input_assinatura_eletronica}    123
    Sleep    1
    Clicar em "Confirmar"