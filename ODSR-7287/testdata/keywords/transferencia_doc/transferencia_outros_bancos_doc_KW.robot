*** Settings ***
Resource    resources/commons/Base.robot
Resource    resources/commons/CommonsKeywords.robot
Library     SeleniumLibrary
Variables   variables/atendimento.py
Variables   variables/consultaSaldo.py

*** Variables ***
${botao_favorecido_conta_corrente}    (//td[contains(text(),'Conta Corrente')]//preceding::a[contains(@onclick,'Selecionar favorecido')][1])[1]
${botao_favorecido_conta_poupanca}    (//td[contains(text(),'Conta Poupança')]//preceding::a[contains(@onclick,'Selecionar favorecido')][1])[1]

${combobox_finalidade}                //div[@title='Selecione uma finalidade']

${option_credito_em_conta_corrente}   //li[@data-label='Crédito em conta corrente']

${input_valor_transferencia}          //input[@title='Informe o valor da transferência']
${input_identificador_transferencia}  //input[@title='Digite uma descrição para esta transferência.']
${input_assinatura_eletronica}        //input[@title='Por favor, informe a senha.']
${input_banco}                        //div[contains(@class,'codigoBanco')]
${input_agencia}                      //input[@title='Digite o número da agência.']
${input_conta_doc}                    //input[@title='Digite o número da conta.']

${radiobutton_conta_corrente}         //input[@title='Digite o número da conta.']//following::label[contains(text(),'Conta Corrente')]

${banco}                              070
${agencia}                            647
${conta}                              2801

*** Keywords ***
Informar dados de transferência "${tipo_de_transferencia}"
    Press Keys    None    END
    Press Keys    None    END
    Wait Until Element Is Visible    ${combobox_finalidade}
    Sleep     1
    Click Element  ${combobox_finalidade}
    Wait Until Element Is Visible    ${option_credito_em_conta_corrente}
    Click Element  ${option_credito_em_conta_corrente}
    Press Keys    None    END
    Press Keys    None    END
    Sleep    1
    Press Keys     ${input_valor_transferencia}          1
    Input Text     ${input_identificador_transferencia}  MYB AUTOMACAO ROBOT PAGAMENTO
    Clicar em "Continuar"
    Wait Until Page Contains    Confirmação
    Run Keyword If  '${tipo_de_transferencia}'=='válido'   Input Text    ${input_assinatura_eletronica}    ${properties.telebanco_assEle}
    Run Keyword If  '${tipo_de_transferencia}'=='inválido'   Input Text    ${input_assinatura_eletronica}    123
    Run Keyword If  '${tipo_de_transferencia}'=='com número de tentativas excedido'   Realizar excesso de tentativas
    Wait Until Element Is Visible    ${botao_confirmar}
    Clicar em "Confirmar"
    Run Keyword If  '${tipo_de_transferencia}'=='válido'   Wait Until Page Contains    realizada com sucesso

Validar transferência
    [Arguments]    ${screenshot}
    Wait Until Page Contains    Comprovante
    Press Keys    None    DOWN
    Definir zoom para "75"%
    Page Should Contain         Canal de Atendimento
    Page Should Contain         NSU da Transação
    Page Should Contain         Data da Transferência
    Page Should Contain         Valor da Transferência
    Page Should Contain         Identificador da Transferência
    Capture Page Screenshot     ${screenshot}
    
Informar valor acima do limite permitido
    Press Keys    None    END
    Press Keys    None    END
    Sleep    1
    Wait Until Element Is Visible    ${combobox_finalidade}    
    Click Element  ${combobox_finalidade}
    Wait Until Element Is Visible    ${option_credito_em_conta_corrente}
    Click Element  ${option_credito_em_conta_corrente}
    Sleep    1
    Press Keys     ${input_valor_transferencia}          99999999999999999
    Input Text     ${input_identificador_transferencia}  MYB AUTOMACAO ROBOT TRANSFERENCIA
    Clicar em "Continuar"
    
Validar limite máximo permitido
    [Arguments]    ${screenshot}
    Wait Until Page Contains    Atenção: {0} Limite máximo para DOC excedido.
    Capture Page Screenshot     ${screenshot}
    
Validar campos obrigatórios não informados
    [Arguments]    ${screenshot}
    Wait Until Page Contains    O campo Finalidade deve ser informado.
    Capture Page Screenshot     ${screenshot}
    
#Aguardando resposta do e-mail encaminhado dia 17/05/2021 às 17:10 referente aos campos duplicados.
# Realizar DOC para mesma titularidade com limite ultrapassado
    # Press Keys    None    DOWN
    # Press Keys    None    DOWN
    # Press Keys    None    DOWN
    # Press Keys    ${input_banco}      ${banco}
    # Press Keys    ${input_agencia}    ${agencia}
    # Press Keys    ${input_conta_doc}  ${conta}
    # Sleep    1
    # Wait Until Element Is Visible    ${combobox_finalidade}    
    # Click Element  ${combobox_finalidade}
    # Wait Until Element Is Visible    ${option_credito_em_conta_corrente}
    # Click Element  ${option_credito_em_conta_corrente}
    # Press Keys     ${input_valor_transferencia}          99999999999999999
    # Input Text     ${input_identificador_transferencia}  MYB AUTOMACAO ROBOT PAGAMENTO
    # Clicar em "Consultar"
    # Wait Until Page Contains    Preenchimento
    # Wait Until Element Is Visible    ${combobox_finalidade}    
    # Click Element  ${combobox_finalidade}
    # Wait Until Element Is Visible    ${option_credito_em_conta_corrente}
    # Click Element  ${option_credito_em_conta_corrente}
    # Press Keys     ${input_valor_transferencia}          99999999999999999
    # Input Text     ${input_identificador_transferencia}  MYB AUTOMACAO ROBOT PAGAMENTO
    # Clicar em "Continuar"
    
Realizar DOC com dados não informados
    Press Keys    None    DOWN
    Press Keys    None    DOWN
    Press Keys    None    DOWN
    Sleep    2    
    Clicar em "Consultar"
    Press Keys    None    DOWN
    Press Keys    None    DOWN  
    Sleep    2    
    Clicar em "Continuar"
    
Validar assinatura eletrônica incorreta
    [Arguments]    ${screenshot}
    Wait Until Page Contains    A assinatura eletrônica informada não confere com a cadastrada
    Capture Page Screenshot     ${screenshot}
    
Realizar excesso de tentativas
    FOR  ${i}  IN RANGE  2
    Press Keys    None    DOWN
    Press Keys    None    DOWN
    Input Text    ${input_assinatura_eletronica}    123
    Clicar em "Confirmar"
    Wait Until Page Contains  A assinatura eletrônica informada não confere com a cadastrada
    END
    Input Text    ${input_assinatura_eletronica}    123
    
Validar excesso de tentativas
    [Arguments]    ${screenshot}
    Wait Until Page Contains    O número máximo de tentativas foi excedido.
    Capture Page Screenshot     ${screenshot}
    
Informar valor superior ao saldo
    Press Keys    None    END 
    Press Keys    None    END
    Wait Until Element Is Visible    ${combobox_finalidade}
    Sleep    1    
    Click Element  ${combobox_finalidade}
    Wait Until Element Is Visible    ${option_credito_em_conta_corrente}
    Click Element  ${option_credito_em_conta_corrente}
    Press Keys    None    END
    Press Keys    None    END
    Sleep    1
    Press Keys     ${input_valor_transferencia}          1
    Input Text     ${input_identificador_transferencia}  MYB AUTOMACAO ROBOT PAGAMENTO
    Clicar em "Continuar"
    
Validar saldo insuficiente
    [Arguments]    ${screenshot}
    Wait Until Page Contains    Atenção: {0} Saldo insuficiente.
    Capture Page Screenshot     ${screenshot}

Realizar DOC "${tipo_de_fluxo}"
    Run Keyword If    '${tipo_de_fluxo}'=='válido'   Preencher dados de conta destino "válido"
    Clicar em "Continuar"
    Run Keyword If    '${tipo_de_fluxo}'=='válido'   Informar assinatura eletrônica "válida"
    Sleep    1
    Clicar em "Confirmar"