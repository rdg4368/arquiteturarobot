*** Settings ***
Library     SeleniumLibrary 
Variables   variables/login.py
Variables   variables/atendimento.py
Resource    resources/commons/CommonsKeywords.robot

*** Variables ***


*** Keywords ***
Validar extrato
    [Arguments]    ${tipo_de_conta}    ${screenshot}
    Wait Until Page Contains    DETALHAMENTO
    Page Should Contain         Data:
    Page Should Contain         Hora:
    Page Should Contain         Lançamentos
    Page Should Contain         Data
    Page Should Contain         Descrição
    Page Should Contain         DOC
    Page Should Contain         Valor
    Page Should Contain         Saldo
    Run Keyword If    '${tipo_de_conta}' == 'Conta Corrente'    Page Should Contain    Saldo da Conta Corrente
    Run Keyword If    '${tipo_de_conta}' == 'Conta Salário'     Page Should Contain    Saldo de Conta Salário
    Run Keyword If    '${tipo_de_conta}' == 'Conta Poupança'    Page Should Contain    Saldo da conta poupança:
    Capture Page Screenshot     ${screenshot}