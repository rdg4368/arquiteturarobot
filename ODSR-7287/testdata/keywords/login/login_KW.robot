*** Settings *** 
Resource    resources/commons/Global.robot
Library     SeleniumLibrary 
Variables   variables/login.py

*** Variables ***
${input_login}                           //input[@title='Informe o número da Matrícula para realizar a consulta']
${input_senha}                           (//input[@type='password'])[1]
${input_nova_senha}                      (//input[@type='password'])[2]
${input_confirma_nova_senha}             (//input[@type='password'])[3]
${input_assinatura_eletronica_acesso}    (//input[@type='password'])[4]
${input_ramal_gestao}                    id=formulario:ramal
${input_ramal_atendimento}               id=formRamal:ramal

${botao_acessar}                         //span[contains(text(),'ACESSAR')]
${botao_confirmar}                       //span[contains(text(),'CONFIRMAR')]
${botao_sair}                            //a[@title='Sair']
${botao_sair_confirmar}                  id=formMenu:confirmaSair
${botao_continuar}                       id=formRamal:botaoContinuar
${botao_gestao}                          //a[@title='Gestão']

*** Keywords ***
Realizar login "${tipo_login}"
    Run Keyword If    '${tipo_login}'=='com confirmação de senha divergente'    Realizar login com confirmação de senha divergente
    Run Keyword If    '${tipo_login}'=='com matrícula não cadastrada'    Input Text    ${input_login}    111111
    Run Keyword If    '${tipo_login}'=='com matrícula não cadastrada'    Input Text    ${input_senha}    111111
    Run Keyword If    '${tipo_login}'=='válido'    Input Text      ${input_login}      ${properties.telebanco_login}
    Run Keyword If    '${tipo_login}'=='válido'    Input Text      ${input_senha}      ${properties.telebanco_senha}
    Run Keyword If    '${tipo_login}'=='válido com outro usuário'    Input Text      ${input_login}      646547
    Run Keyword If    '${tipo_login}'=='válido com outro usuário'    Input Text      ${input_senha}      Nuteb159
    Run Keyword If    '${tipo_login}'=='inválido'    Input Text    ${input_login}      ${properties.telebanco_login}
    Run Keyword If    '${tipo_login}'=='inválido'    Input Text    ${input_senha}    a    
    Run Keyword If    '${tipo_login}'!='com confirmação de senha divergente'    Click Element     ${botao_acessar}

Realizar login com confirmação de senha divergente
    Realizar login "válido"
    Wait Until Element Is Visible    ${input_nova_senha}    
    Input Text    ${input_senha}                  ${properties.telebanco_senha}
    Input Text    ${input_nova_senha}             111111
    Input Text    ${input_confirma_nova_senha}    222222
    Click Element    ${botao_confirmar}    

Iniciar atendimento "${tipo_de_atendimento}"
    Run Keyword If  '${tipo_de_atendimento}'=='inválido'  Press Keys       ${input_ramal_atendimento}    0000
    Run Keyword If  '${tipo_de_atendimento}'=='válido'  Press Keys       ${input_ramal_atendimento}    1234
    Click Element    ${botao_continuar}

Validar login
    [Arguments]        ${mensagem}       ${screenshot}
    Page Should Contain          ${mensagem} 
    Capture Page Screenshot      ${screenshot}
    
Validar tela
    [Arguments]        ${opcao}       ${screenshot}
    Run Keyword If    '${opcao}' == 'Gestão'    Wait Until Page Contains    Consultar - Atendimentos
    Run Keyword If    '${opcao}' == 'Senha expirada'    Page Should Contain    Nova Senha
    Run Keyword If    '${opcao}' == 'Senha não confere'    Page Should Contain    Nova Senha
    Capture Page Screenshot      ${screenshot}
    
Validar atendimento
    [Arguments]                 ${screenshot}
    Wait Until Page Contains    Informações Conta X Cliente
    Page Should Contain         Número da Conta
    Page Should Contain         Tipo de Conta
    Page Should Contain         Situação da Conta
    Page Should Contain         Código do Cliente
    Page Should Contain         Tipo de Cliente
    Page Should Contain         Data de Cadastro no Segmento
    Page Should Contain         Kit Serviços
    Page Should Contain         Titular em Atendimento
    Page Should Contain         Nome do Titular
    Page Should Contain         Ordem do Titular
    Page Should Contain         Outras Ordens
    Capture Page Screenshot     ${screenshot}
    
Validar mensagens de erro
    [Arguments]                ${mensagem}    ${screenshot}
    Page Should Contain        ${mensagem}    
    Capture Page Screenshot    ${screenshot}
    
Definir nova senha "${tipo_de_senha}"
    Run Keyword If    '${tipo_de_senha}'=='igual as anteriores'    Input Text     ${input_senha}             ${properties.telebanco_senha}
    Run Keyword If    '${tipo_de_senha}'=='igual as anteriores'    Input Text     ${input_nova_senha}         ${properties.telebanco_senha}
    Run Keyword If    '${tipo_de_senha}'=='igual as anteriores'    Input Text     ${input_confirma_nova_senha}    ${properties.telebanco_senha} 
    Run Keyword If    '${tipo_de_senha}'=='com sequências numéricas ou repetições'    Input Text     ${input_senha}             ${properties.telebanco_senha}
    Run Keyword If    '${tipo_de_senha}'=='com sequências numéricas ou repetições'    Input Text     ${input_nova_senha}         999999999
    Run Keyword If    '${tipo_de_senha}'=='com sequências numéricas ou repetições'    Input Text     ${input_confirma_nova_senha}    999999999
    Run Keyword If    '${tipo_de_senha}'=='inválida'    Input Text     ${input_senha}             Nuteb160
    Run Keyword If    '${tipo_de_senha}'=='inválida'    Input Text     ${input_nova_senha}         Nuteb161
    Run Keyword If    '${tipo_de_senha}'=='inválida'    Input Text     ${input_confirma_nova_senha}    Nuteb161
    Run Keyword If    '${tipo_de_senha}'=='inválida'    Input Text     ${input_assinatura_eletronica_acesso}    ${properties.telebanco_assEle}
    Run Keyword If    '${tipo_de_senha}'=='com assinatura eletrônica inválida'    Input Text     ${input_senha}             ${properties.telebanco_senha}
    Run Keyword If    '${tipo_de_senha}'=='com assinatura eletrônica inválida'    Input Text     ${input_nova_senha}         ${properties.telebanco_senha}
    Run Keyword If    '${tipo_de_senha}'=='com assinatura eletrônica inválida'    Input Text     ${input_confirma_nova_senha}    ${properties.telebanco_senha} 
    Run Keyword If    '${tipo_de_senha}'=='com assinatura eletrônica inválida'    Input Text     ${input_assinatura_eletronica_acesso}    A

    Click Element  ${botao_confirmar}
    
Exceder número de tentativas de acesso
     FOR  ${i}  IN RANGE  0  5
    \  Realizar login "inválido"

Exceder número de tentativas de primeiro acesso
     FOR  ${i}  IN RANGE  0  5
    \  Definir nova senha "inválida"