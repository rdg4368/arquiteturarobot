*** Settings ***
Resource    testdata/keywords/extrato/extrato_KW.robot
Resource    resources/commons/CommonsKeywords.robot
Resource    testdata/keywords/transferencia_doc/transferencia_outros_bancos_doc_KW.robot
Resource    testresources/base_dados/myb/utb.robot
Resource    testresources/base_dados/blk2/sld.robot
Resource    testdata/keywords/login/login_KW.robot
Variables   resources/commons/screenshot.py
Library     resources/commons/MassaDados.py

*** Keywords ***
#CT001
Realizar DOC - Gestor - CC para CC - PF
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Colocar saldo    ${conta}
    Realizar DOC "válido"
    Validar transferência    ${screenshots.ST007_CT001}
    Zerar saldo      ${conta}

#CT002
Realizar DOC - Gestor - CP para CC - PF
    ${conta}    Recuperar Massa    CONTA_POUPANCA_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Colocar saldo    ${conta}
    Realizar DOC "válido"
    Validar transferência    ${screenshots.ST007_CT002}
    Zerar saldo      ${conta}

#CT007    
Realizar DOC - Gestor - CC para CC - PJ
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PJ
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PJ
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Colocar saldo    ${conta}
    Realizar DOC "válido"
    Validar transferência    ${screenshots.ST007_CT005}
    Zerar saldo      ${conta}

#CT008
Realizar DOC - Gestor - CP para CC - PJ
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Colocar saldo    ${conta}
    Realizar DOC "válido"
    Validar transferência    ${screenshots.ST007_CT006}
    Zerar saldo      ${conta}

#CT059 
Realizar DOC - Limite ultrapassado para favorecido selecionado
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Colocar saldo    ${conta}
    Informar valor acima do limite permitido
    Validar limite máximo permitido    ${screenshots.ST007_CT017}
    Zerar saldo      ${conta}
    
#Aguardando resposta do e-mail encaminhado dia 17/05/2021 às 17:10 referente aos campos duplicados.
# Realizar DOC - Limite ultrapassado para mesma titularidade
    # ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    # Realizar login "válido"
    # Clicar em "Gestão"
    # Clicar em "Incluir Atendimento"
    # Incluir atendimento manual    ${conta}  PF
    # Acessar a opção "DOC" do menu "Transferências"
    # Clicar em "Realizar DOC para mesma titularidade"
    # Realizar DOC para mesma titularidade com limite ultrapassado
    # Validar limite máximo permitido    ${screenshots.ST007_CT017}
    
Realizar DOC - Dados obrigatórios não informados para favorecido selecionado
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Clicar em "Continuar"
    Validar campos obrigatórios não informados    ${screenshots.ST007_CT019}
    
Realizar DOC - Dados obrigatórios não informados para mesma titularidade
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Clicar em "Realizar DOC para mesma titularidade"
    Realizar DOC com dados não informados
    Validar campos obrigatórios não informados    ${screenshots.ST007_CT020}
    
Realizar DOC - Assinatura eletrônica incorreta
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Resetar contador de tentativas de assinatura eletrônica incorreta
    Colocar saldo    ${conta}
    Informar dados de transferência "inválido"
    Validar assinatura eletrônica incorreta    ${screenshots.ST007_CT021}
    Zerar saldo      ${conta}
    
Realizar DOC - Realizar transferência com número de tentativas excedido
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Colocar saldo    ${conta}
    Resetar contador de tentativas de assinatura eletrônica incorreta
    Informar dados de transferência "com número de tentativas excedido"
    Validar excesso de tentativas    ${screenshots.ST007_CT022}
    Zerar saldo      ${conta}
    
Realizar DOC - Realizar transferência com saldo insuficiente
    ${conta}    Recuperar Massa    CONTA_CORRENTE_PF
    Realizar login "válido"
    Clicar em "Gestão"
    Clicar em "Incluir Atendimento"
    Incluir atendimento manual    ${conta}  PF
    Acessar a opção "DOC" do menu "Transferências"
    Selecionar favorecido com "Conta Corrente"
    Zerar saldo    ${conta}
    Informar valor superior ao saldo
    Validar saldo insuficiente    ${screenshots.ST007_CT023}
    Colocar saldo    ${conta}