#menus
menuConsultas = "//a[@title='Consultas']"
menuSaldo = "//a[@title='Saldo']"

#campos
campoLogin = "//input[@id='j_idt51:matricula']"
campoSenha = "//input[@id='j_idt51:senha']"
campoNovaSenha = "//input[@id='j_idt42:novaSenha']"
campoConfirmarSenha = "//input[@id='j_idt42:confirmacaoSenha']"
campoRamal = "//input[@id='formRamal:ramal']"
 
#botões
btnAcessarTelebanco = "//span[contains(text(),'ACESSAR')]" 
btnSairTelebanco = "//a[@id='formMenu:j_idt93']"
btnConfirmarSair = "//button[@id='formMenu:confirmaSair']"
btnGestao = "//a[@id='j_idt24:setVisaoGestor']"      
btnConfirmarNovaSenha = "//span[contains(text(),'CONFIRMAR')]" 
btnAtendimento = "//a[@id='j_idt24:setVisaoAtendente']"
btnContinuarAtendimento = "//button[@id='formRamal:botaoContinuar']"
saldo ="//a[@id='formMenu:j_idt62:1:j_idt69:0:j_idt71:9:idMenu']"
radioPoupancaInt = "//label[contains(text(),'Poupança Integrada')]"

#dados
login = "644971"
senhaInvalida = "123456"
novaSenhaTexto = "123456789"
novaSenhaTexto2 = "123456789"
novaSenhaDiferente = "321654789"
ramalForaDeFaixa = "0000"

#validações  
msgSenhaNaoConfere = "A senha informada não confere"
msgUsrBloqueado = "O usuário informado está bloqueado."
msgSenhaExpirada = "Erro não identificado" 
msgNovaSenhaNaoConfere = "A confirmação da nova senha não confere."
msgNovaSenhaSequencia = "A nova senha informada não pode conter sequências numéricas ou repetições" 
msgSenhaIgual = "A nova senha informada deve ser diferente das últimas 5 (cinco) senhas." 
validacaoModuloGestao = "Consultar - Atendimentos"
divBemVindo = "//div[@class='bemVindoLogin']"
msgRamalForaFaixa = "O ramal informado está fora da faixa de ramais disponíveis."

msgSenhaNaoConfere = "A senha informada não confere"

msgMatriculaNaoCadastrada = "A matrícula informada não está cadastrada."

matricula = "111111"