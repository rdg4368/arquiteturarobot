class Mensagens(object):

#mensagens do login
	
	msg_bem_vindo							=  "Bem vindo ao"
	msg_senha_nao_confere 					=  "A senha informada não confere"
	msg_usuario_bloqueado 					=  "O usuário informado está bloqueado."
	msg_senha_expirada 						=  "A senha informada está expirada."
	msg_confirmacao_nova_senha_nao_confere  =  "A confirmação da nova senha não confere."
	msg_senha_sequencia_invalida  			=  "A nova senha informada não pode conter sequências numéricas ou repetições"
	msg_senha_igual_5_ultimas 				=  "A nova senha informada deve ser diferente das últimas 5 (cinco) senhas."
	msg_ramal_fora_da_faixa 				=  "O ramal informado está fora da faixa de ramais disponíveis."
	msg_matricula_nao_cadastrada			=  "A matrícula informada não está cadastrada."
	msg_excedido_num_de_tentativas_senha	=  "O número máximo de tentativas foi excedido."
	msg_ramal_em_uso					    =  "O ramal informado está em uso por outro operador."
	msg_assinatura_eletronica_invalida		=  "A assinatura eletrônica não pode possuir sequência de números ou letra"
	
#mensagens de transações
	msg_limite_excedido						=  "Atenção: {0} Limite definido para o perfil excedido."
	msg_valor_zerado						=  "O Valor de Transferência deve ser superior a zero."
	msg_assinatura_eletronica_incorreta		=  "A assinatura eletrônica informada não confere com a cadastrada"
	msg_cadastro_de_favorecido				=  "A ação de Cadastrar Favorecido foi realizada com sucesso."