atendimento = "//a[@id='j_idt24:setVisaoAtendente']"
gestao      = "//a[@id='j_idt24:setVisaoGestor']"



gestaoAtendimento = "//a[@title='Gestão de Atendimentos']"
atendimentoManual = "//a[contains(text(),'Inclui Atendimento Manual')]"
contaInput        = "//input[@id='formulario:conta:capturaConta__conta__conta:capturaNumerico__conta__input']"
tipoServico       = "//label[@id='formulario:tipoServico_label']"
opServico         = "//li[contains(text(),'23')]"
ramal             = "//input[@id='formulario:ramal']"
tipoPessoa        = "//label[@id='formulario:tipoPessoa_label']"
opPessoaPF        = "//li[contains(text(),'Pessoa Física')]"
opPessoaPJ        = "//li[contains(text(),'Pessoa Jurídica')]"
telefoneDiscado   = "//input[@title='Informe o número do telefone discado']"
telefoneDigitado  = "//input[@title='Informe o número do telefone digitado']"
extratoMensal     = "//li[@data-label='Mensal']"
tpMes             = "//*[@id='formulario:FTN_MesAno:capturaLista__FTN_MesAno__lista_panel']/div/ul/li[2]"

incluirAtendimento = "//span[contains(text(),'Incluir Atendimento')]"


consulta        = "//a[@title='Consultas']"
extrato         = "//a[@title='Extrato']"
  
  
contaPI         = "//body/div[@class='container']/div[@id='content']/div[@class='filtros']/form[@id='formulario']/div[@class='row']/div[@id='formulario:divFormulario']/div/div[@id='formulario:FTN_Sessao_TipoConta:FTN_Sessao_TipoConta_panel']/span[@id='formulario:FTN_Sessao_TipoConta:FTN_Sessao_TipoConta_panel2']/fieldset/table[@id='formulario:FTN_Sessao_TipoConta:capturaRadio__FTN_Sessao_TipoConta__grupo']/tbody/tr/td[3]/div[1]"
tpExtrato       = "//div[@title='Seleciona uma opção para o extrato']"
tpExtratoMes    = "//div[@title='Seleciona uma opção para o mês']"

extratoMesAtual = "//li[contains(text(),'Mês Atual')]"