import _jpype
import os
import jaydebeapi
from properties import Properties

def recuperarMassa(chave):
        
        if chave == 'PF_Conta_Corrente_Solidária':
            result = '14381168100'
        elif chave == 'PJ_Conta_Corrente_Solidária_Titular':
            result = '75164060159'
        elif chave == 'PJ_Conta_Poupança_Solidária_Titular':
            result = '05757134100'
        elif chave == 'PF_Conta_Poupança_Solidária':
            result = '01698051158'
        elif chave == 'PJ_Conta_Corrente_Não_Solidária_Titular':
            result = '11529830168'
        elif chave == 'PJ_Conta_Poupança_Não_Solidária_Titular':
            result = '33409080104'
        elif chave == 'PF_Conta_Salário':
            result = '14381168100'
        
        return result
