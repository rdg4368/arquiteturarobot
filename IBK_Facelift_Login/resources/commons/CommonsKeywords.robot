*** Settings ***
Library    SeleniumLibrary    

*** Variables ***

#botões
${botão_entrar}                 //label[text()='Entrar']
${botão_iniciais}               //div[@class='entrar ']
${botão_senha_1}                //a[contains(text(),'1')]
${botão_atualizar_quadros}      //span[text()='Atualizar Quadros']
#campos
${input_cpf}                    (//label[text()='CPF:']//following::input)[1]

#Titulos
${Titulo_página_inicial}        Olá,
${Titulo_página_login}          //label[text()='Bem-vindo ao BRB Banknet']
