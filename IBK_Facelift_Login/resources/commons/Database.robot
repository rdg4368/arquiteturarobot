*** Settings ***
Variables                       properties.py
Library                         DatabaseLibrary
Library                         SeleniumLibrary
Library                         Collections
Library                         String

*** Keywords ***
Conectar BD
    Connect To Database Using Custom Params  jaydebeapi  'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@${properties.host}:${properties.port}/${properties.name}', ['${properties.username_ibk}', '${properties.password_ibk}']

Desconectar BD
    Disconnect from Database
    
Conectar BD ATB
    Connect To Database Using Custom Params  jaydebeapi  'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@${properties.host}:${properties.port}/${properties.name_ATB}', ['${properties.username_atb}', '${properties.password_atb}']