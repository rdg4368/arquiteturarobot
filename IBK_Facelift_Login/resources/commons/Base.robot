*** Settings ***
Resource                            Database.robot
Resource                            Global.robot
Library                             SeleniumLibrary
Variables                           properties.py

*** Keywords ***
Preparar Suite
   Conectar BD
   Abrir navegador

Terminar Suite
   Desconectar BD
   Fechar Navegador

Acessar Pagina Inicial
   Delete All Cookies
   Go To          ${properties.url}