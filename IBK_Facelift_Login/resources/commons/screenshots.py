class Screenshots(object):

	ST001_CT001 = "IBK-20238 CT001 - FP - PF Conta Corrente - Solidária - Login.png"
	ST001_CT002 = "IBK-20239 CT002 - FP - PJ Solidária - Conta Corrente - Titular - Login.png"
	ST001_CT003 = "IBK-20240 CT003 - FP - PJ Solidária - Conta Poupança - Titular - Login.png"
	ST001_CT004 = "IBK-20241 CT004 - FP - PF Conta Poupança - Solidária - Login.png"
	ST001_CT005 = "IBK-20242 CT005 - FP - PJ Não-Solidária - Conta Corrente - Titular - Login.png"
	ST001_CT006 = "IBK-20243 CT006 - FP - PJ Não-Solidária - Conta Poupança - Titular - Login.png"
	ST001_CT007 = "IBK-20244 CT007 - FP - PF - Conta Salário - Login.png"
	
screenshots = Screenshots()