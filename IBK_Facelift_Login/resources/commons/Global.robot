*** Settings ***
Resource                  Database.robot
Library                   SeleniumLibrary
Variables                 properties.py

*** Variables ***
${diretorio_screenshot}     results

*** Keywords ***
Abrir navegador
	Open Browser                   ${properties.url}    Chrome
	Set Screenshot Directory       ${diretorio_screenshot}
	Maximize Browser Window
	#Set Window Size    1920    1080
	Delete All Cookies
	
Fechar Navegador
    Close Browser