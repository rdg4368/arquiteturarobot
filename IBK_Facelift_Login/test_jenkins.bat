echo ".Bat robot iniciado."
echo %execucao%
set DIR_ATUAL=%cd%
set CLASSPATH=%DIR_ATUAL%\lib\ojdbc6-11.2.0.3.jar;%DIR_ATUAL%\lib\mysql-connector-java-5.1.40.jar;%CLASSPATH%
set JAVA_HOME=C:\jdk1.8.0_66
set PATH=C:\python37\;C:\python37\Scripts;%PATH%

echo Variaveis ambiente configuradas

python --version
python -u

del %DIR_ATUAL%\resources\commons\properties.py
copy %DIR_ATUAL%\resources\commons\properties_hmo.py %DIR_ATUAL%\resources\commons\properties.py
echo %OPCAO%

if "%OPCAO%" == "Login" ( robot --name Login  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/login )

if "%OPCAO%" == "Extrato" ( robot --name Extrato -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/extrato )

if "%OPCAO%" == "Saldo" ( robot --name Saldo -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/saldo )

if "%OPCAO%" == "Consulta Comprovantes" ( robot --name Consulta_Comprovantes -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/consulta_comprovantes )

if "%OPCAO%" == "Posicao Integrada Saldo" ( robot --name Posicao_Integrada_Saldo  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/consulta_posicao_integrada_saldo )

if "%OPCAO%" == "Pagamento Codigo Barras" ( robot --name Pagamento_Codigo_Barras  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/pagamentos_codigo_barras )

if "%OPCAO%" == "Transferencia DOC" ( robot --name Transferencia_DOC  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/transferencia_doc )

if "%OPCAO%" == "Transferencia TED" ( robot --name Transferencia_TED  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/transferencia_ted )

if "%OPCAO%" == "Transferencia Entre Contas" ( robot --name Transferencia_Entre_Contas  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/transferencia_entre_contas )

if "%OPCAO%" == "Gerencia Favorecidos" ( robot --name Gerencia_Favorecidos  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/gerencia_favorecidos )

if "%OPCAO%" == "Aplicar Fundo de Investimento" ( robot --name Fundo_Investimento  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/fundo_investimento )

if "%OPCAO%" == "Atualizar Endereço" ( robot --name Atualizar_Endereço  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/atualizar_endereco )

if "%OPCAO%" == "Consultar Token" ( robot --name Consultar_Token  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/consultar_token )

if "%OPCAO%" == "Todos" (
	robot --name Login  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/login
	robot --name Extrato  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/extrato
	robot --name Saldo -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/saldo
	robot --name Consulta_Comprovantes -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/consulta_comprovantes
	robot --name Posicao_Integrada_Saldo  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/consulta_posicao_integrada_saldo
	robot --name Pagamento_Codigo_Barras  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/pagamentos_codigo_barras
	robot --name Transferencia_DOC  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/transferencia_doc
	robot --name Transferencia_TED  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/transferencia_ted
	robot --name Transferencia_Entre_Contas  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/transferencia_entre_contas
	robot --name Gerencia_Favorecidos  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/gerencia_favorecidos
	robot --name Fundo_Investimento  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/fundo_investimento
	robot --name Atualizar_Endereço  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/atualizar_endereco
	robot --name Consultar_Token  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/consultar_token
)

mkdir output
cd output
mkdir allure
cd ..
copy "./environment.properties" "./output/allure/environment.properties"

echo "Testes ibk executados."