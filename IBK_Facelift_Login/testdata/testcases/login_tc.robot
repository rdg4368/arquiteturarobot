*** Settings ***
Library     resources/commons/MassaDados.py
Resource    testdata/keywords/login_kw.robot
Resource    resources/commons/CommonsKeywords.robot
Resource    utils/utils.robot
Variables   resources/commons/screenshots.py


*** Keywords ***
#CT001
Realizar login - PF - conta corrente - solidária
     ${cpf} =  Recuperar massa     CPF_CC_SOLIDARIA_PF
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha            
     Realizar login válido         ${screenshots.ST001_CT001}

#CT002
Realizar login - PJ - solidária - conta corrente - titular
     ${cpf} =  Recuperar massa     CPF_CC_SOLIDARIA_PJ
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha
     Realizar login válido         ${screenshots.ST001_CT002}

#CT003
Realizar login - PJ - solidária - conta poupança - titular
     ${cpf} =  Recuperar massa     CPF_CP_SOLIDARIA_PJ
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha
     Realizar login válido         ${screenshots.ST001_CT003}

#CT004
Realizar login - PF - conta poupança - solidária
     ${cpf} =  Recuperar massa     CPF_CP_SOLIDARIA_PF
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha
     Realizar login válido         ${screenshots.ST001_CT004}

#CT005
Realizar login - PJ - não-solidária - conta corrente - titular
     ${cpf} =  Recuperar massa     CPF_CC_N_SOLIDARIA_PJ
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha
     Realizar login válido         ${screenshots.ST001_CT005}

#CT006          
Realizar login - PJ - não-solidária - conta poupança - titular
     ${cpf} =  Recuperar massa     CPF_CP_N_SOLIDARIA_PJ
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha
     Realizar login válido         ${screenshots.ST001_CT006}

#CT007          
Realizar login - PF - conta Salário
     ${cpf} =  Recuperar massa     CPF_CONTA_SALARIO
     Definir primeiro acesso de conta    ${cpf}
     Informar cpf                  ${cpf}
     Informar senha
     Realizar login válido         ${screenshots.ST001_CT007}     