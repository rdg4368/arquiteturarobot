*** Settings ***
Resource    resources/commons/CommonsKeywords.robot
Library     SeleniumLibrary    

*** Variables ***

*** Keywords ***
Informar cpf
    [Arguments]                            ${cpf}
    Digitar cpf                            ${cpf}
    Clicar em "Entrar"
    CLicar em "Iniciais" 
    
Digitar cpf
    [Arguments]                            ${cpf}
    Input Text                             ${input_cpf}        ${cpf}

Clicar em "${botão}"
    Run Keyword If    '${botão}' == 'Entrar'      Click Element        ${botão_entrar}
    Run Keyword If    '${botão}' == 'Iniciais'    Click Element        ${botão_iniciais}
    
Informar senha
    FOR  ${i}  IN RANGE  8
        Click Element                      ${botão_senha_1}       
    END
    Clicar em "Entrar"
    
Realizar login válido
    [Arguments]                            ${screenshot}
    Page Should Contain                    ${Titulo_página_inicial}
    Wait Until Element Is Visible          ${botão_atualizar_quadros}    timeout=10
    Capture Page Screenshot                ${screenshot}