***Settings***
Library        DatabaseLibrary
Library        String

***Variables***
#Atualiza a senha para 11111111 na tabela BLK2.FTN_SEN
${SQL_ATUALIZAR_SENHA_FTN_SEN}    UPDATE BLK2.FTN_SEN SET FTN_SEN.SENVAL = '8d11de9130770dcc4df3ec29158032c10f6dfcf272f058ce1f662cb3e561ca72' WHERE FTN_SEN.SENPRPID = '{0}'

***Keywords***
Atualizar senha
    [Arguments]        ${cpf}
    ${SQL}  Format String    ${SQL_ATUALIZAR_SENHA_FTN_SEN}        ${cpf}
    Execute Sql String    ${SQL}