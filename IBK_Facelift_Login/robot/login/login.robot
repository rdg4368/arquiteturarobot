*** Settings ***
Resource         resources/commons/Base.robot
Resource         testdata/testcases/login_tc.robot
Suite Setup      Preparar Suite
Suite TearDown   Terminar Suite
Test Setup       Acessar Pagina Inicial

*** Test Cases ***
CT001 - PF - conta corrente - solidária - login
    [Documentation]    login de conta corrente solidária de pessoa física
    [Tags]             login  PF  CC  solidária
    Realizar login - PF - Conta Corrente - Solidária

CT002 - PJ - solidária - conta corrente - titular - login
    [Documentation]    login de conta corrente solidária titular de pessoa física
    [Tags]             login  PJ  CC  titular
    Realizar login - PJ - solidária - conta corrente - titular
    
CT003 - PJ - solidária - conta poupança - titular - login
    [Documentation]    login de conta poupança titular de pessoa jurídica
    [Tags]             login  PJ  CP  solidária
    Realizar login - PJ - solidária - conta poupança - titular
    
CT004 - PF - conta poupança - solidária - login
    [Documentation]    login de conta poupança solidária de pessoa física
    [Tags]             login  PF  CP  solidária
    Realizar login - PF - conta poupança - solidária
    
CT005 - PJ - não-solidária - conta corrente - titular - login
    [Documentation]    login de conta corrente não-solidária titular de pessoa jurídica
    [Tags]             login  PJ  CC  titular  não-solidária
    Realizar login - PJ - não-solidária - conta corrente - titular
    
CT006 - PJ - não-solidária - conta poupança - titular - login
    [Documentation]    login de conta poupança não-solidária titular de pessoa jurídica
    [Tags]             login  PJ  CP  titular  não-solidária
    Realizar login - PJ - não-solidária - conta poupança - titular
    
CT007 - PF - conta salário - login
    [Documentation]    login de conta salário de pessoa física
    [Tags]             login  PF  CS
    Realizar login - PF - conta Salário