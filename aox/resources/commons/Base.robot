*** Settings ***
Resource                            Database.robot
Resource                            Mainframe.robot



*** Variable ***
${PF3}  PF(3)

*** Keywords ***

Preparar Suite
   Conectar BD ALTA
   Acessar SADS

Terminar Suite
   Fechar Terminal
   Desconectar BD
   
Preparar Suite PF Simplificada 
   Conectar BD ALTA
   Acessar SADS Com Estabelecimento Especifico Do Cliente  

Preparar Suite PJ Simplificada 
   Conectar BD ALTA
   Acessar SADS Com Estabelecimento Especifico Do Cliente PJ  

Logar com Estabelecimento Especifico Cliente Pendente
   Conectar BD ALTA
   Acessar SADS Com Estabelecimento Especifico Cliente Pendente De Dupla Assinatura