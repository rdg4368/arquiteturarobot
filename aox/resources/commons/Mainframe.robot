*** Settings ***
Variables         properties.py
Library           Mainframe3270                
Library           ../../testresources/utils/utils.py 
Library           ../../testresources/utils/utilsMask.py 
Resource          ../../testresources/base_dados/AOXB01.robot
Resource          ../../testresources/base_dados/AOXB33.robot
       

*** Variable  ***
${MAINFRAME}          ${properties.mainframe}
${OPCAO_SADS_CICS}    ${properties.opcao_cics}
${USUARIO_SADS}       ${properties.usuario_sads}
${PASSWORD_SADS}      ${properties.password_sads}
${AMBIENTE}           ${properties.ambiente_sads}
${EMPRESA}            ${properties.empresa_sads}
${ESTABELECIMENTO}    ${properties.estabelecimento_sads}
${PF3}      PF(3)
${END}      END
${LIMPAR}   Clear

*** Keywords ***

Validacao Disponibilidade SADS         
    ${on} =   Run Keyword And Return Status    Wait Until String   HABILITACAO DE USUARIO   20   
    Run Keyword If    '${on}' == 'False'   Log              AMBIENTE INSTÁVEL OU INDISPONÍVEL ...   
    Run Keyword If    '${on}' == 'False'   Log To Console   AMBIENTE INSTÁVEL OU INDISPONÍVEL ...  
    
Abrir Terminal
    Open Connection    ${MAINFRAME}

Autenticar SADS1
    Sleep    2    
    Digitar Na Posicao  ${OPCAO_SADS_CICS}    24  7
    Transmitir

Autenticar SADS    
    Digitar Na Posicao    B    24  7
    Transmitir          
    Validacao Disponibilidade SADS
    Digitar Na Posicao  ${USUARIO_SADS}     17  20
    Digitar Na Posicao  ${PASSWORD_SADS}    17  42
    Digitar Na Posicao  ${AMBIENTE}         19  16
    Digitar Na Posicao  ${EMPRESA}          19  30
    Digitar Na Posicao  ${ESTABELECIMENTO}  19  53
    Transmitir
    # Digitar  ${USUARIO_SADS}
    # Digitar  ${PASSWORD_SADS}
    # Digitar  ${AMBIENTE}
    # Digitar  ${EMPRESA}
    # Tabular
    # Digitar  ${ESTABELECIMENTO}
    # Tabular  
    # Transmitir
z
Autenticar SADS Estabelecimento Especifico 
    [Arguments]     ${estabelecimento}  
    Digitar  ${OPCAO_SADS_CICS}    
    Transmitir           
    Validacao Disponibilidade SADS
    Digitar Na Posicao  ${USUARIO_SADS}     17  20
    Digitar Na Posicao  ${PASSWORD_SADS}    17  42
    Digitar Na Posicao  ${AMBIENTE}         19  16
    Digitar Na Posicao  ${EMPRESA}          19  30
    Digitar Na Posicao  ${estabelecimento}  19  53
    Transmitir

Acessar SADS
    Abrir Terminal
    Autenticar SADS1
    
Fechar Terminal
    Close Connection

Acessar SADS Com Estabelecimento Especifico Do Cliente
    ${agenciaCliente} =  Recuperar Cliente PF Valido   
    ${agencia} =   utilsMask.Formatar Mascara Agencia   ${agenciaCliente}[0][9]  
    Abrir Terminal
    Autenticar SADS Estabelecimento Especifico   ${agencia} 

Acessar SADS Com Estabelecimento Especifico Do Cliente PJ
    ${agenciaCliente} =  Recuperar Cliente PJ Valido Dados Simplificados     
    ${agencia} =   utilsMask.Formatar Mascara Agencia   ${agenciaCliente}[0][12] 
    Abrir Terminal
    Autenticar SADS Estabelecimento Especifico   ${agencia}

Acessar SADS Com Estabelecimento Especifico Cliente Pendente De Dupla Assinatura  
    ${cltClientePendente} =   Recuperar Cod Cliente Pendente De Dupla Assinatura   
    ${agenciaCliente} =       Recuperar Agencia Cliente Pendente De Dupla Assinatura  ${cltClientePendente}[0][0]   
    ${agencia} =              utilsMask.Formatar Mascara Agencia String Limpa   ${agenciaCliente}[0][0] 
    Abrir Terminal
    Autenticar SADS Estabelecimento Especifico   ${agencia}
    
    
Trocar Host
   [Arguments]   ${HOST}
   Execute Command  ${PF3}
   Digitar  ${HOST}
   Transmitir

Transmitir
   #Capturar Screenshot 
   Send Enter   
   #Capturar Screenshot

Capturar Screenshot
    Take Screenshot

Digitar
   [Arguments]   ${conteudo}  
   Write Bare  ${conteudo}

Digitar Na Posicao 
    [Arguments]  ${text}  ${y}  ${x}
    #Limpar campo   ${y}  ${x} 
    Write Bare In Position  ${text}  ${y}  ${x}

Digitar Na Posicao Por Lista
    [Arguments]   ${params} 
    #String , posicao Y , posição X
    Write Bare In Position  ${params}[0][0]  ${params}[0][1]  ${params}[0][2]

Tabular
   Execute Command  Tab  

Limpar campo
    [Arguments]     ${x}  ${y}   
    Delete Field    ${x}  ${y}
    
Limpar Campo Por Lista
    [Arguments]     ${params}        
    Delete Field    ${params}[0][0]  ${params}[0][1]
    
Tabular "${qnt}" Vezes     
    Change Wait Time    0.01
    :FOR    ${i}    IN RANGE    ${qnt}
    \    Tabular
    Change Wait Time    0.2

Verificar se existe 
   [Arguments]   ${conteudo}  
   Page Should Contain String  ${conteudo}

Validar Informacoes Tela
    [Arguments]         ${campos}  
    Page Should Contain All Strings     ${campos}

Ler Dado Tela
    [Arguments]        @{params}
    ${valorCampo} =     Read  ${params}[0][0]  ${params}[0][1]  ${params}[0][2] 
    [Return]           ${valorCampo}  
  
Ler Dado Tela 2
    [Arguments]        ${y}  ${x}  ${l}
    ${valorCampo} =     Read  ${y}  ${x}  ${l}  
    [Return]           ${valorCampo}  

Ler Dado Tela Array Simples
    [Arguments]        @{params}
    ${valorCampo} =     Read  ${params}[0]  ${params}[1]  ${params}[2] 
    [Return]           ${valorCampo}  
Comparar Valores na Tela
    [Arguments]     ${dadoTela}   ${dadoBanco}   
    ${saoIguais} =  Compara Valores  ${dadoTela}  ${dadoBanco}    
    Run keyword if   '${saoIguais}'=='False'  FAIL      

Validar Informacoes Tela Dinamicamente
    [Arguments]    @{input}
    ${cnt}=    Get length    ${input}
    :FOR    ${i}    IN RANGE    ${cnt}
    \    Page Should Contain String     ${input}[${i}]
    
Validar Mensagem
    [Arguments]                  ${msgValidacao}
    Page Should Contain String   ${msgValidacao}

Validar Mensagem Generica
    [Arguments]                      ${msgValidacao}
    Page Should Contain Any String   ${msgValidacao}

Alterar Campo
    [Arguments]   @{params} 
    Digitar Na Posicao Por Lista   ${params} 
    Transmitir
 
Apagar Conteudo Campo Ocorrencia
    Tabular "1" Vezes 
    Execute Command  ${END}     
    Tabular "1" Vezes 
    Execute Command  ${END}
       


    
    

    
    