*** Settings ***
Resource     testresources/base_dados/AOXB01.robot 
Resource     testresources/base_dados/AOXB33.robot
Resource     testresources/base_dados/AOXB14.robot 
Resource     testresources/base_dados/AOXB02.robot
Resource     resources/commons/Mainframe.robot
Library      Mainframe3270  
Library      String

*** Keywords ***

Inserir Valor 
    [Arguments]            @{params}    # Valor    ${xPos}    ${yPos} 
    
    Digitar Na Posicao     ${params}[0][0]    ${params}[0][1]    ${params}[0][2]    
    Transmitir

Validar Mensagem Campo Obrigatorio
    [Arguments]              ${msgTela}     ${msgEsperada}    
    ${msg} =  Ler Dado Tela  ${msgTela} 
    Should Be Equal           ${msg}        ${msgEsperada} 
    
Transmitir Transacao
    Tabular
    Transmitir   