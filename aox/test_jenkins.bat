set DIR_ATUAL=%cd%
set CLASSPATH=%DIR_ATUAL%\lib\ojdbc6-11.2.0.3.jar;%DIR_ATUAL%\lib\db2jcc4-10.1.jar;%DIR_ATUAL%\lib\db2jcc_license_cisuz.jar;%CLASSPATH%
echo %CLASSPATH%
set JAVA_HOME=C:\jdk1.8.0_66
set PATH=%DIR_ATUAL%\src\main\resources;%PATH%
set PATH=D:\allure-2.11.0\bin\;%PATH%
set PATH=C:\python37\;C:\python37\Scripts;C:\wc3270;%PATH%

echo Variaveis ambiente configuradas

echo Start robot.....
python --version

mkdir output
cd output
mkdir allure
cd ..
copy "./environment.properties" "./output/allure/environment.properties"

robot --name AOX08_Pessoa_Fisica_Juridica_Web  -P %DIR_ATUAL%   --listener allure_robotframework  --outputdir results  robot/aox08

echo Fim robot.
echo "Testes alta executados."