*** Settings ***
Library    testresources/utils/utilsMask.py
Resource   testdata/keywords/aox08/009_classificacao_economico_financeiro_PJ/classificacaoEconomicoFinanceiroPjKW.robot
Resource   testresources/base_dados/AOXB01.robot


*** Variables ***
${msg}  Não é possível efetuar automação, pois o programa faz o controle de acesso por matricula.
*** Keywords *** 
Incluir Classificação Econ - Financeira (PJ)
    #Acessar Sistema De Clientes
    Log To Console  ${msg}
Alterar Classificação Econ - Financeira (PJ) 
    #Acessar Sistema De Clientes
    Log To Console  ${msg}
Consultar Classificação Econ - Financeira (PJ)
    #Acessar Sistema De Clientes
    Log To Console  ${msg}