*** Settings ***
Library    testresources/utils/utilsMask.py
Resource   testdata/keywords/aox08/008_classificacao_economico_financeiro_PF/classificacaoEconomicoFinanceiroPfKW.robot
Resource   testresources/base_dados/AOXB01.robot


*** Variables ***
${mnsg}  Não é possível efetuar automação, pois o programa faz o controle de acesso por matricula.
*** Keywords ***
Incluir Classificacao Economica - Financeiros (PF)
    #Acessar Sistema De Clientes
    Log To Console  ${mnsg}   
    
Alterar Classificacao Economica - Financeiros (PF)
    #Acessar Sistema De Clientes
    Log To Console  ${mnsg}
    
Consultar Classificacao Economica - Financeiros (PF)
    #Acessar Sistema De Clientes
    Log To Console  ${mnsg}