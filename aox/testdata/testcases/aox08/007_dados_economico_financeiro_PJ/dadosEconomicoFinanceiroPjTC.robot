*** Settings ***
Library    testresources/utils/utilsMask.py
Resource   testdata/keywords/aox08/007_dados_economico_financeiro_PJ/dadosEconomicoFinanceiroPjKW.robot
Resource   testresources/base_dados/AOXB13.robot


*** Variables ***
${Opcao07}    07
${Opcao17}    17
${Opcao27}    27
${Opcao57}    57
${ReceitaBruta}    100
${MediaBruta}    8


*** Keywords ***
Incluir Dados Economicos - Financeira (PJ)
    Acessar Menu AOX8 
    ${DadosAcesso}    Recuperar Dados Economicos Financeiros PJ
    Acessar Tela Dados Economicos    ${DadosAcesso}    ${Opcao07}
    Realizar Inclusao    ${ReceitaBruta}    ${MediaBruta} 
    ${DadosDesfazer}    Validacao Inclusao Dados Economicos    ${DadosAcesso}    ${ReceitaBruta}  ${MediaBruta}        
    Desfazer Dados Economicos Inclusao/Alteracao    ${DadosDesfazer}
    
Alterar Dados Economicos - Financeira (PJ)
    Preparar Dados Economicos PJ
    Acessar Menu AOX8
    ${DadosAcesso}    Recuperar Dados Economicos Financeiros PJ
    Acessar Tela Dados Economicos    ${DadosAcesso}    ${Opcao17}
    Realizar Alteracao    ${ReceitaBruta}    ${MediaBruta} 
    ${DadosDesfazer}    Validacao Inclusao Dados Economicos    ${DadosAcesso}    ${ReceitaBruta}  ${MediaBruta}        
    Desfazer Dados Economicos Inclusao/Alteracao    ${DadosDesfazer}
      
     
Consultar Dados Economicos - Financeira (PJ)    
    Preparar Dados Economicos PJ
    Acessar Menu AOX8
    ${DadosAcesso}    Recuperar Dados Economicos Financeiros PJ
    Acessar Tela Consulta Dados Economicos    ${DadosAcesso}    ${Opcao27}
    ${IdParaDesfazer}    Validar Consultar Dados Economicos PJ
    Desfazer Dados Economicos Consulta    ${IdParaDesfazer}
    
Consultar RFB Dados Economicos - Financeira (PJ)
    Acessar Menu AOX8
    ${DadosAcesso}    Recuperar Dados Economicos Financeiros PJ
    Acessar Tela Consulta Dados Economicos    ${DadosAcesso}    ${Opcao57}
    Log to console     Tela não cadastrada no ambiente, Mantis 31663
