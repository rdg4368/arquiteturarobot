*** Settings ***
Library    testresources/utils/utils.py
Library    testresources/utils/utilsMask.py
Resource   testresources/commons_keywords/commonsKeywords.robot
Resource   resources/commons/Mainframe.robot
Resource   testresources/base_dados/AOXB13.robot
Library    Mainframe3270  


*** Keywords ***
Acessar Menu AOX8
   Trocar Host    AOX8
    
Acessar Tela Dados Economicos
    [Arguments]    ${DadosAcesso}     ${Opcao}
    Digitar Na Posicao    ${DadosAcesso}[0][0]    19    12 
    Digitar Na Posicao    ${DadosAcesso}[0][1]    19    27   
    Digitar Na Posicao    ${DadosAcesso}[0][2]    21    21
    Digitar Na Posicao    ${Opcao}    21    74
    Transmitir
    Digitar Na Posicao    S    06    78  
    Transmitir  
    
Realizar Inclusao
    [Arguments]    ${ReceitaBruta}    ${MediaBruta} 
    Digitar Na Posicao    ${ReceitaBruta}    18    26
    Digitar Na Posicao    ${MediaBruta}    19    26
    Digitar Na Posicao    S    22    75
    Transmitir
    
Validacao Inclusao Dados Economicos
    [Arguments]    ${DadosAcesso}    ${ReceitaBruta}  ${MediaBruta}
    ${DadosBanco}    Recuperar Dados Validar Inclusao Economicos Financeiros PJ
    Should Be Equal As Strings    ${DadosAcesso}[0][3]    ${DadosBanco}[0][0]    
    ${DataInvertida}    Inverter Data    ${DadosBanco}[0][1]
    Should Be Equal As Strings    ${DataInvertida}    ${DadosAcesso}[0][2]    
    Should Be Equal As Strings    ${DadosBanco}[0][2]    ${ReceitaBruta}
    Should Be Equal As Strings    ${DadosBanco}[0][3]    ${MediaBruta}  
    [Return]    ${DadosBanco}   
    
Realizar Alteracao 
    [Arguments]    ${ReceitaBrutaAlt}    ${MediaBrutaAlt} 
    Digitar Na Posicao    ${ReceitaBrutaAlt}    18    65
    Digitar Na Posicao    ${MediaBrutaAlt}    19    65
    Digitar Na Posicao    S    22    75
    Transmitir
       
Acessar Tela Consulta Dados Economicos
    [Arguments]    ${DadosAcesso1}     ${Opcao}
    Digitar Na Posicao    ${DadosAcesso1}[0][0]    19    12 
    Digitar Na Posicao    ${DadosAcesso1}[0][1]    19    27     
    Digitar Na Posicao    ${DadosAcesso1}[0][2]    21    21
    Digitar Na Posicao    ${Opcao}    21    74
    Transmitir
    Digitar Na Posicao    S    06    78  
    Transmitir 
    
Validar Consultar Dados Economicos PJ
    ${Cltcod}    Ler Dado Tela 2    03    10    08    
    ${DadoValidacao}    Recuperar Dados Validar Consulta Economicos Financeiros PJ    ${Cltcod}
    Verificar se existe    ${DadoValidacao}[0][0]
    Verificar se existe    ${DadoValidacao}[0][1]
    Verificar se existe    ${DadoValidacao}[0][2]
    [Return]    ${DadoValidacao}[0][3]