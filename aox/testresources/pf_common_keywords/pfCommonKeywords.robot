# Neste arquivo se encontram todas as Keywords reaproveitáveis aos testes relacionados a pessoa físicas
*** Settings ***
Resource     testresources/base_dados/AOXB01.robot
Resource     testresources/base_dados/AOXB33.robot 
Resource     testresources/base_dados/AOXB14.robot
Resource     testresources/base_dados/AOXB48.robot
Resource     testresources/base_dados/AOXB17.robot
Resource     testresources/base_dados/AOXB19.robot  
Resource     resources/commons/Mainframe.robot
Library      testresources/utils/utilsMask.py  
Library      testresources/dto/pfCompleto.py  
Library      testresources/dto/pfSimplificado.py 
Library      testresources/dto/correspondenciaPf.py  
Library      Mainframe3270   

*** Keywords ***
#RECUPERA DADOS COMPLETOS PF 
Recuperar Dados Completos Cliente PF Valido AOX    
    ${cpfAox} =  Recuperar Dado Cliente PF Valido
    ${result} =  Disponibilizar Dados Completos Cliente PF  ${cpfAox}
    [Return]     ${result}
      
Disponibilizar Dados Completos Cliente PF
    [Arguments]             ${queryResult}     
    ${dadosCompletosClientePf} =     Criar Objeto Cliente Completo    ${queryResult}
    [Return]     ${dadosCompletosClientePf}

#RECUPERA DADOS SIMPLIFICADOS PF 
Recuperar Dados Cliente PF Valido AOX    
    ${cpfAox} =  Recuperar Cliente PF Valido
    ${result} =  Disponibilizar Dados Cliente PF  ${cpfAox}
    [Return]     ${result}
    
#RECUPERA DADOS SIMPLIFICADOS PJ 
Recuperar Dados Cliente PJ Valido AOX    
    ${cnpjAox} =  Recuperar Cliente PJ Valido Dados Simplificados
    ${result} =  Disponibilizar Dados Cliente PJ  ${cnpjAox}
    [Return]     ${result}
      
Disponibilizar Dados Cliente PF
    [Arguments]             ${queryResult}  
    ${dadosSimplificadosClientePf} =     Criar Objeto Cliente Simplificado     ${queryResult}
    [Return]     ${dadosSimplificadosClientePf}
    
Disponibilizar Dados Cliente PJ
    [Arguments]             ${queryResult}  
    ${dadosSimplificadosClientePj} =     Criar Objeto Cliente Simplificado     ${queryResult}
    [Return]     ${dadosSimplificadosClientePj}

#RECUPERAR DADOS DA CORRESPONDENCIA PF
Recuperar Dados Correspondencia Cliente PF Valido AOX3  
    ${dadosAox3} =  Selecionar Dados Correspondencia Cliente PF Valido
    ${result} =  Disponibilizar Dados Correspondencia Cliente PF  ${dadosAox3}
    [Return]     ${result}
      
Disponibilizar Dados Correspondencia Cliente PF
    [Arguments]             ${queryResult}  
    ${dadosCorrespPf} =    Criar Objeto Correspondencia Cliente Pf   ${queryResult}
    [Return]  ${dadosCorrespPf}
         
Detalhar Pessoa Fisica
   [Arguments]   ${opcao} 
    Tabular 
    Digitar      ${opcao}
    Transmitir





           