*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary

*** Variable ***
${SQL_VALIDACAO_DADOS_INCLUSAO}
...    SELECT XB200_GCVCOD AS CODIGO, XB200_GCVNOM AS NOME FROM AOXB00.AOXBB2 WHERE XB200_GCVCOD = 'FTESTE'

${SQL_EXCLUIR_DADOS_INCLUSAO}
...    DELETE AOXB00.AOXBB2 WHERE XB200_GCVCOD = 'FTESTE'

${SQL_VALIDACAO_ALTERACAO_GRUPO}
...    SELECT XB200_GCVNOM AS NOME FROM AOXB00.AOXBB2 WHERE XB200_GCVCOD = '{0}'

${SQL_VALIDACAO_CONSULTA_GRUPO}
...    SELECT    XB200_GCVCOD AS CODIGO,    XB200_GCVNOM AS NOME,    CASE XB200_GRESIT WHEN 2 THEN 'Inativo'    WHEN 1 THEN 'Ativo' END AS SITUACAO
...    FROM AOXB00.AOXBB2 WHERE XB200_GCVCODOPE = 1 ORDER BY SUBSTR(XB200_GCVCOD, 1, 1) FETCH FIRST 14 ROWS ONLY

${SQL_CONSULTA_HISTORICO_GRUPO}
...    SELECT XB200_GCVCOD AS GRUPO FROM AOXB00.AOXBB2 FETCH FIRST ROW ONLY 
 
${SQL_VALIDAR_ALTERACAO_GRUPO_CONVENIO}
...    SELECT    LPAD(XB300_CLTCOD,    8,    '0')|| ' ' || X0100_CLTDGV AS CLIENTE,    XB200_GCVNOM AS NOME_GRUPO,    X0100_CLTNOM AS NOME_CLIENTE,    CASE XB300_CGCTIPREL WHEN 1 THEN 'Componente' WHEN 2 THEN 'Controlador' END AS TIPO,
...    TRIM(b.SEDIDR) AS ID FROM    AOXB00.AOXBB2 a,    AOXB00.AOXBB3 b,    AOXB00.AOXB01 c WHERE    XB300_CLTCOD = X0100_CLTCOD    AND XB200_GCVCOD = XB300_GCVCOD    AND XB300_GRESIT = 1    AND XB300_GCVCOD = 'C00002' FETCH FIRST ROW ONLY           

${SQL_DESFAZER_ALTERACAO_GRUPO_CONVENIO}
...    UPDATE    AOXB00.AOXBB3 SET    XB300_GRESIT = 1 WHERE    SEDIDR = {0}

${SQL_VALIDACAO_CONSULTA_HISTORICO_GRUPO_CONVENIO}
...    SELECT    XB200_GCVCOD AS GRUPO,    XB200_GCVNOM AS NOME,    CASE XB200_GRESIT WHEN 2 THEN 'Inativo'    WHEN 1 THEN 'Ativo'END AS SITUACAO,SUBSTR(XB200_GCVDATATZ,7,2)|| '/' || SUBSTR(XB200_GCVDATATZ,5,2)|| '/' || SUBSTR(XB200_GCVDATATZ,
...    1,4) AS INCLUSAO_DATA,LPAD(XB200_GCVCODOPE,9,'0') AS OPERACAO,SUBSTR(XB200_GCVHORATZ,1,2)|| ':' || SUBSTR(XB200_GCVHORATZ,3,2)|| ':' || SUBSTR(XB200_GCVHORATZ,5,2) AS HORA,LPAD(XB200_CLTNUMUSU,6,'0') AS MATRICULA FROM
...    AOXB00.AOXBB2 WHERE XB200_GCVCOD = '{0}' ORDER BY XB200_GCVCODOPE DESC
*** Keywords ***
Recuperar Dados Validacao Inclusao Grupo
    ${status}  Run Keyword And Return Status   Check If Exists In Database    ${SQL_VALIDACAO_DADOS_INCLUSAO}
    [return]  ${status}

Recuperar Dados Validacao Alteracao Grupo
    [Arguments]  ${codGrupo}
    ${sql}  Format String   ${SQL_VALIDACAO_ALTERACAO_GRUPO}    ${codGrupo}
    ${result}  Query  ${sql}
    [return]  ${result} 
    
Recuperar Dados Validacao Consulta Grupo
    ${result}  Query  ${SQL_VALIDACAO_CONSULTA_GRUPO}
    [return]  ${result} 
    
Recuperar Dado Consulta Historico
    ${result}  Query  ${SQL_CONSULTA_HISTORICO_GRUPO} 
    [return]  ${result}
    
Recuperar Dados Validacao Alteracao Grupo Convenio
    ${result}  Query  ${SQL_VALIDAR_ALTERACAO_GRUPO_CONVENIO}
    [return]  ${result}
 
Excluir Dados Inclusao Grupo
    Execute Sql String    ${SQL_EXCLUIR_DADOS_INCLUSAO}
    
Desfazer Aleteracao
    [Arguments]  ${id}
    ${sql}  Format String  ${SQL_DESFAZER_ALTERACAO_GRUPO_CONVENIO}  ${id}
    Execute Sql String    ${sql} 
    
Recuperar Dados Validacao Historico Grupo Convenio
    [Arguments]  ${grupo}
    ${sql}  Format String  ${SQL_VALIDACAO_CONSULTA_HISTORICO_GRUPO_CONVENIO}  ${grupo}
    ${result}  Query  ${sql}
    [return]  ${result}        