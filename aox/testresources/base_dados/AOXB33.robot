*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py   
Library      String       

*** Keywords ***

Consultar Insercao Vinculacao
    [Arguments]     ${cltcodCliente}  ${cltcodClienteVinculado}  ${codigoTipoDeVinculacao}
    ${validacao} =  Run Keyword And Return Status     Check If Exists In Database  
    ...             SELECT * FROM AOXB00.AOXB33 WHERE X3300_CLTCOD = ${cltcodCliente} AND X3300_VINCODCLI = ${cltcodClienteVinculado} 
    ...             AND X3300_TVNCOD = ${codigoTipoDeVinculacao}
    [return]        ${validacao}
    
Deletar Cliente Vinculado
    [Arguments]     ${cltcodCliente}  ${cltcodClienteVinculado}  ${codigoTipoDeVinculacao}              
    ${result} =     Run Keyword And Return Status     Execute Sql String  
    ...  DELETE FROM AOXB00.AOXB33 WHERE X3300_CLTCOD = ${cltcodCliente} AND X3300_VINCODCLI = ${cltcodClienteVinculado} AND X3300_TVNCOD = ${codigoTipoDeVinculacao} 
    [Return]        ${result}
    
Recuperar Codigos Vinculacao Cliente AOX2
    [Arguments]     ${cltcod}  
    @{result}       Query  SELECT TRIM(X3300_VINCODCLI) FROM AOXB00.AOXB33 WHERE X3300_CLTCOD = ${cltcod} FETCH FIRST 4 ROWS ONLY  
    [Return]        ${result}

Recuperar Dados Validacao Cliente AOX2
    [Arguments]     ${cltcod}      
    ${result}       Query  SELECT TRIM(X0100_CLTNOM), TRIM(LPAD(X0100_CLTCOD,8,'0')) concat LPAD(X0100_CLTDGV,2,' ') FROM AOXB00.AOXB01 WHERE X0100_CLTCOD = ${cltcod}      
    [Return]        ${result}[0]
  
Defazer Alteracoes Na Vinculacao Do Cliente
    [Arguments]       ${codVinculacao}       ${tipoVinculacaoOriginal} 
    ${resultado} =    Run Keyword And Return Status      Execute Sql String  
    ...  UPDATE AOXB00.AOXB33 SET X3300_TVNCOD = '${tipoVinculacaoOriginal}' WHERE X3300_VINCODCLI = ${codVinculacao};
    [Return]          ${resultado}
   
Recuperar Tipo De Vinculacao Alterado Do Cliente
    [Arguments]     ${codVinculacao}  
    ${result}       Query  SELECT TRIM(X3300_TVNCOD) FROM AOXB00.AOXB33 WHERE X3300_VINCODCLI = ${codVinculacao}   
    [Return]        ${result}[0][0]
    
Recuperar Nome Anterior Incluido
    [Arguments]     ${nomeAnterior}  ${cltCod}
    ${result} =     Query   SELECT TRIM(X2100_NOMVALATR) FROM AOXB00.AOXB21 WHERE X2100_CLTCOD = ${cltCod} AND X2100_NOMVALATR = '${nomeAnterior}'
    [Return]        ${result}[0][0]
    
Deletar Inclusao De Dado Anterior   
    [Arguments]     ${param}  ${cltCod}            
    ${result} =     Run Keyword And Return Status     Execute Sql String  
    ...  DELETE FROM AOXB00.AOXB21 WHERE X2100_CLTCOD = ${cltCod} AND X2100_NOMVALATR = '${param}';      
    [Return]        ${result}
    
Recuperar Cod Cliente Pendente De Dupla Assinatura
    ${result}       Query  SELECT TRIM(X0100_CLTCOD), TRIM(X0100_CLTDGV) FROM AOXB00.AOXB01 WHERE X0100_CLTCOD NOT IN(SELECT X1300_CLTCOD FROM AOXB00.AOXB13) AND X0100_CLTCOD NOT IN(SELECT X1800_CLTCOD FROM AOXB00.AOXB18) FETCH FIRST 1 ROWS ONLY
    [Return]        ${result}
    
Recuperar Agencia Cliente Pendente De Dupla Assinatura
    [Arguments]     ${cltCod}  
    ${result}       Query  SELECT TRIM(X0100_CLTCODDEP) FROM AOXB00.AOXB01 WHERE X0100_CLTCOD = ${cltCod} AND X0100_CLTCODDEP <> 0
    [Return]        ${result}
   
Alterar Tipo De Item
    [Arguments]     ${n1}    ${n2}
    ${status}       Run Keyword And Return Status     Execute Sql String    UPDATE AOXB00.AOXB33 SET X3300_TVNCOD = ${n1} WHERE X3300_TVNCOD = ${n2}
    [Return]        ${status}
    