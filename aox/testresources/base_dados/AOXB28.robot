*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      String    

*** Variables ***
${SQL_CODIGO_CLIENTE}
...    	SELECT LPAD(X2800_CLTCOD,8,'0')
...		FROM AOXB00.AOXB28
...		ORDER BY X2800_HDRDATA DESC FETCH FIRST 1 ROW ONLY


${SQL_RECUPERAR_DADO_ACESSO_CONSULTA_COMPOSICAO_PF}
...    SELECT LPAD(X2800_CLTCOD,8,'0') AS CLIENTE 
...    FROM AOXB00.AOXB28, AOXB00.AOXB01 
...    WHERE X0100_CLTTIP = 1 AND X2800_CLTCOD != 1 FETCH FIRST ROW ONLY

#TODO validacoes deve ocorrer no modulo de keywords.
#TODO caso seja necessária, essa query deveria explicar o que ela recuperar
#TODO por exemplo, neste caso SQL_RECUPERAR_PARTICIPACAO_PF talvez seja um nome razoavel.
${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA_PARTICIPACAO_PF}
...    SELECT X2800_CLTCOD||'-'||X0100_CLTDGV AS CODIGO, X0100_CLTNOM AS NOME, 
...    LPAD(X0100_CLTCGC,9,'0')||'-'||LPAD(X0100_CLTCGCDIG,2,'0') AS CPF
...    FROM AOXB00.AOXB28, AOXB00.AOXB01
...    WHERE X0100_CLTCOD = X2800_CLTCOD AND X0100_CLTTIP = 1
...    AND X2800_CLTCOD = {0}


${SQL_RECUPERAR_ACESSO_PJ}
...    SELECT LPAD(X2800_VINCODCLI,8,'0') AS CLIENTE_PJ 
...    FROM AOXB00.AOXB28, AOXB00.AOXB01 
...    WHERE X0100_CLTTIP = 1 AND X2800_CLTCOD != 1 FETCH FIRST ROW ONLY

#FIXME Modificar o nome para SQL_RECUPERAR_PARTICIPACAO_SOCIETARIA
${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA_PARTICIPACAO_SOCIETARIA}
...    SELECT SUBSTR(X2800_VINCODCLI,1,3)||'.'||SUBSTR(X2800_VINCODCLI,4,3)||'-'||X2800_PTCSIT AS COD_PJ, X0100_CLTCOD|| '-'||X0100_CLTDGV AS CODIGO,
...    TRIM(X0100_CLTNOM) AS NOME, LPAD(X0100_CLTCGC,9,'0')||'-'||LPAD(X0100_CLTCGCDIG,2,'0') AS CPF
...    FROM AOXB00.AOXB28, AOXB00.AOXB01
...    WHERE X0100_CLTCOD = X2800_CLTCOD AND X0100_CLTTIP = 1 AND X2800_VINCODCLI = {0}

#TODO modificar para SQL_RECUPERAR_GRUPOS_ECONOMICOS
${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA_GRUPOS_ECONOMICOS}
...    SELECT LPAD(XA400_GECCOD,6,'0'), SUBSTR(TRIM(XA400_GECNOM),1,15), LPAD(XA400_GECCODCONT,8,'0'),
...    CASE XA400_GECSIT WHEN 1 THEN 'ATIVO' WHEN 2 THEN 'INATIVO' END
...    FROM AOXB00.AOXBA4 A      
...    WHERE XA400_GECSIT = 1 ORDER BY XA400_GECCOD ASC FETCH FIRST 14 ROW ONLY
   

${SQL_ACESSO_ENTRADA_GRUPO}
...    SELECT XA400_GECCOD
...    FROM AOXB00.AOXBA4
...    INNER JOIN AOXB00.AOXBA5 ON (XA500_GECCOD = XA400_GECCOD)
...    WHERE XA400_GECSIT <> 2 AND XA400_GECCODGER <> 1
...    FETCH FIRST ROW ONLY

#TODO modificar para SQL_RECUPERAR_GRUPO_ECONOMICO, ou algo semelhante
${SQL_RECUPERAR_DADOS_VALIDAR_DADO_GRUPO_ECONOMICO}
...    SELECT XA400_GECCOD, XA400_GECNOM, LPAD(XA500_CLTCOD, 8, '0'), TRIM(X0100_CLTNOM),
...    SUBSTR(XA500_HDRDATA, 7, 2) || '/' || SUBSTR(XA500_HDRDATA, 5, 2) || '/' || SUBSTR(XA500_HDRDATA, 1, 4)
...    FROM AOXB00.AOXBA4 INNER JOIN AOXB00.AOXBA5 ON
...    (XA500_GECCOD = XA400_GECCOD) INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD = XA500_CLTCOD)
...    WHERE XA400_GECCOD = {0} AND XA400_GECSIT <> 2 AND XA400_GECCODGER <> 1

${SQL_RECUPERAR_CONSULTA_GEIC}
...    SELECT LPAD(XA500_CLTCOD,8,'0') FROM AOXB00.AOXBA5 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD = XA500_CLTCOD)    
...    WHERE XA500_GECSIT = 1 FETCH FIRST ROW ONLY

#TODO validacoes sempre no modulo de kweywords
${SLQ_RECUPERAR_DADOS_VALIDAR_CONSULTA_GEIC}
...    SELECT LPAD(SUBSTR(XA500_CLTCOD,1,1),1,'0')||'.'||SUBSTR(XA500_CLTCOD,2,3)||'-'||X0100_CLTDGV, 
...    CASE X0100_CLTTIP WHEN 2 THEN 'P.JURIDICA' WHEN 1 THEN 'P.FISICA' END,
...    LPAD(X0100_CLTCGC,9,'0')||'-'||LPAD(X0100_CLTCGCDIG,2,'0'), TRIM(XA400_GECNOM)
...    FROM AOXB00.AOXBA5 INNER JOIN AOXB00.AOXB01 ON (X0100_CLTCOD = XA500_CLTCOD) INNER JOIN AOXB00.AOXBA4 ON (XA400_GECCOD = XA500_GECCOD)    
...    WHERE XA500_CLTCOD = {0} AND XA500_GECSIT = 1 FETCH FIRST ROW ONLY


*** Keywords ***
Recuperar Codigo Do Cliente 
    ${result}    Query    ${SQL_CODIGO_CLIENTE}    
    [Return]     ${result}
    
Recuperar Dados Do Cliente
    [Arguments]  ${codigo_cliente}
    ${result}    Query    	SELECT LPAD(X0100_CLTCOD,8,'0'), TRIM(X0100_CLTNOM), LPAD(X0100_CLTCGC,12,'0'), LPAD(X0100_CLTCGCDIG,2,'0'), LPAD(X0100_CLTDATNAS,8,'0') FROM AOXB00.AOXB01 A WHERE X0100_CLTCOD = ${codigo_cliente}    
    [Return]     ${result}
    
Recuperar Acesso Composicao Societaria
    ${result}    Query    ${SQL_RECUPERAR_DADO_ACESSO_CONSULTA_COMPOSICAO_PF}    
    [Return]     ${result}

#TODO validação sempre no módulo de keywords
Recuperar Dados Validar Consulta Participacao PF
    [Arguments]    ${codigo}
    ${dado}    Format String    ${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA_PARTICIPACAO_PF}    ${codigo}
    ${result}    Query    ${dado}    
    [Return]    ${result}

Recuperar Acesso Composicao Societaria PJ
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_PJ}    
    [Return]     ${result}

#TODO validação sempre no módulo de keywords
Recuperar Dados Validar Consulta Participacao PJ
    [Arguments]    ${codigo}
    ${dado}    Format String    ${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA_PARTICIPACAO_SOCIETARIA}    ${codigo}
    ${result}    Query    ${dado}    
    [Return]    ${result}

#TODO validação sempre no módulo de keywords
Recuperar Dados Validar Grupos Economicos
    ${result}    Query    ${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA_GRUPOS_ECONOMICOS}    
    [Return]     ${result}
    
Recuperar Acesso Codigo Grupo
    ${result}    Query    ${SQL_ACESSO_ENTRADA_GRUPO}
    [Return]    ${result}

#TODO validação sempre no módulo de keywords
Recuperar Dados Validar Grupo Economico
    [arguments]    ${grupo}
    ${dado}    Format String    ${SQL_RECUPERAR_DADOS_VALIDAR_DADO_GRUPO_ECONOMICO}    ${grupo}
    ${result}    Query    ${dado}    
    [Return]    ${result}

#FIXME corrigir grafia
Recuerar Acesso GEIC
    ${result}    Query    ${SQL_RECUPERAR_CONSULTA_GEIC}    
    [Return]    ${result}
    
#TODO validação sempre no módulo de keywords
Recuperar Dados Validar Consulta GEIC
    [Arguments]    ${cliente}
    ${dado}    Format String    ${SLQ_RECUPERAR_DADOS_VALIDAR_CONSULTA_GEIC}    ${cliente}
    ${result}    Query    ${dado}
    [Return]    ${result}
    