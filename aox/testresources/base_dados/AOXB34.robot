*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py      
Library      Collections    

*** Variables ***

${SQL_CLIENTE_IMPRESSAO_CADASTRAL} =  SELECT LPAD(X0100_CLTCOD,8,'0'), 
...  CAST (X0100_CLTDGV AS CHAR) FROM AOXB00.AOXB01 WHERE X0100_CLTCOD=( 
...    SELECT X3400_CLTCOD FROM (SELECT AOXB00.AOXB34.X3400_CLTCOD, MAX
...    (AOXB00.AOXB34.X3400_IMPDAT) FROM AOXB00.AOXB34 WHERE AOXB00.AOXB34.X3400_IMLSEQ = 1 
...    GROUP BY AOXB00.AOXB34.X3400_CLTCOD ORDER BY 2 DESC FETCH FIRST ROW ONLY));



# ${SQL_CLIENTE_IMPRESSAO_CADASTRAL} =  
# ...    SELECT LPAD(X0100_CLTCOD,8,'0'), CAST (X0100_CLTDGV AS CHAR)
# ...    FROM AOXB00.AOXB34
# ...    INNER JOIN AOXB00.AOXB01
# ...    ON  X3400_CLTCOD = X0100_CLTCOD
# ...    WHERE X3400_CLTCOD IS NOT NULL
# ...    AND X3400_IMLSEQ = 1
# ...    ORDER BY X3400_IMPDAT DESC
# ...    FETCH FIRST 1 ROW ONLY
# ...    




*** Keywords ***
Retornar Codigo Cliente Impressao Cadastral
    @{query} =  Query  ${SQL_CLIENTE_IMPRESSAO_CADASTRAL}
    ${cltCod} =  Get From List  ${query}[0]  0
    ${digito} =  Get From List  ${query}[0]  1
    [return]  ${cltCod}  ${digito}

Selecionar Dados Observacao Impressao Cadastral
    [Arguments]  ${cltcod}
    ${result} =  Query  SELECT TRIM(X3400_IMLDES), CAST (X3400_IMPDAT AS CHAR (8)) FROM AOXB00.AOXB34 WHERE X3400_CLTCOD = ${cltcod} AND X3400_IMLSEQ = 1 ORDER BY X3400_IMPDAT DESC FETCH FIRST 1 ROW ONLY
    #${observacao} =   Get From List  ${result}[0]  0
    #${dataRevisao} =  Get From List  ${result}[0]  1 
    #[Return]     ${observacao}  ${dataRevisao}
    [Return]     ${result}
    
Defazer Alteracoes Observacao Cadastral
    [Arguments]       ${cltcod}       ${obsImpressaoCadastral}  ${dataRevisao}
    ${resultado} =    Run Keyword And Return Status      Execute Sql String  
    ...  UPDATE AOXB00.AOXB34 SET X3400_IMLDES = '${obsImpressaoCadastral}' WHERE X3400_CLTCOD = ${cltcod} AND X3400_IMPDAT = ${dataRevisao} AND X3400_IMLSEQ = 1;
    [Return]          ${resultado}