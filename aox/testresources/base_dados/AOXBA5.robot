*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py
Library      String          

*** Variable ***

${SQL_CONSULTA_GRUPO}
...    SELECT LPAD(XA400_GECCOD,6,'0') AS GRUPO FROM AOXB00.AOXBA4 
...    INNER JOIN AOXB00.AOXBA5 ON (XA500_GECCOD = XA400_GECCOD)
...    WHERE XA400_GECSIT <> 2 AND XA400_GECCODGER <> 1 FETCH FIRST ROW ONLY
    
${SQL_UPDATE_MANUTENCAO_GEIC_MANUAL}
...  UPDATE AOXB00.AOXBA5 SET XA500_GECCODGER = 0, XA500_GECSIT = 0 WHERE XA500_GECCOD = {0}
    
***Keywords** 
   
Recuperar Daddos Manutencao GEIC Manual  
    ${result}   Query    ${SQL_CONSULTA_GRUPO} 
    Atualizar Dados Manutenacao GEIC Manual   ${result}[0][0]   
    [Return]    ${result}  
    
Atualizar Dados Manutenacao GEIC Manual
    [Arguments]      ${grupo}
    ${query}    Format String      ${SQL_UPDATE_MANUTENCAO_GEIC_MANUAL}   ${grupo}
    ${result}   Execute Sql String    ${query}    
    [Return]    ${result} 
    