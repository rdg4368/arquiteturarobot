*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem    
Library      Collections
Library      utilsMask
Library      String    

*** Variables ***
${SQL_CLIENTE_CONCEITO_CLIENTE} =  
...		SELECT
...			LPAD(X1000_CLTCOD,
...			8,
...			'0'),
...			CAST (X0100_CLTDGV AS CHAR)
...		FROM
...			AOXB00.AOXB10
...		INNER JOIN AOXB00.AOXB01 ON
...			X1000_CLTCOD = X0100_CLTCOD
...		ORDER BY
...			X1000_HDRDATA DESC FETCH FIRST 1 ROWS ONLY

${SQL_CONCEITO_CLIENTE}
...		SELECT
...			*
...		FROM
...			AOXB00.AOXB10
...		WHERE
...			X1000_CLTCOD = {0}
...			AND X1000_CADDESOBS1 LIKE '{1}'

${SQL_DELETAR_CONCEITO_CLIENTE}
...		DELETE
...		FROM
...			AOXB00.AOXB10
...		WHERE
...			X1000_CLTCOD = {0}
...			AND X1000_CADDESOBS1 LIKE '{1}'

${SQL_RECUPERAR_CONCEITO_CLIENTE}	
...		SELECT
...			X1000_CADCODRSTNG AS RESTRICOES,
...			X1000_CADCODTMPNG AS TEMPO_EXISTENCIA,
...			X1000_CADCODFINNG AS ANALICA_ECO_FINAN,
...			X1000_CADCODATRS AS HIST_MED_ATRASO,
...			X1000_CADCODCAD AS CONCEITO_CLIENTE,
...			CAST(X1000_CADDATSERCD AS CHAR (8))
...		FROM
...			AOXB00.AOXB10
...		WHERE
...			X1000_CLTCOD = {0}
...		ORDER BY
...			X1000_HDRDATA DESC FETCH FIRST 1 ROWS ONLY

*** Keywords ***
Retornar Dados do Cliente Conceito De Cliente
    ${query} =   Query  ${SQL_CLIENTE_CONCEITO_CLIENTE}
    ${cltCod} =  Get From List  ${query}[0]  0
    ${digito} =  Get From List  ${query}[0]  1
    [return]     ${cltCod}  ${digito}
    
Consultar Conceito De Cliente
    [Arguments]  ${cltCod}  ${txtObs}
    ${DADO}      Format String    ${SQL_CONCEITO_CLIENTE}    ${cltCod}  ${txtObs}
    ${query} =   Run Keyword And Return Status     Check If Exists In Database    ${DADO}    
    [return]     ${query}
    
Deletar Conceito De Cliente
    [Arguments]  ${cltCod}  ${txtObs}
    ${QUERY}     Format String    ${SQL_DELETAR_CONCEITO_CLIENTE}        ${cltCod}  ${txtObs}    
    ${delete} =  Run Keyword And Return Status     Execute Sql String    ${QUERY}         
    [return]     ${delete}
    
Retornar Conceito De Cliente
    [Arguments]  ${cltCod}
    ${DADO}      Format String    ${SQL_RECUPERAR_CONCEITO_CLIENTE}    ${cltCod}    
    @{query} =   Query      ${DADO}
    ${restricoes} =       Get From List  ${query}[0]  0
    ${tempoExistencia} =  Get From List  ${query}[0]  1
    ${analiseEcoFinan} =  Get From List  ${query}[0]  2
    ${histMedAtraso} =    Get From List  ${query}[0]  3
    ${conceitoCliente} =  Get From List  ${query}[0]  4
    ${formatarData} =     Formatar Mascara Data Trim    ${query}[0][5]
    [Return]         ${restricoes}  ${tempoExistencia}  ${analiseEcoFinan}  ${histMedAtraso}  ${conceitoCliente}  ${formatarData}