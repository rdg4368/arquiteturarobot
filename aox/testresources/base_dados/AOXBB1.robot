*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem   
Library      utils
Library      utilsMask       
Library      String     
 
*** Variables***
${SQL_RECUPERAR_DADOS_PODERES_CLIENTE}
...		SELECT
...			TRIM(XB100_CLTCOD) AS CLIENTE,
...			TRIM(X0100_CLTDGV) AS DIG
...		FROM
...			AOXB00.AOXBB1,
...			AOXB00.AOXB01
...		WHERE
...			XB100_CLTCOD = X0100_CLTCOD FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_DE_ENTRADA}
...	 SELECT
...	 	TRIM(XB100_CLTCOD)
...	 FROM
...	 	AOXB00.AOXBB1
...	 WHERE
...	 	XB100_CLTCOD > 0
...	 	AND XB101_CLTTIP = 1
...	 	AND XB101_CLTSTAALT = 0 FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADO_CONSULTA_ENDERECO}
...  	SELECT
...  	TRIM(XB100_CLTCOD)
...  FROM
...  	AOXB00.AOXBB1
...  WHERE
...  	XB100_CLTCOD > 0
...  	AND XB101_CLTTIP = 1
...  	AND XB101_CLTSTAALT = 0
...  	AND XB100_TPREGISTRO = 1 FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_ENTRADA_ATIVIDADE_EMPRESARIAL}
...    SELECT
...    	    TRIM(XB100_CLTCOD),
...    	    A.*
...    FROM
...    	    AOXB00.AOXBB1 A
...    WHERE
...    	    XB100_TPREGISTRO = 22
...    AND 
...         XB100_CLTCOD > 0 FETCH FIRST ROW ONLY


${SQL_RECUPERAR_DADO_REFERENCIAS} 
...    SELECT
...    	    TRIM(XB100_CLTCOD),
...    	    A.*
...    FROM
...    	    AOXB00.AOXBB1 A
...    WHERE
...    	    XB100_TPREGISTRO = 23
...    AND 
...         XB100_CLTCOD > 0 FETCH FIRST ROW ONLY

${SQL_CONSULTA_REFERENCIA}
...     SELECT LPAD(XB100_CLTCOD,8,'0'), A.* FROM AOXB00.AOXBB1 A WHERE XB100_TPREGISTRO = 22 AND XB100_CLTCOD > 0 FETCH FIRST ROW ONLY

*** Keywords ***
Recuperar Dados Poderes Cliente     
    ${result}  Query   ${SQL_RECUPERAR_DADOS_PODERES_CLIENTE}
    [return]  ${result}
    
Recuperar Dados Entrada 
    ${result}        query    ${SQL_RECUPERAR_DADOS_DE_ENTRADA}        
    [Return]         ${result} 
    
Recuperar Dados Entrada Consulta Endereco
    ${result}       query    ${SQL_RECUPERAR_DADO_CONSULTA_ENDERECO}       
    [Return]        ${result} 
    
Recuperar Dados Entrada Atividade Empresarial
    ${result}       query     ${SQL_RECUPERAR_DADOS_ENTRADA_ATIVIDADE_EMPRESARIAL}   
    [Return]        ${result} 
    
Recuperar Dados Referencias
    ${result}       query     ${SQL_RECUPERAR_DADO_REFERENCIAS}   
    [Return]        ${result} 
    
Recuperar Dados Entrada Consulta Referencia
    ${result}       query        ${SQL_CONSULTA_REFERENCIA}
    [Return]        ${result}
    




