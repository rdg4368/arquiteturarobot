*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary

*** Variable ***

${SQL_VALIDAR_PAISES_IBGE}    
...    SELECT   
...        XB500_PAINOMPAIS AS NOME_PAIS 
...    FROM    
...        AOXB00.AOXBB5
...    ORDER BY    
...        XB500_PAICODPAIS  FETCH FIRST 14 ROWS ONLY

*** Keywords ***

#TODO renomear para 'Recuperar Paises IBGE'
Recuperar Validar Dados Paises IBGE
   ${result}   Query   ${SQL_VALIDAR_PAISES_IBGE}
   [Return]  ${result}
    
#TODO essa keyword não tem necessidade de existir. Usar a keyword acima.
Recuperar Validar Dados Consulta RFB
    ${result}  Query  ${SQL_VALIDAR_PAISES_IBGE}  
    [return]  ${result}   