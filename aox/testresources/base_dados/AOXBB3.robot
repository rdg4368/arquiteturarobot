*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py
Library      String       
 
*** Variable ***
${SQL_VALIDAR_INCLUSAO_RELACIONAMENTO}
...    SELECT XB300_GCVCOD AS GRUPO, TRIM(XB300_CLTCOD ) AS CLIENTE, X0100_CLTNOM AS NOME FROM AOXB00.AOXBB3, AOXB00.AOXB01 WHERE XB300_CLTCOD = {0} AND XB300_GCVCOD = '{1}' AND X0100_CLTNOM = '{2}' FETCH FIRST 1 ROWS ONLY
   
${SQL_EXCLUIR_INCLUSAO_RELACIONAMENTO}
...    DELETE FROM AOXB00.AOXBB3 WHERE XB300_CLTCOD = {0}  

${SQL_RECUPERAR_DADOS_CONSULTA_CLIENTE_GRUPO_CONVENIO}
...    SELECT LPAD(XB300_CLTCOD,8,'0') AS CLIENTE FROM AOXB00.AOXBB3 WHERE XB300_GRESIT = 1 FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_CONSULTA_CLIENTE_GRUPO_CONVENIO}
...    SELECT   LPAD(X0100_CLTCOD,    8,    '0')|| ' ' || X0100_CLTDGV AS CLIENTE,    X0100_CLTNOM AS NOME,    XB200_GCVCOD AS GRUPO,    XB200_GCVNOM AS NOME_GRUPO,    CASE XB200_GRESIT WHEN 1 THEN 'Ativo' WHEN 2 THEN 'Inativo' END SITUACAO
...    FROM    AOXB00.AOXBB2,    AOXB00.AOXBB3,    AOXB00.AOXB01 WHERE    XB200_GCVCOD = XB300_GCVCOD    AND    X0100_CLTCOD = XB300_CLTCOD    AND    X0100_CLTCOD = '{0}'   

${SQL_DADOS_HISTORICO_GRUPO_CONVENIO_CLIENTE}
...    SELECT XB200_GCVCOD AS GRUPO FROM AOXB00.AOXBB2, AOXB00.AOXBB3 WHERE XB200_GCVCOD = XB300_GCVCOD

${SQL_VALIDAR_DADOS_HISTORICO_GRUPO_CONVENIO_CLIENTE}
...    SELECT XB200_GCVCOD AS GRUPO, XB200_GCVNOM AS NOME FROM AOXB00.AOXBB2 WHERE XB200_GCVCOD = '{0}' FETCH FIRST ROW ONLY 
   
${SQL_DADOS_HISOTRICO_CLIENTE_GRUPO}
...    SELECT LPAD(XB300_CLTCOD,8,'0') AS CLIENTE FROM AOXB00.AOXBB3 WHERE XB300_CLTCOD = 969645  FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_HISTORICO_CLIENTE_GRUPO}
...    SELECT XB300_GCVCOD AS GRUPO, XB300_CLTCOD||' '||X0100_CLTDGV AS CLIENTE, X0100_CLTNOM AS NOME, SUBSTR(XB200_GCVNOM,1,23) AS NOME_GRUPO
...    FROM AOXB00.AOXBB3,AOXB00.AOXB01,AOXB00.AOXBB2 WHERE XB300_CLTCOD = X0100_CLTCOD AND XB200_GCVCOD = XB300_GCVCOD AND XB300_CLTCOD = 969645
...    FETCH FIRST 2 ROWS ONLY

*** Keywords ***
Recuperar Dados Validacao Inclusao Relacionamento Grupo 
    [Arguments]   ${codCliente}  ${codGrupo}  ${nomeCliente}
    ${sql}  Format String  ${SQL_VALIDAR_INCLUSAO_RELACIONAMENTO}  ${codCliente}  ${codGrupo}  ${nomeCliente}
    ${status}  Run Keyword And Return Status  Check If Exists In Database  ${sql}   
    [return]  ${status}

Recuperar Dados Consulta Cliente Grupo Convenio
    ${result}  Query  ${SQL_RECUPERAR_DADOS_CONSULTA_CLIENTE_GRUPO_CONVENIO}
    [return]  ${result} 
    
Excluir Inclusao De Relacionamento
    [Arguments]  ${codCliente}
    ${sql}  Format String  ${SQL_EXCLUIR_INCLUSAO_RELACIONAMENTO}  ${codCliente}
    Execute Sql String    ${sql}   
    
Recuperar Dados Validacao Consulta Cliente Grupo Convenio
    [Arguments]  ${codCliente}
    ${sql}    Format String    ${SQL_VALIDAR_DADOS_CONSULTA_CLIENTE_GRUPO_CONVENIO}  ${codCliente} 
    ${result}  Query  ${sql} 
    [return]  ${result}    
    
Recuperar Dados Historico Relacionamento Grupo Covenio Cliente
    ${result}  Query  ${SQL_DADOS_HISTORICO_GRUPO_CONVENIO_CLIENTE}
    [return]  ${result}
    
Recuperar Dados Validacao Historico Relacionamento Grupo Convenio Cliente
    [Arguments]  ${grupo}
    ${sql}  Format String  ${SQL_VALIDAR_DADOS_HISTORICO_GRUPO_CONVENIO_CLIENTE}  ${grupo}
    ${result}  Query  ${sql}
    [return]  ${result}
    
Recuperar Dados Relacionamento Cliente Grupo
    ${result}  Query  ${SQL_DADOS_HISOTRICO_CLIENTE_GRUPO}
    [return]  ${result}
    
Recuperar Dados Validacao Relacionamento Cliente Grupo
    ${result}  Query  ${SQL_VALIDAR_DADOS_HISTORICO_CLIENTE_GRUPO}
    [return]  ${result} 
     
 


