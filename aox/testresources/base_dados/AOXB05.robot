*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      utilsMask 
Library      String 

*** Variable ***

${SQL_RECUPERAR_DADOS_ALTERACAO_GRUPO_ECONOMICO}
...    SELECT
...        X0500_GRECOD AS COD_GRUPO
...    FROM
...      AOXB00.AOXB05
...    WHERE
...        X0500_GRESIT = 1 FETCH FIRST ROW ONLY

${SQL_DESFAZER_DADOS_ALTERACAO_GRUPO_ECONOMICO}
...    UPDATE
...    	AOXB00.AOXB05
...    SET
...    	X0500_GRENOM = '{0}'
...    WHERE
...    	X0500_GRECOD = '{1}'

${SQL_VALIDA_CONSULTA_GRUPOS_ECONOMICOS}
...    SELECT
...    	X0500_GRENOM AS NOME,
...    	X0500_GRECOD AS CODIGO
...    FROM
...    	AOXB00.AOXB05
...    ORDER BY
...    	X0500_GRECOD FETCH FIRST ROWS ONLY

${SQL_VALIDAR_DADOS_ALTERACAO_GRUPO_ECONOMIGO}
...    SELECT
...    	TRIM(X0500_GRECOD) AS CODIGO,
...    	TRIM(X0500_GRENOM) AS NOME
...    FROM
...    	AOXB00.AOXB05
...    WHERE
...    	X0500_GRECOD = '{0}'

${SQL_RECUPERAR_DADOS_CONSULTA_HISTORICO_GRUPO_ECONOMICO}
...     SELECT
...    	X0500_GRECOD AS CODIGO
...    FROM
...    	AOXB00.AOXB05 FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_CONSULTA_HISTORICO_GRUPO_ECONOMICO}
...    SELECT
...    	X0500_GRENOM AS NOME,
...    	TRIM(X0500_GRECODOPE) AS OPERACAO,
...    	X0500_GREDATATZ || '-' || X0500_GREDATATZ || '-' || X0500_GREDATATZ AS DATA_OPER,
...    	X0500_GREHORATZ || ':' || X0500_GREHORATZ || ':' || X0500_GREHORATZ AS HORA_OPE,
...    	TRIM(X0500_CLTNUMUSU) AS MATRICULA
...    FROM
...    	AOXB00.AOXB05
...    WHERE
...    	X0500_GRECOD = '{0}'

${SQL_RECUPERAR_DADOS_CONSULTA_RELACIONAMENTO_GRUPO_ECONOMICO}
...    SELECT
...    	X0500_GRECOD AS GRUPO
...    FROM
...    	AOXB00.AOXB05,
...    	AOXB00.AOXB06
...    WHERE
...    	X0500_GRECOD = X0600_GRECOD FETCH FIRST ROW ONLY

${SQL_VALIDAR_CONSULTA_RELACIONAMENTO_GRUPO_ECONOMICO}
...    SELECT
...    	X0500_GRECOD AS GRUPO,
...    	X0500_GRENOM AS NOME
...    FROM
...    	AOXB00.AOXB05
...    WHERE
...    	x0500_GRECOD = '{0}'

*** Keywords ***
Recupera Dados Alteração Grupo Economico
    ${result}    query             ${SQL_RECUPERAR_DADOS_ALTERACAO_GRUPO_ECONOMICO}
    [Return]     ${result}
    
Desfaz Dados Alteração Grupo Economico
   [Arguments]   ${nome}    ${CODIGO}
   ${dado}       Format String    ${SQL_DESFAZER_DADOS_ALTERACAO_GRUPO_ECONOMICO}    ${nome}    ${CODIGO}    
   ${result}     Execute Sql String    ${dado}          
   
Valida Consulta Grupos Economicos
   ${status}       query  ${SQL_VALIDA_CONSULTA_GRUPOS_ECONOMICOS}  
   [Return]      ${status}

Valida Dados Alteração Grupo Economico
   [Arguments]   ${CODIGO}
   ${dado}       Format String    ${SQL_VALIDAR_DADOS_ALTERACAO_GRUPO_ECONOMIGO}    ${CODIGO}
   ${status}     query    ${dado}    
   [Return]      ${status}
   
Recuperar Dados Consulta Historico Grupo Economico
    ${result}    query      ${SQL_RECUPERAR_DADOS_CONSULTA_HISTORICO_GRUPO_ECONOMICO}
    [Return]     ${result}
    
Validar Dados Consulta Historico Grupo Economico
   [Arguments]   ${CODIGO}
   ${dado}      Format String     ${SQL_VALIDAR_DADOS_CONSULTA_HISTORICO_GRUPO_ECONOMICO}    ${CODIGO}
   ${status}     Query     ${dado}
   [Return]      ${status}
   
Recuperar Dados Consulta Relacionamento Grupo Economico/Cliente
    ${result}    query     ${SQL_RECUPERAR_DADOS_CONSULTA_RELACIONAMENTO_GRUPO_ECONOMICO}
    [Return]     ${result}

Validar Consulta Relacionamento Grupo Economico/Clientes
   [Arguments]   ${CODIGO}
   ${dado}      Format String    ${SQL_VALIDAR_CONSULTA_RELACIONAMENTO_GRUPO_ECONOMICO}    ${CODIGO}
   ${status}     Query    ${dado}    
   [Return]      ${status}

