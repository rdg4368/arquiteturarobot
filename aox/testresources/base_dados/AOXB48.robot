*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
 
*** Variable ***

${LINHA}    1

${SQL_CONSULTAR_CLTCOD_HISTORICO_ENDERECO}  
...  SELECT TRIM(X4800_CLTCOD), 
...  TRIM(X0100_CLTDGV)
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB48 
...  ON X0100_CLTCOD = X4800_CLTCOD 
...  WHERE X4800_BDECEP <> 0 
...  AND X4800_BDELOG IS NOT NULL 
...  AND X4800_DEPSEQ <> 0 
...  AND X4800_BDEDATEND <> 0 
...  AND X4800_BDELOGEND IS NOT NULL  
...  FETCH FIRST ${LINHA} ROWS ONLY 

${SQL_CONSULTAR_CLTCOD_END_CORRESP_ATIVO}  
...  SELECT TRIM(X4800_CLTCOD), 
...  TRIM(X0100_CLTDGV),
...  TRIM(X4800_BDESEQEND)
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB48 
...  ON X0100_CLTCOD = X4800_CLTCOD 
...  WHERE X4800_BDECEP <> 0 
...  AND X4800_BDELOG IS NOT NULL 
...  AND X4800_DEPSEQ <> 0 
...  AND X4800_BDEDATEND <> 0 
...  AND X4800_BDELOGEND IS NOT NULL 
...  AND X4800_BDETIPEND = '2'   
...  AND X4800_BDESITEND = '1'     
...  FETCH FIRST ${LINHA} ROWS ONLY 


*** Keywords ***
Recuperar Codigo Cliente Marcacao De Endereco Invalido    
    ${result}       Query    SELECT TRIM(X4800_CLTCOD),TRIM(X0100_CLTDGV),TRIM(X4800_BDESEQEND) FROM AOXB00.AOXB48 INNER JOIN AOXB00.AOXB01 ON X4800_CLTCOD = X0100_CLTCOD WHERE X4800_BDESITEND = '1' AND X4800_CLTIDENDINV = '0' FETCH FIRST 1 ROWS ONLY 
    [Return]        ${result}

Recuperar Dados Cliente Marcacao De Endereco Invalido 
    [Arguments]     ${cltCod}
    ${result}       Query    SELECT TRIM(X4800_CLTIDENDINV) FROM AOXB00.AOXB48 INNER JOIN AOXB00.AOXB01 ON X4800_CLTCOD = X0100_CLTCOD WHERE X4800_CLTCOD = ${cltCod} FETCH FIRST 1 ROWS ONLY
    [Return]        ${result}  

Desfazer Alteracao Endereco Invalido
    [Arguments]       ${cltCod}  ${codSeq}              
    ${result} =  Run Keyword And Return Status       Execute Sql String  
    ...  UPDATE AOXB00.AOXB48 SET X4800_CLTIDENDINV = '0' WHERE X4800_CLTCOD = ${cltCod} AND X4800_BDESEQEND = ${codSeq}
    [Return]    ${result} 

Recuperar Codigo Cliente Endereco Correspondencia Ativo
    ${result}  Query  ${SQL_CONSULTAR_CLTCOD_END_CORRESP_ATIVO}
    [Return]    ${result}

Recuperar Codigo Cliente Historico Endereco
    ${result}  Query  ${SQL_CONSULTAR_CLTCOD_HISTORICO_ENDERECO}  
    [Return]    ${result}

Recuperar Dados Historico Endereco
    [Arguments]     ${cltCod}       
    ${result}       Query    SELECT CONCAT(CONCAT(TRIM(X4800_CLTCOD), ' - '), TRIM(X0100_CLTDGV)), TRIM(X4800_BDEDATEND),TRIM(X4800_BDECEP),TRIM(X4800_BDELOGEND),TRIM(X4800_BDECOMPEND),TRIM(X4800_BDEBAI),TRIM(X4800_BDECID),TRIM(X4800_BDEUFE),TRIM(LPAD(X4800_DEPSEQ,4,'0')),TRIM(LPAD(X4800_BDEUSUNUM,6,'0')),TRIM(LPAD(X4800_BDESEQEND,4,'0')), TRIM(X4800_BDESITEND) FROM AOXB00.AOXB01 INNER JOIN AOXB00.AOXB48 ON X0100_CLTCOD = X4800_CLTCOD WHERE X4800_CLTCOD = ${cltCod} ORDER BY X4800_BDESEQEND DESC FETCH FIRST 1 ROWS ONLY 
    [Return]        ${result}

Recuperar Dados Alteracao Correspondencia
    [Arguments]     ${cltCod}     
    ${result}       Query    SELECT TRIM(X4800_CLTCOD), TRIM(X4800_BDECEP),TRIM(X4800_BDELOGEND),TRIM(X4800_BDECOMPEND), TRIM(X4800_BDEBAI), TRIM(X4800_BDECID), TRIM(X4800_BDEUFE), TRIM(X4800_BDESEQEND) FROM AOXB00.AOXB48 WHERE X4800_CLTCOD = ${cltCod} AND X4800_BDETIPEND = '2' AND X4800_BDESITEND = '1' ORDER BY X4800_BDESEQEND DESC FETCH FIRST 1 ROWS ONLY 
    [Return]        ${result}

Deletar Novo Dado Alteracao Correspondencia
    [Arguments]       ${cltCod}   ${codSeq}           
    ${result} =  Run Keyword And Return Status       Execute Sql String  
    ...  DELETE FROM AOXB00.AOXB48 WHERE X4800_CLTCOD = ${cltCod} AND X4800_BDESEQEND = ${codSeq};
    [Return]    ${result}     

Deletar Dados Endereco Correspondencia X4800 
    [Arguments]       ${cep}              
    ${result} =  Run Keyword And Return Status       Execute Sql String  
    ...  DELETE FROM AOXB00.AOXB48 WHERE X4800_BDECEP = ${cep} ;
    [Return]    ${result} 
    
Defazer Alteracoes Dados Correspondencia Nova
    [Arguments]        ${cltCod}   ${codSeq} 
    ${resultado} =    Run Keyword And Return Status      Execute Sql String  
    ...  UPDATE AOXB00.AOXB48 SET X4800_BDESITEND = '1' WHERE X4800_CLTCOD = ${cltCod} AND X4800_BDESEQEND = ${codSeq} 
    [Return]          ${resultado}    
