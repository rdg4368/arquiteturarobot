*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String     

*** Variable ***

${SQL_CONSULTAR_ALTERACAO_PESSOA_JURIDICA}
...    SELECT X1900_JURNRNIF AS NIF FROM AOXB00.AOXB19 WHERE X1900_CLTCOD = {0} FETCH FIRST ROW ONLY

${SQL_UPDATE_ALTERACAO_PESSOA_JURIDICA}
...    UPDATE AOXB00.AOXB19 SET X1900_JURNRNIF = '0000000000000000000000000' WHERE X1900_CLTCOD = {0}

${SQL_RECUPERAR_NOME_FANTASIA}    
...    SELECT TRIM(X1900_JURNOMFANT) FROM AOXB00.AOXB19 WHERE X1900_CLTCOD = {0}

${SQL_DESFAZER_CLIENTE_PJ}    
...  UPDATE AOXB00.AOXB19 SET X1900_JURNOMFANT = '{0}' WHERE X1900_CLTCOD = {1}

${SQL_CODIGO_MUNICIPIO_CLIENTE_PJ}
...    SELECT X1900_MUCCODDNE FROM AOXB00.AOXB19 WHERE X1900_CLTCOD = {0}

${SQL_DESFAZER_ALTERACAO_CLIENTE_PJ}
...      UPDATE AOXB00.AOXB01 SET X0100_CLTCODSEG = '{0}' WHERE X0100_CLTCOD = {1}

${SQL_ALTERAR_CLIENTE_PJ_SIMPLIFICADO}    
...  UPDATE AOXB00.AOXB19 SET X1900_MUCCODDNE = '{0}' WHERE X1900_CLTCOD = {1}



*** Keywords ***

#TODO AOXB19 :: DONE
Recuperar Nome Fantasia PJ
    [Arguments]     ${cltcod}  
    ${QUERY}        Format String    ${SQL_RECUPERAR_NOME_FANTASIA}     ${cltcod}    
    ${result}       Query            ${QUERY}     
    [Return]        ${result}[0][0]  

Defazer Alteracoes Cliente PJ Completo
    [Arguments]       ${nomeFantasia}    ${cltcod}      
    ${QUERY}          Format String    ${SQL_DESFAZER_CLIENTE_PJ}    ${nomeFantasia}    ${cltcod}    
    ${resultado} =    Run Keyword And Return Status      Execute Sql String    ${QUERY}  
    [Return]          ${resultado}
    
#TODO AOXB19 :: DONE
Recuperar Codigo Municipio Cliente PJ
    [Arguments]     ${cltcod}
    ${QUERY}        Format String    ${SQL_CODIGO_MUNICIPIO_CLIENTE_PJ}    ${cltcod}      
    ${result}       Query            ${QUERY}     
    [Return]        ${result}[0][0]  
    
#TODO AOXB19 :: DONE
Defazer Alteracoes Cliente PJ Simplificado
    [Arguments]     ${cltcod}      ${setorPublico}     ${codigoMunicipio}   
    ${QUERY}        Format String    ${SQL_DESFAZER_ALTERACAO_CLIENTE_PJ}     ${setorPublico}      ${cltcod}
    ${query1}       Format String    ${SQL_ALTERAR_CLIENTE_PJ_SIMPLIFICADO}   ${codigoMunicipio}   ${cltcod}     
    ${resultado} =  Run Keyword And Return Status       Execute Sql String    ${QUERY}   
    ${resultado} =  Run Keyword And Return Status       Execute Sql String    ${query1}   
    [Return]    ${resultado} 
    
    
Consultar Dados Alterar Pessoa Juridica Completa
    [Arguments]    ${codigo}    
    ${query}    Format String      ${SQL_CONSULTAR_ALTERACAO_PESSOA_JURIDICA}   ${codigo}   
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query}    
    [Return]    ${result}     
    
Desfazer Alteracoes Pessoa Juridica Completa
    [Arguments]    ${codigo}              
    ${query}     Format String      ${SQL_UPDATE_ALTERACAO_PESSOA_JURIDICA}  ${codigo}
    ${result}    Run Keyword And Return Status    Execute Sql String    ${query}    
    [Return]    ${result}
    
    
    
    
    