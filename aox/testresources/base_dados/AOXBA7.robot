*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py
Library      String          

*** Variable ***
${SQL_VALIDAR_INCLUSAO_DADO_GEIC}
...    SELECT * FROM AOXB00.AOXBA7
...    WHERE XA700_GECCOD = '{0}' AND XA700_HDRDATA = {1}

${SQL_DESFAZER_ICLUSAO}
...    DELETE  AOXB00.AOXBA7 WHERE XA700_GECCOD = '{0}' AND XA700_HDRDATA = {1}

${SQL_RECUPERAR_DADOS_MANUTENCAO_JUSTIFICATIVA}
...    SELECT TRIM(XA700_GECDESJUS) FROM AOXB00.AOXBA7
...    WHERE XA700_GECCOD = '{0}' AND XA700_HDRDATA = {1}

***Keywords**
#TODO não deve haver keywords de validação no modulo de banco de dados.
Recuperar Dados Validar Inclusao Justificativa
    [Arguments]    ${grupo}    ${data_atual}
    ${query}    Format String    ${SQL_VALIDAR_INCLUSAO_DADO_GEIC}    ${grupo}    ${data_atual}
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query} 
    [Return]    ${result}   
    
Desfazer Inclusao GEIC
    [Arguments]    ${grupo}    ${data_atual}
    ${dado}    Format String    ${SQL_DESFAZER_ICLUSAO}    ${grupo}    ${data_atual}
    ${query}    Execute Sql String    ${dado}
     
Recuperar Dado Antigo Alterar Manutencao
    [Arguments]    ${grupo}    ${data}
    ${query}    Format String    ${SQL_RECUPERAR_DADOS_MANUTENCAO_JUSTIFICATIVA}    ${grupo}    ${data}
    ${result}    Query    ${query}
    [Return]    ${result}

Armazenar Dado Novo
    [Arguments]    ${grupo}    ${data}
    ${query}       Format String    ${SQL_RECUPERAR_DADOS_MANUTENCAO_JUSTIFICATIVA}    ${grupo}    ${data}
    ${result}      Query    ${query}
    [Return]       ${result}

#TODO não deve haver keywords de validação no modulo de banco de dados.
Recuperar Dados Validar Exclusao Via Sistema
    [Arguments]    ${grupo}    ${data}
    ${query}       Format String      ${SQL_RECUPERAR_DADOS_MANUTENCAO_JUSTIFICATIVA}    ${grupo}    ${data}
    ${result}      Run Keyword And Return Status    Check If Exists In Database           ${query}    
    [Return]       ${result}
