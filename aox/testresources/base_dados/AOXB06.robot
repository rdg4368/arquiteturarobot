*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      testresources/utils/utils.py
Library      DateTime
Library      String    

*** Variable ***
${SQL_RECUPERAR_DADOS_CONSULTA_RELACIONAMENTO_CLIENTE}
...    SELECT
...    TRIM(X0600_CLTCOD) AS CLIENTE
...    FROM
...    	AOXB00.AOXB06 FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_CONSULTA_RELACIONAMENTO_CLIENTE}
...    SELECT
...        	X0600_CLTCOD AS CLIENTE,
...    	    X0100_CLTNOM AS NOME
...    FROM
...    	    AOXB00.AOXB06,
...    	    AOXB00.AOXB01
...    WHERE
...    	    X0600_CLTCOD = X0100_CLTCOD
...    	    AND X0600_CLTCOD = '{0}'

${SQL_RECUPERAR_DADOS_CONSULTA_COMPONETEE_CONTROLADORES_DO_GRUPO}
...    SELECT
...    	X0600_GRECOD AS CODIGO_GRUPO
...    FROM
...    	AOXB00.AOXB06 FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO}
...    SELECT
...    	TRIM(X0600_CLTCOD) AS CLIENTE
...    FROM
...    	AOXB00.AOXB06

${SQL_RECUPERAR_DADOS_ALTERACAO_RELACIONAMENTO_GRUPO_ECONOMICO_COMPONENTE_CONTROLADOR}
...    SELECT
...    	X0600_GRECOD AS GRUPO
...    FROM
...    	AOXB00.AOXB06

${SQL_RECUPERAR_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO_GRUPO_ECONOMICO_CLIENTE}
...    SELECT
...    	X0600_GRECOD AS GRUPO
...    FROM
...    	AOXB00.AOXB06 FETCH FIRST ROW ONLY

${SQL_VALIDA_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO_GRUPO_COMPONENTE}
...    SELECT
...    	DISTINCT LPAD(X0600_CLTCOD AS CLIENTE
...    FROM
...    	AOXB00.AOXB06,
...    	AOXB00.AOXB01
...    WHERE
...    	X0600_GRECOD = '{0}'

${PF}      1
${PJ}      2
${LINHAS}  1

#TODO todas as querys deveriam ser declaradas aqui. Existem muitas querys no meio do codigo.
#TODO isso prejudica manutenções futuras.

${SQL_CONSULTA_CODIGO_RENDA_DECLARADA}
...    SELECT 
...        TRIM(X0600_CLTCOD) AS CODIGO
...    FROM 
...        AOXB00.AOXB06 
...    FETCH FIRST ROW ONLY
 
#TODO validacoes não devem ser feitos em .robot que tratam apenas de Bancos de Dados.
#TODO Em arquivos que tratam de SQL, devem existir apenas operações de Consulta, Gravação, Alteração e Exclusão.
${SQL_VALIDA_RENDA_DECLARADA}
...    SELECT 
...        XB600_CLTCOD AS CLIENTE, 
...        XB600_PREVAL AS RENDA_PRESUMIDA
...    FROM 
...        AOXB00.AOXBB6 
...    WHERE 
...        XB600_CLTCOD = {0}
 
#TODO Não faz sentido o nome 'Preparar' para uma query SQL se a operação é DELETE
${SQL_DELETAR_RENDA_DECLARADA}
...    DELETE 
...    FROM 
...        AOXB00.AOXBB6 
...    WHERE 
...        XB600_CLTCOD = {0}


${SQL_RECUPERAR_DEPENDENCIA_DETENDORA_DATA_IGUAL_MAIOR_CORRENTE}
...    SELECT 
...        TRIM(XA600_CLTCODDEP) 
...    FROM 
...        AOXB00.AOXBA6 
...    WHERE 
...        XA600_CLTCODDEP > 0 
...    AND 
...        XA600_NOMDATATU >= {0}
...     FETCH FIRST 1 ROWS ONLY 


${SQL_RECUPERAR_DEPENDENCIAS_DETENTORA_DATA_CORRENTE} 
...    SELECT 
...        TRIM(XA600_CLTCODDEP) 
...    FROM 
...        AOXB00.AOXBA6 
...    WHERE 
...        XA600_CLTCODDEP > 0 
...    AND 
...        XA600_NOMDATATU >= {0} 
...    FETCH FIRST 1 ROWS ONLY

${SQL_RECUPERAR_ATUALIZAR_MAIOR_DATA_DEPENDENCIA_DETENTORA}
...    SELECT 
...        XA600_NOMDATATU AS DATA 
...    FROM 
...        AOXB00.AOXBA6 
...    WHERE 
...        XA600_CLTCODDEP > 0


${SQL_ATUALIZAR_MAIOR_DATA_DEPENDENCIA_DETENTORA}  
...    UPDATE 
...        AOXB00.AOXBA6 
...    SET 
...        XA600_NOMDATATU = {0} 
...    WHERE 
...        XA600_NOMDATATU = {1}

${SQL_RECUPERAR_CLIENTES_PENDENTES} 
...    SELECT 
...        TRIM(XA600_CLTCODDEP) 
...    FROM 
...        AOXB00.AOXBA6 
...    WHERE 
...        XA600_CLTCODDEP > 0 
...    AND
...         XA600_NOMDATATU >= {0} 
...    FETCH FIRST 1 ROWS ONLY


${SQL_LISTAR_DADOS_CONSULTA_CLIENTE_PENDENTE}
...    SELECT 
...        DISTINCT(XA600_CLTCOD), 
...        X0100_CLTDGV, TRIM(X0100_CLTNOM), 
...        XA600_NOMDATATU, 
...        XA600_NOMHORATU, 
...        XA600_CLTCODDEP, 
...        XA600_CLTNUMUSU 
...    FROM 
...        AOXB00.AOXBA6 
...    INNER JOIN 
...        AOXB00.AOXB01 ON X0100_CLTCOD = XA600_CLTCOD 
...    WHERE 
...        XA600_CLTCODDEP = {0} 
...    AND 
...        XA600_NOMDATATU >= {1}
 

${SQL_DESFAZER_ATUALIZACAO_MAIOR_DATA_DEPENDENCIA_DETENTORA}
...    UPDATE 
...        AOXB00.AOXBA6 
...    SET 
...        XA600_NOMDATATU = {0} 
...    WHERE 
...        XA600_NOMDATATU = {1}

*** Keywords ***


Recuperar Dependencia Detentora Data Maior Corrente
    [Arguments]     ${data_corrente}
    ${result}=      Recuperar Dependencia Detendora Data Igual Maior Corrente  ${data_corrente}
    ${data}         Run Keyword if    "${result}" == "False"     Atualizar Maior Data Dependencia Detentora     ${data_corrente}
    ${return}=      Recuperar Dependencia Detentora Data Corrente  ${data_corrente}    
    [Return]        ${return}  ${data}           

#TODO remover as querys de dentro das keywords e colocar no cabeçalho acima
#TODO esta query deveria ter o nome de 'Recuperar Dependencia Detendora Data Igual Maior Corrente'
#TODO esta query não deveria estar neste arquivo. Ela deve estar no arquivo AOXBA6.robot
Recuperar Dependencia Detendora Data Igual Maior Corrente
    [Arguments]     ${data_corrente}
    ${sql}    Format String    ${SQL_RECUPERAR_DEPENDENCIA_DETENDORA_DATA_IGUAL_MAIOR_CORRENTE}
    ${result}=      Run Keyword And Return Status  Check If Exists In Database  ${sql} 
    [Return]        ${result}


#TODO esta query não deveria estar neste arquivo. Ela deve estar no arquivo AOXBA6.robot
#TODO remover as querys de dentro das keywords e colocar no cabeçalho acima
Recuperar Dependencia Detentora Data Corrente
    [Arguments]     ${data_corrente}
    ${sql}    Format String    ${SQL_RECUPERAR_DEPENDENCIAS_DETENTORA_DATA_CORRENTE}    
    ${result}=      Query      ${sql}
    [Return]        ${result}[0][0]

#TODO esta query não deveria estar neste arquivo. Ela deve estar no arquivo AOXBA6.robot
#TODO essa keyword faz uma operação de Consulta e outra operação de Atualização. Portanto, deveria ser quebrada em duas outras keywords.
#TODO remover as querys de dentro das keywords e colocar no cabeçalho acima
Atualizar Maior Data Dependencia Detentora
    [Arguments]                      ${data_corrente}
    ${resulto}=     Query            ${SQL_RECUPERAR_ATUALIZAR_MAIOR_DATA_DEPENDENCIA_DETENTORA}
    ${sql}          Format String    ${SQL_ATUALIZAR_MAIOR_DATA_DEPENDENCIA_DETENTORA}    ${data_corrente}    ${resulto}[0][0]   
    Execute Sql String               ${sql}      
    [Return]                         ${resulto}[0][0]
    
#TODO esta query não deveria estar neste arquivo. Ela deve estar no arquivo AOXBA6.robot
#TODO remover as querys de dentro das keywords e colocar no cabeçalho acima
Recuperar Clientes Pendentes
    [Arguments]     ${data_corrente} 
    ${sql}    Format String    ${SQL_RECUPERAR_CLIENTES_PENDENTES}    ${data_corrente}     
    ${result}=      Query    ${sql}  
    [Return]        ${result}[0][0]

#TODO esta query não deveria estar neste arquivo. Ela deve estar no arquivo AOXBA6.robot
#TODO remover as querys de dentro das keywords e colocar no cabeçalho acima
#TODO deve-se evitar utilizar DISTINCT
Listar Dados Consulta Clientes Pendente
    [Arguments]         ${dependencia_detentora}  ${data_corrente}
    ${sql}    Format String    ${SQL_LISTAR_DADOS_CONSULTA_CLIENTE_PENDENTE}    ${dependencia_detentora}  ${data_corrente}
    ${queryResult}=     Query      ${sql} 
    [Return]            ${queryResult}       

#TODO esta query não deveria estar neste arquivo. Ela deve estar no arquivo AOXBA6.robot
#TODO Em arquivos que tratam de SQL, devem existir apenas operações de Consulta, Gravação, Alteração e Exclusão.
#TODO neste caso, não é necessária uma keyword com o nome 'Desfazer....". Utilizar apenas uma keyword de atualização comum.
Desfazer Atualizacao Maior Data Dependencia Detentora
    [Arguments]     ${data_corrente}   ${data}
    ${sql}    Format String    ${SQL_DESFAZER_ATUALIZACAO_MAIOR_DATA_DEPENDENCIA_DETENTORA}    ${data}    ${data_corrente}   
    Execute Sql String    ${sql}  

#TODO renomear para 'Recuperar Inclusão de Renda Declarada"
Recuperar Dados Inclusão de Renda Declarada
    ${results}    query    ${SQL_CONSULTA_CODIGO_RENDA_DECLARADA}
    [Return]    ${results}
    
    
Recuperar Dados Consulta Relacionamento Cliente/Grupos Economicos
    ${result}    query                            ${SQL_RECUPERAR_DADOS_CONSULTA_RELACIONAMENTO_CLIENTE}
    [Return]     ${result} 

Validar Dados Consulta Relacionamento Cliente/Grupos Economicos
   [Arguments]   ${cliente}
   ${dado}      Format String    ${SQL_VALIDAR_DADOS_CONSULTA_RELACIONAMENTO_CLIENTE}     ${cliente}
   ${status}     query   ${dado}
   [Return]      ${status}

Recuperar Dados Consulta Componentes e Controladores do Grupo Economico
    ${result}    query    ${SQL_RECUPERAR_DADOS_CONSULTA_COMPONETEE_CONTROLADORES_DO_GRUPO}
    [Return]     ${result}

Recuperar Dados Consulta Historico Relacionamento
    ${result}    query    ${SQL_RECUPERAR_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO}                            
    [Return]     ${result}

Recuperar Dados Alteração Relacionamento Grupo Economico/Componente-Controlador
    ${result}    query          ${SQL_RECUPERAR_DADOS_ALTERACAO_RELACIONAMENTO_GRUPO_ECONOMICO_COMPONENTE_CONTROLADOR}
    [Return]     ${result}

Recuperar Dados Consulta Historico Relacionamento Grupo Economico Cliente
    ${result}    query            ${SQL_RECUPERAR_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO_GRUPO_ECONOMICO_CLIENTE}
    [Return]     ${result}

Valida Dados Consulta Historico Relacionamento Grupo/Componente 
    [Arguments]   ${grupo}
    ${dado}      Format String    ${SQL_VALIDA_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO_GRUPO_COMPONENTE}
    ${status}        query    ${dado}
    [Return]      ${status}