*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      testresources/utils/utils.py
Library      DateTime       
Library      String    

*** Variable ***
${SQL_RECUPERAR_DADOS_SOLICITACAO}
...    SELECT 
...        X3200_CLTCOD || X0100_CLTDGV 
...    FROM 
...        AOXB00.AOXB32,
...        AOXB00.AOXB01,
...        AOXB00.AOXBB1 
...    WHERE 
...        X3200_CLTCOD = X0100_CLTCOD 
...    AND 
...        X3200_CLTCOD = XB100_CLTCOD 
...    FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_SOLICITACAO}
...    SELECT 
...         TRIM(X0100_CLTNOM) AS NOME,
...         X0100_CLTCGC ||'-'|| X0100_CLTCGCDIG AS CPF 
...    FROM 
...        AOXB00.AOXB32,
...        AOXB00.AOXB01 
...    WHERE 
...        X3200_CLTCOD = X0100_CLTCOD 
...    AND 
...        X3200_CLTCOD = {0}

${SQL_VALIDAR_CONSULTA_COMPLETA}
...    SELECT
...        TRIM(X0100_CLTNOM) AS NOME,
...        X0100_CLTCGC || '-' || X0100_CLTCGCDIG AS CPF,
...        TRIM(X3200_CLTCOD)|| '-' || TRIM(X0100_CLTDGV) AS CODIGO,
...        X3200_HDRDATA AS DT_ALT
...    FROM
...        AOXB00.AOXB32,
...        AOXB00.AOXB01
...    WHERE
...        X3200_CLTCOD = X0100_CLTCOD
...    AND 
...        X3200_CLTCOD = {0}

*** Keywords ***
Recuperar Dados Solicitacao
    ${result}  Query  ${SQL_RECUPERAR_DADOS_SOLICITACAO}
    [return]  ${result}
    
Recuperar Dados Validacao Consulta Solicitacao
    [Arguments]  ${codSemDigito}
    ${sql}  Format String  ${SQL_VALIDAR_DADOS_SOLICITACAO}  ${codSemDigito}
    ${result}  Query  ${sql}
    [return]  ${result} 

Recuperar Dados Validar Consulta Solicitacao Cliente
    [Arguments]  ${codSemDigito}
    ${sql}  Format String  ${SQL_VALIDAR_CONSULTA_COMPLETA}  ${codSemDigito}
    ${result}  Query  ${sql}
    [return]  ${result}