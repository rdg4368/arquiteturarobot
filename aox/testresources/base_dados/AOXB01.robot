*** Settings ***

Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem   
    
Library      utils
Library      utilsMask       
Library      String     
 
*** Variables***
${SQL_RECUPERAR_DADOS_CADASTRAIS}
...    SELECT
...    	TRIM(X0100_CLTCOD) AS CLIENTE
...    FROM
...    	AOXB00.AOXB01 FETCH FIRST ROW ONLY
 
${SQL_VALIDAR_DADOS_CADASTRAIS_WEB}
...    SELECT
...    	TRIM(X0100_CLTCOD) AS CLIENTE,
...    	X0100_CLTCGC || '-' || TRIM(X0100_CLTCGCDIG) AS CPF
...    FROM
...    	AOXB00.AOXB01
...    WHERE
...    	X0100_CLTCOD = '{0}' FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_CONSULTA_CONPONENTES_E_CONTROLES_DO_GRUPO} 
...    SELECT
...    	X0600_GRECOD AS GRUPO_ECONOMICO,
...    	X0500_GRENOM AS NOME_GRUPO,
...    	X0600_CLTCOD || ' ' || X0100_CLTDGV AS CODIGO_CLIENTE,
...    	X0100_CLTNOM AS NOME,
...    	X0100_CLTCGC|| '-' || X0100_CLTCGCDIG AS CPF_CNPJ
...    FROM
...    	AOXB00.AOXB01,
...    	AOXB00.AOXB05,
...    	AOXB00.AOXB06
...    WHERE
...    	X0600_CLTCOD = X0100_CLTCOD
...    	AND X0600_GRECOD = X0500_GRECOD
...    	AND X0500_GRECOD = '{0}'
...    ORDER BY
...    	X0100_CLTDGV FETCH FIRST ROW ONLY

${SQL_VALIDA_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO}
...    SELECT
...    	X0100_CLTCOD|| ' ' || X0100_CLTDGV AS CLIENTE,
...    	X0100_CLTNOM AS NOME,
...    	X0600_GRECOD AS GRUPO,
...    	X0500_GRENOM AS NOME_GRUPO,
...    	X0600_CGEDATOPE  AS DT_OPER,
...    	X0600_CGEHOROPE AS HORA_OP,
...    	X0600_HDRESTACAO AS ESTACAO,
...    	X0600_CLTNUMUSU AS MATRICULA
...    FROM
...    	AOXB00.AOXB01,
...    	AOXB00.AOXB06,
...    	AOXB00.AOXB05
...    WHERE
...    	X0100_CLTCOD = X0600_CLTCOD
...    	AND X0100_CLTCOD = 13511 FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO_GRUPO_ECONOMICO_CLIENTE}
...    SELECT
...    	X0100_CLTCOD || ' ' || X0100_CLTDGV AS CLIENTE,
...    	X0100_CLTNOM AS NOME,
...    	X0600_GRECOD AS GRUPO,
...    	X0500_GRENOM AS NOME_GRUPO,
...    	X0100_CLTCGC || '-' || X0100_CLTCGCDIG AS CPF_CNPJ
...    FROM
...    	AOXB00.AOXB01,
...    	AOXB00.AOXB06,
...    	AOXB00.AOXB05
...    WHERE
...    	X0100_CLTCOD = X0600_CLTCOD
...    	AND X0100_CLTCOD = '{0}' FETCH FIRST ROW ONLY

${PF}  1
${PJ}  2
${LINHAS}  1

#TODO: quebrar as query em várias linhas para facilitar a legibilidade
${SQL_CONSULTAR_CPF_VALIDO_DADOS_SIMPLIFICADOS}  
...  SELECT LPAD(X0100_CLTCGC,12,'0') AS CPF_CNPJ, 
...  X0100_CLTCGCDIG AS DIGITO_CPF_CNPJ, 
...  TRIM(X0100_CLTNOM) AS NOME, 
...  X0100_CLTCOD AS CODIGO, 
...  X0100_CLTDGV AS DIG_CODIGO, 
...  X0100_CLTCID AS CIDADE, 
...  X0100_CLTLOGEND AS ENDERECO, 
...  X0100_CNRDATCONS, 
...  X0100_CLTCEP AS CEP, 
...  X0100_CLTCODDEP AS AGENCIA, 
...  X1700_MUCCODDNE AS COGIDO_MUNICIPIO, 
...  TRIM(X3300_TVNCOD) AS TIPO_VINCULACAO,
...  TRIM(X3300_VINCODCLI) AS CODIGO_VINCULACAO_CLIENTE       
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB17 
...  ON X0100_CLTCOD = X1700_CLTCOD
...  INNER JOIN AOXB00.AOXB33
...  ON X1700_CLTCOD = X3300_CLTCOD    
...  WHERE LENGTH(RTRIM(X0100_CLTCGC))=9 
...  AND LENGTH(RTRIM(X0100_CLTCOD))=5 
...  AND LENGTH(RTRIM(X0100_CLTCODDEP))=5 
...  AND LENGTH(RTRIM(X3300_VINCODCLI))=6   
...  AND X0100_CLTTIP = ${PF}  
...  FETCH FIRST ${LINHAS} ROWS ONLY
   
${SQL_CONSULTAR_CPF_VALIDO_DADOS_COMPLETOS}  
...  SELECT LPAD(X0100_CLTCGC,12,'0') AS CPF_CNPJ, 
...  X0100_CLTCGCDIG AS DIGITO_CPF_CNPJ,
...  X0100_CLTNOM AS NOME,
...  X1700_FISNOMPAI AS NOME_PAI,
...  X1700_FISNOMMAE AS NOME_MAE,
...  X0100_CLTNOMSRF AS NOME_SRF, 
...  X1700_FISUFENAT AS UF,
...  X0100_CLTCOD AS CODIGO,
...  X0100_CLTDGV AS DIG_CODIGO, 
...  X1700_FISCIDNAT AS CIDADE_NATURAL, 
...  X0100_CLTLOGEND AS ENDERECO, 
...  X1100_COJNOM AS NOME_CONJUGE, 
...  X1100_COJCPF AS CPF_CONJUGE, 
...  X1100_COJCPFDGV AS COD_CPFCONJUGE, 
...  X1700_FISDATIDT AS DATA_EMISSA0,
...  X1700_FISNUMIDT AS CEDULA_IDENTIDADE,
...  X0100_CLTBAI AS BAIRRO,
...  X0100_CLTLOG AS LOGRADOURO_RED,
...  TRIM(X0100_CLTDATEND),
...  TRIM(X0100_CLTTELDATCT),
...  X0100_CLTCEP AS CEP,
...  TRIM(X0100_CLTCEL2) AS CELULAR        
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB11 ON X0100_CLTCOD = X1100_CLTCOD 
...  INNER JOIN AOXB00.AOXB17 ON X1100_CLTCOD = X1700_CLTCOD 
...  WHERE LENGTH(RTRIM(X0100_CLTCGC))=9 
...  AND LENGTH(RTRIM(X0100_CLTCGCDIG))=2 
...  AND X0100_CLTTIP = ${PF} 
...  AND X1700_FISNOMMAE IS NOT NULL 
...  AND X1700_FISNOMPAI IS NOT NULL
...  AND X1100_COJCPF IS NOT NULL 
...  AND X1700_MUCCODDNE <> 0
...  AND X0100_CLTTEL IS NOT NULL
...  AND X1700_FISCODOCUP <> 99999
...  AND X1700_FISCODOCUP <> 0
...  AND LENGTH(RTRIM(X1100_COJCPF))=9  
...  AND LENGTH(RTRIM(X0100_CLTCOD))=6  
...  AND X0100_CLTIDCMIL = '1'
...  AND X0100_CLTCEL2 <> 0  
...  FETCH FIRST ${LINHAS} ROWS ONLY
    
${SQL_CONSULTAR_CNPJ_VALIDO_DADOS_SIMPLIFICADOS}
...  SELECT X0100_CLTCGC AS CNPJ, 
...  X0100_CLTCGCDIG AS DIGITO_CNPJ, 
...  X0100_CLTNOM AS NOME, 
...  X0100_CLTCOD AS CODIGO, 
...  X0100_CLTDGV AS DIG_CODIGO, 
...  X0100_CLTCEP AS CEP, 
...  X0100_CLTLOG AS LOGRADOURO_RED, 
...  X0100_CLTBAI AS BAIRRO, 
...  X0100_CLTCID AS CIDADE, 
...  X0100_CLTUFE AS UF, 
...  X0100_CLTLOGEND AS END_LOGRADOURO, 
...  X1900_AECSUBCNAE AS COD_CNAE,
...  X0100_CLTCODAUT AS AGENCIA,
...  X0100_CLTTIP AS SETOR_PUPLICO,
...  X1900_MUCCODDNE AS CODIGO_MUNICIPIO    
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB19 
...  ON X0100_CLTCOD = X1900_CLTCOD 
...  WHERE LENGTH(RTRIM(X0100_CLTCGC))=12 
...  AND LENGTH(RTRIM(X0100_CLTCGCDIG))=2 
...  AND LENGTH(RTRIM(X0100_CLTCEP))=8 
...  AND X0100_CLTTIP = ${PJ}
...  AND LENGTH(RTRIM(X0100_CLTCOD))=6 
...  AND X1900_AECSUBCNAE <> 0
...  AND X0100_CLTCODAUT > 0 
...  AND X0100_CLTCODDEP = X0100_CLTCODAUT 
...  AND X0100_CLTCODDEP > 0      
...  FETCH FIRST ${LINHAS} ROWS ONLY

${SQL_DELETAR_CGC_INSERIDO_VIA_SISTEMA}
...  DELETE FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_CONSULTAR_CNPJ_VALIDO_DADOS_COMPLETOS}    
...  SELECT X0100_CLTCGC AS CNPJ, 
...  X0100_CLTCGCDIG AS DIGITO_CNPJ, 
...  X0100_CLTNOM AS NOME, 
...  X0100_CLTCOD AS CODIGO, 
...  X0100_CLTDGV AS DIG_CODIGO, 
...  X0100_CLTCEP AS CEP, 
...  X0100_CLTLOG AS LOGRADOURO_RED, 
...  X0100_CLTBAI AS BAIRRO, 
...  X0100_CLTCID AS CIDADE, 
...  X0100_CLTUFE AS UF, 
...  X0100_CLTLOGEND AS END_LOGRADOURO,
...  X1900_AECSUBCNAE AS COD_CNAE, 
...  LPAD(X1900_MUCCODDNE,8,'0') AS COD_MUNICIPIO_BRB, 
...  X1900_JURVALCAP AS CAPITAL, 
...  X1900_JURDATVAL AS DATA_VALORIZACAO, 
...  X1900_JURGIIN AS GINN, 
...  X1900_JURNRNIF AS NIF,
...  X1900_JURNOMFANT AS NOME_FANTASIA,
...  X0100_CLTCOMPEND AS COMPLEM_END,
...  TRIM(X0100_CLTDATEND),
...  TRIM(X0100_CLTTELDATCT),
...  TRIM(X0100_CLTCEL2) AS CELULAR    
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB19 
...  ON X0100_CLTCOD = X1900_CLTCOD 
...  WHERE LENGTH(RTRIM(X0100_CLTCGC))=12 
...  AND LENGTH(RTRIM(X0100_CLTCGCDIG))=2 
...  AND LENGTH(RTRIM(X0100_CLTCEP))=8 
...  AND X0100_CLTTIP = ${PJ} 
...  AND X1900_AECSUBCNAE <> 0 
...  AND X0100_CLTLOGEND IS NOT NULL 
...  AND X0100_CLTLOG IS NOT NULL
...  AND X0100_CLTCOMPEND IS NOT NULL     
...  AND LENGTH(RTRIM(X0100_CLTCOD))=6 
...  AND LENGTH(RTRIM(X1900_MUCCODDNE))=4 
...  AND LENGTH(RTRIM(X1900_JURVALCAP))=8 
...  AND X0100_CLTDATEND <> 0  #DATA DE FIXACAO NO ENDERECO
...  AND X0100_CLTTELDATCT <> 0  #DATA ATUALIZACAO DO TELEFONE DE CONTAT
...  AND X0100_CLTCEL2 <> 0   #NOVO NUMERO DO TELEFONE CELULAR  
...  FETCH FIRST ${LINHAS} ROWS ONLY


${SQL_CODIGO_DO_CLIENTE_PF}
...  SELECT LPAD(X0100_CLTCOD,8,'0') 
...		FROM AOXB00.AOXB01
...     INNER JOIN AOXB00.AOXB25 ON (X2500_CLTCOD = X0100_CLTCOD) 
...     INNER JOIN AOXB00.AOXB41 ON (X4100_CLTCOD = X0100_CLTCOD)
...		WHERE X0100_CLTTIP = 1
...		AND X0100_CLTSTAEXC = 0 
...		AND X0100_CLTSTATRC = 0 
...		ORDER BY X0100_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_CODIGO_DO_CLIENTE_IMPRESSAO_CADASTRAL}
...  SELECT 
...    X0100_CLTCOD 
...		FROM AOXB00.AOXB01
...     INNER JOIN AOXB00.AOXB25 ON (X2500_CLTCOD = X0100_CLTCOD) 
...     INNER JOIN AOXB00.AOXB18 ON (X1800_CLTCOD = X0100_CLTCOD)
...     INNER JOIN AODB00.AODB01 ON (D0100_DEPEMP = SUBSTR(LPAD(X0100_CLTCODDEP,6,'0'),1,2) 
...     AND D0100_DEPSEQ = SUBSTR(LPAD(X0100_CLTCODDEP,6,'0'),3,4))
...		WHERE X0100_CLTTIP = 1
...		AND X0100_CLTSTAEXC = 0 
...		AND X0100_CLTSTATRC = 0
...     AND X0100_CLTCODAUT > 0 
...		ORDER BY X0100_HDRDATA DESC FETCH FIRST 1 ROW ONLY    

${SQL_CODIGO_DO_CLIENTE_PJ}
...   SELECT LPAD(X0100_CLTCOD,8,'0')
...		FROM AOXB00.AOXB01 
...		WHERE X0100_CLTTIP = 2
...		AND X0100_CLTSTAEXC = 0
...		AND X0100_CLTSTATRC = 0 
...		ORDER BY X0100_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_CONSULTAR_CPF_VALIDO_DADOS_COMPLETO}  
...  SELECT 
...  X0100_CLTCGC AS CPF_CNPJ,
...  X0100_CLTCGCDIG AS DIGITO_CPF_CNPJ,
...  TRIM(X0100_CLTNOM) AS NOME,
...  X0100_CLTCOD AS CODIGO,
...  X0100_CLTDGV AS DIG_CODIGO,
...  X0100_CLTCID AS CIDADE,
...  X0100_CLTLOGEND AS ENDERECO,
...  X0100_CNRDATCONS,
...  X0100_CLTCEP AS CEP,
...  X0100_CLTCODDEP AS AGENCIA,
...  X1700_MUCCODDNE AS COGIDO_MUNICIPIO,
...  TRIM(X3300_TVNCOD) AS TIPO_VINCULACAO,
...  TRIM(X3300_VINCODCLI) AS CODIGO_VINCULACAO_CLIENTE       
...  FROM AOXB00.AOXB01
...  INNER JOIN AOXB00.AOXB17
...  ON X0100_CLTCOD = X1700_CLTCOD
...  INNER JOIN AOXB00.AOXB33
...  ON X1700_CLTCOD = X3300_CLTCOD    
...  WHERE LENGTH(RTRIM(X0100_CLTCGC))=9
...  AND LENGTH(RTRIM(X0100_CLTCOD))=5
...  AND LENGTH(RTRIM(X0100_CLTCODDEP))=5
...  AND LENGTH(RTRIM(X3300_VINCODCLI))=6   
...  AND X0100_CLTTIP = ${PF}  
...  FETCH FIRST ${LINHAS} ROWS ONLY

${SQL_RECUPERAR_DADOS_INCLUSAO_RELACIONAMENTO}
...    SELECT XB300_GCVCOD AS GRUPO, LPAD(X0100_CLTCOD,8,'0') AS CLIENTE, X0100_CLTNOM AS NOME 
...    FROM AOXB00.AOXB01, AOXB00.AOXBB3 
...    WHERE XB300_GRESIT = 1 AND X0100_CLTCOD > 1000 FETCH FIRST 1 ROWS ONLY

${SQL_RECUPERAR_ACESSO}
...    SELECT 
...    X0100_CLTCGC || X0100_CLTCGCDIG AS CNPJ
...    FROM AOXB00.AOXB01
...    WHERE X0100_CLTTIP > 1 FETCH FIRST ROW ONLY

#TODO validacoes devem ser realizadas dentro das keywords, não há necessidade de querys para validação
#CORRIGIDO
${SQL_CONSULTA_GRUPO_CONVENIO_CLIENTES}
...    SELECT    LPAD(XB300_CLTCOD,8,'0')||' '||SUBSTR(X0100_CLTDGV,1,1) AS CODIGO,    
...    SUBSTR(XB200_GCVNOM,1,11) AS NOME_GRUPO,    X0100_CLTNOM AS NOME,    
...    SUBSTR(XB300_CGCDATATZ,7,2)||'/'||SUBSTR(XB300_CGCDATATZ,5,2)||'/'||SUBSTR(XB300_CGCDATATZ,1,4) AS DT_INCLUSAO,    
...    CASE XB300_CGCTIPREL WHEN 1 THEN 'COMPONENTE' WHEN 2 THEN 'CONTROLADOR' END AS VINCULACAO
...    FROM    AOXB00.AOXB01,    AOXB00.AOXBB3,    AOXB00.AOXBB2 
...    WHERE    XB200_GCVCOD = XB300_GCVCOD    AND    XB300_CLTCOD = X0100_CLTCOD    AND    XB300_GCVCOD = '{0}'    
...    AND XB300_GRESIT = 1 ORDER BY SUBSTR (LPAD(XB300_CLTCOD,8,'0'),7,1)
#TODO validacoes devem ser realizadas dentro das keywords, não há necessidade de querys para validação
#CORRIGIDO
${SQL_CONSULTA_COMPONENTES_CONTROLADORES}
...    SELECT DISTINCT  TRIM(XB300_CLTCOD) AS CODIGO, X0100_CLTNOM AS NOME,    LPAD(X0100_CLTCGC,    12,    '0')|| '-' || LPAD(X0100_CLTCGCDIG,    2,    '0') AS CPF,
...    CASE XB300_CGCTIPREL WHEN 1 THEN 'Componente'    WHEN 2 THEN 'Controlador'END AS VINCULACAO FROM AOXB00.AOXB01,AOXB00.AOXBB3,AOXB00.AOXBB2
...    WHERE XB300_CLTCOD = X0100_CLTCOD AND XB200_GCVCOD = XB300_GCVCOD AND XB300_GCVCOD = 'C00002'

${SQL_CONSULTA_PESSOA_JURIDICA_SIMPLIFICADA}
...    SELECT LPAD(X0100_CLTCGC, 12, '0')|| LPAD(X0100_CLTCGCDIG, 2, '0') AS CNPJ, LPAD(X0100_CLTCOD, 7, '0') AS CODIGO
...    FROM AOXB00.AOXB01 WHERE X0100_CLTTIP > 1 FETCH FIRST ROW ONLY

#TODO não há necessidade de 3 querys iguais. Bastava uma com o nome 'SQL_RECUPERAR_PESSOA_JURIDICA_SIMPLIFICADA OK' 
#CORRIGIDO
${SQL_RECUPERAR_PESSOA_JURIDICA_SIMPLIFICADA}
...     SELECT
...	    LPAD(X0100_CLTCGC,
...	    12,
...	    '0') AS CNPJ,
...    	X0100_CLTNOM AS NOME,
...	    SUBSTR(X0100_CLTDATNAS,
...	    7,
...    	2)|| SUBSTR(X0100_CLTDATNAS,
...	    5,
...	    2)|| SUBSTR(X0100_CLTDATNAS,
...	    1,
...	    4) AS DT_NASCIMENTO,
...	    LPAD(X0100_CLTCOD,
...	    7,
...	    '0') AS CODIGO
...    FROM
...	    AOXB00.AOXB01
...    WHERE
...      X0100_CLTTIP > 1
...	    AND X0100_CLTCOD = {0} FETCH FIRST ROW ONLY    

${SQL_ACESSAR_ALTERACAO}     
...    SELECT LPAD(X0100_CLTCOD,8,'0')||X0100_CLTDGV AS CODIGO_CLIENTE 
...    FROM AOXB00.AOXB30,AOXB00.AOXB01 
...    WHERE X3000_CLTCOD = X0100_CLTCOD FETCH FIRST ROW ONLY
    

${SQL_RECUPERAR_DADOS_PJ}
...	 SELECT
...	     LPAD(X0100_CLTCGC,
...	     12,
...	     '0')|| LPAD(X0100_CLTCGCDIG,
...	     2,
...	     '0') AS CNPJ,
...	 X0100_CLTCOD AS CODIGO
...	 FROM
...	     AOXB00.AOXB01
...	 WHERE
...	     X0100_CLTTIP > 1 FETCH FIRST ROW ONLY

${SQL_CONSULTAR_PESSOA_JURIDICA_COMPLETA}
...     SELECT
... 	SUBSTR(LPAD(X0100_CLTCGC,
... 	12,
... 	'0'),
... 	1,
... 	2) || '.' || SUBSTR(LPAD(X0100_CLTCGC,
... 	12,
... 	'0'),
... 	3,
... 	3) || '.' || SUBSTR(LPAD(X0100_CLTCGC,
... 	12,
... 	'0'),
... 	6,
... 	3) || '/' || SUBSTR(LPAD(X0100_CLTCGC,
... 	12,
... 	'0'),
... 	9,
... 	4) || '-' || LPAD(X0100_CLTCGCDIG,
... 	2,
... 	'0') AS CNPJ,
... 	X0100_CLTCID AS CIDADE,
... 	X0100_CLTNOM AS NOME,
... 	LPAD(X0100_CLTCOD,
... 	7,
... 	'0') AS CODIGO
...     WHERE
... 	X0100_CLTTIP > 1
... 	AND X0100_CLTCOD = {0} FETCH FIRST ROW ONLY
...     FROM
... 	AOXB00.AOXB01

${SQL_CONSULTAR_RFB_PESSOA_JURIDICA_COMPLETA}
... 	SELECT
... 		LPAD(X0100_CLTCGC,
... 		12,
... 		'0')|| '  ' || LPAD(X0100_CLTCGCDIG,
... 		2,
... 		'0') AS CNPJ,
... 		X0100_CLTCID AS CIDADE,
... 		LPAD(X0100_CLTCOD,
... 		7,
... 		'0') AS CODIGO
... 	FROM
... 		AOXB00.AOXB01
... 	WHERE
... 		X0100_CLTTIP > 1
... 		AND X0100_CLTCOD = {0} FETCH FIRST ROW ONLY

${SQL_VALIDAR_REGISTRO_PESSOA_JURIDICA}
...	 SELECT
...	     X0100_CLTCGC
...    ||'  '|| X0100_CLTCGCDIG
...	     AS CNPJ,
...	     X0100_CLTNOM AS NOME,
...	     SUBSTR(X0100_CLTDATNAS,7,2)||SUBSTR(X0100_CLTDATNAS,5,2)||SUBSTR(X0100_CLTDATNAS,1,4) AS DT_NASCIMENTO,
...	     X0100_CLTCOD AS CODIGO    
...	 FROM
...	     AOXB00.AOXB01
...	 WHERE
...	     X0100_CLTTIP > 1
...	     AND
...	     X0100_CLTCOD ={0} 
...	     FETCH FIRST ROW ONLY

${SQL_RECUPERAR_ACESSO_CONSULTA}
...    SELECT 
...    TRIM(X0100_CLTCGC) || 
...    TRIM(X0100_CLTCGCDIG) AS CNPJ, 
...    TRIM(X0100_CLTCOD) AS CODIGO_CLIENTE
...    FROM AOXB00.AOXB01 WHERE X0100_CLTTIP > 1 FETCH FIRST ROW ONLY
 

${SQL_CONSULTA_RFB}
...	 SELECT
...	 	LPAD(X0100_CLTCGC,
...	 	12,
...	 	'0')|| ' ' || LPAD(X0100_CLTCGCDIG,
...	 	2,
...	 	'0') AS CNPJ,
...	 	X0100_CLTNOM AS NOME,
...	 	SUBSTR(X0100_CLTDATNAS,
...	 	7,
...	 	2)|| '/' || SUBSTR(X0100_CLTDATNAS,
...	 	5,
...	 	2)|| '/' || SUBSTR(X0100_CLTDATNAS,
...	 	1,
...	 	4) AS DT_NASCIMENTO,
...	 	TRIM(X0100_CLTCOD) AS CODIGO
...	 FROM
...	 	AOXB00.AOXB01
...	 WHERE
...	 	X0100_CLTTIP > 1
...	 	AND X0100_CLTCOD = {0} FETCH FIRST ROW ONLY

#TODO provavelmente esta query nao é necessaria. Validacoes devem ser feitas no nivel de keywords
#CORRIGIDO
${SQL_INCLUSAO_PODERES}
...	 SELECT
...  LPAD(X0100_CLTCGC,
...    12,
...    '0')
...	 	|| '' || LPAD(X0100_CLTCGCDIG,
...	 	2,
...	 	'0') AS CNPJ,
...	 	X0100_CLTNOM AS NOME,
...	 	SUBSTR(X0100_CLTDATNAS,
...	 	7,
...	 	2)|| SUBSTR(X0100_CLTDATNAS,
...	 	5,
...	 	2)|| SUBSTR(X0100_CLTDATNAS,
...	 	1,
...	 	4) AS DT_NASCIMENTO,
...	 	X0100_CLTCOD AS CODIGO
...	 FROM
...	 	AOXB00.AOXB01
...	 WHERE
...	 	X0100_CLTTIP > 1
...	 	AND X0100_CLTCOD = {0} FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADO}
...    SELECT 
...    X0100_CLTCGC || 
...    X0100_CLTCGCDIG AS CNPJ, 
...    X0100_CLTCOD || X0100_CLTDGV AS CODIGO_CLIENTE
...    FROM AOXB00.AOXB01 
...    WHERE X0100_CLTTIP > 1 FETCH FIRST ROW ONLY


${SQL_RECUPERAR_ACESSO_CONSULTAR_PODERES}
...    SELECT LPAD(X0100_CLTCOD,8,'0')||X0100_CLTDGV AS CODIGO_CLIENTE 
...    FROM AOXB00.AOXB30,AOXB00.AOXB01 
...    WHERE X3000_CLTCOD = X0100_CLTCOD FETCH FIRST ROW ONLY

${SQL_DADO_CONSULTA_PODERES}
...    SELECT X0100_CLTNOM AS NOME, LPAD(X0100_CLTCGC,12,'0')|| '-' || LPAD(X0100_CLTCGCDIG, 2, '0') AS CNPJ
...    FROM AOXB00.AOXB01 
...    WHERE LPAD(X0100_CLTCOD, 8, '0')|| X0100_CLTDGV = {0}

#TODO provavelmente esta query nao é necessaria. Validacoes devem ser feitas no nivel de keywords
#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot
#CORRIGIDO
${SQL_VALIDAR_DADOS_SIMPL_PJ}
...    SELECT
...         TRIM(X0100_CLTNOM) AS NOME,
...    	    X0100_CLTCGC || '-' || X0100_CLTCGCDIG AS CNPJ
...    FROM
...        	AOXB00.AOXB01
...    WHERE
...        X0100_CLTCOD || X0100_CLTDGV = {0}

${SQL_VALIDAR_DADOS_PJ}
...    SELECT
...    	    X0100_CLTCGC || '-' || X0100_CLTCGCDIG AS CNPJ
...    FROM
...        	AOXB00.AOXB01
...    WHERE
...        X0100_CLTCOD || X0100_CLTDGV = {0}

${SQL_RECUPERAR_DADOS_BASICOS_PJ}
...    SELECT    
...        X0100_CLTCOD || X0100_CLTDGV AS CLIENTE 
...    FROM    
...        AOXB00.AOXB01 
...    WHERE
...        X0100_CLTTIP = 2 FETCH FIRST ROW ONLY
    
#TODO provavelmente esta query nao é necessaria. Validacoes devem ser feitas no nivel de keywords
#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot
#CORRIGIDO
${SQL_DADOS_BASICOS_PJ}
...    SELECT    
...         TRIM(X0100_CLTNOM) AS NOME,    
...    	    TRIM(X0100_CLTCGC) AS PRIMEIROS_DIGITOS, 
...    	    TRIM(X0100_CLTCGCDIG) AS DIGITO_CNPJ 
...     FROM   
...         AOXB00.AOXB01 
...     WHERE 
...          X0100_CLTCOD|| X0100_CLTDGV = {0}

${SQL_RECUPERAR_ACESSO_CONSULTAR_RFB_PODERES}
...    SELECT LPAD(X0100_CLTCOD,8,'0')||X0100_CLTDGV AS CODIGO_CLIENTE 
...    FROM AOXB00.AOXB30,AOXB00.AOXB01 
...    WHERE X3000_CLTCOD = X0100_CLTCOD FETCH FIRST ROW ONLY
 
${SQL_RECUPERAR_DADOS_REGISTRO_PJ}
...    SELECT    LPAD(X0100_CLTCOD,    8,    '0')|| X0100_CLTDGV AS CLIENTE 
...    FROM    AOXB00.AOXB01 
...    WHERE    X0100_CLTTIP = 2 FETCH FIRST ROW ONLY

#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot
#CORRIGIDO
${SQL_VALIDAR_DADOS_PARTICIPACAO_EMPRESA}
...	 SELECT
...        TRIM(X0100_CLTCGC) AS CNPJ,
...        TRIM(X0100_CLTCGCDIG) AS DIG
...    FROM
...        AOXB00.AOXB01
...    WHERE
...        X0100_CLTCOD || X0100_CLTDGV = {0}

# ${SQL_RECUPERAR_DADO_PARTICIPACAO_EM_OUTRAS_EMPRESAS}
# ...    SELECT
# ...        LPAD(X0100_CLTCOD,
# ...        8,
# ...       '0')|| X0100_CLTDGV AS CLIENTE
# ...    FROM
# ...        AOXB00.AOXB01
# ...    WHERE
# ...        X0100_CLTTIP = 2 FETCH FIRST ROW ONLY

# ${SQL_VALIDAR_DADOS_REGISTRO_PJ}
# ...    SELECT    X0100_CLTCGC ||'-'|| X0100_CLTCGCDIG AS CNPJ 
# ...    FROM     AOXB00.AOXB01
# ...    WHERE     X0100_CLTCOD || X0100_CLTDGV = {0}
    
#TODO provavelmente esta query nao é necessaria. Validacoes devem ser feitas no nivel de keywords
#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot

# ${SQL_VALIDAR_DADOS_ECONOMICOS_PJ}
# ...   SELECT    SUBSTR(X0100_CLTCGC,1,11)||'-'|| X0100_CLTCGCDIG AS CNPJ 
# ...   FROM     AOXB00.AOXB01
# ...   WHERE     LPAD(X0100_CLTCOD,    8,    '0')|| X0100_CLTDGV = {0}
#CORRIGIDO

#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot
# ${SQL_RECUPERAR_VALIDAR_DADOS_PODERES_CLIENTE}
# ...    SELECT    X0100_CLTNOM AS NOME 
# ...   FROM  AOXB00.AOXB01
# ...   WHERE    LPAD(X0100_CLTCOD,    8,    '0')|| X0100_CLTDGV = {0}

${SQL_RECUPERAR_DADOS_INCLUIR_RENDA_TEMPORARIA}     
...		SELECT
...			TRIM(X0300_CLTCOD) AS CODIGO,
...			TRIM(X0100_CLTDGV) AS DIG,
...			TRIM(X0300_ORGCODIGO) AS ORGAO,
...			TRIM(A.SEDIDR)
...		FROM
...			AOXB00.AOXB03 A,
...			AOXB00.AOXB01,
...			DCCB00.DCCB11
...		WHERE
...			X0300_CLTCOD = X0100_CLTCOD
...			AND C1100_ORGCODIGO = X0300_ORGCODIGO
...		ORDER BY
...			X0300_ORGCODIGO DESC FETCH FIRST ROW ONLY
 

${SQL_EXCLUIR_RENDA_TEMPORARIA}   
...  DELETE 
...    FROM AOXB00.AOXB03 
...    WHERE SEDIDR = '{0}'

#TODO provavelmente esta query nao é necessaria. Validacoes devem ser feitas no nivel de keywords
#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot
#CORRIGIDO
${SQL_QUALIDADE_CADASTRO}
...		SELECT
...			TRIM(X0100_CLTCGC) AS CPF,
...			TRIM(X0100_CLTCGCDIG) AS DIG_CPF,
...			TRIM(X0100_CLTCOD) AS COD_CLIENTE,
...			TRIM(X0100_CLTDGV) AS DIG_CLIENTE
...		FROM
...			AOXB00.AOXB01
...		WHERE
...			X0100_CLTCOD || X0100_CLTDGV = {0}

#TODO provavelmente esta query nao é necessaria. Validacoes devem ser feitas no nivel de keywords
#FIXME remover concatenacao com digito verificador
#FIXME remover LPADs e SUBSTR. Fazer tratamento de mascaramento do CNPJ no PYTHON no robot
#CORRIGIDO (NAO FOI POSSIVEL FAZER O TRATAMENTO, POIS A CONCATENAÇÃO ACONTECE NO WHERE E NAO NO SELECT)
${SQL_RECUPERAR_VALIDAR_INCLUSAO_RENDA_TEMPORARIA}
...	 SELECT
...	 	X0300_CLTCOD AS CLIENTE,
...	 	X0300_ASSVAL AS SALARIO,
...	 	X0300_CLTNUMUSU AS USUARIO
...	 FROM
...	 	AOXB00.AOXB03 A,
...	 	AOXB00.AOXB01
...	 WHERE
...	 	X0100_CLTCOD = X0300_CLTCOD
...	 	AND X0100_CLTCOD || X0100_CLTDGV = {0}
...	 ORDER BY
...	 	A.SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_ALTERACAO_RENDA_TEMPORARIA}
...	 SELECT
...	 	TRIM(X0300_CLTCOD) || X0100_CLTDGV AS CODIGO,
...	 	TRIM(X0300_ORGCODIGO) AS ORGAO,
...	 	TRIM(A.SEDIDR) AS ID
...	 FROM
...	 	AOXB00.AOXB03 A,
...	 	AOXB00.AOXB01,
...	 	DCCB00.DCCB11
...	 WHERE
...	 	X0300_CLTCOD = X0100_CLTCOD
...	 	AND C1100_ORGCODIGO = X0300_ORGCODIGO
...	 ORDER BY
...	 	X0300_ORGCODIGO DESC FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_VALIDACAP_ALTERACAO_RENDA_TEMPORARIA} 
...  SELECT
...  	X0300_CLTCOD AS CLIENTE,
...  	TRIM(X0300_ASSVAL) AS SALARIO,
...  	X0300_CLTNUMUSU AS USUARIO
...  FROM
...  	AOXB00.AOXB03 A,
...  	AOXB00.AOXB01
...  WHERE
...  	X0100_CLTCOD = X0300_CLTCOD
...  	AND X0100_CLTCOD = {0}
...  ORDER BY
...  	A.SEDIDR DESC FETCH FIRST ROW ONLY

#FIXME essa query esta igual a SQL_RECUPERAR_DADOS_EXCLUSAO_RENDA_TEMPORARIA . Manter apenas uma
#CORRIGIDO
# ${SQL_RECUPERAR_DADOS_CONSULTA_RENDA_TEMPORARIA}
# ...    SELECT LPAD(X0300_CLTCOD || X0100_CLTDGV,9,'0') AS CODIGO,LPAD(X0300_ORGCODIGO,5,'0') AS ORGAO,TRIM(A.SEDIDR) 
# ...    FROM AOXB00.AOXB03 A,AOXB00.AOXB01,DCCB00.DCCB11
# ...   WHERE X0300_CLTCOD = X0100_CLTCOD AND C1100_ORGCODIGO = X0300_ORGCODIGO ORDER BY X0300_ORGCODIGO DESC FETCH FIRST ROW ONLY
    
${SQL_RECUPERAR_DADOS_VALIDACAO_CONSULTA_RENDA_TEMPORARIA}
...	 SELECT
...	 	X0300_CLTCOD || ' ' || X0100_CLTDGV AS CLIENTE
...	 FROM
...	 	AOXB00.AOXB03 A,
...	 	AOXB00.AOXB01
...	 WHERE
...	 	X0100_CLTCOD = X0300_CLTCOD
...	 	AND X0100_CLTCOD = {0}
...	 ORDER BY
...	 	A.SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_EXCLUSAO_RENDA_TEMPORARIA}
...	 SELECT
...	 	X0300_CLTCOD || X0100_CLTDGV AS CODIGO,
...	 	TRIM(X0300_ORGCODIGO) AS ORGAO,
...	 	TRIM(A.SEDIDR)
...	 FROM
...	 	AOXB00.AOXB03 A,
...	 	AOXB00.AOXB01,
...	 	DCCB00.DCCB11
...	 WHERE
...	 	X0300_CLTCOD = X0100_CLTCOD
...	 	AND C1100_ORGCODIGO = X0300_ORGCODIGO
...	 ORDER BY
...	 	X0300_ORGCODIGO DESC FETCH FIRST ROW ONLY
 
${SQL_RECUPERAR_RENDA_TEMPORARIA}
...	 SELECT
...	 	X0300_CLTCOD AS CLIENTE,
...	 	X0300_ASSVAL AS SALARIO,
...	 	X0300_CLTNUMUSU AS USUARIO
...	 FROM
...	 	AOXB00.AOXB03 A,
...	 	AOXB00.AOXB01
...	 WHERE
...	 	X0100_CLTCOD = X0300_CLTCOD
...	 	AND X0100_CLTCOD = {0}
...	 ORDER BY
...	 	A.SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_RECUPERA_QUALIDADE_CADASTRO} 
...		SELECT
...			TRIM(X0100_CLTCOD) AS CODIGO,
...			TRIM(X0100_CLTDGV) AS DIG
...		FROM
...			AOXB00.AOXB01,
...			AOXB00.AOXBA6
...		WHERE
...			X0100_CLTCOD != XA600_CLTCOD
...			AND X0100_CLTNOM != '' FETCH FIRST ROW ONLY

${SQL_CONSULTA_SIMPLIFICADO}
...	 SELECT
...	 	TRIM(X0100_CLTCODDEP),
...	 	TRIM(X0100_CLTNOM),
...	 	TRIM(X0100_CLTCGC),
...     TRIM(X0100_CLTCGCDIG),
...	 	TRIM(X0100_CLTDATNAS)
...	 FROM
...	 	AOXB00.AOXB01
...	 WHERE
...	 	X0100_CLTCOD = {0}

${SQL_CONSULTA_ENDERECO}
...	 SELECT
...     TRIM(X0100_CLTCODDEP),
...  	TRIM(X0100_CLTNOM),
...   	X0100_CLTCGC || '-' || X0100_CLTCGCDIG,
...   	TRIM(X0100_CLTCOD)
...  FROM
...  	AOXB00.AOXB01
...  WHERE
...	     X0100_CLTCOD = {0}

${SQL_RECUPERAR_DADOS_ENTRADA_ALTERACAO_PF_COMPLETA}
...    SELECT
...    	X0100_CLTCOD || X0100_CLTDGV AS CODIGO
...    FROM
...    	AOXB00.AOXB01
...    WHERE
...    	X0100_CLTCODDEP = 12645

${SQL_RECUPERAR_DADOS_VALIDAR_ALTERACAO_PF_COMPLETA} 
...    SELECT
...    	X0100_CLTNOM AS NOME,
...    	X0100_CLTCOD || X0100_CLTDGV AS COD_CLIENTE,
...    	X0100_CLTCONSISB AS OCORRENCIA,
...    	X0100_CLTCGC || '-' || X0100_CLTCGCDIG AS CPF,
...    	X0100_CLTTIP AS TIPO,
...    	TRIM(X0100_CLTDATNAS) AS DT_NASCIMENTO,
...    	X0100_CLTIDTEMAN  AS EMANCIPADO,
...    	X0100_CLTINDFALEC AS FALECIDO,
...    	TRIM(X1300_BALVALPTLQ) AS VLR_PATRIMONIO,
...    	TRIM(X1300_BALDAT) AS DATA_PATRIMONIO
...    FROM
...    	AOXB00.AOXB01,
...    	AOXB00.AOXB13
...    WHERE
...    	X0100_CLTCOD || X0100_CLTDGV = '{0}'
...    AND X1300_CLTCOD = X0100_CLTCOD

${SQL_ATUALIZAR_ALTERACAO_DADOS_ECON_PJ}
...    SELECT
...        X0100_CLTCGC || '-' || X0100_CLTCGCDIG AS CNPJ
...    FROM
...        AOXB00.AOXB01
...    WHERE
...        X0100_CLTCOD || X0100_CLTDGV = {0}

${SQL_VERIFICAR_ATUALIZACAO_CLIENTE_CGC}    
...    SELECT X0100_CLTNOM FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_RECUPERAR_CODIGO_Z}    
...    SELECT X0100_CLTIDCMIL FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_CONSULTA_SETOR_PUBLICO}    
...    SELECT X0100_CLTTIP FROM AOXB00.AOXB01 WHERE X0100_CLTCOD = {0}

${SQL_CAMPOS_NAO_PREENCHIDOS}
...		SELECT
...			TRIM(X0100_CLTCOD),
...			TRIM(X0100_CLTDGV),
...			TRIM(X0100_CLTCGC),
...			TRIM(X0100_CLTNOM)
...		FROM
...			AOXB00.AOXB25
...		INNER JOIN AOXB00.AOXB01 ON
...			X2500_CLTCOD = X0100_CLTCOD
...		WHERE
...			X2500_OUTVAL = 0.00
...			AND X0100_CLTCGC IS NOT NULL
...			AND X0100_CLTNOM IS NOT NULL FETCH FIRST 1 ROWS ONLY

${SQL_RECUPERAR_CLIENTE_PF}    
...		SELECT
...			TRIM(X0100_CLTCOD),
...			TRIM(X0100_CLTNOM),
...			TRIM(X0100_CLTCGC),
...			TRIM(X0100_CLTCGCDIG),
...			TRIM(X0100_CLTDATNAS)
...		FROM
...			AOXB00.AOXB01 A
...		WHERE
...			X0100_CLTCOD = {0}

${SQL_RENDA_CLIENTE_PF}    
...		SELECT
...			LPAD(X0100_CLTCOD,
...			8,
...			'0'),
...			TRIM(X0100_CLTNOM),
...			LPAD(X0100_CLTCGC,
...			12,
...			'0'),
...			LPAD(X0100_CLTCGCDIG,
...			2,
...			'0'),
...			LPAD(X0100_CLTDATNAS,
...			8,
...			'0'),
...			LPAD(X2500_OUTDATVAL,
...			8,
...			'0'),
...			TRIM(X2500_OUTVAL)
...		FROM
...			AOXB00.AOXB01
...		INNER JOIN AOXB00.AOXB25 ON
...			(X2500_CLTCOD = X0100_CLTCOD)
...		WHERE
...			X0100_CLTCOD = {0}

${SQL_CLIENTE_PJ}    
...		SELECT
...			LPAD(X0100_CLTCOD,
...			8,
...			'0'),
...			TRIM(X0100_CLTNOM),
...			LPAD(X0100_CLTCGC,
...			12,
...			'0'),
...			LPAD(X0100_CLTCGCDIG,
...			2,
...			'0'),
...			LPAD(X0100_CLTDATNAS,
...			8,
...			'0')
...		FROM
...			AOXB00.AOXB01 A
...		WHERE
...			X0100_CLTCOD = {0}

${SQL_PF_SIMPLIFICADA}    
...    SELECT X0100_CLTNOM FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_DESFAZER_DADO_PF_SIMPLIFICADO}
...    DELETE FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_RECUPERAR_ALTERACAO_PF_SIMPLIFICADA}     
...    SELECT X1700_MUCCODDNE FROM AOXB00.AOXB17 WHERE X1700_CLTCOD = {0}

${SQL_RECUPERAR_DADOS_VALIDAR_PF}    
...    SELECT X0100_CLTNOM FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_DELETAR_DADO_PF_COMPLETO}         
...    DELETE FROM AOXB00.AOXB01 WHERE X0100_CLTCGC = {0}

${SQL_ATUALIZAR_PF_COMPLETA}    
...    UPDATE AOXB00.AOXB17 SET X1700_MUCCODDNE = '{0}' WHERE X1700_CLTCOD = {1};

${SQL_VALIDAR_ATIVIDADE_EMPRESARIAL}  
...		SELECT
...			LPAD(X0100_CLTCOD,
...			8,
...			'0') || '-' || LPAD(X0100_CLTDGV,
...			1,
...			'0'),
...			X0100_CLTNOM,
...			LPAD(X0100_CLTCGC,
...			12,
...			'0') || '-' || LPAD(X0100_CLTCGCDIG,
...			2,
...			'0'),
...			TRIM(XB123_REFNOM),
...			TRIM(XB123_REFEND)
...		FROM
...			AOXB00.AOXB01
...		INNER JOIN AOXB00.AOXBB1 ON
...			(XB100_CLTCOD = X0100_CLTCOD)
...		WHERE
...			X0100_CLTCOD = {0}



${SQL_ENTRADA_CONSULTA_REFERENCIADA}    
...		SELECT
...			LPAD(X0100_CLTCOD,
...			8,
...			'0') || '-' || LPAD(X0100_CLTDGV,
...			1,
...			'0'),
...			X0100_CLTNOM,
...			LPAD(X0100_CLTCGC,
...			12,
...			'0') || '-' || LPAD(X0100_CLTCGCDIG,
...			2,
...			'0'),
...			TRIM(XB123_REFNOM),
...			TRIM(XB123_REFEND)
...		FROM
...			AOXB00.AOXB01
...		INNER JOIN AOXB00.AOXBB1 ON
...			(XB100_CLTCOD = X0100_CLTCOD)
...		WHERE
...			X0100_CLTCOD = {0}

*** Keywords ***

#TODO Alterar o nome da keyword para 'Recuperar Cliente PF Valido'
#CORRIGIDO
Recuperar Cliente PF Valido
    ${result}  Query  ${SQL_CONSULTAR_CPF_VALIDO_DADOS_SIMPLIFICADOS}
    [Return]    ${result}

#TODO Alterar o nome da keyword para 'Recuperar Cliente PJ Valido Dados Simplificados'
#CORRIGIDO
Recuperar Cliente PJ Valido Dados Simplificados
    ${result}  Query  ${SQL_CONSULTAR_CNPJ_VALIDO_DADOS_SIMPLIFICADOS}
    [Return]    ${result}     

#TODO Alterar o nome da keyword para 'Recuperar Cliente PF Valido'
#CORRIGIDO
Recuperar Dado Cliente PF Valido
    ${result}  Query  ${SQL_CONSULTAR_CPF_VALIDO_DADOS_COMPLETOS}
    [Return]    ${result}    

#TODO Alterar o nome da keyword para 'Recuperar CNPJ Valido'
#CORRIGIDO
Recuperar CNPJ Valido
    ${result}  Query  ${SQL_CONSULTAR_CNPJ_VALIDO_DADOS_SIMPLIFICADOS}
    [Return]    ${result}    

#TODO Alterar o nome da keyword para 'Recuperar Cliente PJ Valido Dados Completos'
#CORRIGIDO
Recuperar Cliente PJ Valido Dados Completos
    ${result}  Query  ${SQL_CONSULTAR_CNPJ_VALIDO_DADOS_COMPLETOS}
    [Return]    ${result}  
    
Recuperar Dados Simplificados Cliente PJ Valido
    ${result}  Query  ${SQL_CONSULTAR_CNPJ_VALIDO_DADOS_SIMPLIFICADOS}
    [Return]    ${result}  

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Deletar Cliente Cgc Inserido Via Sistema
    [Arguments]       ${cgc}              
    ${query}    Format String    ${SQL_DELETAR_CGC_INSERIDO_VIA_SISTEMA}     ${cgc}    
    ${result}   Run Keyword And Return Status       Execute Sql String    ${query}  
    [Return]    ${result} 

#TODO perceba que esta keyword não se difere da keyword logo abaixo. 'Recuperar Nome Cliente PF'
#TODO remover esta query e utilizar a query abaixo na validacao
#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Verificar Atualizacao de Cliente CGC
    [Arguments]        ${cgc}   
    ${query}           Format String    ${SQL_VERIFICAR_ATUALIZACAO_CLIENTE_CGC}     ${cgc}
    ${result} =        Run Keyword And Return Status  Check If Exists In Database    ${query}                    
    [Return]           ${result}  

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Nome Cliente PF
    [Arguments]        ${cpf}   
    ${query}           Format String    ${SQL_VERIFICAR_ATUALIZACAO_CLIENTE_CGC}     ${cpf}
    ${result} =        Query            ${query}                 
    [Return]           ${result}[0][0] 

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO Alterar o nome para 'Recuperar Codigo Z de Cliente PF'
# CORRIGIDO
Recuperar Codigo Z de Cliente PF
    [Arguments]     ${cpf}        
    ${query}    Format String    ${SQL_RECUPERAR_CODIGO_Z}   ${cpf}        
    ${result}   Query            ${query}           
    [Return]    ${result} 
   
#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO alterar o nome para 'Alterar Cliente PF '
Alterar Cliente PF
    [Arguments]     ${cpf}      ${nome}    
    ${nomeHash} =    utilsMask.Remove Ultimos Caracteres   ${nome}  20  
    ${resultado} =  Run Keyword And Return Status       Execute Sql String  
    ...  UPDATE AOXB00.AOXB01 SET X0100_CLTNOM = '${nome}', X0100_CLTNOMHASH = '${nomeHash}' WHERE X0100_CLTCGC = ${cpf}; 
    [Return]    ${resultado}

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Codigo Setor Publico Cliente PJ Simplificado
    [Arguments]     ${cltcod}  
    ${query}        Format String    ${SQL_CONSULTA_SETOR_PUBLICO}    ${cltcod}    
    ${result}       Query    ${query}    
    [Return]        ${result}[0][0]

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Codigo Consulta Campos Nao Preenchidos    
    ${result}       Query    ${SQL_CAMPOS_NAO_PREENCHIDOS} 
    [Return]        ${result}   

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Codigo Do Cliente Pessoa Fisica
    ${result}       Query  ${SQL_CODIGO_DO_CLIENTE_PF}
    [Return]        ${result}

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Codigo Do Cliente Impressao Cadastral
    ${result}       Query  ${SQL_CODIGO_DO_CLIENTE_IMPRESSAO_CADASTRAL}
    [Return]        ${result}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO Alterar nome para 'Recuperar Cliente Pessoa Fisica'
#CORRIGIDO
Recuperar Dados Cliente Pessoa Fisica
    [Arguments]    ${codigo_cliente}
    ${query}       Format String    ${SQL_RECUPERAR_CLIENTE_PF}    ${codigo_cliente}         
    ${result}      Query    ${query}       
    [Return]       ${result}

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIR
Recuperar Renda Do Cliente Pessoa Fisica
    [Arguments]    ${codigo_cliente}
    ${QUERY}       Format String    ${SQL_RENDA_CLIENTE_PF}    ${codigo_cliente}    
    ${result}      Query            ${QUERY}        
    [Return]       ${result}
    
Recuperar Codigo do Cliente Pessoa Juridica
    @{result}      Query    ${SQL_CODIGO_DO_CLIENTE_PJ}
    [Return]       ${result}[0][0]
    
#TODO as querys devem ser declaradas no cabeçalho acima.   
#TODO alterar nome para 'Recuperar Cliente Pessoa Juridica'
#CORRIGIDO
Recuperar Cliente Pessoa Juridica
    [Arguments]    ${codigo_cliente}
    ${query}       Format String    ${SQL_CLIENTE_PJ}    ${codigo_cliente}    
    ${result}      Query            ${query}       
    [Return]       ${result}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO alterar nome para 'Recuperar Entrada Validar PF Simplificada'
#CORRIGIDO
Recuperar Entrada Validar PF Simplificada
    [Arguments]   ${cgc}
    ${QUERY}      Format String    ${SQL_PF_SIMPLIFICADA}    ${cgc}
    ${status}     Run Keyword And Return Status    query     ${QUERY}    
    [Return]      ${status}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO esta keyword esta com o nome 'Recuperar ...' porém ela faz uma operação de exclusão.
#TODO Modificar o nome da query
#CORRIGIDO
Desfazer Dados Desfazer PF Simplificada
    [Arguments]   ${cgc}
    ${QUERY}      Format String    ${SQL_DESFAZER_DADO_PF_SIMPLIFICADO}    ${cgc}    
    ${result}     Execute Sql String    ${QUERY}       

#TODO modificar o nome desta query para 'Recuperar Alteracao PF Simplificada'
Recuperar Dados Entrada Alteracao PF Simplificada
    [Arguments]   ${pf}    ${linhas}   
    ${result}     query      ${SQL_CONSULTAR_CPF_VALIDO_DADOS_SIMPLIFICADOS}
    [Return]      ${result}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO modificar nome para 'Recuperar Validar Alteracao PF Simplificada'
# CORRIGIDO
Recuperar Validar Alteracao PF Simplificada
    [Arguments]   ${codigo}
    ${query}      Format String    ${SQL_RECUPERAR_ALTERACAO_PF_SIMPLIFICADA}      ${codigo}
    ${status}     Run Keyword And Return Status    query    ${query}   
    [Return]      ${status}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO o nome da keyword remete a 'Recuperar ...' porém nada está sendo recuperado. É uma operação de atualização
#TODO renomear para atualizar Campo Falecido
#CORRIGIDO KW nao está sendo usada no projeto
Atualizar Campo Falecido
    [Arguments]   ${valor_antigo}                  ${codigo}
    ${result}     Execute Sql String               UPDATE AOXB00.AOXB01 SET @CampoAlterado = ${valor_antigo} WHERE LPAD(X0100_CLTCOD, 8, '0')|| X0100_CLTDGV = ${codigo}   

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO keyword em lugar indevido. Esta keyuword deveria estar em AOXB13.robot
# Query removida para AOXB13.robot

  
Recuperar Dados Entrada Consultar PF Simplificada
    [Arguments]   ${pf}    ${linhas}
    ${result}     query      ${SQL_CONSULTAR_CPF_VALIDO_DADOS_SIMPLIFICADOS}  
    [Return]      ${result}

Recuperar Dados Valida Consultar PF Simplificada
    [Arguments]   ${pf}    ${linhas}
    ${status}     Run Keyword And Return Status    query    ${SQL_CONSULTAR_CPF_VALIDO_DADOS_SIMPLIFICADOS}  
    [Return]      ${status}

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Dados Entrada Validar PF Completa
    [Arguments]   ${cgc}
    ${QUERY}      Format String    ${SQL_RECUPERAR_DADOS_VALIDAR_PF}    ${cgc}
    ${status}     Run Keyword And Return Status    query    ${QUERY}      
    [Return]      ${status}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO o nome da query é 'Recupear.... porém ela faz uma operacao de exclusão
#CORRIGIDO
Excluir Dados Desfazer PF Completa
    [Arguments]   ${cgc}
    ${QUERY}      Format String    ${SQL_DELETAR_DADO_PF_COMPLETO}         ${cgc}      
    ${result}     Execute Sql String    ${QUERY}       

#TODO as querys devem ser declaradas no cabeçalho acima.    
Recuperar Dados Validar Alteracao PF Completa
    [Arguments]   ${cltcod}
    ${dado}       Format String     ${SQL_RECUPERAR_DADOS_VALIDAR_ALTERACAO_PF_COMPLETA}    ${cltcod}     
    ${status}     Query   ${dado} 
    [Return]      ${status}

#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO a keyword é nomeada como 'Recuperar...' mas ela faz uma operacao de atualizacao.
#TODO nomear a keyword de acordo
#CORRIGIR
Atualizar Alteracao PF Completa
    [Arguments]   ${codMunOriginal}    ${cltcod}
    ${QUERY}      Format String    ${SQL_ATUALIZAR_PF_COMPLETA}    ${codMunOriginal}    ${cltcod};      
    ${status}     Run Keyword And Return Status    query    ${QUERY}          
    [Return]      ${status}
    
Recuperar Dados Entrada Consultar PF Completa
    [Arguments]   ${pf}        ${linhas}
    ${result}     query        ${SQL_CONSULTAR_CPF_VALIDO_DADOS_COMPLETO}
    [Return]      ${result}
  
Recuperar Dados Valida Consultar PF Completa
    [Arguments]   ${pf}        ${linhas}
    ${status}     Run Keyword And Return Status    query    ${SQL_CONSULTAR_CPF_VALIDO_DADOS_COMPLETO}
    [Return]      ${status}

#TODO as querys devem ser declaradas no cabeçalho acima.  
Recuperar Dados Entrada Alteracao PF Completa
    ${result}     query       ${SQL_RECUPERAR_DADOS_ENTRADA_ALTERACAO_PF_COMPLETA} 
    [Return]      ${result}
     
Recuperar Dados De Inclusao De Relacionamento
    ${result}  Query  ${SQL_RECUPERAR_DADOS_INCLUSAO_RELACIONAMENTO}
    [Return]  ${result}
    
Recuperar Dados Validacao Consulta Grupo Convenio Cliente
    [Arguments]  ${codGrupo}
    ${sql}  Format String  ${SQL_CONSULTA_GRUPO_CONVENIO_CLIENTES}  ${codGrupo}
    ${result}  Query  ${sql}
    [return]  ${result}
    
Recuperar Dados Consulta Componentes e Controlodadores
    ${result}  Query  ${SQL_CONSULTA_COMPONENTES_CONTROLADORES}
    [return]  ${result}    

Recuperar Dados Alterar Pessoa Juridica Simplificada   
    ${result}     query     ${SQL_CONSULTA_PESSOA_JURIDICA_SIMPLIFICADA}  
    [Return]      ${result} 
    
Consultar Dados Alterar Pessoa Juridica Simplificada
    [Arguments]    ${cnpj}
    ${query}    Format String     ${SQL_RECUPERAR_PESSOA_JURIDICA_SIMPLIFICADA}    ${cnpj}
    ${result}   query    ${query}    
    [Return]    ${result}  

Consultar RFB Pessoa Jurídica Simplificada
    [Arguments]    ${codigo}
    ${query}    Format String     ${SQL_RECUPERAR_PESSOA_JURIDICA_SIMPLIFICADA}    ${codigo}
    ${result}   query    ${query}    
    [Return]    ${result}         

Recuperar Acesso Inclusao Sistema Cliente
    ${result}    Query    ${SQL_RECUPERAR_ACESSO}    
    [Return]    ${result}

Recuperar Acesso Alteracao
    ${result}    Query    ${SQL_ACESSAR_ALTERACAO}    
    [Return]    ${result}

#TODO Renomear para 'Consultar Pessoa Juridica Completa'
#TODO validacoes nao podem ser feitas no modulo de banco de dados. Devem ser feitas no módulo de 'Teste....KW' (keywords)
#CORRIGIR
Consultar Pessoa Juridica Completa
    [Arguments]    ${codigo}
    ${query}    Format String     ${SQL_CONSULTAR_PESSOA_JURIDICA_COMPLETA}    ${codigo}
    ${result}   Query    ${query}    
    [Return]    ${result}     
    
#TODO validacoes nao podem ser feitas no modulo de banco de dados. Devem ser feitas no módulo de 'Teste....KW' (keywords)
#CORRIGIR
Consultar RFB Pessoa Juridica Completa
    [Arguments]    ${codigo}
    ${query}    Format String     ${SQL_CONSULTAR_RFB_PESSOA_JURIDICA_COMPLETA}    ${codigo}
    ${result}   query    ${query}    
    [Return]    ${result} 

Consultar Dado
    [Arguments]    ${codigo}
    ${query}    Format String    ${SQL_VALIDAR_REGISTRO_PESSOA_JURIDICA}    ${codigo}
    ${result}    Query    ${query}    
    [Return]    ${result}    
    
Recuperar Acesso Consulta Registros 
    ${result}    Query    ${SQL_ACESSAR_ALTERACAO}    
    [Return]    ${result}
    
Recuperar Acesso Consultar PJ
    ${result}    Query    ${SQL_RECUPERAR_DADOS_PJ}    
    [Return]    ${result}
    
Recuperar Acesso Consulta RFB Registros
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_CONSULTA}    
    [Return]    ${result}
  
Consultar RFB Registros
    [Arguments]    ${codigo}
    ${query}     Format String    ${SQL_CONSULTA_RFB}    ${codigo}
    ${result}    Query    ${query}
    [Return]     ${result}    
 
Recuperar Acesso Inclusao Informacoes Adicionais
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_CONSULTA}        
    [Return]    ${result}

Consultar Dado Na Base
    [Arguments]    ${codigo}
    ${query}     Format String    ${SQL_INCLUSAO_PODERES}    ${codigo}
    ${status}    Run Keyword And Return Status    Query    ${query}
    [Return]     ${status}

Recuperar Acesso Consultar Poderes
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_CONSULTA}        
    [Return]    ${result}

Recuperar Acesso Alteracao Informacoces Adicionais
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_CONSULTA}        
    [Return]    ${result}

#TODO as querys devem ser declaradas no cabeçalho acima.    
#TODO esta query deve estar no arquivo AOXBB1.robot
#TODO as duas querys abaixo estão repetidas
#TODO esta query deveria estar no arquivo AOXBB1.robot
  


#TODO as duas querys abaixo estão repetidas
#TODO as querys devem ser declaradas no cabeçalho acima.
#TODO não é necessário ter query com o nome validação. 
#TODO validações serão realizadas apenas no modulo de keywords
Recuperar Dados Consulta Simplificado PF
    [Arguments]    ${codigo_cliente}
    ${sql}      Format String    ${SQL_CONSULTA_SIMPLIFICADO}    ${codigo_cliente}       
    ${result}    query    ${sql}
    [Return]       ${result} 

#TODO as querys devem ser declaradas no cabeçalho acima.
Recuperar Dados Valida Consulta Endereco
    [Arguments]    ${codigo_cliente}
    ${sql}      Format String     ${SQL_CONSULTA_ENDERECO}    ${codigo_cliente}       
    ${result}    query    ${sql}
    [Return]       ${result} 
    
#TODO as querys devem ser declaradas no cabeçalho acima.

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Dados Valida Atividade Empresarial
    [Arguments]    ${codigo_cliente}
    ${QUERY}       Format String    ${SQL_VALIDAR_ATIVIDADE_EMPRESARIAL}   ${codigo_cliente}    
    ${status}      Run Keyword And Return Status    Execute Sql String    ${QUERY}     
    [Return]       ${status} 
    
#TODO as querys devem ser declaradas no cabeçalho acima.

#TODO as querys devem ser declaradas no cabeçalho acima.
#CORRIGIDO
Recuperar Dados Valida Entrada Consulta Referencia
    [Arguments]    ${codigo_cliente}
    ${QUERY}       Format String    ${SQL_ENTRADA_CONSULTA_REFERENCIADA}    ${codigo_cliente}     
    ${status}      Run Keyword And Return Status    Execute Sql String      ${QUERY}       
    [Return]       ${status} 

Recuperar Acesso Alteracao Informacoces PJ PF Via Web
    ${result}    Query    ${SQL_RECUPERAR_DADO}        
    [Return]    ${result}
    
Recuperar Acesso Consultar Informacoes Poderes 
    ${results}    Query    ${SQL_RECUPERAR_ACESSO_CONSULTAR_PODERES}
    [Return]    ${results}       

Consultar Dados Consulta Poderes
    [Arguments]    ${codigo}
    ${dado}    Format String    ${SQL_DADO_CONSULTA_PODERES}    ${codigo}
    ${result}    Query    ${dado}    
    [Return]    ${result}

Recuperar Dados Simplificados PJ
    [Arguments]  ${cliente}
    ${sql}  Format String  ${SQL_VALIDAR_DADOS_SIMPL_PJ}  ${cliente}
    ${result}  Query  ${sql}
    [return]  ${result}
    
Recuperar Avaliar Registro Pj 
    [Arguments]  ${cliente}
    ${sql}  Format String  ${SQL_VALIDAR_DADOS_PJ}  ${cliente}
    ${result}  Query  ${sql}
    [return]  ${result}
    
Recuperar Dados Basicos Pj
    ${result}  Query  ${SQL_RECUPERAR_DADOS_BASICOS_PJ}
    [return]  ${result}

Recuperar Validar Dados Basicos Pj
    [Arguments]  ${dadosValidacao}
    ${sql}  Format String  ${SQL_DADOS_BASICOS_PJ}  ${dadosValidacao}
    ${result}  Query  ${sql}
    [return]  ${result}


Recuperar Participacao Em Outras Empresas
    [Arguments]  ${dado}
    ${sql}  Format String  ${SQL_VALIDAR_DADOS_PARTICIPACAO_EMPRESA}   ${dado}
    ${result}  Query  ${sql}
    [Return]    ${result}

#TODO validacoes nao podem ser feitas no modulo de banco de dados. Devem ser feitas no módulo de 'Teste....KW' (keywords)
#CORRIGIDO
Recuperar Consulta Dados Registro PJ
    [Arguments]  ${cliente}
    ${sql}  Format String  ${SQL_VALIDAR_DADOS_SIMPL_PJ}  ${cliente}
    ${result}  Query  ${sql}
    [return]  ${result}

Recuperar Validar Dados Economico Financeiro PJ
    [Arguments]  ${cliente}
    ${sql}   Format String   ${SQL_ATUALIZAR_ALTERACAO_DADOS_ECON_PJ}  ${cliente}
    ${result}  Query  ${sql}
    [return]  ${result}
    
Recuperar Dados Consultar RFB Poderes
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_CONSULTAR_RFB_PODERES}
    [Return]    ${result}
    
Recuperar Dado Validar Poderes Cliente
    [Arguments]  ${cliente}
    ${sql}  Format String  ${SQL_ATUALIZAR_ALTERACAO_DADOS_ECON_PJ}  ${cliente} 
    ${result}  Query  ${sql}
    [return]  ${result}
    
Recuperar Dados Qualidade Cadastro
    ${results}    query    ${SQL_RECUPERA_QUALIDADE_CADASTRO}  
    [Return]    ${results} 
    
Recuperar Dados Qualidade Cadastro Para Validar   
    [Arguments]    ${codigo}    
    ${query}    Format String    ${SQL_QUALIDADE_CADASTRO}     ${codigo} 
    ${result}    Query    ${query}    
    [Return]     ${result}     

#TODO 'Recuperar Renda Temporaria'
#CORRIGIDO
Recuperar Dados Renda Temporaria
    ${result}   Query   ${SQL_RECUPERAR_DADOS_INCLUIR_RENDA_TEMPORARIA}
    [return]  ${result}
 
#TODO validacoes nao podem ser feitas no modulo de banco de dados. Devem ser feitas no módulo de 'Teste....KW' (keywords) 
#CORRIGIDO
Recuperar Consulta Dados Inclusao Renda Temporaria
    [Arguments]  ${codigo}
    ${sql}     Format String    ${SQL_RECUPERAR_VALIDAR_INCLUSAO_RENDA_TEMPORARIA}     ${codigo}
    ${status}  Run Keyword And Return Status    Check If Exists In Database  ${sql}  
    [return]  ${status}     

#TODO excluir para Incluir o que e aonde ?
#TODO provavelmente esta query deveria se chamar 'Excluir Renda Temporaria por ID'
#CORRIGIDO
Excluir Renda Temporaria por ID
    [Arguments]  ${id}
    ${sql}  Format String  ${SQL_EXCLUIR_RENDA_TEMPORARIA}  ${id}
    Execute Sql String    ${sql}         
 
#TODO validacoes nao podem ser feitas no modulo de banco de dados. Devem ser feitas no módulo de 'Teste....KW' (keywords)
#CORRIGIDO
Recuperar Dados Alteracao De Renda Temporaria
    ${result}  Query  ${SQL_RECUPERAR_DADOS_ALTERACAO_RENDA_TEMPORARIA}
    [return]  ${result}

#TODO renomear esta keyword para 'Recuperar Renda Temporaria'
#CORRIGIDO
Recuperar Renda Temporaria
    [Arguments]  ${cod}
    ${sql}  Format String  ${SQL_RECUPERAR_DADOS_VALIDACAP_ALTERACAO_RENDA_TEMPORARIA}  ${cod}
    ${result}  Query   ${sql}
    [return]      ${result}
    
Recuperar Dados Consulta Renda Temporaria
    ${result}  Query  ${SQL_RECUPERAR_DADOS_EXCLUSAO_RENDA_TEMPORARIA} 
    [return]  ${result}
   
#TODO validacoes nao podem ser feitas no modulo de banco de dados. Devem ser feitas no módulo de 'Teste....KW' (keywords)
#CORRIGIDO
Recuperar Consulta Renda Temporaria
    [Arguments]  ${codCliente}
    ${sql}  Format String  ${SQL_RECUPERAR_DADOS_VALIDACAO_CONSULTA_RENDA_TEMPORARIA}  ${codCliente}  
    ${result}  Query  ${sql}
    [return]  ${result}

#TODO  esta keyword deveria se chamar 'Recuperar Renda Temporaria'
#CORRIGIDO 
Recuperar Dado Renda Temporaria
    ${result}  Query  ${SQL_RECUPERAR_DADOS_EXCLUSAO_RENDA_TEMPORARIA}
    [return]   ${result}

#TODO esta keyword deveria se chamar 'Recuperar Renda Temporaria por Codigo do Cliente'
#CORRIGIDO
Recuperar Renda Temporaria por Codigo do Cliente
    ${status}   Query      ${SQL_RECUPERAR_DADOS_EXCLUSAO_RENDA_TEMPORARIA}   
    [return]  ${status} 

Recuperar Dados Cadastrais Web
    ${result}    query    ${SQL_RECUPERAR_DADOS_CADASTRAIS}
    [Return]     ${result}    
    
Validar Dados Cadastrais Web
    [Arguments]   ${cliente}
    ${dado}       Format String     ${SQL_VALIDAR_DADOS_CADASTRAIS_WEB}    ${cliente}
    ${status}     Run Keyword And Return Status    Check If Exists In Database       ${dado}
    [Return]      ${status}

Validar Dados Consulta Componentes e Controladores do Grupo Economico
   [Arguments]   ${grupo}
   ${dado}      Format String    ${SQL_VALIDAR_DADOS_CONSULTA_CONPONENTES_E_CONTROLES_DO_GRUPO}    ${grupo}
   ${status}         query    ${dado}   
   [Return]      ${status}
   
Valida Dados Consulta Historico Relacionamento
    [Arguments]   ${cliente}
    ${dado}      Format String    ${SQL_VALIDA_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO}    ${cliente} 
    ${status}      query    ${dado}
    [Return]      ${status}

Validar Dados Consulta Historico Relacionamento Grupo Economico Cliente
    [Arguments]   ${cliente}
    ${dado}      Format String     ${SQL_VALIDAR_DADOS_CONSULTA_HISTORICO_RELACIONAMENTO_GRUPO_ECONOMICO_CLIENTE}
    ${status}      query    ${dado}
    [Return]      ${status}

