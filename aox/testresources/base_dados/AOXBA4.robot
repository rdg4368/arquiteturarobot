*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py
Library      String          

*** Variable ***
${SQL_CONSULTA_DADOS_CLIENTE}
...    SELECT XA400_GECNOM AS NOME, LPAD(XA400_GECCODCONT,8,'0') AS ORIGEM FROM AOXB00.AOXBA4 
...    WHERE XA400_GECCODCONT = {0} AND XA400_HDRDATA = {1} AND XA400_GECNOM = '{2}'

${RECUPERAR_DADOS_EXCLUSAO_DESATIVAR_GRUPO}
...    SELECT * AOXB00.AOXBA4 WHERE XA400_GECCOD = {0}

${SQL_DELETA_DADOS_CLIENTE}
...   DELETE AOXB00.AOXBA4 WHERE XA400_GECCODCONT = {0}  AND XA400_HDRDATA =  {1}

${SQL_CONSULTA_MANUTENCAO_GEIC_MANUAL}    
...  SELECT * AOXB00.AOXBA4 WHERE XA400_GECCOD = {0}
 
${SQL_CONSULTA_GEICS_MANUAL}   
...  SELECT LPAD(XA400_GECCOD,6,'0'), TRIM(XA400_GECNOM ), 
...    LPAD(XA400_GECCODCONT,08,'0'), CASE XA400_GECSIT WHEN 1 THEN 'ATIVO' WHEN 2 THEN 'INATIVO' END
...  FROM AOXB00.AOXBA4 
...  FETCH FIRST 9 ROW ONLY

${SQL_CONSULTA_HISTORICO_GRUPO}
...   SELECT LPAD(XA400_GECCOD,6,'0') 
...   FROM AOXB00.AOXBA4 
...   INNER JOIN AOXB00.AOXBA5 ON (XA500_GECCOD = XA400_GECCOD)
...   WHERE XA400_GECSIT <> 2 AND XA400_GECCODGER <> 1 FETCH FIRST ROW ONLY 
    
${SQL_CONSULTA_HISTORICO}
...    SELECT LPAD(XA400_GECCOD, 6, '0'), TRIM(XA400_GECNOM), 
...    CASE XA400_GECSIT WHEN 1 THEN 'ATIVO'
...    WHEN 2 THEN 'INATIVO' END, LPAD(XA400_CLTNUMUSU,6,'0'), LPAD(XA500_CLTCOD,8,'0'), 
...    SUBSTR(LPAD(XA500_HDRHORA,6,'0'), 3, 2) || ':' || SUBSTR(LPAD(XA500_HDRHORA,6,'0'), 5, 2),
...    LPAD(XA500_CLTNUMUSU,6,'0') 
...    FROM AOXB00.AOXBA4 INNER JOIN AOXB00.AOXBA5 ON (XA400_GECCOD = XA500_GECCOD) INNER JOIN AOXB00.AOXB01  
...      ON (X0100_CLTCOD = XA500_CLTCOD)
...    WHERE XA400_GECCOD = {0}

${SQL_CONSULTA_CLIENTE_HISTORICO}
...    SELECT LPAD(XA500_CLTCOD,8,'0') 
...    FROM AOXB00.AOXBA4 
...    INNER JOIN AOXB00.AOXBA5 
...    ON (XA500_GECCOD = XA400_GECCOD)
...    WHERE XA400_GECSIT <> 2 AND XA400_GECCODGER <> 1 
...    FETCH FIRST ROW ONLY
   
${SQL_CONSULTA_HISTORICO_GRUPO_RELACIONAMENTO}
...    SELECT LPAD(XA400_GECCOD,6, '0'), TRIM(XA400_GECNOM), 
...    LPAD(XA500_CLTCOD, 6,'0'), TRIM(X0100_CLTNOM), 
...    LPAD(X0100_CLTCGC,12,'0') || '-' || LPAD(X0100_CLTCGCDIG,2,'0')
...    FROM AOXB00.AOXBA4 INNER JOIN AOXB00.AOXBA5 
...      ON (XA400_GECCOD = XA500_GECCOD) INNER JOIN AOXB00.AOXB01 ON(X0100_CLTCOD = XA500_CLTCOD)
...    WHERE XA400_GECCOD = {0}

${SQL_CONSULTA_HISTORICO_CLIENTE_RELACIONAMENTO}
...    SELECT LPAD(XA400_GECCOD, 6, '0'), TRIM(XA400_GECNOM), 
...    CASE XA400_GECSIT WHEN 1 THEN 'ATIVO' WHEN 2 THEN 'INATIVO'
...    END, LPAD(XA500_CLTCOD, 6,'0'), TRIM(X0100_CLTNOM), 
...    SUBSTR(XA500_HDRDATA, 7, 2) || '/' || SUBSTR(XA500_HDRDATA, 5, 2) || '/' || 
...    SUBSTR(XA500_HDRDATA, 1, 4), SUBSTR(LPAD(XA500_HDRHORA,6,'0'), 1, 2) || ':' || 
...    SUBSTR(LPAD(XA500_HDRHORA,6,'0'), 3, 2) || ':' || SUBSTR(LPAD(XA500_HDRHORA,6,'0'), 5, 2), LPAD(XA500_CLTNUMUSU,6,'0')
...    FROM AOXB00.AOXBA4 INNER JOIN AOXB00.AOXBA5 ON (XA400_GECCOD = XA500_GECCOD) INNER JOIN AOXB00.AOXB01  ON (X0100_CLTCOD = XA500_CLTCOD)
...    WHERE XA500_CLTCOD = {0}   
    
${SQL_RECUPERAR_ACESSO_MANUTENCAO}
...    SELECT LPAD(XA400_GECCOD,6,'0') FROM AOXB00.AOXBA4
...    INNER JOIN AOXB00.AOXBA5 ON (XA500_GECCOD = XA400_GECCOD)
...    WHERE XA400_GECSIT <> 2 AND XA400_GECCODGER <> 1 FETCH FIRST ROW ONLY

${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA}
...    SELECT LPAD(XA400_GECCOD, 6, '0'), TRIM(XA400_GECNOM), TRIM(XA700_GECDESJUS)
...    FROM AOXB00.AOXBA4 INNER JOIN AOXB00.AOXBA7 ON (XA400_GECCOD = XA700_GECCOD)
...    WHERE XA400_GECCOD = {0}

***Keywords***
Validar Dados Inclusao Manutencao GEIC Manual
    [Arguments]    ${codCliente}    ${data}    ${txt}
    ${query}    Format String      ${SQL_CONSULTA_DADOS_CLIENTE}    ${codCliente}    ${data}    ${txt}
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query}    
    [Return]    ${result}  
    
Desfazer Inclusao Manutencao GEIC Manual
    [Arguments]       ${codCliente}    ${data}    ${txt}          
    ${query}     Format String      ${SQL_DELETA_DADOS_CLIENTE}    ${codCliente}    ${data}
    ${result}    Execute Sql String    ${query}    
    [Return]    ${result}
 
Validar Exclusao Manutencao GEIC Manual
    [Arguments]    ${grupo}
    ${query}       Format String      ${SQL_CONSULTA_DADOS_CLIENTE}    ${grupo}
    ${result}      Run Keyword And Return Status    Check If Exists In Database    ${query}    
    [Return]       ${result}  

Validar GEICS Manuais 
    ${query}     query      ${SQL_CONSULTA_GEICS_MANUAL}     
    [Return]    ${query}

Recuperar Dados De Grupo
    ${result}   query    ${SQL_CONSULTA_HISTORICO_GRUPO}  
    [Return]    ${result} 
    
Validar Consulta Historico Grupo
    [Arguments]    ${grupo}
    ${query}    Format String     ${SQL_CONSULTA_HISTORICO}    ${grupo}
    ${result}   query    ${query}    
    [Return]    ${result}
    
Recuperar Dados Cliente Historico
    ${result}   Query    ${SQL_CONSULTA_CLIENTE_HISTORICO}  
    [Return]    ${result}     
    
Validar Consulta Historico Grupo Relacionamento
    [Arguments]      ${grupo}         
    ${query}     Format String      ${SQL_CONSULTA_HISTORICO_GRUPO_RELACIONAMENTO}    ${grupo}
    ${result}    Query    ${query}    
    [Return]    ${result}        

Validar Historico Grupo Relac Componentes
    [Arguments]      ${cliente}        
    ${query}     Format String      ${SQL_CONSULTA_HISTORICO_CLIENTE_RELACIONAMENTO}    ${cliente}
    ${result}    Query    ${query}    
    [Return]    ${result}     
         
Recuperar Acesso Manutencao
    ${result}    Query    ${SQL_RECUPERAR_ACESSO_MANUTENCAO}    
    [Return]    ${result}    

Validar Consulta 
    [Arguments]    ${grupo}
    ${query}     Format String    ${SQL_RECUPERAR_DADOS_VALIDAR_CONSULTA}    ${grupo}
    ${result}    Query    ${query}    
    [Return]     ${result}
         
Validar Dados Desativar Grupo
    [Arguments]    ${grupo}
    ${sql}         Format String    ${RECUPERAR_DADOS_EXCLUSAO_DESATIVAR_GRUPO}     ${grupo}
    ${status}      Run Keyword And Return Status     Check If Exists In Database    ${sql}
    [Return]       ${status}    