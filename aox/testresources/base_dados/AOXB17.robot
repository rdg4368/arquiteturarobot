*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py  
Library      String     

***Variables***
${SQL_DESFAZER_ALTERACAO_CLIENTE_PF}    
...		UPDATE
...			AOXB00.AOXB17
...		SET
...			X1700_MUCCODDNE = '{0}'
...		WHERE
...			X1700_CLTCOD = {1}

${SQL_RECUPERAR_CODIGO_MUNICIPIO}    
...		SELECT
...			X1700_MUCCODDNE
...		FROM
...			AOXB00.AOXB17
...		WHERE
...			X1700_CLTCOD = {0}


*** Keywords ***

#TODO AJUSTAR AOXB00.AOXB17
# Corrigido
Defazer Alteracoes Cliente PF Simplificado
    [Arguments]       ${cltcod}      ${codMunOriginal}
    ${query}          Format String    ${SQL_DESFAZER_ALTERACAO_CLIENTE_PF}    ${cltcod}      ${codMunOriginal}
    ${resultado} =    Run Keyword And Return Status      Execute Sql String    ${query}      
    [Return]          ${resultado}
    
#TODO AOXB17
# Corrigido
Recuperar Codigo Municipio Cliente PF
    [Arguments]     ${cltcod}
    ${query}        Format String    ${SQL_RECUPERAR_CODIGO_MUNICIPIO}    ${cltcod}    
    ${result}       Query            ${query}             
    [Return]        ${result}[0][0]