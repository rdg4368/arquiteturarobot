*** Settings ***
Library      DatabaseLibrary
Library      OperatingSystem 
Library      Collections     

*** Variables ***

${SQL_CONTEUDO_INFORMACOES_ANTERIORES_OLD} = 
...	   SELECT TRIM(X2100_NOMVALATR), TRIM(X2100_CLTNUMUSU),
...	   LPAD(X0100_CLTCOD,8,'0'), CAST(X0100_CLTDGV AS CHAR)
...	   FROM AOXB00.AOXB21 
...	   INNER JOIN AOXB00.AOXB01
...	   ON X0100_CLTCOD = X2100_CLTCOD
...	   WHERE X2100_CLTCOD IS NOT NULL
...	   AND X2100_NOMVALATR IS NOT NULL
...	   AND X0100_CLTDGV IS NOT NULL
...	   AND X2100_CLTNUMUSU IS NOT NULL
...	   ORDER BY X2100_HDRDATA DESC
...	   FETCH FIRST 1 ROW ONLY;

${SQL_CONTEUDO_INFORMACOES_ANTERIORES} =  
...    SELECT
...    	TRIM(T2.X2100_NOMVALATR),
...    	TRIM(T2.X2100_CLTNUMUSU),
...    	TRIM(A1.X0100_CLTCOD),
...    	TRIM(A1.X0100_CLTDGV) AS CHAR
...    FROM
...    	(
...    	SELECT
...    		*
...    	FROM
...    		(
...    		SELECT
...    			X2100_CLTCOD ,
...    			MAX(X2100_HDRDATA) COL2
...    		FROM
...    			(
...    			SELECT
...    				*
...    			FROM
...    				AOXB00.AOXB21
...    			JOIN AOXB00.AOXB01 ON
...    				X0100_CLTCOD = X2100_CLTCOD
...    			WHERE
...    				X2100_CLTCOD IS NOT NULL
...    				AND X2100_NOMVALATR IS NOT NULL
...    				AND X2100_CLTNUMUSU IS NOT NULL
...    				AND X0100_CLTDGV IS NOT NULL )
...    		GROUP BY
...    			X2100_CLTCOD ) FETCH FIRST 1 ROW ONLY ) T1
...    JOIN AOXB00.AOXB21 T2 ON
...    	T1.X2100_CLTCOD = T2.X2100_CLTCOD
...    	AND T1.COL2 = T2.X2100_HDRDATA
...    JOIN AOXB00.AOXB01 A1 ON
...    	T1.X2100_CLTCOD = A1.X0100_CLTCOD




  
# ...    SELECT T2.X2100_NOMVALATR, T2.X2100_CLTNUMUSU, LPAD ( A1.X0100_CLTCOD , 8 , '0' ) , CAST( A1.X0100_CLTDGV AS CHAR)   FROM ( SELECT * FROM ( 
# ...    SELECT X2100_CLTCOD  , MAX(X2100_HDRDATA) COL2 
# ...    FROM ( 
# ...  SELECT *  FROM AOXB00.AOXB21  JOIN AOXB00.AOXB01 ON X0100_CLTCOD = X2100_CLTCOD
# ...  WHERE X2100_CLTCOD IS NOT NULL AND X2100_NOMVALATR IS NOT NULL AND X2100_CLTNUMUSU IS NOT NULL AND X0100_CLTDGV IS NOT NULL
# ...  ) 
# ...  GROUP BY  X2100_CLTCOD   )  FETCH FIRST 1 ROW ONLY ) T1
# ...  JOIN  AOXB00.AOXB21 T2 ON T1.X2100_CLTCOD = T2.X2100_CLTCOD AND T1.COL2 = T2.X2100_HDRDATA   
# ...  JOIN AOXB00.AOXB01 A1 ON T1.X2100_CLTCOD = A1.X0100_CLTCOD

*** Keywords ***
Recuperar Conteudo Informacoes Anteriores
    @{query} =      Query  ${SQL_CONTEUDO_INFORMACOES_ANTERIORES}
    ${conteudo} =   Get From List  ${query}[0]  0
    ${matricula} =  Get From List  ${query}[0]  1
    ${cltCod} =     Get From List  ${query}[0]  2
    ${digito} =     Get From List  ${query}[0]  3
    [return]  ${conteudo}  ${matricula}  ${cltCod}  ${digito}

# Recuperar Conteudo Informacoes Anteriores
    # [Arguments]    ${cltcod}  ${indice}
    # @{conteudo}=  Query  SELECT TRIM(X2100_NOMVALATR) FROM AOXB00.AOXB21 WHERE X2100_CLTCOD = ${cltcod} ORDER BY X2100_HDRDATA DESC;
    # ${retorno} =  Get From List  ${conteudo}[${indice}]  0
    # [return]      ${retorno}
    
# Recuperar Matricula Responsavel Informacoes Anteriores
    # [Arguments]    ${cltcod}  ${indice}
    # @{cltResponsavel}=  Query  SELECT TRIM(X2100_CLTNUMUSU) FROM AOXB00.AOXB21 WHERE X2100_CLTCOD = ${cltcod} ORDER BY X2100_HDRDATA DESC
    # ${retorno} =   Get From List  ${cltResponsavel}[${indice}]  0
    # [return]       ${retorno}