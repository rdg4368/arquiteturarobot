*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem           
Library      Collections    
Library      utilsMask
Library        String    
***Variables***
${SQL_SELECIONAR_DADOS_IMPRESSAO}    
...		SELECT
...			CAST(X1800_HDRDATA AS CHAR(8)),
...			CAST(X1800_CLTNUMUSU AS CHAR(6)),
...			TRIM(X1800_IMPDATTRF),
...			CAST(X1800_IMPDATVENC AS CHAR(8))
...		FROM
...			aoxb00.aoxb18
...		WHERE
...			x1800_cltcod = {0}
...		ORDER BY
...			x1800_hdrdata DESC FETCH FIRST 1 ROW ONLY

*** Keywords ***

Selecionar Dados Impressao Cadastral
    [Arguments]       ${cltcod}
    ${query}    Format String    ${SQL_SELECIONAR_DADOS_IMPRESSAO}    ${cltcod}    
    @{result}  Query             ${query}          
    ${dataUltimaAlteracao} =  Formatar Mascara Data Trim    ${result}[0][0]
    ${matExecutor} =  Get From List  ${result}[0]  1
    ${dataRecebimentoTarifa} =  Formatar Mascara Data Trim    ${result}[0][2]          
    ${dataVencimentoCadastro} =  Formatar Mascara Data Trim    ${result}[0][3]
    [Return]   ${dataUltimaAlteracao}  ${matExecutor}  ${dataRecebimentoTarifa}  ${dataVencimentoCadastro}   