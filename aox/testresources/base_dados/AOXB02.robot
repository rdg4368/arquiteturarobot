*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py   

#TODO as querys devem ser declaradas como Variáveis
*** Variables***
${SQL_RECUPERAR_PRODUTO_INCLUIDO}
...    SELECT 
...        TRIM(X0200_PRDDESC) 
...    FROM 
...        AOXB00.AOXB02 
...    WHERE 
...        X0200_PRUCOD = {0}

${SQL_REMOVER_NOVO_PRODUTO}
...  DELETE 
...    FROM 
...        AOXB00.AOXB02 
...    WHERE X0200_PRUCOD = {0}

${SQL_RECUPERAR_PRODUTO_SITUACAO_INTAIVA}
...    SELECT 
...        TRIM(X0200_PRUCOD), 
...        TRIM(SEDIDR) 
...    FROM 
...        AOXB00.AOXB02 
...    WHERE 
...        SEDIDR = {0}
...    AND
...        X0200_HDRSTATGER = 2 FETCH FIRST ROWS ONLY

${SQL_RECUPERAR_PRODUTO_ATIVADO} 
...    SELECT 
...        TRIM(X0200_HDRSTATGER) 
...    FROM
...         AOXB00.AOXB02 
...    WHERE 
...        X0200_PRUCOD = {0} 
...    AND 
...        SEDIDR = {1}

    
${SQL_DESFAZER_ALTERACOES_PRODUTO} 
...    UPDATE 
...        AOXB00.AOXB02 
...    SET 
...        X0200_HDRSTATGER = {0} 
...    WHERE 
...        X0200_PRUCOD = {1} 
...    AND     
...        SEDIDR = {2}


${SQL_RECUPERAR_PRODUTO_SITUACAO_ATIVA}   
...    SELECT 
...        TRIM(X0200_PRUCOD),
...        TRIM(SEDIDR) 
...    FROM 
...        AOXB00.AOXB02 
...    WHERE 
...        X0200_HDRSTATGER = 1 FETCH FIRST 1 ROWS ONLY


${SQL_RECUPERAR_PRODUTOS}
...    SELECT 
...        TRIM(X0200_PRUCOD),
...        TRIM(X0200_PRDDESC) 
...    FROM 
...        AOXB00.AOXB02

*** Keywords ***
Recuperar Produto Incluido 
    [Arguments]     ${codProd} 
    ${dado}       Format String     ${SQL_RECUPERAR_PRODUTO_INCLUIDO}    ${codProd}    
    ${status}     Query   ${dado}
    [Return]      ${status}

    
#TODO Alterar o nome para 'Excluir Novo Produto' ou 'Deletar Novo Produto' ou 'Remover Novo Produto'
Remover Novo Produto
    [Arguments]       ${codProd} 
    ${sql}     Format String    ${SQL_REMOVER_NOVO_PRODUTO}    ${codProd}
    Execute Sql String    ${sql} 

#TODO 'Recuperar' é a grafia correta
Recuperar Produtos 
    ${result}       Query      ${SQL_RECUPERAR_PRODUTOS}   
    [Return]        ${result}

#TODO 'Recuperar' é a grafia correta
Recuperar Produto Situacao Inativa
     [Arguments]    ${sedir}
     ${dado}     Format String    ${SQL_RECUPERAR_PRODUTO_SITUACAO_INTAIVA}    ${sedir}
     ${status}     Query   ${dado}
     [Return]      ${status}

#TODO 'Recuperar' é a grafia correta
Recuperar Produto Situacao Ativa
    ${result}       Query   ${SQL_RECUPERAR_PRODUTO_SITUACAO_ATIVA}   
    [Return]        ${result}

#TODO 'Recuperar' é a grafia correta
Recuperar Produto Ativado
    [Arguments]     ${pruProd}  ${sedir}
    ${dado}       Format String    ${SQL_RECUPERAR_PRODUTO_ATIVADO}    ${pruProd}	${sedir} 
    ${status}     Query   ${dado}
    [Return]      ${status}
    

    
#TODO Alterar o nome da keyword para Atualizar Produto
Defazer Alteracoes Produto
    [Arguments]     ${pruCod}  ${sedir}   ${opcao}
    ${sql} =  Format String    ${SQL_DESFAZER_ALTERACOES_PRODUTO}    ${pruCod}	${sedir}   ${opcao}   
    Execute Sql String    ${sql} 
