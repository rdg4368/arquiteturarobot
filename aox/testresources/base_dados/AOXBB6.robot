*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      testresources/utils/utils.py
Library      DateTime
Library      String    

*** Variable ***

${PF}      1
${PJ}      2
${LINHAS}  1

#TODO: As tres querys estão iguais. Bastava apenas uma, com o nome 'SQL_RECUPERAR_CODIGO_CLIENTE'
${SQL_VALIDA_RENDA_DECLARADA}
...    SELECT XB600_CLTCOD AS CLIENTE,
...     XB600_PREVAL AS RENDA_PRESUMIDA
...    FROM AOXB00.AOXBB6
...     WHERE XB600_CLTCOD = {0}
    
${SQL_VALIDA_ALTERACAO_RENDA_DECLARADA}
...    SELECT XB600_CLTCOD AS CLIENTE,
...     XB600_PREVAL AS RENDA_PRESUMIDA
...    FROM AOXB00.AOXBB6 
...    WHERE XB600_CLTCOD = {0}    

${SQL_VALIDA_EXCLUSAO_RENDA_DECLARADA}
...    SELECT XB600_CLTCOD AS CLIENTE,
...     XB600_PREVAL AS RENDA_PRESUMIDA
...    FROM AOXB00.AOXBB6 
...    WHERE XB600_CLTCOD = {0}

${SQL_DELETAR_RENDA_DECLARADA}
...    DELETE FROM AOXB00.AOXBB6 
...    WHERE XB600_CLTCOD = {0}



${SQL_CONSULTA_RENDA_DECLARADA}
...    SELECT
...    	XB600_CLTCOD || ' ' || ' ' || X0100_CLTDGV AS COD,
...    	X0100_CLTNOM AS NOME,
...    	XB600_SISCODSIS AS ORIG,
...    	CASE XB600_RNDTIPREN WHEN 09 THEN 'RENDA DECLARADA'
...    	WHEN 10 THEN 'RENDA PRESUMIDA'
...    	WHEN 11 THEN 'FAT.PRESUMIDO'
...    END AS TIPO_RENDA,
...    TRIM(XB600_CLTNUMUSU) AS USUARIO
...    FROM
...    AOXB00.AOXBB6,
...    AOXB00.AOXB01
...    WHERE
...    XB600_CLTCOD = X0100_CLTCOD
...    AND XB600_CLTCOD = {0}

*** Keywords ***

#TODO: Não pode haver Validações no arquivo de banco de dados. Neste caso, ela deveria ser feita em manutencaoRendasPresumidaDeclaradaKW.robot.
#TODO: A query em questão, deveria ser SQL_RECUPERAR_CODIGO_CLIENTE, deverá ser movida para o arquivo mencionado acima.
Validar Dados de Inclusão Renda Declarada
    [Arguments]    ${codigo}    
    ${query}    Format String    ${SQL_VALIDA_RENDA_DECLARADA}    ${codigo}    
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query} 
    [Return]    ${result} 

#TODO: modificar o nome para Deletar Renda Declarada Por Codigo do Cliente
#TODO: Perceba que esta keyword é a mesma da keyword abaixo. Avalie a possiblidade de usar apenas uma delas. 
Preparar Base Dados Inclusão de Renda Declarada    
    [Arguments]    ${codigo}    
    ${dado}    Format String   ${SQL_DELETAR_RENDA_DECLARADA}    ${codigo}    
    ${query}    Execute Sql String    ${dado}
    
Excluir Inclusão de Renda Declarada
    [Arguments]    ${codigo}              
    ${query}     Format String      ${SQL_DELETAR_RENDA_DECLARADA}  ${codigo}
    ${result}    Execute Sql String    ${query}    
    [Return]    ${result}   

#TODO: Não pode haver Validações no arquivo de banco de dados. Ela deveria ser feita no modulo de keywords do teste
Validar Dados Alteração de Renda Declarada
    [Arguments]    ${codigo}    
    ${query}    Format String    ${SQL_VALIDA_ALTERACAO_RENDA_DECLARADA}     ${codigo}    
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query} 
    [Return]    ${result} 

#TODO: Não pode haver Validações no arquivo de banco de dados. Ela deveria ser feita no modulo de keywords do teste
Validar Dados Exlusao de Renda Declarada
    [Arguments]    ${codigo}    
    ${query}    Format String    ${SQL_VALIDA_EXCLUSAO_RENDA_DECLARADA}     ${codigo}    
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query} 
    [Return]    ${result}  

#TODO: Não pode haver Validações no arquivo de banco de dados. Ela deveria ser feita no modulo de keywords do teste
#FIXME Não pode haver Validações no arquivo de banco de dados. Ela deveria ser feita no modulo de keywords do teste
Validar Consulta de Renda Declarada
    [Arguments]    ${codigo}    
    ${query}    Format String    ${SQL_CONSULTA_RENDA_DECLARADA}     ${codigo} 
    ${result}    Query    ${query}    
    [Return]     ${result} 