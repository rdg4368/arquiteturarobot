*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary

*** Variable ***

${SQL_CONSULTAR_CC_VALIDA}  SELECT LPAD(C0100_DEPSEQ,4,'0'), LPAD(C0100_CTCCHVCONTA,10,'0'), RAND() as IDX FROM DCCB00.DCCB01 WHERE C0100_TFDCODFXDEP > 57 AND C0100_TFDCODFXDEP < 64 ORDER BY IDX FETCH FIRST 1 ROWS ONLY

*** Keywords ***

Selecionar conta corrente valida
   ${result}  Query  ${SQL_CONSULTAR_CC_VALIDA}
   [Return]  ${result}