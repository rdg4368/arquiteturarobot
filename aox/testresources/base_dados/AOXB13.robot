*** Settings ***

Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem   
    
Library      utils
Library      utilsMask       
Library      String     
 
*** Variables***
${SQL_RECUPERAR_DADOS_DESFAZER_ALTERACAO_PF}
...		UPDATE
...			AOXB00.AOXB17
...		SET
...			X1700_MUCCODDNE = '{0}'
...		WHERE
...			X1700_CLTCOD = {1};

${SQL_RECUPERAR_DADOS_ECONOMICOS_FINANCEIROS_PJ}
...		SELECT
...			TRIM(X0100_CLTCGC),
...			TRIM(X0100_CLTCGCDIG),
...			'14042020' AS DATA,
...         TRIM(X0100_CLTCOD)   
...		FROM
...			AOXB00.AOXB13
...		INNER JOIN AOXB00.AOXB01 ON
...			X0100_CLTCOD = X1300_CLTCOD
...    WHERE
...        	X0100_CLTTIP = 2 
...    FETCH FIRST ROW ONLY

${SQL_VALIDAR_DADOS_INCLUIR_DADOS_FINANCEIROS_PJ}
...		SELECT
...			TRIM(X1300_CLTCOD),
...			TRIM(X1300_BALDAT),
...         TRIM(X1300_BALVALRCOP),
...        	TRIM(X1300_BALVALMDRC),
...			TRIM(SEDIDR)
...		FROM
...			AOXB00.AOXB13
...		ORDER BY
...			SEDIDR DESC FETCH FIRST ROW ONLY

${SQL_DESFAZER_DADOS_INCLUIR_DADOS_FINANCEIROS_PJ}
...		DELETE
...		FROM
...			AOXB00.AOXB13
...		WHERE
...			SEDIDR = {0}	

${SQL_PREPARAR_ALTERAR_DADOS_FINANCEIROS_PJ}
...		INSERT
...			INTO
...			AOXB00.AOXB13 (X1300_HDRDATA,
...			X1300_HDRHORA,
...			X1300_HDRESTACAO,
...			X1300_HDRPROGRAMA,
...			X1300_HDRSTATGER,
...			X1300_CLTCOD,
...			X1300_BALDAT,
...			X1300_XOXINDMON,
...			X1300_BALVALATC,
...			X1300_BALVALPSCC,
...			X1300_BALVALRLLP,
...			X1300_BALVALATPM,
...			X1300_BALVALEXLP,
...			X1300_BALVALPTLQ,
...			X1300_BALVALPSDC,
...			X1300_BALVALLCOP,
...			X1300_BALVALPJOP,
...			X1300_BALVALLCIR,
...			X1300_BALVALLCEX,
...			X1300_BALVALPJEX,
...			X1300_BALVALRCOP,
...			X1300_BALVALMDRC,
...			X1300_CLTNUMUSU,
...			X1300_BALDATVAL,
...			X1300_BALVALRNOP)
...		VALUES (20200414,
...		111007,
...		500,
...		'AOXN99',
...		'1',
...		375407,
...		20200414,
...		'R$  ',
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		0,
...		100,
...		8,
...		644971,
...		20211014,
...		0) ;

${SQL_VALIDAR_CONSULTA_DADOS_ECONOMICOS_FINANCEIROS_PJ}
...		SELECT
...			TRIM(X1300_CLTCOD),
...			TRIM(X0100_CLTCGC),
...			TRIM(X0100_CLTNOM), 
...			TRIM(A.SEDIDR)
...		FROM
...			AOXB00.AOXB13 A
...		INNER JOIN AOXB00.AOXB01 ON
...			X1300_CLTCOD = X0100_CLTCOD
...		WHERE
...			X0100_CLTCOD = {0}

*** Keywords ***
Recuperar Dados Desfazer Alteracao PF Simplificada Campo Patrimonio
    [Arguments]    ${dado1}    ${dado2}
    ${query}    Format String    ${SQL_RECUPERAR_DADOS_DESFAZER_ALTERACAO_PF}    ${dado1}    ${dado2}    
    Execute Sql String    ${query}    
    
Recuperar Dados Economicos Financeiros PJ
    ${Result}    Query    ${SQL_RECUPERAR_DADOS_ECONOMICOS_FINANCEIROS_PJ}
    [Return]    ${Result}
    
Recuperar Dados Validar Inclusao Economicos Financeiros PJ
    ${Result}    Query    ${SQL_VALIDAR_DADOS_INCLUIR_DADOS_FINANCEIROS_PJ}
    [Return]    ${Result}
    
Desfazer Dados Economicos Inclusao/Alteracao
    [Arguments]    ${Dado1}
    ${query}    Format String    ${SQL_DESFAZER_DADOS_INCLUIR_DADOS_FINANCEIROS_PJ}    ${Dado1}[0][4]
    Execute Sql String    ${query}   
    
Preparar Dados Economicos PJ
    Execute Sql String    ${SQL_PREPARAR_ALTERAR_DADOS_FINANCEIROS_PJ}         
    
Recuperar Dados Validar Consulta Economicos Financeiros PJ
    [Arguments]    ${Cltcod}
    ${query}    Format String    ${SQL_VALIDAR_CONSULTA_DADOS_ECONOMICOS_FINANCEIROS_PJ}    ${Cltcod}
    ${result}    Query    ${query}   
    [Return]    ${result} 
    
Desfazer Dados Economicos Consulta
    [Arguments]    ${Dado1}
    ${query}    Format String    ${SQL_DESFAZER_DADOS_INCLUIR_DADOS_FINANCEIROS_PJ}    ${Dado1}
    Execute Sql String    ${query}   