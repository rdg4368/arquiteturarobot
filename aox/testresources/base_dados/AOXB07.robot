*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      testresources/utils/utils.py
Library      DateTime       
Library      String    

*** Variable ***
${SQL_VALIDAR_INCLUSAO_GRUPO_RISCO}
...    SELECT 
...        * 
...    FROM 
...        AOXB00.AOXB07 
...    WHERE 
...        X0700_GRRCOD = '{0}'

${SQL_CONSULTAR_GRUPOS_DE_RISCO}
...    SELECT 
...        X0700_GRRCOD AS CODIGO, 
...        TRIM(X0700_GRRNOM) AS NOME,
...    FROM 
...        AOXB00.AOXB07 
...    WHERE 
...        X0700_GRRCOD = '{0}' 
...    AND
...        X0700_GRRNOM = 'ALTERAR TESTE AUTOMACAO'
...    FETCH FIRST ROWS ONLY

${SQL_RECUPERAR_DADO_CONSULTA_HISTORICO}
...    SELECT 
...        X0700_GRRCOD 
...    FROM 
...        AOXB00.AOXB07,
...        AOXB00.AOXB08 
...    WHERE 
...        X0700_GRRCOD=X0800_GRRCOD 
...    FETCH FIRST ROW ONLY

${SQL_VALIDAR_HISTORICO_GRUPO}
...		SELECT
...			X0700_GRRCOD AS GRUPO,
...			TRIM(X0700_GRRNOM) AS NOME,
...			TRIM(X0700_HDRDATA) AS DT_OPER,
...			TRIM(X0700_GRRHORATZ) AS HORA,
...			TRIM(X0700_GRRREGSIT) AS STATUS
...		FROM
...			AOXB00.AOXB07
...		WHERE X0700_GRRCOD = '{0}'

${SQL_VALIDAR_CONSULTA_RELACIONAMENTO_ATIVOS}
...    SELECT 
...        X0700_GRRCOD AS GRUPO, 
...        TRIM(X0700_GRRNOM) AS NOME 
...    FROM 
...        AOXB00.AOXB07 
...    FETCH FIRST ROW ONLY


${SQL_VALIDAR_HISTORICO_RELACIONAMENTO}
...    SELECT 
...        X0700_GRRCOD AS GRUPO, 
...        TRIM(X0700_GRRNOM) AS NOME, 
...    CASE X0700_GRRREGSIT WHEN 1 THEN 'Ativo'
...    WHEN 2 THEN 'Inativo' END 
...    FROM 
...        AOXB00.AOXB07 
...    WHERE 
...        X0700_GRRCOD = '{0}'


${SQL_VALIDAR_DADO_CONSULTA_HISTORICO_RELACIONAMENTO_COMPONENTES}
...		SELECT
...			TRIM(X0800_CGRTIPCMP) AS TIPO,
...			X0800_CGRCODCMP AS GR_ECONOMICO
...		FROM
...			AOXB00.AOXB08,
...			AOXB00.AOXB05,
...			AOXB00.AOXB07
...		WHERE
...			X0800_CGRCODCMP = X0500_GRECOD 
...    FETCH FIRST ROW ONLY

${SQL_DESFAZER_DADOS_INCLUSAO_DE_GRUPO_DE_RISCO}
...    DELETE FROM 
...    AOXB00.AOXB07 
...    WHERE X0700_GRRCOD = {0}

 
*** Keywords ***
Validar Inclusao GR
    [Arguments]    ${codigo}
    ${result}      Format String    ${SQL_VALIDAR_INCLUSAO_GRUPO_RISCO}    ${codigo}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${result}
    [Return]       ${status}
    
Validar Alteracao Grupo De Risco
    [Arguments]    ${codigo}
    ${result}      Format String    ${SQL_VALIDAR_INCLUSAO_GRUPO_RISCO}    ${codigo}
    ${status}      Run Keyword And Return Status    Check If Exists In Database    ${result}
    [Return]       ${status}
    
Validar Consulta GR
    [Arguments]    ${codigo}
    ${result}      Format String    ${SQL_CONSULTAR_GRUPOS_DE_RISCO}    ${codigo} 
    ${dado}        Query    ${result}      
    [Return]       ${dado}   
    
Desfazer Dados
    [Arguments]    ${dado}
        Execute Sql String    ${SQL_DESFAZER_DADOS_INCLUSAO_DE_GRUPO_DE_RISCO}    ${dado}     
    
Recuperar Dado De Acesso Consulta Historico
    ${dado}     Query    ${SQL_RECUPERAR_DADO_CONSULTA_HISTORICO}
    [Return]    ${dado}
    
Validar Historico Grupo
    [Arguments]    ${dado}
    ${sql}      Format String    ${SQL_VALIDAR_HISTORICO_GRUPO}    ${dado}
    ${result}        Query    ${sql}    
    [Return]       ${result}  
    
Recuperar Dado De Acesso Consulta Consulta Relacionamentos Ativos
    ${dado}     Query    ${SQL_RECUPERAR_DADO_CONSULTA_HISTORICO}
    [Return]    ${dado}   
    
Validar Consulta Relacionamentos
    ${result}      Format String    ${SQL_VALIDAR_CONSULTA_RELACIONAMENTO_ATIVOS}
    ${dado}        Query    ${result}    
    [Return]       ${dado}   

   
Validar Historico Relacionamento Grupo Risco
    [Arguments]    ${GRUPO_RISCO}
    ${dado}      Format String    ${SQL_VALIDAR_HISTORICO_RELACIONAMENTO}    ${GRUPO_RISCO}
    ${result}    Query    ${dado}    
    [Return]     ${result}    


Validar Consulta Historico Relacionamento
    ${sql}       Format String   ${SQL_VALIDAR_DADO_CONSULTA_HISTORICO_RELACIONAMENTO_COMPONENTES}
    ${result}    Query           ${sql}        
    [Return]    ${result}
       



    
