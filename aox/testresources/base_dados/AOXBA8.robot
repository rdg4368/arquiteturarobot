*** Settings ***
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py 
Library      String      

*** Variable ***

${SQL_CONSULTA_INCLUSAO_VINCULACOES}
...   SELECT * FROM AOXB00.AOXBA8 WHERE XA800_TVNCOD = 01 AND XA800_HDRDATA = {0}
    
${SQL_DELETA_INCLUSAO_VINCULACOES} 
...   DELETE AOXB00.AOXBA8 WHERE XA800_TVNCOD = 01 AND XA800_HDRDATA = {0} 

${SQL_CONSULTA_VINCULACOES}
...    SELECT LPAD(XA800_GECSEQHIER,2,'0'), LPAD(XA800_TVNCOD,2,'0') FROM AOXB00.AOXBA8 WHERE XA800_TVNCOD = 01

#TODO as duas querys abaixo estão iguais. Bastava uma com o nome SQL_RECUPERAR_PESSOA_FISICA_POR_TVNCOD
#TODO nomes deste tipo favorecem bastante a visibilidade.
${SQL_CONSULTA_ALTERACOES_VINCULACOES}
...   SELECT * FROM AOXB00.AOXBA8 WHERE XA800_TVNCOD = 01 AND XA800_GECINDPART = 'N' 
#TODO query igual a acima.
${SQL_CONSULTA_EXCLUSAO}
...    SELECT * FROM AOXB00.AOXBA8 WHERE XA800_TVNCOD = 01 AND XA800_GECINDPART = 'N'

*** Keywords ***
Valida Inclusao Vinculacoes
    [Arguments]    ${data}    
    ${query}    Format String      ${SQL_CONSULTA_INCLUSAO_VINCULACOES}   ${data}   
    ${result}   Run Keyword And Return Status    Check If Exists In Database    ${query}    
    [Return]    ${result} 
    
Excluir Inclusao Vinculacoes
    [Arguments]    ${data}              
    ${query}     Format String      ${SQL_DELETA_INCLUSAO_VINCULACOES}  ${data}
    ${result}    Execute Sql String    ${query}    
    [Return]    ${result}  
    
Validar Consulta Vinculacoes
    [Arguments]    ${data}    
    ${result}    query     ${SQL_CONSULTA_VINCULACOES}   ${data}    
    [Return]    ${result}      
    
Validar Consulta Alteracao Vinculacoes
    [Arguments]    ${data}    
    ${result}    Run Keyword And Return Status    Check If Exists In Database     ${SQL_CONSULTA_ALTERACOES_VINCULACOES}   ${data}    
    [Return]    ${result} 
    
Validar Consulta Exclusao Vinculacoes   
    ${result}    Run Keyword And Return Status    Check If Exists In Database     ${SQL_CONSULTA_EXCLUSAO}      
    [Return]    ${result}    
    