*** Settings ***
Library      String
Resource     resources/commons/Database.robot
Library      DatabaseLibrary
Library      OperatingSystem       
Library      testresources/utils/utilsMask.py      
Library      Collections    

*** Variables ***
${SQL_RECUPERAR_DADOS_ALTERACAO_CEP}
...    SELECT TRIM(X3900_PSTCEPLOG) AS CEP FROM AOXB00.AOXB39 WHERE X3900_PSTCEPLOG > 7000000 FETCH FIRST ROW ONLY

${SQL_VALIDAR_ALTERACAO_CEP}
...    SELECT TRIM(X3900_PSTCEPLOG) AS CEP, X3900_PSTNOMLOGED AS LOGRADOURO_SIMPLIFICADO, X3900_PSTNOMBAIED AS BAIRRO_SIMPLIFICADO, X3900_MUCNOMED  AS NOME_MUNICIPIO FROM AOXB00.AOXB39 WHERE X3900_PSTCEPLOG = {0}
    
${SQL_UPDATE_CEP}
...    UPDATE AOXB00.AOXB39 SET X3900_PSTNOMLOGED = 'PRAA PRESIDENTE GETLIO VARGAS', X3900_PSTNOMBAIED = 'CENTRO', X3900_MUCNOMED = 'GUARULHOS'   WHERE X3900_PSTCEPLOG = {0}            

#TODO esta query esta repetida. Vide ${SQL_EXCLUIR_PARA_INCLUIR} abaixo (nome que tambem esta indevido)
${SQL_EXCLUIR_DADOS_BASE_AOXB39_CEPS_GENERICOS}
...    DELETE FROM AOXB00.AOXB39 WHERE X3900_PSTCEPLOG = 70722500         

${SQL_INCLUIR_DADOS_BASE_AOXB39_CEPS_GENERICOS}
...    INSERT    INTO    AOXB00.AOXB39 (X3900_HDRDATA,    X3900_HDRHORA,    X3900_HDRESTACAO,    X3900_HDRPROGRAMA,    X3900_HDRSTATGER,    X3900_PSTCEPLOG,    X3900_PSTCODLOG,    X3900_MUCCODDNE,
...    X3900_PSTTIPLOG,    X3900_PSTNOMLOG,    X3900_PSTCOMPLLOG,    X3900_PSTNOMLOGED,    X3900_PSTNOMBAI,    X3900_PSTNOMBAIED,    X3900_MUCNOMED,    X3900_UFESGL,    X3900_PSTCODLGDNE,    X3900_PSTCODLCL)
...    VALUES(20190434, 183402, 0, 'AOXPFQ',0, 70722500, 0, 1778, 'Q', 'CLN 102', ' ', 'CLN 102', 'ASA NORTE', 'ASA NORTE', 'BRASILIA', 'DF', 46262, 0)    

${SQL_RECUPERAR_DADOS_VALIDAR_EXCLUSAO}
...    SELECT * FROM AOXB00.AOXB39 WHERE X3900_PSTCEPLOG = 70722500 

${SQL_RECUPERAR_DADOS_CONSULTA_CEP}
...    SELECT LPAD(X3900_PSTCEPLOG,5,'0')||'-'||LPAD(X3900_PSTCEPLOG,2,'0') AS COD_CEP, LPAD(X3900_MUCCODDNE,8,'0') COD_MUNICIPIO  FROM AOXB00.AOXB39 WHERE X3900_PSTCEPLOG = 0    

*** Keywords ***
Recuperar Dados AOX39 Alteracao Cep
    ${result}      Query      ${SQL_RECUPERAR_DADOS_ALTERACAO_CEP}
    [return]	   ${result}

Recuperar Dados Validacao Alteracao Cep
    [Arguments]   ${cep}
    ${sql}        Format String  ${SQL_VALIDAR_ALTERACAO_CEP}  ${cep}
    ${result}     Query          ${sql} 
    [return]      ${result}
    
Recuperar Dados Validacao Exclusao CEP
    ${status}   Run Keyword And Return Status   Check If Exists In Database  ${SQL_RECUPERAR_DADOS_VALIDAR_EXCLUSAO} 
    [return]    ${status}
    
Recuperar Dados Validacao Consulta CEP
    ${result}   Query      ${SQL_RECUPERAR_DADOS_CONSULTA_CEP}
    [return]    ${result}
        
Desfazer Alteracao CEP 
    [Arguments]            ${cep}
    ${sql}                 Format String  ${SQL_UPDATE_CEP}  ${cep}
    Execute Sql String     ${sql}    
 
Excluir Dados Base AOXB39 CEPS Genericos
    Execute Sql String     ${SQL_EXCLUIR_DADOS_BASE_AOXB39_CEPS_GENERICOS}    
    
Incluir Dados Base AOXB39 CEPS Genericos
    Execute Sql String     ${SQL_INCLUIR_DADOS_BASE_AOXB39_CEPS_GENERICOS}


                                                                                                                                
 