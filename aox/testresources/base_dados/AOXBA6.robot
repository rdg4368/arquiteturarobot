*** Settings ***
Library  Collections    
Library  DatabaseLibrary    
Library  utilsMask    

*** Variables ***
${LINHAS}  1

${SQL_CLIENTE_REGISTRO_ALTERADO} =  
...  SELECT CAST(XA600_CLTCOD AS CHAR(8)), CAST(X0100_CLTDGV AS CHAR) FROM AOXB00.AOXBA6 INNER JOIN AOXB00.AOXB01 ON XA600_CLTCOD = X0100_CLTCOD 
...    ORDER BY XA600_HDRDATA DESC FETCH FIRST 1 ROW ONLY

${SQL_CONSULTAR_DADOS_DUPLA_ASSINATURA}  
...  SELECT DISTINCT TRIM(X0100_CLTCOD), TRIM(X0100_CLTDGV) ,TRIM(LPAD(XA600_CLTCODDEP,6,'0')), TRIM(X0100_CLTNOM)
...  FROM AOXB00.AOXB01 
...  INNER JOIN AOXB00.AOXB03 ON (X0300_CLTCOD = X0100_CLTCOD)
...  INNER JOIN AOXB00.AOXBA6 ON (XA600_CLTCOD = X0100_CLTCOD)
...  INNER JOIN AOXB00.AOXB17 ON (X1700_CLTCOD = X0100_CLTCOD)
...  INNER JOIN AOXB00.AOXB22 ON (X2200_CLTCOD = X0100_CLTCOD)
...  INNER JOIN AOXB00.AOXB29 ON (X2900_CLTCOD = X0100_CLTCOD)
...  WHERE 
...  X0100_CLTCONSISB <> '' AND X0100_CLTTIPSERV <> '' AND X0100_CLTLOGEND  <> '' AND X0100_CLTCONSISB  <> '' AND X0100_CLTCEP > 0  AND X0100_CLTCOMPEND  <> '' AND X0100_CLTBAI  <> '' AND X0100_CLTCID  <> '' AND X0100_CLTUFE  <> '' AND X0100_CLTDATNAS > 0
...  AND X1700_FISSEX > 0 AND X1700_FISESTINSTR > 0 AND X1700_FISESTCVL > 0 AND X1700_FISTIPIDT > 0 AND X1700_FISNUMIDT <> '' AND X1700_FISLOCIDT <> '' AND X1700_FISCIDNAT <> '' AND X1700_FISUFENAT <> '' AND X1700_FISLOCPAIS <> '' AND X1700_FISNOMMAE <> '' AND X1700_FISCODNATU > 0 AND X1700_FISCODOCUP > 0 AND X1700_FISUFEIDT <> '' AND X1700_FISDATIDT > 0
...  AND X1700_FISESTCVL = 1 
...  AND X2900_REFEND IS NOT NULL AND X2900_REFTEL <> 0 AND X2900_REFTIP <> 0
...  AND XA600_NOMDATATU <> 0
...  AND X2200_PTRVAL > 0.00 AND X2200_PTRVAL <> 0.00 AND X2200_PTRVALPAR <> 0.00
...  AND X0300_ASSVAL > 2.00
...  AND X0100_CLTCOD = (SELECT X2900_CLTCOD FROM AOXB00.AOXB29 WHERE X2900_CLTCOD = X0100_CLTCOD GROUP BY X2900_CLTCOD HAVING COUNT(*) > 1 )
...  FETCH FIRST 1 ROWS ONLY;


*** Keywords ***

Recuperar Dados Validacao Cliente Pendente Aguardando Autorizacao 
    [Arguments]     ${codDep}  ${dataAtualInvertida}     
    ${result}       Query   SELECT XA600_CLTCOD CONCAT ' ' CONCAT X0100_CLTDGV, TRIM(X0100_CLTNOM), TRIM(XA600_CLTCODDEP), TRIM(XA600_CLTNUMUSU) FROM AOXB00.AOXBA6 INNER JOIN AOXB00.AOXB01 ON XA600_CLTCOD = X0100_CLTCOD WHERE AOXB00.AOXBA6.SEDIDR IN (SELECT MAX(SEDIDR) FROM AOXB00.AOXBA6 WHERE XA600_NOMDATATU >= ${dataAtualInvertida} AND XA600_CLTCODDEP = ${codDep} GROUP BY XA600_CLTCOD)
    [Return]        ${result}

Recuperar Dados Para Preparacao Teste
    [Arguments]     ${codDep}  ${dataAtualInvertida}     
    ${result}       Query   SELECT TRIM(XA600_CLTCOD), TRIM(XA600_NOMDATATU) FROM AOXB00.AOXBA6 INNER JOIN AOXB00.AOXB01 ON XA600_CLTCOD = X0100_CLTCOD WHERE AOXB00.AOXBA6.SEDIDR IN (SELECT MAX(SEDIDR) FROM AOXB00.AOXBA6 WHERE XA600_NOMDATATU >= ${dataAtualInvertida} AND XA600_CLTCODDEP = ${codDep} GROUP BY XA600_CLTCOD)
    [Return]        ${result}

Recuperar Cliente Pendente Aguardando Autorizacao    
    [Arguments]     ${codDep}
    ${result}       Query   SELECT TRIM(XA600_CLTCOD), TRIM(XA600_NOMDATATU) FROM AOXB00.AOXBA6 INNER JOIN AOXB00.AOXB01 ON XA600_CLTCOD = X0100_CLTCOD WHERE XA600_CLTCODDEP = ${codDep} ORDER BY XA600_HDRDATA DESC FETCH FIRST 1 ROWS ONLY
    [Return]        ${result}

Migrar Cliente Autorizacao Pendente
    [Arguments]     ${dataAtualInvertida}  ${cltCod}        
    ${resultado} =  Run Keyword And Return Status       Execute Sql String  
    ...  UPDATE AOXB00.AOXBA6 SET XA600_NOMDATATU = ${dataAtualInvertida} WHERE XA600_CLTCOD = ${cltCod}; 
    [Return]    ${resultado}

Checar Autorizacao De Pendencia
    [Arguments]       ${ctlCod}              
    ${result} =  Run Keyword And Return Status       Execute Sql String  
    ...  SELECT * FROM AOXB00.AOXBA6 WHERE XA600_CLTCOD =  ${ctlCod};
    [Return]    ${result} 

Checar Consulta Registros Validados
    [Arguments]     ${cltcod}  
    ${result}       Query  SELECT TRIM(X0100_CLTCOD), TRIM(X0100_CLTDGV), TRIM(X0100_CLTNOM) FROM AOXB00.AOXB01 WHERE X0100_CLTCOD = ${cltcod} 
    [Return]        ${result}

Selecionar Dados Dupla Assinatura
    ${result}  Query  ${SQL_CONSULTAR_DADOS_DUPLA_ASSINATURA}
    [Return]    ${result}

Cliente Registro Alterado
    ${query} =  Query  ${SQL_CLIENTE_REGISTRO_ALTERADO}
    ${cltcod} =  Get From List  ${query}[0]  0
    ${digito} =  Get From List  ${query}[0]  1
    [return]  ${cltcod}  ${digito}

Resgatar Registros Alterados
    [Arguments]  ${cltCod}
   @{query} =  Query  
   ...    SELECT CAST(XA600_NOMDATATU AS CHAR(8)), TRIM(XA600_NOMVALATR), CAST(XA600_CLTNUMUSU AS CHAR (6)) FROM AOXB00.AOXBA6 WHERE XA600_CLTCOD = ${cltCod}
   ${data} =  Formatar Mascara Data Trim  ${query}[0][0]
   ${registro} =           Get From List  ${query}[0]  1
   ${usuario} =            Get From List  ${query}[0]  2
   [Return]   ${data}  ${registro}  ${usuario}