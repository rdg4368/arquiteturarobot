# Neste arquivo se encontram todas as Keywords reaproveitáveis em suítes diferentes
*** Settings ***
Resource    resources/commons/Mainframe.robot
Resource    testresources/base_dados/AOXB06.robot
Resource    testresources/base_dados/AOXB02.robot
Resource    testresources/base_dados/AOXB01.robot
Resource    testresources/base_dados/AOXB17.robot
Resource    testresources/base_dados/AOXB19.robot
Resource    testresources/base_dados/AOXBA6.robot
Library     testresources/utils/utils.py  
Library     DateTime
Library     BuiltIn    
  
*** Variables ***
${nome}                ${EMPTY}
${cpfValido}           ${EMPTY}
${cnpjValido}          ${EMPTY}
${motivoDuplicidade}   ${EMPTY}
${host7}               AOX7
${host9}               AOX9

*** Keywords ***
# Exporta as datas em forma de Lista. Modo de uso: 
# ${data} = Gerar Data Atual
# ${data}[0] || ${data}[1] || ${data}[2]    
#TODO essa keyword deveria ser criada em um arquivo chamado DataKeywords.robot em resources/commons
Gerar Data Atual
    ${dataAtual} =            Get Current Date	UTC  result_format=%d%m%Y 
    ${dataAtualInvertida} =   Get Current Date	UTC  result_format=%Y%m%d
    ${dataOntem} =            Get Current Date	UTC  result_format=%Y%m%d  
    ${dataOntem} =	          Add Time To Date	${dataOntem}	-1 day  result_format=%Y%m%d     
    [Return]    ${dataAtual}  ${dataAtualInvertida}  ${dataOntem} 

Consultar Opcao "${opcao}" Menu Principal AOX03
     Digitar Na Posicao  ${opcao}   21  72 
     Transmitir 

Consultar Opcao "${opcao}" Menu Principal AOX02
     Digitar Na Posicao  ${opcao}   22  74 
     Transmitir 

Consultar Funcoes de Apoio Sistema De Clientes
    [Arguments]  ${host}  ${opcao}  ${cltCod}  ${digCltCod}  ${opcaoPrimeiroMenu}  
    Trocar Host  ${host}
    Consultar Opcao "${opcaoPrimeiroMenu}" Menu Principal AOX02
    Digitar Na Posicao  ${cltCod}   18  15  
    Digitar Na Posicao  ${digCltCod}   18  26  
    Digitar Na Posicao  ${opcao}    21  69 
    Transmitir 
           
Consultar Inclusao De Produtos
    [Arguments]  ${host}  ${opcao}  ${codProdt}  ${opcaoPrimeiroMenu}  
    Trocar Host  ${host}
    Consultar Opcao "${opcaoPrimeiroMenu}" Menu Principal AOX03
    Digitar Na Posicao  ${codProdt}   20  52   
    Digitar Na Posicao  ${opcao}  20  70 
    Transmitir     
      
    
Consultar De Produtos
    [Arguments]  ${host}  ${opcao}  ${opcaoPrimeiroMenu}  
    Trocar Host  ${host}
    Consultar Opcao "${opcaoPrimeiroMenu}" Menu Principal AOX03    
    Digitar Na Posicao  ${opcao}  20  70 
    Transmitir  
                     
Consultar Manutencao De Dados Cliente
    [Arguments]  ${host}  ${opcao}  ${cltCod}  ${digCltCod}  ${opcaoPrimeiroMenu}  
    Trocar Host  ${host}
    Consultar Opcao "${opcaoPrimeiroMenu}" Menu Principal AOX03
    Digitar Na Posicao  ${cltCod}   16  15  
    Digitar Na Posicao  ${digCltCod}   16  26  
    Digitar Na Posicao  ${opcao}    18  69 
    Transmitir 

Consultar Endereco De Correspondencia
    [Arguments]  ${host}  ${opcao}  ${cltCod}  ${digCltCod}  ${opcaoPrimeiroMenu}  
    Trocar Host  ${host}
    Consultar Opcao "${opcaoPrimeiroMenu}" Menu Principal AOX03
    Digitar Na Posicao  ${cltCod}   19  13  
    Digitar Na Posicao  ${digCltCod}   19  23  
    Digitar Na Posicao  ${opcao}    20  70 
    Transmitir 
        
Consultar Cliente Valido AOX Para Inclusao De Pj
    [Arguments]  ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${motivoDuplicidade}  
    Trocar Host  ${host}
    Tabular "3" Vezes  
    Digitar  ${nome}
    Tabular
    Run Keyword If   '${tipoCpfCnjp}' == 'Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}' == 'Cnpj'   Digitar  ${cpfCnjp}  
    Digitar  ${motivoDuplicidade}
    Tabular "4" Vezes 
    Digitar  ${opcao}
    #TODOS AJUSTAR :: DONE
    Transmitir

Consultar Cliente Valido AOX Para Alteração PF Simplificada
    [Arguments]   ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Trocar Host  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}  
    Tabular 
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'        Tabular   
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    #TODO AJUSTAR  :: DONE
    Transmitir
 
Consultar Cliente Valido AOX Para Alteração PJ Simplificada
    [Arguments]   ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Trocar Host  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}  
    Tabular 
    Digitar  ${nome}
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    #TODO AJUSTAR  :: DONE
    Transmitir
   
Consultar Cliente Valido AOX Para Alteração PJ Completa
    [Arguments]   ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Trocar Host  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}  
    Tabular 
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'        Tabular   
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    #TODO AJUSTAR  :: DONE
    Transmitir

Consultar Cliente Valido AOX 
    [Arguments]  ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp} 
    Trocar Host  ${host}
    Tabular "3" Vezes  
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'        Tabular  
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    #TODO verificar necessidade de sleep   :: DONE
    Transmitir    

Consultar Cliente Valido AOX2 
    [Arguments]   ${host}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Trocar Host  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}      
    Digitar Na Posicao  ${nome}  20  10  
    Digitar Na Posicao  ${cpfCnjp}  21  12     
    Digitar Na Posicao  ${opcao}  22  74      
    Transmitir 
    
Consultar Cliente Pendente De Dupla Assinatura
    [Arguments]  ${host}  ${cltCod}  ${digCltCod}  ${opcao}
    Trocar Host  ${host}
    Digitar Na Posicao  ${cltCod}   19  10  
    Digitar Na Posicao  ${digCltCod}   19  21  
    Digitar Na Posicao  ${opcao}    22  74 
    Transmitir 
    
Consultar Cliente Valido AOX Para Inclusao
    [Arguments]  ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${motivoDuplicidade}  
    Trocar Host  ${host}
    Tabular "3" Vezes  
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'          Tabular              
    Run Keyword If   '${tipoCpfCnjp}' == 'Cpf'    Digitar  ${cpfCnjp}  
    Run Keyword If   '${tipoCpfCnjp}' == 'Cnpj'   Digitar  ${cpfCnjp}  
    Digitar  ${motivoDuplicidade}
    Tabular "4" Vezes 
    Digitar  ${opcao}
    #TODO verificar necessidade de sleep  :: DONE
    Tabular
    Transmitir
    
Transmitir Formulario
    [Arguments]  ${cpfCnjp}  ${opcao}
    Digitar  ${cpfCnjp}
    Tabular "2" Vezes
    Digitar  ${opcao}
    #TODO AJUSTAR :: DONE
    Transmitir    
    


Validar Campos na Tela
    [Arguments]               @{campos}
    ${cnt} =    Get length    @{campos}
    :FOR    ${i}    IN RANGE    ${cnt}
    \    ${cmpComparacao} =   Set Variable  ${campos}[0][${i}][3]   #Captura e percorre o (CAMPO), última posição da lista dada como parametro        
    \    ${cmp} =     Retira Elemento Lista   ${campos}[0][${i}]   3  #Retira o elemento CAMPO da lista , restando apenas as posições que serão utilizadas para capturar o campo da tela
    \    ${cmpTela} =     Ler Dado Tela     ${cmp}                  #Captura o elemento na tela por (X,Y,tamanho)       
    \    Comparar Valores na Tela  ${cmpComparacao}  ${cmpTela}     #Compara o campo vindo da tela o campo setado como parâmetro na lista( @{campos} )

    
Acessar Menu Codigo Cliente AOX2                                          #Esta opção está limitada a informar um codigo com digito
    [Arguments]    	    ${cltCod}  ${digito}    ${opcaoMenu}  
    Trocar Host         AOX2
    Digitar Na Posicao  ${cltCod}     19  10
    Digitar Na Posicao  ${digito}     19  21
    Digitar Na Posicao  ${opcaoMenu}  22  74   #Deve receber o valor da opção desejada conforme meu
    Transmitir
    
Acessar Menu Manutencao Segunda Assinatura
    Trocar Host         AOX2
    Digitar Na Posicao  70  22  74
    Transmitir

Acessar Menu Manutencao Segunda Assinatura Generico
    [Arguments]    	    ${host}  ${opcao}
    Trocar Host         ${host}
    Digitar Na Posicao  ${opcao}  22  74
    Transmitir

Acessar Menu Incluir Vinculacao
    [Arguments]           ${opcao} 
    Trocar Host           AOX2
    Digitar Na Posicao    ${opcao}  22  74
    Transmitir

Selecionar Opcao Menu
    [Arguments]  ${host}  ${opcao}
    Trocar Host           ${host}
    Digitar Na Posicao    ${opcao}    22    75
    
Acessar Menu Dados Cadastrais
    [Arguments]           ${opcao} 
    Trocar Host           AOX7
    Digitar Na Posicao    ${opcao}  21    73
    Transmitir
    
Acessar Controle De Grupo De Risco
    [Arguments]    ${grupoRisco}    ${tipo}    ${cliente}    ${grEcon}    ${opcao}
    Digitar Na Posicao    ${grupoRisco}    21    16
    Digitar Na Posicao    ${tipo}          21    32
    Digitar Na Posicao    ${cliente}       21    48
    Digitar Na Posicao    ${grEcon}        21    73
    Digitar Na Posicao    ${opcao}         22    73
    Transmitir
    

Acessar Grupo Economico De Interesse Comum
    [Arguments]           ${opcao} 
    Trocar Host           AOX7
    Digitar Na Posicao    ${opcao}  21    73
    Transmitir

Acessar Menu Sistema De Clientes
    [Arguments]           ${opcao} 
    Trocar Host           ${host7}
    Digitar Na Posicao    ${opcao}   21    73
    Transmitir
    
Acessar Controle De Grupo Economico
    [Arguments]    ${grupo}  ${cliente}  ${vinculacao}  ${hierarquia}  ${opcao}
    Digitar Na Posicao    ${grupo}        21    12
    Digitar Na Posicao    ${cliente}      21    32
    Digitar Na Posicao    ${vinculacao}   21    57
    Digitar Na Posicao    ${hierarquia}   21    76
    Digitar Na Posicao    ${opcao}        22    72
    Transmitir
    
Acessar Submenu AOX7 CEP 
    [Arguments]         ${cep}      ${opcao}
    Digitar Na Posicao  ${cep}      20  12
    Digitar Na Posicao  ${opcao}    21  73    
    Transmitir

Acessar Aleteracao Relac Grup Econ De Interesse Comum/Clientes
    Digitar Na Posicao     1             08  68
    Digitar Na Posicao     1             09  68
    Digitar Na Posicao     1             10  68
    Digitar Na Posicao     99            21  72
    Transmitir
    
Acessar Historico Do Grupo
    [Arguments]    ${grupo}  ${cliente}  ${vinculacao}  ${hierarquia}  ${opcao}
    Digitar Na Posicao    ${grupo}        21    12
    Digitar Na Posicao    ${cliente}      21    32
    Digitar Na Posicao    ${vinculacao}   21    57
    Digitar Na Posicao    ${hierarquia}   21    76
    Digitar Na Posicao    ${opcao}        22    72
    Transmitir  
   
#TODO essa Keyword não deveria existir. Pois já existe uma keyword que realiza a mesma função no arquivo Mainframe.robot
Acessar Sistema De Cliente 
    Trocar Host           AOX8
    Transmitir 

Acessar Submenu Controle De Grupo Convenio  
    [Arguments]  ${codGrupo}  ${codCliente}  ${opcao}
    Digitar Na Posicao  ${codGrupo}       21  17  
    Digitar Na Posicao  ${codCliente}     21  71
    Digitar Na Posicao  ${opcao}          22  71
    Transmitir

Acessar Menu AOX Sistema De Clientes 
    [Arguments]    ${codigo}    ${impressora}    ${nome}    ${cpfCnpj}    ${motivoDuplicidade}
    ...    ${agencia}    ${conta}    ${ordem}    ${dataEconFinan}    ${opcao}
    Trocar Host           AOX8
    Digitar Na Posicao    ${codigo}                17    12
    Digitar Na Posicao    ${impressora}            17    62
    Digitar Na Posicao    ${nome}                  18    12
    Digitar Na Posicao    ${cpfCnpj}               19    12
    Digitar Na Posicao    ${motivoDuplicidade}     19    54
    Digitar Na Posicao    ${agencia}               20    12
    Digitar Na Posicao    ${conta}                 20    26
    Digitar Na Posicao    ${ordem}                 20    48
    Digitar Na Posicao    ${dataEconFinan}         21    21
    Digitar Na Posicao    ${opcao}                 21    74
    Transmitir

Acessar Menu AOX Sistema De Clientes CNPJ 
    [Arguments]    ${codigo}    ${impressora}    ${nome}    ${cpfCnpj}   ${digitoCnpj}   ${motivoDuplicidade}
    ...    ${agencia}    ${conta}    ${ordem}    ${dataEconFinan}    ${opcao}
    Trocar Host           AOX8
    Digitar Na Posicao    ${codigo}                17    12
    Digitar Na Posicao    ${impressora}            17    62 
    Digitar Na Posicao    ${nome}                  18    12
    Digitar Na Posicao    ${cpfCnpj}               19    12
    Digitar Na Posicao    ${digitoCnpj}            19    27
    Digitar Na Posicao    ${motivoDuplicidade}     19    54
    Digitar Na Posicao    ${agencia}               20    12
    Digitar Na Posicao    ${conta}                 20    26
    Digitar Na Posicao    ${ordem}                 20    48
    Digitar Na Posicao    ${dataEconFinan}         21    21
    Digitar Na Posicao    ${opcao}                 21    74
    Transmitir


Acessar Menu AOX9 
    [Arguments]                ${opcao}
    Trocar Host                AOX9
    Digitar Na Posicao         ${opcao}            20        67
    Transmitir
    

