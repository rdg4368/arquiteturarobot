# Neste arquivo se encontram todas as Keywords reaproveitáveis em suítes diferentes
*** Settings ***
Resource    resources/commons/Mainframe.robot
Resource    testresources/base_dados/AOXB06.robot
Library     testresources/utils/utils.py
Library     testresources/dto/cliente.py  
Library     DateTime
  

*** Variables ***
${nome}                ${EMPTY}
${cpfValido}           ${EMPTY}
${cnpjValido}          ${EMPTY}
${motivoDuplicidade}   ${EMPTY}


*** Keywords ***

Consultar Cliente Valido AOX Para Inclusao De Pj
    [Arguments]  ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${motivoDuplicidade}  
    Acessar Cadastro de Cliente  ${host}
    Tabular "3" Vezes  
    Digitar  ${nome}
    Tabular
    Run Keyword If   '${tipoCpfCnjp}' == 'Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}' == 'Cnpj'   Digitar  ${cpfCnjp}  
    Digitar  ${motivoDuplicidade}
    Tabular "4" Vezes 
    Digitar  ${opcao}
    Sleep     10
    Transmitir

Consultar Cliente Valido AOX Para Alteração PF Simplificada
    [Arguments]   ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Acessar Cadastro de Cliente  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}  
    Tabular 
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'        Tabular   
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    Sleep     10
    Transmitir
 
Consultar Cliente Valido AOX Para Alteração PJ Simplificada
    [Arguments]   ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Acessar Cadastro de Cliente  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}  
    Tabular 
    Digitar  ${nome}
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    Sleep     10
    Transmitir
   
Consultar Cliente Valido AOX Para Alteração PJ Completa
    [Arguments]   ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Acessar Cadastro de Cliente  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}  
    Tabular 
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'        Tabular   
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    Sleep     10
    Transmitir

Consultar Cliente Valido AOX 
    [Arguments]  ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp} 
    Acessar Cadastro de Cliente  ${host}
    Tabular "3" Vezes  
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'        Tabular  
    Run Keyword If   '${tipoCpfCnjp}'=='Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}'=='Cnpj'   Digitar  ${cpfCnjp}  
    Tabular "5" Vezes 
    Digitar  ${opcao}
    Sleep     10
    Transmitir    

Consultar Cliente Valido AOX2 
    [Arguments]   ${host}  ${opcao}  ${nome}  ${cpfCnjp}  ${cltCod}  ${digCltCod}
    Acessar Cadastro de Cliente  ${host}
    Digitar  ${cltCod}  
    Tabular 
    Digitar  ${digCltCod}      
    Digitar Na Posicao  ${nome}  20  10  
    Digitar Na Posicao  ${cpfCnjp}  21  12     
    Digitar Na Posicao  ${opcao}  22  74      
    Transmitir 
    
Consultar Cliente Valido AOX Para Inclusao
    [Arguments]  ${host}  ${tipoCpfCnjp}  ${opcao}  ${nome}  ${cpfCnjp}  ${motivoDuplicidade}  
    Acessar Cadastro de Cliente  ${host}
    Tabular "3" Vezes  
    Digitar  ${nome}
    ${qntNome} =    Get length    ${nome}
    Run Keyword If   '${qntNome}' < '33'          Tabular        
    Run Keyword If   '${tipoCpfCnjp}' == 'Cpf'    Digitar  ${cpfCnjp}
    Run Keyword If   '${tipoCpfCnjp}' == 'Cnpj'   Digitar  ${cpfCnjp}  
    Digitar  ${motivoDuplicidade}
    Tabular "4" Vezes 
    Digitar  ${opcao}
    Sleep     10
    Transmitir
    
Transmitir Formulario
    [Arguments]  ${cpfCnjp}  ${opcao}
    Digitar  ${cpfCnjp}
    Tabular "2" Vezes
    Digitar  ${opcao}
    Sleep     10
    Transmitir    

Acessar Cadastro de Cliente
    [Arguments]  ${host}
    Trocar Host  ${host}
    
Acessar Host
    [Arguments]  ${host}
    Trocar Host  ${host}

Validar Campos na Tela
    [Arguments]               @{campos}
    ${cnt} =    Get length    @{campos}
    :FOR    ${i}    IN RANGE    ${cnt}
    \    ${cmpComparacao} =   Set Variable  ${campos}[0][${i}][3]   #Captura e percorre o (CAMPO), última posição da lista dada como parametro        
    \    ${cmp} =     Retira Elemento Lista   ${campos}[0][${i}]   3  #Retira o elemento CAMPO da lista , restando apenas as posições que serão utilizadas para capturar o campo da tela
    \    ${cmpTela} =     Ler Dado Tela     ${cmp}                  #Captura o elemento na tela por (X,Y,tamanho)       
    \    Comparar Valores na Tela  ${cmpComparacao}  ${cmpTela}     #Compara o campo vindo da tela o campo setado como parâmetro na lista( @{campos} )

#RECUPERA DADO DUPLA ASSINATURA 
Recuperar Dependencia Detentora
    ${data_corrente}       Get Current Date   result_format=%Y%m%d    
    ${dependencia_detentora} =  Recuperar Dependencia Detentora Data Maior Corrente  ${data_corrente}
    [Return]  ${dependencia_detentora}    

Recupera Dados Clientes Pendentes
    [Arguments]       ${dependencia_detentora}
    ${data_corrente}  Get Current Date   result_format=%Y%m%d    
    ${queryResult}=   Listar Dados Consulta Clientes Pendente  ${dependencia_detentora}  ${data_corrente}
    ${clientes} =     Criar Objeto Cliente                     ${queryResult}
    [Return]          ${clientes}

         
    



    