from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class CorrespondenciaPj():
    def __init__(self,
                cgc,
                digCgc,
                nome,
                codigo, 
                digCodigo, 
                cep, 
                logradouroRedu, 
                logradouro, 
                bairro, 
                cidade, 
                uf, 
                dataFixEnd, 
                bairroCorresp, 
                cidadeCorresp,
                cepCorresp, 
                ufCorresp, 
                telCorresp, 
                datAlteraEndExtra, 
                logradAmpliado,
                nomeFantasia,
                complementLograd):
        
        self.cgc = cgc
        self.digCgc = digCgc 
        self.cnpjFormatado = formatarMascaraCnpjTrim(cgc,digCgc) 
        self.nome = nome
        self.codigo = codigo
        self.digCodigo = digCodigo   
        self.codigoFormatado = formatarMascaraCodigoClienteTrim(codigo,digCodigo)
        self.cep = formatarMascaraCepTrim(cep)
        self.logradouroRedu = logradouroRedu
        self.logradouro = logradouro
        self.bairro = bairro
        self.cidade = cidade
        self.uf = uf
        self.dataFixEnd = formatarMascaraDataTrim(dataFixEnd)
        self.bairroCorresp = bairroCorresp
        self.cidadeCorresp = cidadeCorresp
        self.cepCorresp = formatarMascaraCepTrim(cepCorresp)
        self.ufCorresp = ufCorresp
        self.telCorresp = telCorresp        
        self.datAlteraEndExtra = formatarMascaraDataTrim(datAlteraEndExtra)
        self.logradAmpliado = logradAmpliado
        self.nomeFantasia = nomeFantasia
        self.cepCorrespLimpo = cepCorresp
        self.complementLograd = complementLograd
        

    
def criarObjetoCorrespondenciaClientePj( queryresult ):
            correspondenciaPj = CorrespondenciaPj(
                                            str(queryresult[0][0]),
                                            str(queryresult[0][1]),
                                            str(queryresult[0][2]),
                                            str(queryresult[0][3]), 
                                            str(queryresult[0][4]), 
                                            str(queryresult[0][5]),
                                            str(queryresult[0][6]),
                                            str(queryresult[0][7]),
                                            str(queryresult[0][8]),
                                            str(queryresult[0][9]),
                                            str(queryresult[0][10]),
                                            str(queryresult[0][11]),
                                            str(queryresult[0][12]),
                                            str(queryresult[0][13]),
                                            str(queryresult[0][14]),
                                            str(queryresult[0][15]),
                                            str(queryresult[0][16]),
                                            str(queryresult[0][17]),
                                            str(queryresult[0][18]),
                                            str(queryresult[0][19]),
                                            str(queryresult[0][20]))                                     
            return      correspondenciaPj
        
        

