from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class Cliente():
    def __init__(self, codigo, digCodigo, nome, data, hora, detentora, matricula):
        
        self.codigo = removeUltimosCaracteres(codigo,2)
        self.digCodigo = removeUltimosCaracteres(digCodigo,2)
        self.nome = nome
        self.data = formatarMascaraData(data)
        self.hora = formatarMascaraHora(hora)
        self.detentora = removeUltimosCaracteres(detentora,2)
        self.matricula = removeUltimosCaracteres(matricula,2)
        
def criarObjetoCliente( queryresult ):
            cliente = Cliente(
                                str(queryresult[0][0]),
                                str(queryresult[0][1]),
                                str(queryresult[0][2]),
                                str(queryresult[0][3]), 
                                str(queryresult[0][4]), 
                                str(queryresult[0][5]),
                                str(queryresult[0][6]))   
            return      cliente

    

