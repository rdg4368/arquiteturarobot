from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class ClientePfSimplificado():
    def __init__(self, cpf, digitoVerificador, nome, codigo, digCodigo, cidade, endereco, data, cep, agencia,codMunicipio,tipoVinculacao, codVinculacao):

        self.nome = nome
        self.codigo = apenasDigitos(codigo)
        self.digCodigo = apenasDigitos(digCodigo)
        self.cpfValidoAox = formatarCpfApenasDigitos(cpf,digitoVerificador)
        self.endereco = endereco
        self.cidade = cidade
        self.cpfFormatado = formatarMascaraCpf(cpf,digitoVerificador)
        self.codigoCliente = formatarMascaraCodigoCliente(codigo,digCodigo)
        self.dataFormatada = formatarMascaraData(data)
        self.cepFormatado = formatarMascaraCep(cep)
        self.agencia = removePrimeirosCaracteres(agencia,1)
        self.codMunicipio = apenasDigitos(codMunicipio)
        self.codigoClienteValidacao = formatarCodigoClienteCompletaZeros(codigo,digCodigo)
        self.tipoVinculacao =  tipoVinculacao
        self.codVinculacao =  codVinculacao 
        
                    
def criarObjetoClienteSimplificado( queryresult ):
        clientepfSimplficado = ClientePfSimplificado(
                                            str(queryresult[0][0]),
                                            str(queryresult[0][1]),
                                            str(queryresult[0][2]),
                                            str(queryresult[0][3]), 
                                            str(queryresult[0][4]), 
                                            str(queryresult[0][5]),
                                            str(queryresult[0][6]),
                                            str(queryresult[0][7]),
                                            str(queryresult[0][8]),
                                            str(queryresult[0][9]),
                                            str(queryresult[0][10]),
                                            str(queryresult[0][11]),
                                            str(queryresult[0][12])) 
  
        return      clientepfSimplficado


