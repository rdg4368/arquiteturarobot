from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class ImpressaoCadastral():
	def __init__(self, imldes, impdat):       
		self.imldes = imldes
		self.impdat = impdat
			
def criarObjetoImpressaoCadastral( queryresult ):
	impressaoCadastral = ImpressaoCadastral(str(queryresult[0][0]),
											str(queryresult[0][1]))   
	return      impressaoCadastral