from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class ClientePjSimplificado():
    
    def __init__(self, cnpj, digitoVerificador, nome, codigo, digCodigo, cep, logradouroRed, bairro, cidade, uf, endLogradouro,codCnae, agencia, setorPublico, codMunicipio):
        
        self.nome = nome
        self.cnpj = apenasDigitos(cnpj)
        self.cnpjValidoAox = cgcApenasDigitos(cnpj,digitoVerificador)
        self.codigoCliente = formatarMascaraCodigoClientePj(codigo,digCodigo)
        self.codigo = apenasDigitos(codigo)
        self.digCodigo = apenasDigitos(digCodigo)
        self.cep = formatarMascaraCep(cep) 
        self.logradouroRed = logradouroRed
        self.bairro = bairro
        self.cidade = cidade
        self.uf = uf
        self.endLogradouro = endLogradouro
        self.cnae = apenasDigitos(codCnae)
        self.agenda = formatarMascaraAgencia(agencia)
        self.setorPublico = setorPublico
        self.codMunicipio = codMunicipio
    
def criarObjetoClientePjSimplificado( queryresult ):
        clientepjSimplificado = ClientePjSimplificado(str(queryresult[0][0]),
                                            str(queryresult[0][1]),
                                            str(queryresult[0][2]),
                                            str(queryresult[0][3]), 
                                            str(queryresult[0][4]), 
                                            str(queryresult[0][5]),
                                            str(queryresult[0][6]),
                                            str(queryresult[0][7]),
                                            str(queryresult[0][8]),
                                            str(queryresult[0][9]), 
                                            str(queryresult[0][10]),
                                            str(queryresult[0][11]),
                                            str(queryresult[0][12]),
                                            str(queryresult[0][13]),
                                            str(queryresult[0][14]))    
        return      clientepjSimplificado

