from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class ClientePjCompleto():
    def __init__(self, cnpj, digitoVerificador, nome,  codigo, digCodigo, cep, logradouroRed, bairro,cidade, uf, endLogradouro, cnae, codMunicipio, capital, dataValorizacao, giin, nif, nomeFantasia, complementoEnder, dataFixEnd, dataUpdTel, celular ):
    
        self.nome = nome
        self.cnpj = apenasDigitos(cnpj) 
        self.cnpjMascara = formatarMascaraCnpj(cnpj,digitoVerificador)   
        self.cnpjValidoAox = cgcApenasDigitos(cnpj,digitoVerificador)
        self.codigoCliente = formatarMascaraCodigoClientePj(codigo,digCodigo) 
        self.cep = formatarMascaraCep(cep) 
        self.logradouroRed = logradouroRed 
        self.bairro = bairro 
        self.cidade = cidade 
        self.uf = uf
        self.endLogradouro = endLogradouro
        self.cnae = apenasDigitos(cnae) 
        self.codMunicipio = codMunicipio
        self.capital = formatarMascaraCapital(capital) 
        self.dataValorizacao = formatarMascaraData(dataValorizacao)  
        self.giin = giin  
        self.nif = nif
        self.nomeFantasia = nomeFantasia
        self.complementoEnder = complementoEnder
        self.codigo = apenasDigitos(codigo)
        self.digCodigo = apenasDigitos(digCodigo)
        self.dataFixEnd = formatarMascaraDataTrim(dataFixEnd)
        self.dataUpdTel = formatarMascaraDataTrim(dataUpdTel)
        self.celular = celular
        
                        
def criarObjetoClientePjCompleto( queryresult ):
        clientepjCompleto = ClientePjCompleto(str(queryresult[0][0]),
                                              str(queryresult[0][1]),
                                              str(queryresult[0][2]),
                                              str(queryresult[0][3]), 
                                              str(queryresult[0][4]), 
                                              str(queryresult[0][5]),
                                              str(queryresult[0][6]),
                                              str(queryresult[0][7]),
                                              str(queryresult[0][8]),
                                              str(queryresult[0][9]), 
                                              str(queryresult[0][10]),
                                              str(queryresult[0][11]),
                                              str(queryresult[0][12]),
                                              str(queryresult[0][13]), 
                                              str(queryresult[0][14]),
                                              str(queryresult[0][15]),
                                              str(queryresult[0][16]), 
                                              str(queryresult[0][17]),
                                              str(queryresult[0][18]),
                                              str(queryresult[0][19]),
                                              str(queryresult[0][20]),
                                              str(queryresult[0][21])) 
        return      clientepjCompleto