from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class HistoricoCliente():
    def __init__(self,
                cltcod,
                digCltCod,
                data,
                cep,                
                logradEnd, 
                logradCorresp,                 
                bairro, 
                cidade, 
                uf, 
                dependencia, 
                numUsuario,
                codSeq):
                
        self.codigoFormatado = formatarMascaraCodigoClienteTrimTelaEspaco(cltcod,digCltCod)
        self.data = formatarMascaraDataTrimChecaZero(data)   
        self.cep = formatarMascaraCepTrimChecaZero(cep)
        self.logradEnd = logradEnd
        self.logradCorresp = logradCorresp
        self.bairro = bairro
        self.cidade = cidade
        self.uf = uf
        self.dependencia = dependencia
        self.numUsuario = numUsuario
        self.codSeq = codSeq
        
        
    def listaAtribHist(self): 
        lis = [self.codigoFormatado,self.data,self.cep,self.logradEnd,self.logradCorresp,self.logradCorresp,self.bairro,self.cidade,self.uf,self.dependencia,self.numUsuario,self.codSeq]
        return  lis
                
def criarObjetoHistoricoCliente( queryresult, posList ):
            historicoCliente = HistoricoCliente(
                                            str(queryresult[int(posList)][0]),
                                            str(queryresult[int(posList)][1]),
                                            str(queryresult[int(posList)][2]),
                                            str(queryresult[int(posList)][3]), 
                                            str(queryresult[int(posList)][4]), 
                                            str(queryresult[int(posList)][5]),
                                            str(queryresult[int(posList)][6]),
                                            str(queryresult[int(posList)][7]),
                                            str(queryresult[int(posList)][8]),
                                            str(queryresult[int(posList)][9]),
                                            str(queryresult[int(posList)][10]),
                                            str(queryresult[int(posList)][11]))
                                                                                
            return    historicoCliente
        
   
