from testresources.utils.utilsMask import *
from testresources.utils.utils import auto_str

@auto_str
class ClientePfCompleto():
    def __init__(self, cpf, digitoVerificador, nome, nomePai, nomeMae, nomeSrf, uf, codigo , digCodigo, cidadeNatural, endereco, nomeConjuge, cpfConjuge, codCpfConjuge, dataEmissao, cedulaIdentidade, bairro, logradouroRedu, dataFixEnd, dataUpdTel, cep, celular ):
        
        self.codigo = apenasDigitos(codigo)
        self.digCodigo = apenasDigitos(digCodigo)
        self.cpf = apenasDigitos(cpf)
        self.cpfBanco = formatarCpfParametroBanco(cpf)
        self.nome = nome
        self.nomePai = nomePai
        self.nomeMae = nomeMae
        self.nomeSrf = nomeSrf
        self.codigoCliente = formatarMascaraCodigoCliente(codigo,digCodigo)
        self.uf = uf
        self.cidadeNatural = cidadeNatural
        self.endereco = endereco
        self.nomeConjuge = nomeConjuge
        self.cpfConjugeFormatado = formatarMascaraCpfConjuge(cpfConjuge,codCpfConjuge)
        self.cedulaIdentidade = cedulaIdentidade
        self.cpfValidoAox = formatarCpfApenasDigitos(cpf,digitoVerificador)
        self.cpfFormatado = formatarMascaraCpf(cpf,digitoVerificador)
        self.bairro = bairro
        self.logradouroRedu = logradouroRedu
        self.dataFixEnd = formatarMascaraDataTrim(dataFixEnd)
        self.dataUpdTel = formatarMascaraDataTrim(dataUpdTel)
        self.cep = formatarMascaraCep(cep)
        self.celular = celular
        
        
def criarObjetoClienteCompleto( queryresult ):
            clientepfCompleto = ClientePfCompleto(
                                            str(queryresult[0][0]),
                                            str(queryresult[0][1]),
                                            str(queryresult[0][2]),
                                            str(queryresult[0][3]), 
                                            str(queryresult[0][4]), 
                                            str(queryresult[0][5]),
                                            str(queryresult[0][6]),
                                            str(queryresult[0][7]),
                                            str(queryresult[0][8]),
                                            str(queryresult[0][9]),
                                            str(queryresult[0][10]),
                                            str(queryresult[0][11]),
                                            str(queryresult[0][12]),
                                            str(queryresult[0][13]),
                                            str(queryresult[0][14]),
                                            str(queryresult[0][15]),
                                            str(queryresult[0][16]),
                                            str(queryresult[0][17]),
                                            str(queryresult[0][18]),
                                            str(queryresult[0][19]),
                                            str(queryresult[0][20]),
                                            str(queryresult[0][21]))   
            return      clientepfCompleto

    

