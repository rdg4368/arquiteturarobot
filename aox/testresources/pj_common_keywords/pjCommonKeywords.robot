# Neste arquivo se encontram todas as Keywords reaproveitáveis aos testes relacionados a pessoa físicas
*** Settings ***
Resource     testresources/base_dados/AOXB01.robot
Resource     testresources/base_dados/AOXB33.robot 
Resource     testresources/base_dados/AOXB14.robot
Resource     testresources/base_dados/AOXB48.robot 
Resource     testresources/base_dados/AOXB02.robot
Resource     resources/commons/Mainframe.robot 
Library      testresources/dto/pjSimplificado.py  
Library      testresources/dto/pjCompleto.py
Library      testresources/dto/correspondenciaPj.py 
Library      testresources/utils/utilsMask.py   

*** Keywords ***
#RECUPERAR DADOS DA CORRESPONDENCIA PJ
Recuperar Dados Correspondencia Cliente PJ Valido AOX3  
    ${dadosAox3} =  Selecionar Dados Correspondencia Cliente PJ Valido
    ${result} =  Disponibilizar Dados Correspondencia Cliente PJ  ${dadosAox3}
    [Return]     ${result}
      
Disponibilizar Dados Correspondencia Cliente PJ
    [Arguments]            ${queryResult}  
    ${dadosCorrespPj} =    Criar Objeto Correspondencia Cliente Pj   ${queryResult}
    [Return]  ${dadosCorrespPj}

#RECUPERAR DADOS COMPLETOS CLIENTE PJ    
Recuperar Dados Simplificados Cliente CNPJ Valido AOX    
    ${cnpjAox} =  Recuperar Dados Simplificados Cliente PJ Valido
    ${result} =  Disponibilizar Dados Simplificados Cliente PJ  ${cnpjAox}
    [Return]     ${result}

#RECUPERAR DADOS COMPLETOS CLIENTE PJ    
Recuperar Dados Completos Cliente CNPJ Valido AOX    
    ${cnpjAox} =  Recuperar Cliente PJ Valido Dados Completos
    ${result} =  Disponibilizar Dados Completos Cliente PJ  ${cnpjAox}
    [Return]     ${result}
      
Disponibilizar Dados Completos Cliente PJ
    [Arguments]             ${queryResult}  
    ${dadosClientePj} =     Criar Objeto Cliente Pj Completo   ${queryResult}
    [Return]  ${dadosClientePj}

#RECUPERAR DADOS SIMPLIFICADOS CLIENTE PJ
Recuperar Dados Cliente CNPJ Valido AOX    
    ${cnpjAox} =  Recuperar CNPJ Valido
    ${result} =   Disponibilizar Dados Simplificados Cliente PJ  ${cnpjAox}
    [Return]     ${result}
      
Disponibilizar Dados Simplificados Cliente PJ
    [Arguments]             ${queryResult}  
    ${dadosClientePjSimplificados} =     Criar Objeto Cliente Pj Simplificado    ${queryResult} 
    [Return]  ${dadosClientePjSimplificados}

Detalhar Pessoa Juridica
   [Arguments]   ${opcao} 
    Tabular 
    Digitar      ${opcao}
    Transmitir
          
           