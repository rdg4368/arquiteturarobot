*** Settings ***
Resource          testdata/testcases/aox08/007_dados_economico_financeiro_PJ/dadosEconomicoFinanceiroPjTC.robot
Resource          resources/commons/Base.robot
#Suite Setup       Preparar Suite 
#Suite Teardown    Terminar Suite
 

*** Test Cases ***
CT001 - FP - Incluir - Classificação Dados Economicos Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Incluir  Dados Economicos Financeira (PJ)
    [Tags]           Fluxo Principal    
    Incluir Dados Economicos - Financeira (PJ)   
      
CT002 - FP - Alterar - Classificação Dados Econonicos - Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Alterar Dados Econonicos Financeira (PJ)
    [Tags]           Fluxo Principal
    Alterar Dados Economicos - Financeira (PJ)
    
CT003 - FP - Consultar - Classificação Dados Economicos - Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Consultar Dados Economicos Financeira (PJ)
    [Tags]           Fluxo Principal
    Consultar Dados Economicos - Financeira (PJ)    
    
CT004 - FP - Consultar RFB - Classificação Dados Economicos - Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Consultar Dados Economicos Financeira (PJ)
    [Tags]           Fluxo Principal
    Consultar RFB Dados Economicos - Financeira (PJ)   
    
CT005 - teste
    Preparar Suite