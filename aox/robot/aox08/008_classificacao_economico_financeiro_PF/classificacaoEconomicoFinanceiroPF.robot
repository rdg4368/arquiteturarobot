*** Settings ***
Resource          testdata/testcases/aox08/008_classificacao_economico_financeiro_PF/classificacaoEconomicoFinanceiroPfTC.robot
Resource          resources/commons/Base.robot 
Suite Setup       Preparar Suite
Suite Teardown    Terminar Suite


*** Test Cases ***
CT001 - FP - Incluir - Dados Economico - Financeiros (PF)
   [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Dados Cadastrais Web - INCLUSAO
   [Tags]           Fluxo Principal
   Incluir Classificacao Economica - Financeiros (PF)  
    
CT002 - FP - Alterar - Dados Economico - Financeiros (PF)
   [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Dados Cadastrais Web - INCLUSAO
   [Tags]           Fluxo Principal
   Alterar Classificacao Economica - Financeiros (PF)
     
CT003 - FP - Consultar - Dados Economico - Financeiros (PF)
   [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Dados Cadastrais Web - INCLUSAO
   [Tags]           Fluxo Principal
   Consultar Classificacao Economica - Financeiros (PF)