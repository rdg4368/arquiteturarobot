*** Settings ***
Resource          testdata/testcases/aox08/009_classificacao_economico_financeiro_PJ/classificacaoEconomicoFinanceiroPjTC.robot
Resource          resources/commons/Base.robot
Suite Setup       Preparar Suite
Suite Teardown    Terminar Suite


*** Test Cases ***
CT001 - FP - Incluir - Classificação Econ - Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Dados Cadastrais Web - INCLUSAO
    [Tags]           Fluxo Principal
    Incluir Classificação Econ - Financeira (PJ)
    
CT002 - FP - Alterar - Classificação Econ - Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Dados Cadastrais Web - INCLUSAO
    [Tags]           Fluxo Principal
    Alterar Classificação Econ - Financeira (PJ) 
     
CT003 - FP - Consultar - Classificação Econ - Financeira (PJ)
    [Documentation]  Este Caso de Teste tem como objetivo avaliar a seguinte funcionalidade: Dados Cadastrais Web - INCLUSAO
    [Tags]           Fluxo Principal
    Consultar Classificação Econ - Financeira (PJ)
 