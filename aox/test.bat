:: Este script auxilia a integra��o do allure com o robot
:: Para rodar corretamente, certifique-se que o Python, o servidor do Apache 
:: e o caminho para o .bat do allure, estejam devidamente configurados na 
:: vari�vel de ambiente Path do Windows. 
@echo off
set DIR_ATUAL=%cd%
set CLASSPATH=%DIR_ATUAL%\lib\ojdbc6-11.2.0.3.jar;%DIR_ATUAL%\lib\db2jcc4-10.1.jar;%CLASSPATH%

::set PYTHON_PATH=D:\python37
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_66
set PATH=%DIR_ATUAL%\src\main\resources;%PATH%
set PATH=C:\allure-2.11.0\bin\;%PATH%
set PATH=C:\python37\;C:\python37\Scripts;C:\wc3270;%PATH%


set CATALINA_HOME=D:\apache-tomcat-9.0.17

set dataatual=%mydate%_%mytime%

echo Variaveis ambiente configuradas

del /q /S output
del /q /S allure




echo Start robot.....
python --version
python -m robot --listener allure_robotframework  --outputdir results   robot

echo Fim robot.

del /q /S screenshot*
echo "Testes alta executados."



